<?php
 
/**
 * @copyright Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * @copyright Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License
 * http://ajatus.info
 *
 */
require_once "phing/Task.php";
require_once "File/Archive.php";

File_Archive::setOption("zipCompressionLevel", 0);

/**
 * Phing release script for Ajatus
 */
class releaseAjatus extends Task
{
	function __construct()
	{
	}

	protected $returnProperty; // name of property to set to return value

	/**
	 * The target directory where the packed release file should be saved.
	 */
	protected $target_dir = null;
	
	/**
	 * The target directory where the files to be packed should be saved temporarily.
	 */
	protected $temp_dir = null;
		
	/**
	 * Version number for the release
	 */
	private $version = null;
	
	/**
	 * List of files to include in release
	 */
	private $app_files = null;
	
	private $packjs_skip_files = array();
	
	/**
	 * The setter for the attribute "target_dir"
	 */
	public function setTarget_dir($str)
	{
		$this->target_dir = $str;
	}
	
	public function setTemp_dir($str)
	{
		$this->temp_dir = $str;
	}
	
	public function setAction($str)
	{
		$this->action = $str;
	}
	
	public function setVersion($str)
	{
		$this->version = $str;
	}
	
	/**
	 * Sets property name to set with return value of function or expression.
	 */
	public function setReturnProperty($r)
	{
		$this->returnProperty = $r;
	}
	
	/**
	 * The init method: Do init steps.
	 */
	public function init()
	{
		$this->packjs_skip_files = array(
		);
	}
	
	/**
	 * The main entry point method.
	 */
	public function main()
	{
	    $this->_collect_files('.');
	    
        $temp_folder = $this->_copy_to_temp();
        $this->_packjs_files($temp_folder);
        $this->_pack_files($temp_folder);
	    
	    $this->project->setProperty($this->returnProperty, "Ajatus-{$this->version}.zip");
    }
    
    private function _collect_files($path)
    {
        $directory = dir($path);

		// List contents
		while (false !== ($entry = $directory->read()))
		{
			if (substr($entry, 0, 1) == '.') {
				// Ignore dotfiles
				continue;
			}
			if ($entry == '.git') {
				// Ignore GIT directories
				continue;
			}
			if (   $entry == 'build.properties'
			    || $entry == 'build.xml'
			    || $entry == 'test.html')
			{
				// Ignore GIT directories
				continue;
			}
			
			$include = true;
			
			$path_parts = pathinfo($entry);
			switch ($path_parts['extension'])
			{
				case 'tmproj':
				    $include = false;
				break;
				case 'svg':
				    $include = false;
				break;
				case 'js':
                    // if (strpos($path_parts['filename'], '.src') !== false) {
                    //     $include = false;
                    // }
				break;
				default:
				break;
			}
						
			if (is_dir("{$path}/{$entry}"))
			{
			    if (   $entry != 'build'
			        && $entry != 'testsuite'
			        && $entry != 'temp'
			        && $entry != 'target'
			        && $entry != 'openpsa')
			    {			        
    				// List the subdirectory
    				$subpath = "{$path}/{$entry}";
			        $this->app_files[] = $subpath;
			        
    				$this->_collect_files($subpath);			        
			    }
			}
			else
			{
			    if ($include) {
			        $this->app_files[] = "{$path}/{$entry}";
			    }
			}
		}
    }
    
    private function _copy_to_temp()
    {
        if (! is_dir($this->temp_dir)) {
            mkdir($this->temp_dir);
        }
        
        $tmp_dir_root = "{$this->temp_dir}/{$this->version}";
        
        if (is_dir($tmp_dir_root)) {
            $this->_deltree($tmp_dir_root);
        }
        mkdir($tmp_dir_root);
        
        $tmp_dir = "{$tmp_dir_root}/ajatus";
        mkdir($tmp_dir);
        
        $this->_dcopy($this->app_files, $tmp_dir);
        
        return $tmp_dir_root;
    }
    
    private function _packjs_files($path)
    {
        
    }
    
    private function _pack_files($path)
    {
        File_Archive::extract(
            $path,
            File_Archive::toArchive("{$this->target_dir}/Ajatus-{$this->version}.zip", File_Archive::toFiles())
        );
        
        $this->_deltree($path);
    }
    
    private function _prepare_ajatus_core($core_file)
    {
        $handle = fopen($core_file, "r");
        $contents = fread($handle,  filesize($core_file));
        
        $version_str = str_replace(".", ", ", $this->version);
        
        $pattern = '/(\$\.ajatus\.version) = \[(\d+), (\d+), (\d+)\]/i';
        $replacement = '${1} = ['.$version_str.']';
        $contents = preg_replace($pattern, $replacement, $contents);
        
        $contents = str_replace("application_url: '/_utils/ajatus_dev/',", "application_url: '/_utils/ajatus/',", $contents);
        $contents = str_replace("application_database_identifier: 'dev'", "application_database_identifier: ''", $contents);
        
        fclose($handle);
        
        $handle = fopen($core_file,  "w+");
        fwrite($handle,  $contents);
        fclose($handle);
        
        return true;
    }
    
    private function _dcopy($files, $to)
    {
        if (is_array($files))
        {
            foreach ($files as $file)
            {
                if (is_dir($file))
                {
                    $to_f = "{$to}/{$file}";
                    mkdir("{$to_f}");
                }
                else
                {
                    $to_f = "{$to}/{$file}";
                    copy("{$file}", "{$to_f}");
                    
                    if (strpos($file, 'ajatus.core') !== false) {
                        $this->_prepare_ajatus_core($to_f);
                    }
                }
            }            
        }
    }
    
    private function _deltree($f)
    {
        if ( is_dir($f) )
        {
            foreach( scandir($f) as $item ) {
                if ( !strcmp( $item, '.' ) || !strcmp( $item, '..' ) ) {
                    continue;
                }
                $this->_deltree("{$f}/{$item}");
            }
            rmdir($f);
        } else {
            unlink($f);
        }
    }
}

?>