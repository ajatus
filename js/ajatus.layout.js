/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.layout = $.ajatus.layout || {};
    
    $.ajatus.layout
    
    $.ajatus.layout.body = {
    };
    $.extend($.ajatus.layout.body, {
        set_class: function(new_class) {
            $('body').attr('class', new_class);
        },
        append_class: function(class_name) {
            $('body').addClass(class_name);
        }
    });
    
    $.ajatus.layout.title = {
        parts: {
            base: 'Ajatus CRM',
            view: ''
        }
    };
    $.extend($.ajatus.layout.title, {
        update: function(data) {
            if (typeof data != 'object') {
                return;
            }
            
            $.each(data, function(key,value){
                if (key == 'base') {
                    return;
                }
                
                $.ajatus.layout.title.parts[key] = value;
            });
            
            $.ajatus.layout.title._set_title();
        },
        _set_title: function() {
            var string = $.ajatus.layout.title.parts.base;
            if ($.ajatus.layout.title.parts.view != '') {
                string += ' : ' + $.ajatus.layout.title.parts.view;
            }

            document.title = string;
        }
    });
    
    $.ajatus.layout.styles = {
        loaded: []
    };
    $.extend($.ajatus.layout.styles, {
        load: function(style) {
            if ($.ajatus.layout.styles.loaded[style]) {
                return;
            }
            
            $.ajatus.layout.styles.loaded[style] = true;
            
            var style_tag = $('<link rel="stylesheet" href="' + style + '" type="text/css">');
            
            $('head').prepend(style_tag);
        }
    });
    
})(jQuery);