/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};    

    $.ajatus.preferences = {
        revision: null,
        modified: false
    };
    
    $.ajatus.preferences.local = {};
    $.ajatus.preferences.local_defaults = {
        user: {
            name: 'Root Ajatus',
            email: 'root@ajatus.info'
        },
        replication: {
            enabled: false,
            public_key: '',
            allowed_keys: ''
        },
        layout: {
            theme_name: 'default',
            icon_set: 'default'
        },
        localization: {
            language: 'en_GB'
        },
        backups: {
            enabled: false,
            auto: false
        },
        expansions: {
            active: []
        },
        auto_save: {
            enabled: true,
            interval: 300
        }
    };
    
    $.ajatus.preferences.client = {};
    $.ajatus.preferences.client_defaults = {
        debug: false,
        language: null,
        server_url: '/',
        theme_url: 'themes/default/',
        theme_icons_url: 'themes/default/images/icons/',
        application_url: '/_utils/ajatus/',
        application_database_identifier: '',
        content_types: {},
        custom_views: [],
        custom_widgets: [],
        extensions: [],
        types: {
            in_use: [
                'note', 'contact', 'event', 'expense', 'hour_report'
            ],
            system: [
                'tag'
            ]
        },
        themes: {
            available: [
                'default'
            ]
        },
        developer_tools: false// ,
        //         cookies: {            
        //             expire: (24 * 60 * 60) // 24hrs in seconds
        //         }
    };
            
    $.ajatus.preferences.loader = function() {
        this.loaded = false;
        
        var on_success = function(data) {
            this.loaded = true;
            
            $.ajatus.events.lock_pool.decrease();
            $.ajatus.preferences.revision = data._rev;
            
            $.ajatus.preferences.local = data.value;
            $.each($.ajatus.preferences.local_defaults, function(i,n){
                if (typeof $.ajatus.preferences.local[i] == 'undefined') {
                    $.ajatus.preferences.modified = true;
                    $.ajatus.preferences.local[i] = n;
                }
                if (typeof n == 'object') {
                    $.each(n, function(xi,xn){
                        if (typeof $.ajatus.preferences.local[i][xi] == 'undefined') {
                            $.ajatus.preferences.modified = true;
                            $.ajatus.preferences.local[i][xi] = xn;
                        }
                    });
                }
            });
            
            return data;
        };
        
        this.dc = $.jqCouch.connection('doc', on_success);
    }
    $.extend($.ajatus.preferences.loader.prototype, {
        load: function() {
            $.ajatus.events.lock_pool.increase();
            var data = this.dc.get($.ajatus.preferences.client.application_database + '/preferences');
            if (! data._id) {
                this.loaded = false;
                $.ajatus.events.lock_pool.decrease();
                $.ajatus.ajax_error(data.request);
            }
        }
    });
    
    $.ajatus.preferences.view = {
        title: 'Preferences',
        icon: 'preferences.png',
        options: {},
        sections: [
            'user', 'localization', 'layout', 'replication', 'backups', 'expansions', 'auto_save'
        ],
        
        tab: {
            on_click: function(e)
            {
                $.ajatus.tabs.on_click(e);
                $.ajatus.preferences.view.render();
            }
        },
        
        render: function() {
            $.ajatus.application_content_area.html('');
            $.ajatus.layout.body.set_class('preferences');
            $.ajatus.views.on_change("#view.preferences");
            
            $.ajatus.toolbar.show();
            
            var form = $('<form name="preferences" id="preferences_form"/>');
            form.appendTo($.ajatus.application_content_area);
            
            var hidden_data = {
                _id: 'preferences',
                _rev: $.ajatus.preferences.revision
            };
            var hidden_fields = function() {
                return [
                    'input', { type: 'hidden', id: 'preferences_id', name: '_id', value: this._id }, '',
                    'input', { type: 'hidden', id: 'preferences_revision', name: '_rev', value: this._rev }, ''
                ];
            };
            $(form).tplAppend(hidden_data, hidden_fields);
            
            var form_actions = $('<div class="form_actions"><input type="submit" name="save" value="' + $.ajatus.i10n.get('Save') + '" /><input type="submit" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" /></div>');
            
            var sections_holder = $('<div id="preference_sections" />');
            sections_holder.appendTo(form);
            
            $.each($.ajatus.preferences.view.sections, function(i,s){
                var section_holder = $('<div class="section" id="section_' + s + '" />');
                section_holder.appendTo(sections_holder);
                
                var form_items = $.ajatus.preferences.view.generate_section_form(s);
                
                if (form_items) {
                    var normalized_title = s.toString().replace('_', ' ');
                    var title = $('<h3 />').html($.ajatus.i10n.get(normalized_title));
                    title.appendTo(section_holder);
                    
                    form_items.appendTo(section_holder);
                }
            });

            form_actions.appendTo(form);
            
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Save'), 'save.png', function(){
                $('#preferences_form input[@type=submit][name*=save]').trigger("click", []);
            });
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Cancel'), 'cancel.png', function(){
                $('#preferences_form input[@type=submit][name*=cancel]').trigger("click", []);
            });
            
            $.ajatus.forms.register.custom(form, '$.ajatus.preferences.view.process_form');
        },
        
        process_form: function(form_data, form) {
            // $.ajatus.debug('Process preferences');
            // console.log(form_data);
            
            var doc = {
    	        _id: '',
    	        _rev: '',
    	        value: {}
    	    };
    	    var form_values = {};
    	    var form_id = '';
            
    	    $.each(form_data, function(i,row){
                if (row.name.toString().match(/__(.*?)/)) {
                    return;
                }
    	        if (   row.name == '_id'
    	            && (   row.value != undefined
    	                && row.value != ''))
    	        {
    	            doc['_id'] = String(row.value);
    	            form_id = doc['_id'];
    	        }
    	        else if(   row.name == '_rev'
    	                && (   typeof row.value != "undefined"
    	                    && row.value != ''))
    	        {
    	            doc['_rev'] = String(row.value);
    	        } else {
    	            if (row.name != 'submit') {
    	                if (row.name == '_type') {
	                    }
        	            else if (   row.name.substr(0,6) != "widget"
        	                     && row.name.substr(0,8) != "metadata")
        	            {
        	                var item = {};
        	                var widget = {};
        	                var prev_val = false;
        	                var name_parts = [];
        	                var name_parts_count = 0;
        	                if (row.name.toString().match(/;/g)) {
        	                    name_parts = row.name.toString().split(";");
        	                    name_parts_count = name_parts.length;
        	                }
        	                
        	                $.each(form_data, function(x,r){
        	                    if (r.name == 'widget['+row.name+':name]') {
        	                        widget['name'] = r.value;
        	                    } else if (r.name == 'widget['+row.name+':config]') {
    	                            widget['config'] = $.ajatus.converter.parseJSON(r.value);
    	                        } else if (r.name == 'widget['+row.name+':prev_val]') {
    	                            prev_val = $.ajatus.converter.parseJSON(r.value);
    	                        }
    	                    });
    	                    
    	                    var wdgt = new $.ajatus.widget(widget['name'], widget['config']);
        	                item['val'] = wdgt.value_on_save(row.value, prev_val);
        	                item['widget'] = widget;
        	                
        	                if (name_parts_count > 0) {
        	                    var prevs = [];
        	                    for (var i=0; i < name_parts_count; i++) {        	                        
        	                        var arr_keys = false;
        	                        var key_prefix = '';
        	                        
        	                        if (name_parts[i].match(/\[/g)) {
        	                            var arr_keys = name_parts[i].split('[');
        	                            
        	                            name_parts[i] = name_parts[i].replace(/\[/g, '"][');
        	                            var key = '["'+name_parts[i];
        	                        } else {
        	                            var key = "['"+name_parts[i]+"']";
        	                        }
                                    
                                    if (prevs.length > 0) {
                                        $.each(prevs, function(pi, pk){
                                            key_prefix = "['" + pk + "']" + key_prefix;
                                        });
                                        key = key_prefix + key;
                                    }
        	                                                            
                                    if (arr_keys) {
                                        var tmp_key = key_prefix + "['" + arr_keys[0] + "']";
                                        if (typeof eval("form_values"+tmp_key) == 'undefined') {
            	                            eval("form_values"+tmp_key+"=[];");
            	                        }                    
                                    }
                                    
                                    var multiple = false;
        	                        if (typeof eval("form_values"+key) == 'undefined') {
        	                            if (key_prefix != '') {
            	                            eval("form_values"+key+"=new Array();");
        	                            } else {
            	                            eval("form_values"+key+"={};");        	                                
        	                            }
        	                        } else {        	                            
        	                            multiple = true;
        	                        }
        	                        
        	                        prevs.push(name_parts[i]);
                                    if (i == name_parts_count-1) {
                                        if (typeof item['val'] == 'undefined') {
                                            return;
                                        }
                                        if (multiple) {
                                            if (typeof item == 'object') {
                                                // eval("form_values"+key+".push('"+item.toSource()+"');");
                                                if (item.widget.name == 'boolean') {
                                                    eval("form_values"+key+"="+item.val+";");
                                                } else {
                                                    eval("form_values"+key+".push('"+item.val+"');");                                                    
                                                }
                                            } else {
                                                eval("form_values"+key+".push('"+item+"');");
                                            }                                                
                                        } else {
                                            if (typeof item == 'object') {
                                                eval("form_values"+key+"='"+item.val+"';");
                                            } else {
                                                eval("form_values"+key+"='"+item.val+"';");
                                            }
                                        }
                                    }
        	                    }
        	                } else {
        	                    form_values[row.name] = item;
        	                }
        	            }
        	            else if (row.name.substr(0,8) == "metadata")
        	            {
        	                if (! form_values['metadata']) {
        	                    form_values['metadata'] = {};
        	                }

        	                var re = /\bmetadata\[([a-z]+)\b/;
        	                var results = re.exec(row.name);
        	                var key = results[1];

        	                form_values['metadata'][key] = {
        	                    val: row.value
        	                };
        	            }
    	            }
    	        }
    	    });
    	    doc['value'] = form_values;

            doc = new $.ajatus.document(doc);

            $.ajatus.preferences.view.save(doc.value);
        },
        
        save: function(preferences) {
            var saved = false;

            var on_success = function(data) {
                saved = true;
                $.ajatus.preferences.revision = data.rev;
                $.ajatus.events.named_lock_pool.decrease('unsaved');                
                window.location.reload();
                
                return data;
            };
            
            prefs_doc = new $.ajatus.document({
                _id: 'preferences',
                _rev: $.ajatus.preferences.revision,
                value: preferences
            });
            prefs_doc.value._type = 'preferences';

    	    var new_metadata = {
    	        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
    	        revisor: $.ajatus.preferences.local.user.email
    	    };
    	    prefs_doc = $.ajatus.document.modify_metadata(prefs_doc, new_metadata);
            
            var prefs = {
                metadata: prefs_doc.value.metadata
            };
            $.each($.ajatus.preferences.local_defaults, function(k,data){                
                if (typeof prefs[k] == 'undefined') {
                    prefs[k] = {};
                }
                
                if (typeof data == 'object') {                    
                    $.each(data, function(sk, sdata){                        
                        if (typeof prefs_doc.value[k] == 'undefined') {
                            prefs[k][sk] = sdata;
                        } else {
                            if (typeof prefs_doc.value[k][sk] == 'undefined') {
                                prefs[k][sk] = sdata;
                            } else {
                                if (typeof prefs_doc.value[k][sk]['val'] == 'undefined') {
                                    prefs[k][sk] = prefs_doc.value[k][sk];
                                } else {
                                    prefs[k][sk] = prefs_doc.value[k][sk].val;
                                }
                            }                            
                        }
                    });
                } else {
                    if (typeof prefs_doc.value[k] == 'undefined') {
                        prefs[k] = data;
                    } else {
                        if (typeof prefs_doc.value[k]['val'] == 'undefined') {
                            prefs[k] = prefs_doc.value[k];
                        } else {
                            prefs[k] = prefs_doc.value[k].val;
                        }
                    }
                }                
            });

            prefs_doc.value = prefs;
            
            $.jqCouch.connection('doc', on_success).save($.ajatus.preferences.client.application_database, prefs_doc);
            
            return saved;
        },
        
        generate_section_form: function(section) {
            if (typeof $.ajatus.preferences.local[section] == 'undefined') {
                return false;
            }

            var items = false;            
            var section_data = $.ajatus.preferences.local[section];
            
            function auto_gen(section, data) {
                var rows = $('<ul/>');
                
                $.each(data, function(i,s){
                    // console.log("i: "+i+" s: ");
                    // console.log(s);
                    var s_type = typeof(s);
                    // console.log('type: '+s_type);
                    var wdgt = null;
                    var normalized_name = i.toString().replace('_', ' ');

                    switch (s_type) {
                        case 'boolean':
                            wdgt = $.ajatus.widget('boolean');
                        break;
                        case 'number':
                            wdgt = $.ajatus.widget('integer');
                        break;
                        case 'string':
                        default:
                            wdgt = $.ajatus.widget('text');
                    };

                    rows.createAppend(
                        'li', { id: '_setting_'+section+'_'+i, className: 'row' }, [
                            'label', { id: '_setting_'+section+'_'+i+'_label' }, $.ajatus.i10n.get(normalized_name),
                            'div', { id: '_setting_'+section+'_'+i+'_widget', className: wdgt.name }, wdgt.get_edit_tpl(section+';'+i, {val: s}),
                            'br', { className: 'clear_fix' }, ''
                        ]
                    );            
                    wdgt.init($('#_setting_'+i+'_widget',rows), true);
                });

                $('li:first', rows).addClass('first');
                $('li:last', rows).addClass('last');
                
                return rows;
            }
            
            function gen_localization(data) {
                var rows = $('<ul/>');
                var langs = $.ajatus.i10n.get_available_langs();
                var lang_opts = {};
                
                $.each(langs, function(i,l) {
                    lang_opts[i] = l;
                });

                wdgt = $.ajatus.widget('selection', {
                    items: lang_opts
                });
                rows.createAppend(
                    'li', { id: '_setting_localization_language', className: 'row' }, [
                        'label', { id: '_setting_localization_language_label' }, $.ajatus.i10n.get('Language'),
                        'div', { id: '_setting_localization_language_widget', className: wdgt.name }, wdgt.get_edit_tpl('localization;language', {val: data.language}),
                        'br', { className: 'clear_fix' }, ''
                    ]
                );            
                wdgt.init($('#_setting_localization_language_widget',rows), true);
                
                return rows;                
            }
            
            function gen_layout(data) {
                var rows = $('<ul/>');
                var themes = $.ajatus.preferences.client.themes.available;
                var theme_opts = {};
                
                $.each(themes, function(i,l) {
                    theme_opts[i] = $.ajatus.i10n.get(l);
                });

                wdgt = $.ajatus.widget('selection', {
                    items: theme_opts
                });
                rows.createAppend(
                    'li', { id: '_setting_layout_theme_name', className: 'row' }, [
                        'label', { id: '_setting_layout_theme_name_label' }, $.ajatus.i10n.get('Theme'),
                        'div', { id: '_setting_layout_theme_name_widget', className: wdgt.name }, wdgt.get_edit_tpl('layout;theme_name', {val: data.theme_name}),
                        'br', { className: 'clear_fix' }, ''
                    ]
                );            
                wdgt.init($('#_setting_layout_theme_name_widget', rows), true);
                
                return rows;
            }
            
            function gen_autosave(data) {
                var rows = $('<ul/>');
                var section = 'auto_save';

                $.each(data, function(i,s){
                    // console.log("i: "+i+" s: ");
                    // console.log(s);
                    var s_type = typeof(s);
                    // console.log('type: '+s_type);
                    var wdgt = null;
                    var normalized_name = i.toString().replace('_', ' ');
                    
                    switch (i) {
                        case 'enabled':
                            wdgt = $.ajatus.widget('boolean');
                        break;
                        case 'interval':
                            wdgt = $.ajatus.widget('integer',{
                                value_suffix: 's'
                            });
                        break;
                        default:
                            wdgt = $.ajatus.widget('text');
                    };

                    rows.createAppend(
                        'li', { id: '_setting_'+section+'_'+i, className: 'row' }, [
                            'label', { id: '_setting_'+section+'_'+i+'_label' }, $.ajatus.i10n.get(normalized_name),
                            'div', { id: '_setting_'+section+'_'+i+'_widget', className: wdgt.name }, wdgt.get_edit_tpl(section+';'+i, {val: s}),
                            'br', { className: 'clear_fix' }, ''
                        ]
                    );            
                    wdgt.init($('#_setting_'+i+'_widget',rows), true);
                });

                $('li:first', rows).addClass('first');
                $('li:last', rows).addClass('last');
                
                return rows;
            }
            
            // Example how to handle arrays with checkboxes
            // function gen_types(data) {
            //     var rows = $('<ul />');
            //     
            //     $.each($.ajatus.types.available, function(key,type){
            //         // console.log("key: "+key);
            //         // console.log(type);
            //         
            //         var selected = false;
            //         $.each(data.active, function(i,t){
            //             if (t == key) {
            //                 selected = true;
            //             }
            //         });
            //         
            //         var widget = new $.ajatus.widget('boolean', {
            //             value: key
            //         });
            // 
            //         rows.createAppend(
            //             'li', { id: '_setting_types_active_'+key, className: 'row' }, [
            //                 'label', { id: '_setting_types_active_'+key+'_label' }, $.ajatus.i10n.get(type.title),
            //                 'div', { id: '_setting_types_active_'+key+'_widget', className: widget.name }, widget.get_edit_tpl('types;active', {val: (selected ? key : '')}), //["'+key+'"]
            //                 'br', { className: 'clear_fix' }, ''
            //             ]
            //         );            
            //         widget.init($('#_setting_types_active_'+key+'_widget',rows), true);
            //     });
            //     
            //     return rows;
            // }
            
            switch (section) {
                case 'localization':
                    items = gen_localization(section_data);
                break;
                case 'layout':
                    items = gen_layout(section_data);
                break;
                case 'auto_save':
                    items = gen_autosave(section_data);
                break;
                case 'replication':
                case 'backups':
                case 'expansions':
                break;
                default:
                    items = auto_gen(section, section_data);
            }
            
            return items;
        }
    };
    
})(jQuery);