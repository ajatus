/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
	$.ajatus.history = {
	    _handler: false,
	    last_checked: ''
	};
	$.extend($.ajatus.history, {
	    init: function() {	        
            $.ajatus.history._handler = new $.ajatus.history.handler;
    	    $.ajatus.history._handler.initialize(function(){
    	        $.ajatus.tabs.set_active_by_hash('#view.frontpage');
    	        $.ajatus.views.system.frontpage.render();
    	    });
	    },
	    add_map: function(hash, action) {
	        $.ajatus.history._handler.add_map(hash, action);
	    },
	    add_dynamic_map: function() {
	        var statics = [];
	        var mods = [];
	        var action = '';
	        if (arguments[0]) {
	            statics = arguments[0];
	        }
	        if (arguments[1]) {
	            mods = arguments[1];
	        }
	        if (arguments[2]) {
	            action = arguments[2];
	        }
	        $.ajatus.history._handler.add_dynamic_map(statics, mods, action);
	    },
	    update: function(hash) {
	        $.ajatus.history._handler.update(hash);
	    },
	    check: function(hash) {	        
	        if (typeof hash == 'undefined') {
    	        var hash = $.ajatus.history._handler.get_hash();	            
	        }
            // $.ajatus.debug('$.ajatus.history.check('+hash+')');
            
	        if (   !$.browser.mozilla
	            && $.ajatus.history.last_checked == hash)
	        {
	            //$.ajatus.debug('$.ajatus.history.check skipping hash '+hash);
	            return false;
	        }	        
	        $.ajatus.history.last_checked = hash;            
	        
	        if ($.ajatus.history._handler.executable(hash)) {
	            $.ajatus.tabs.set_active_by_hash(hash);
	            $.ajatus.history._handler.execute(hash);
	            return true;
	        }
	        
	        return false;
	    },
	    navigate: function(count) {
	        if (typeof count == 'undefined') {
	            return;
	        }
	        
	        $.ajatus.history._handler.navigate(count);
	    }
	});
	
    /**
     * Base idea is taken from $.ajaxHistory plugin by Klaus Hartl
     * Difference in this is that we evaluate predefined actions
     * that are mapped against hashes.
     */
    $.ajatus.history.handler = function(){
        
        var mappings = {};
        var dyn_mappings = [];
        
        var RESET_EVENT = 'historyReset';

        var _currentHash = location.hash;
        var _intervalId = null;
        var _observeHistory;

        this.update = function(){}; // empty function body for graceful degradation
        
        this.add_map = function(hash, action) {
            mappings[hash] = action;
        };
        
        this.add_dynamic_map = function(statics, mods, action) {
            var hash = '#';
            $.each(statics, function(i,s){
                hash += s + '.';
            });
            var dyn_map = {
                'hash': hash,
                'statics': statics,
                'mods': mods,
                'action': action
            };
            dyn_mappings.push(dyn_map);
        };
        
        this.is_dyn_mapping = function(hash) {
            var ch = hash.replace('#','');
            var hash_parts = ch.split(/\./);

            var ret_val = false;
            var func_args = [];
            var added_args = [];
            $.each(dyn_mappings, function(i,dm){                
                var matched = false;
                var matched_key = 0;
                $.each(dm.statics, function(k,n){
                    if (hash_parts[k] == n) {
                        matched = true;
                        matched_key = k;
                    }
                });
                
                $.each(hash_parts, function(hk,hp){
                    if (   hk > matched_key
                        && hk < dm.mods)
                    {
                        
                        if ($.inArray(hp, added_args) == -1) {
                            added_args.push(hp);
                            func_args.push(hp);
                        }
                    }
                });
                
                if (func_args.length > dm.mods) {
                    matched = false;
                }

                if (matched) {
                    ret_val = {
                        id: Number(i),
                        args: func_args
                    }
                }
            });
            
            
            
            return ret_val;
        };
        
        this.executable = function(hash){
            if (mappings[hash]) {
                return true;
            } else {
                var dm_data = this.is_dyn_mapping(hash);
                if (dm_data !== false) {
                    return true;
                }
            }
            return false;            
        };
        
        this.execute = function(hash){
            //$.ajatus.debug('$.ajatus.extension.history.execute('+hash+')');
            
            if (mappings[hash]) {
                eval(mappings[hash]);
                
                //$.ajatus.debug('$.ajatus.extension.history.execute evaled from mappings: '+mappings[hash]);
                
                return true;
            } else {
                var dm_data = this.is_dyn_mapping(hash);
                if (dm_data !== false) {
                    //$.ajatus.debug('$.ajatus.extension.history.execute hash is dynamic mapping. Evaling: '+dyn_mappings[dm_data.id].action);
                    
                    var func = eval(dyn_mappings[dm_data.id].action);
                    func.apply(func, dm_data.args);
                                        
                    return true;
                }
            }
            return false;
        };
        
        this.get_hash = function(){
            var hash = '';
            
            if ($.browser.safari) {
                if (window.document.URL.indexOf('#') >= 0) {
                    hash = '#'+window.document.URL.split('#')[1];
                }
            } else {
                hash = location.hash;                
            }
            
            return hash;
        };
        
        // create custom event for state reset
        var _defaultReset = function() {
        };        
        $(window.document).bind(RESET_EVENT, _defaultReset);
        
        if ($.browser.safari) {

            var _backStack, _forwardStack, _addHistory; // for Safari

            // establish back/forward stacks
            $(function() {
                _backStack = [];
                _backStack.length = history.length;
                _forwardStack = [];
            });
            var isFirst = false, initialized = false;
            _addHistory = function(hash) {
                _backStack.push(hash);
                _forwardStack.length = 0; // clear forwardStack (true click occured)
                isFirst = false;
            };

            this.update = function(hash) {
                _currentHash = hash;
                location.hash = hash;
                _addHistory(_currentHash);
            };

            this.navigate = function(count) {
                if (count < 0) {
                    var idx = (_backStack.length + count) - 1;
                    var newHash = _backStack[idx];
                } else {
                    var idx = (_forwardStack.length - count) - 1;
                    var newHash = _forwardStack[idx];
                }
                
                if (typeof newHash != 'undefined') {
                    $.ajatus.history.update(newHash);                        
                }
            }

            _observeHistory = function() {
                var historyDelta = history.length - _backStack.length;
                if (historyDelta) { // back or forward button has been pushed
                    isFirst = false;
                    if (historyDelta < 0) { // back button has been pushed
                        // move items to forward stack
                        for (var i = 0; i < Math.abs(historyDelta); i++) _forwardStack.unshift(_backStack.pop());
                    } else { // forward button has been pushed
                        // move items to back stack
                        for (var i = 0; i < historyDelta; i++) _backStack.push(_forwardStack.shift());
                    }
                    var cachedHash = _backStack[_backStack.length - 1];
                    _currentHash = location.hash;

                    $.ajatus.history.check(_currentHash);
                } else if (typeof(_backStack[_backStack.length - 1]) == 'undefined' && !isFirst) {                    
                    if (location.hash == '') {
                        $(window.document).trigger(RESET_EVENT);
                    }
                    
                    isFirst = true;
                }
                initialized = true;
            };
        } else {
            this.update = function(hash) {
                if (_currentHash != hash) {
                    location.hash = hash;
                    _currentHash = hash;
                }
            };

            _observeHistory = function() {
                if (location.hash) {              
                    if (_currentHash != location.hash) {
                        _currentHash = location.hash;
                        $.ajatus.history.check(_currentHash);
                    }
                } else if (_currentHash) {
                    _currentHash = '';
                    $(window.document).trigger(RESET_EVENT);
                }
            };
            
            this.navigate = function(count) {
                window.history.go(count);
            }
        }
        
        this.initialize = function(callback) {
            // custom callback to reset app state (no hash in url)
            if (typeof callback == 'function') {
                $(window.document).unbind(RESET_EVENT, _defaultReset).bind(RESET_EVENT, callback);
            }
            // look for hash in current URL (not Safari) and execute predefined action if one is found
            if (location.hash && typeof _addHistory == 'undefined') {                
                $.ajatus.history.check(location.hash);
            }
            // start observer
            if (_observeHistory && _intervalId == null) {
                _intervalId = setInterval(_observeHistory, 200); // Safari needs at least 200 ms
            }
        };
    };
    
})(jQuery);