/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.maintenance = {
        recreate: {
            'views': false
        }
    };
    $.extend($.ajatus.maintenance, {
        version_upgrade: function() {
            $.ajatus.maintenance.recreate['views'] = true;
            
            $.jqCouch.connection('doc').save($.ajatus.preferences.client.application_database, {
                _id: 'version',
                _rev: $.ajatus.installer.installed_version_rev,
                value: $.ajatus.version
            });
        }
    });
    
})(jQuery);