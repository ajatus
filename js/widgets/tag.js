/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;

    $.ajatus.widgets.core.tag = {
        name: 'tag',
        settings: {
            color: '8596b6',
            context: '',
            value: ''
        },
        get_create_tpl: function(name, default_value)
        {
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'text', className: 'text', name: name, value: default_value }, ''
            ];
        },
        get_edit_tpl: function(name, data)
        {
            data.val = this.value_on_edit(data.val);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: $.ajatus.converter.toJSON(data.val) }, '',
                'input', { type: 'text', className: 'text', name: name, value: data.val }, ''
            ];
        },
        get_view_tpl: function(name, data)
        {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value' }, [                
                    'span', { className: this.name + '_widget_bgcolor_block' }, data.val
                ]
            ];
        },
        set_config: function(config)
        {
            this.settings = $.extend({}, this.settings, config);
        },
        value_on_save: function(value)
        {
            return value;
        },
        value_on_edit: function(value)
        {
            return value;
        },
        value_on_view: function(value)
        {
            return value;
        },
        loaded: function()
        {
        },
        init: function(holder, form_mode)
        {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }            
        },
        init_form_mode: function(holder)
        {
            // TODO: Append color picker widget after title field.
            // Save value to object settings
        },
        init_view_mode: function(holder)
        {
            // TODO: Find 'tag_widget_bgcolor_block element and set its background
            // from objects settings.color value
        },
    };

})(jQuery);