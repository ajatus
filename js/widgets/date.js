/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;

    $.ajatus.widgets.core.date = {
        name: 'date',
        settings: {
            calendar: {
                dateFormat: $.ajatus.i10n.datetime.date,
                firstDay: 1
            },
            use_time: false,
            time_format: $.ajatus.i10n.datetime.time
        },
        get_create_tpl: function(name, default_value) {
            if (default_value == '') {
                default_value = $.ajatus.formatter.date.js_to_iso8601(new Date());
            }
            default_value = this.value_on_edit(default_value);

            var date_input_name = name;
            var date_val = default_value;
            var time_val = '';            
            var time_tpl = '';
            
            if (this.settings.use_time) {
                date_val = typeof default_value['date'] != 'undefined' ? default_value['date'] : default_value;
                time_val = typeof default_value['time'] != 'undefined' ? default_value['time'] : '';
                
                time_tpl = ['input', { type: 'text', name: name+'|time', value: time_val, className: 'time' }, ''];
                date_input_name = name+'|date';
            }

            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'div', { className: 'date_holder' }, [
                    'input', { type: 'text', name: date_input_name, value: date_val, className: 'date' }, ''
                ],
                'div', { className: 'time_holder', style: 'display: none;' }, time_tpl
            ];
        },
        get_edit_tpl: function(name, data) {
            data.val = this.value_on_edit(data.val);
            
            var date_input_name = name;
            var date_val = data.val;
            var time_val = '';            
            var time_tpl = '';
            
            if (this.settings.use_time) {
                date_val = typeof data.val['date'] != 'undefined' ? data.val['date'] : data.val;
                time_val = typeof data.val['time'] != 'undefined' ? data.val['time'] : '';
                
                time_tpl = ['input', { type: 'text', name: name+'|time', value: time_val, className: 'time' }, ''];
                date_input_name = name+'|date';                
            }
            
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: $.ajatus.converter.toJSON(data.val) }, '',
                'div', { className: 'date_holder' }, [
                    'input', { type: 'text', name: date_input_name, value: date_val, className: 'date' }, '',
                ],
                'div', { className: 'time_holder', style: 'display: none;' }, time_tpl
            ];
        },
        get_view_tpl: function(name, data) {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value' }, data.val
            ];
        },
        set_config: function(config) {
            this.settings = $.extend(this.settings, config);
        },
        value_on_save: function(value, prev_val, multi_inputs) {            
            var date_value = '';
            var time_value = '';
            
            if (multi_inputs) {
                date_value = value['date'];
                time_value = value['time'];
                if (   this.settings.use_time
                    && this.settings.time_format)
                {
                    return $.ajatus.formatter.date.caldate_to_iso8601(date_value, this.settings.calendar.dateFormat, time_value, this.settings.time_format);
                }
            }
            
            if (! $.ajatus.formatter.date.is_iso8601(value)) {
                if (typeof value == 'string') {
                    return $.ajatus.formatter.date.caldate_to_iso8601(value, this.settings.calendar.dateFormat);
                } else {
                    return $.ajatus.formatter.date.js_to_iso8601(value);
                }
            }
            
            return value;
        },
        value_on_edit: function(value) {            
            if (   this.settings.use_time
                && this.settings.time_format)
            {
                var val_data = {};
                
                var format = this.settings.calendar.dateFormat;
                if (format != $.ajatus.i10n.datetime.date) {
                    format = $.ajatus.i10n.datetime.date;
                }
                val_data['date'] = $.ajatus.formatter.date.iso8601_to_caldate(value, format);

                var time_format = this.settings.time_format;
                if (time_format != $.ajatus.i10n.datetime.time) {
                    time_format = $.ajatus.i10n.datetime.time;
                }
                val_data['time'] = $.ajatus.formatter.date.iso8601_to_caltime(value, time_format);
                                
                return val_data;
            } else {
                return this.value_on_view(value);                
            }
        },
        value_on_view: function(value) {
            var format = this.settings.calendar.dateFormat;
            if (format != $.ajatus.i10n.datetime.date) {
                format = $.ajatus.i10n.datetime.date;
            }
            
            if (   this.settings.use_time
                && this.settings.time_format)
            {                
                var time_format = this.settings.time_format;
                if (time_format != $.ajatus.i10n.datetime.time) {
                    time_format = $.ajatus.i10n.datetime.time;
                }
                var date_val = $.ajatus.formatter.date.iso8601_to_caldate(value, format);
                var time_val = $.ajatus.formatter.date.iso8601_to_caltime(value, time_format);
                
                return date_val + " " + time_val;
            } else {
                return $.ajatus.formatter.date.iso8601_to_caldate(value, format);
            }
        },
        loaded: function() {
            $.ajatus.events.lock_pool.increase();
            var cal_url = $.ajatus.preferences.client.application_url + 'js/jquery/ui/ui.calendar.js';
            $.ajatus.utils.load_script(cal_url, "$.ajatus.events.lock_pool.decrease", []);
        },
        init: function(holder, form_mode) {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }            
        },
        init_form_mode: function(holder) {
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/widgets/date.css');
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/jquery/ui/themes/flora/flora.calendar.css');
            
            var date_element = $('input.date[@type=text]', holder);
            if (date_element) {
                date_element.attr('autocomplete', 'off')
                date_element.calendar(this.settings.calendar);                
            }
            
            if (this.settings.use_time) {
                var time_element = $('input.time[@type=text]', holder);
                time_element.parent().show();
                time_element.css({
                    width: 60
                });
            }
        },
        init_view_mode: function(holder) {
        },
        create_widget_details: function(data) {
            var details = $.ajatus.widgets.generate_default_details(this, data);

            return details;
        },
        create_widget_settings: function(data) {
            var settings = $.ajatus.widgets.generate_default_settings(this, data);
            
            return settings;
        }
    };

})(jQuery);