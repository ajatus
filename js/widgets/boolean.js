/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;

    $.ajatus.widgets.core.boolean = {
        name: 'boolean',
        settings: {
            value: true,
            view_values: [ 'False', 'True' ]
        },
        get_create_tpl: function(name, default_value)
        {
            default_value = (typeof this.settings.value == 'boolean') ? ((default_value === true || default_value === 1 || default_value === "true") ? true : false) : default_value;
            var oppos_val = typeof this.settings.value == 'boolean' ? (this.settings.value ? false : true) : '';
            
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: name, value: oppos_val }, '',
                'input', { type: 'checkbox', className: 'checkbox', name: name, value: this.settings.value, checked: (default_value === this.settings.value ? 'checked' : '') }, ''
            ];
        },
        get_edit_tpl: function(name, data)
        {
            data.val = this.value_on_edit(data.val);
            var oppos_val = typeof this.settings.value == 'boolean' ? (this.settings.value ? false : true) : '';
            
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: $.ajatus.converter.toJSON(data.val) }, '',
                'input', { type: 'hidden', name: name, value: oppos_val }, '',
                'input', { type: 'checkbox', className: 'checkbox', name: name, value: this.settings.value, checked: (data.val === this.settings.value ? 'checked' : '') }, ''
            ];
        },
        get_view_tpl: function(name, data)
        {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value' }, data.val
            ];
        },
        set_config: function(config)
        {
            this.settings = $.extend(this.settings, config);
        },
        value_on_save: function(value)
        {
            if (   typeof value == 'object'
                && typeof value[0] != 'undefined')
            {
                value = value[0];
            }
            
            value = (typeof this.settings.value == 'boolean') ? ((value === true || value === 1 || value === "true") ? true : false) : value;
            value = $.ajatus.utils.to_boolean(value);
            return value;
        },
        value_on_edit: function(value)
        {
            if (   typeof value == 'object'
                && typeof value[0] != 'undefined')
            {
                value = value[0];
            }
            
            value = typeof this.settings.value == 'boolean' ? ((value === true || value === 1 || value === "true") ? true : false) : value;
            value = $.ajatus.utils.to_boolean(value);
            return value;
        },
        value_on_view: function(value)
        {
            value = typeof this.settings.value == 'boolean' ? ((value === true || value === 1 || value === "true") ? true : false) : value;
            var v = value === this.settings.value ? 1 : 0;
            value = this.settings.view_values[v];
            return value;
        },
        loaded: function()
        {
        },
        init: function(holder, form_mode)
        {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }            
        },
        init_form_mode: function(holder)
        {
        },
        init_view_mode: function(holder)
        {
        },
    };

})(jQuery);