/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;

    $.ajatus.widgets.core.selection = {
        name: 'selection',
        settings: {
            options: [],
            height: 1,
            items: {}
        },
        selected: null,
        get_create_tpl: function(name, default_value)
        {
            var options_tpl = this.get_options_tpl(default_value);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'select', { className: 'select', name: name, height: this.settings.height }, options_tpl
            ];
        },
        get_edit_tpl: function(name, data)
        {
            data.val = this.value_on_edit(data.val);
            var options_tpl = this.get_options_tpl(data.val);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'select', { className: 'select', name: name, height: this.settings.height }, options_tpl
            ];
        },
        get_view_tpl: function(name, data)
        {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value' }, data.val
            ];
        },
        get_options_tpl: function(selected)
        {
            var options = [];
            var _self = this;
            this.selected = selected;
            
            $.each(this.settings.items, function(i,n){
                var sel = _self.is_selected(n,selected) ? 'selected' : '';

                options.push('option');
                options.push({
                    selected: sel,
                    value: n
                });
                options.push(n+'&nbsp;');
            });
            
            return options;
        },
        is_selected: function(key, sel)
        {
            var selected = false;
            if (typeof sel == 'object') {
                $.each(sel, function(i,s){
                    if (key == s) {
                        selected = true;
                    }
                });
            } else {
                if (key == sel) {
                    selected = true;
                }
            }
            return selected;
        },
        set_config: function(config)
        {
            this.settings = $.extend(this.settings, config);
        },
        value_on_save: function(value)
        {
            return value.toString().replace(/\s/,'');
        },
        value_on_edit: function(value)
        {
            return value;
        },
        value_on_view: function(value)
        {
            if (this.setting.height > 1) {
                value = value.join(', ');
            }
            return value;
        },
        loaded: function()
        {
        },
        init: function(holder, form_mode)
        {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }            
        },
        init_form_mode: function(holder)
        {
            var _self = this;

            $('option', $(holder)).each(function(i,o){
                if (o.value == _self.selected) {
                    o.selected = 'selected';
                }
            });
        },
        init_view_mode: function(holder)
        {
        }
    };

})(jQuery);