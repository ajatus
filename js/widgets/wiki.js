/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;
    
    $.ajatus.widgets.core.wiki = {
        name: 'wiki',
        settings: {
            width: null,
            height: 400,
            format: 'markdown',
            livepreview: true
        },
        rendered_value: null,
        
        get_create_tpl: function(name, default_value) {
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'div', { className: 'widget_actions' }, [
                    'div', { className: 'edit_mode' }, $.ajatus.i10n.get('edit mode'),
                    'div', { className: 'preview_mode' }, $.ajatus.i10n.get('preview mode')
                ],
                'div', { className: 'widget_content' }, [
                    'textarea', { name: name, value: default_value }, ''
                ]
            ];
        },
        get_edit_tpl: function(name, data) {
            data.val = this.value_on_edit(data.val);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: (data.val != '' ? $.ajatus.converter.toJSON(data.val) : '') }, '',
                'div', { className: 'widget_actions' }, [
                    'div', { className: 'edit_mode' }, $.ajatus.i10n.get('Edit mode'),
                    'div', { className: 'preview_mode' }, $.ajatus.i10n.get('Preview mode')
                ],
                'div', { className: 'widget_content' }, [
                    'textarea', { name: name, value: data.val }, ''
                ]
            ];
        },
        get_view_tpl: function(name, data) {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value '+this.name+'_value' }, data.val
            ];
        },
        set_config: function(config) {
            this.settings = $.extend(this.settings, config);
        },
        value_on_save: function(value) {
            return value;
        },
        value_on_edit: function(value) {
            return value;
        },
        value_on_view: function(value) {
            return value;
        },
        loaded: function() {            
            $.ajatus.events.lock_pool.increase();
            
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/widgets/wiki.css');
            
            var showdown_url = $.ajatus.preferences.client.application_url + 'js/widgets/includes/showdown.js';
            $.ajatus.utils.load_script(showdown_url, "$.ajatus.events.lock_pool.decrease", []);
        },
        init: function(holder, form_mode) {            
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }
        },
        init_form_mode: function(holder) {
            var element = $('textarea', holder);
            var actions = $('.widget_actions', holder);
            
            element.css({
                width: (this.settings.width != null && this.settings.width > 0) ? this.settings.width : '99%',
                height: this.settings.height
            });
            if (this.settings.livepreview) {
                element.livePreview({
                    width: this.settings.width != null ? this.settings.width : '99%',
                    height: this.settings.height
                });
                $('.livePreview_holder', holder).hide();
                
                $('.edit_mode', actions).bind('click', function(e){
                    $('.livePreview_holder', holder).hide();
                    $('.widget_content', holder).show();
                });
                $('.preview_mode', actions).bind('click', function(e){
                    $('.widget_content', holder).hide();
                    $('.livePreview_holder', holder).show();
                });
            } else {
                actions.css({
                    display: "none"
                });
            }
        },
        init_view_mode: function(holder) {
            var element = $('div', holder);
            var content = element.html();
            converter = new Showdown.converter();
            if (typeof content == 'undefined') {
                content = '';
            }            
            
            new_content = converter.makeHtml(content);
            element.html(new_content);
        },
        create_widget_details: function(data) {
            var details = $.ajatus.widgets.generate_default_details(this, data);

            return details;
        },
        create_widget_settings: function(data) {
            var settings = $.ajatus.widgets.generate_default_settings(this, data);
            
            return settings;
        }
    };

    $.fn.livePreview = function(options) {
        options = $.extend({
            previewClass: 'livePreview_holder',
            converter: 'markdown',
            remote_url: null,
            auto_hide: true,
            width: null,
            height: null
        }, options);

        this.each(function(i,element) {
            var jq_object = $(element);

            var rand_id = new Date().getTime() + "" + Math.floor(Math.random()*10);
            var preview_id = element.id || rand_id;
            var holder = null;
            var holder_content = null;
            
            create_holder();
            if (options.auto_hide) {
                holder.hide();                
            }
            
            // jq_object.one('focus', function() {
            //     create_holder();
            // });
            jq_object.bind('keyup', update_preview);

            function create_holder() {
                holder = $('<div id="livePreview_holder_for_' + preview_id + '" class="' + options.previewClass + '" />');
                holder.css({
                    width: options.width != null ? options.width : '',
                    display: options.auto_hide ? "none" : "block"
                });
                
                holder_content = $('<div id="livePreview_content_for_' + preview_id + '" class="livePreview_content"></div>');
                holder_content.appendTo(holder);
                holder_content.css({
                    width: options.width != null ? options.width : '',
                    height: options.height != null ? options.height : ''
                });
                                
                jq_object.parent().after(holder);
                
                update_preview();
            }            

            function update_preview() {
                content = jq_object.val();
                
                if (options.converter == 'markdown')
                {
                    
                    converter = new Showdown.converter();
                    new_content = converter.makeHtml(content);
                    //new_content = new_content.replace(/\r/g, "<br />");//.replace(/\r/g, "<br />").replace(/\n/g, "<br />").replace(/\r\n/, "<br />");
                }
                else
                {
                	content = content.replace(/(<\/?)script/g,"$1noscript");
                    new_content = content;
                }

            	holder_content.html( new_content );
            }
        });

    };
    
})(jQuery);