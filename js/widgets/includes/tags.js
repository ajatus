/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

 /*
  * Depends on jquery.bgiframe plugin to fix IE's problem with selects.
  *
  * @name tags_widget
  * @cat Plugins/Autocomplete
  * @type jQuery
  * @param String|Object url an URL to data backend
  * @param Map options Optional settings
  * @option Number min_chars The minimum number of characters a user has to type before the autocompleter activates. Default: 1
  * @option Number delay The delay in milliseconds that search waits after a keystroke before activating itself. Default: 400
  * @option Object extra_params Extra parameters for the backend. Default: {}
  * @option Boolean select_first If this is set to true, the first result will be automatically selected on tab/return. Default: true
  * @option Number width Specify a custom width for the select box. Default: width of the input element
  * @option Boolean autofill_enabled Fill the textinput while still selecting a value, replacing the value if more is typed or something else is selected. Default: false
  * @option Number result_limit Limit the number of items in the results box. Is also send as a "limit" parameter to backend on request. Default: 10
  * @option Number width Specify a custom width for the select box. Default: width of the input element
  * @option Boolean allow_create If this is set to true, then when user presses tab it creates a tag from current input value if we don't have any results. Default: false
  */

(function($){
    
    $.ajatus.widgets.includes = $.ajatus.widgets.includes || {};
    $.ajatus.widgets.includes.tags = {
        defaults: {
        	min_chars: 2,
        	delay: 300,
        	select_first: true,
        	result_limit: 10,
        	autofill_enabled: false,
        	allow_create: true
        }
    };
    $.ajatus.widgets.includes.tags.widget = function(input, options) {
        var KEY = {
    		UP: 38,
    		DOWN: 40,
    		DEL: 46,
    		TAB: 9,
    		RETURN: 13,
    		ESC: 27,
    		COMMA: 188,
    		SPACE: 32,
    		BACKSPACE: 8
    	};
    	
    	var input_element = $(input).attr("autocomplete", "off").addClass('ajatus_tags_widget_input').addClass('ajatus_tags_widget_idle');
        var selection_holder = $.ajatus.widgets.includes.tags.widget.selections(input, options);
        
    	var timeout;
    	var previous_value = "";
    	var has_focus = 0;
    	var last_key_press_code;
    	var select = $.ajatus.widgets.includes.tags.widget.select(input, options, select_current);
    	
    	input_element.keydown(function(event) {
    		last_key_press_code = event.keyCode;

    		switch(last_key_press_code) {
    			case KEY.UP:
    				event.preventDefault();
    				if (select.visible()) {
    					select.prev();
    				} else {
    					on_change(true);
    				}
    			break;
    			case KEY.DOWN:
    				event.preventDefault();
    				if (select.visible()) {
    					select.next();
    				} else {
    					on_change(true);
    				}
    			break;
    			case KEY.TAB:
    			case KEY.RETURN:
    			case KEY.SPACE:
    			case KEY.COMMA:
        			event.preventDefault();
    				if (select_current()) {
    					input_element.focus();
    				} else {
    				    value = input_element.val();
    				    if (   options.allow_create == true
    				        && value != '')
    				    {
    				        var data = {
    				            title: value,
    				            color: '8596b6'
    				        };
    				        input_element.trigger("result", [data]);
    				    }
    				}
    			break;
    			case KEY.ESC:
    				select.hide();
    			break;
    			default:
        			input_element.addClass('ajatus_tags_widget_loading');
    				clearTimeout(timeout);
    				timeout = setTimeout(on_change, options.delay);
    		}
    	}).keypress(function(event) {
    		// having fun with opera - remove this binding and Opera submits the form when we select an entry via return
    		switch(event.keyCode) {
    		    case KEY.TAB:
    		    case KEY:RETURN:
    		        event.preventDefault();
    		    break;
    	    }
    	}).focus(function(){
    		// track whether the field has focus, we shouldn't process any
    		// results if the field no longer has focus
    		has_focus++;
    	}).blur(function() {
    		has_focus = 0;
    		hide_results();
    	}).click(function() {
    		// show select when clicking in a focused field
    		if (   has_focus++ > 1
    		    && !select.visible())
    		{
    			on_change(true);
    		}
    	}).bind("result", function(event, data){
    	    input_element.val('');
    	    input_element.focus();
    	    stop_loading();
    	    selection_holder.add_item(data);
    	}).bind("add_selection_item", function(event, item, is_raw){
    	    selection_holder.add_item(item, is_raw);
    	}).bind("remove_selection_item", function(event, id){
    	    selection_holder.del_item(id);
    	});
    	
    	hide_results_now();
    	
    	function select_current() { 	    
    		var selected = select.selected();
    		
    		if (! selected) {
    			return false;    		    
    		}

    		var v = selected.result;
    		previous_value = v;

    		input_element.val('');
    		input.focus();
    		hide_results_now();
    		input_element.trigger("result", [selected.data]);
    		return true;
    	}
    	
    	function on_change(skip_prev_check) {    	    
    		if (last_key_press_code == KEY.DEL) {
    			select.hide();
    			return;
    		}

    		var current_value = input_element.val();
            
            var context_key = '';
            if (current_value.match(/:/)) {
                var value_parts = current_value.split(':');
                context_key = value_parts[0];
                current_value = value_parts[1];
            }

    		if (   !skip_prev_check
    		    && current_value == previous_value )
    		{
    			return;
    		}

    		previous_value = current_value;

    		current_value = last_word(current_value);
    		if (current_value.length >= options.min_chars) {    			
    			current_value = current_value;//.toLowerCase();
    			request(current_value, context_key, receive_data, stop_loading);
    		} else {
    			stop_loading();
    			select.hide();
    		}
    	}
    	
    	function trim_words(value) {
    		if (! value) {
    			return [""];
    		}
    		
    		var words = value.split($.trim(options.multiple_separator));
    		var result = [];
    		jQuery.each(words, function(i, value) {
    			if ($.trim(value)) {
    				result[i] = $.trim(value);    			    
    			}
    		});
    		
    		return result;
    	}
    	
    	function last_word(value) {
    		if (! options.multiple) {
    			return value;    		    
    		}
    		var words = trim_words(value);
    		return words[words.length - 1];
    	}
    	
    	function autofill(q, value) {
    		if (   options.autofill_enabled
    		    && (last_word(input_element.val()).toLowerCase() == q.toLowerCase())
    		    && last_key_press_code != KEY.BACKSPACE )
    		{
    			// fill in the value (keep the case the user has typed)
    			input_element.val(input_element.val() + value.substring(last_word(previous_value).length));
    			// select the portion of the value not typed by the user (so the next character will erase)
    			$.ajatus.widgets.includes.tags.widget.move_selection(input, previous_value.length, previous_value.length + value.length);
    		}
    	}
    	
    	function hide_results() {
    		clearTimeout(timeout);
    		timeout = setTimeout(hide_results_now, 200);
    	}

    	function hide_results_now() {
    		select.hide();
    		clearTimeout(timeout);
    		stop_loading();
    	}
    	
    	function receive_data(q, data) {            
    		if (   data
    		    && data.length
    		    && has_focus)
    		{
    			stop_loading();
    			select.display(data, q);
    			autofill(q, data[0].title);
    			select.show();
    		} else {
    			hide_results_now();
    		}
    	}
    	
    	function request(term, context, success, failure) {            
    		term = last_word(term); //.toLowerCase()
            
            // Search first from cache.
            var results = $.ajatus.tags.search_cache(term, context, options.result_limit);
            if (   typeof results != 'object'
                || results.length <= 0)
            {                            
                results = $.ajatus.tags.search(term, context, options.result_limit);
                if (   typeof results != 'object'
                    || results.length <= 0)
                {
                    failure(true);
                    return false;
                }
            }
            
            results = $.ajatus.widgets.includes.tags.widget.parse_raw_data(results);
            success(term, results);
            
            return true;
    	}

    	function stop_loading(had_errors) {    	    
    	    if (typeof had_errors == 'undefined') {
    	        var had_errors = false;
    	    }
    	    
    		input_element.removeClass('ajatus_tags_widget_loading');
    		    		
    		if (had_errors) {
        	    hide_results();
        		input_element.removeClass('ajatus_tags_widget_idle');
        		input_element.addClass('ajatus_tags_widget_error');
    		} else {
        		input_element.removeClass('ajatus_tags_widget_error');
        		input_element.addClass('ajatus_tags_widget_idle');
    		}
    	}
    }
    
    $.ajatus.widgets.includes.tags.widget.parse_raw_data = function(data) {        
        var results = [];
        $(data).each(function(i,t) {
            results[i] = {
                id: t.id || t._id,                
                title: t.value.title.val,
                color: t.value.title.widget.config.color || '8596b6',
                context: (typeof(t.value.title.widget.config['context']) != 'undefined') ? (t.value.title.widget.config.context || '') : '',
                value: (typeof(t.value.title.widget.config['value']) != 'undefined') ? (t.value.title.widget.config.value || '') : ''
            };
        });

        var parsed = [];
        $(results).each(function(i){
            var item = results[i];
            if (item) {
                parsed[parsed.length] = {
                    data: item,
                    id: item.id,
                    title: (item.context != '' ? item.context + ':' : '') + item.title,
                    context: item.context,
                    value: item.value,
                    result: $.ajatus.widgets.includes.tags.widget.format_item(item)
                };
            }           
        });
        
        return parsed;
    }
    
    $.ajatus.widgets.includes.tags.widget.select = function(input, options, select) {
    	var CLASSES = {
    		ACTIVE: "ajatus_tags_widget_result_item_active"
    	};
    	
    	// Create results holder element
    	var element = $('<div />')
    	    .css({
    	        display: "none"
    	    })
    		.addClass('ajatus_tags_widget_results');
    	$(input).after(element);
    	
    	var list = $('<ul />').appendTo(element).mouseover(function(event) {
    		active = $('li', list).removeClass(CLASSES.ACTIVE).index(target(event));
    		$(target(event)).addClass(CLASSES.ACTIVE);
    	}).mouseout( function(event) {
    		$(target(event)).removeClass(CLASSES.ACTIVE);
    	}).click(function(event) {
    		$(target(event)).addClass(CLASSES.ACTIVE);
    		select();
    		input.focus();
    		return false;
    	});
    	
    	var list_items,
    		active = -1,
    		data,
    		term = "";
    	
    	if (options.width > 0) {
    		element.css("width", options.width);
    	}
    	
    	function target(event) {
    		var element = event.target;
    		while(element.tagName != "LI") {
    			element = element.parentNode;
    		}
    		return element;
    	}
    	
    	function move_select(step) {
    		active += step;
    		wrap_selection();
    		list_items.removeClass(CLASSES.ACTIVE).eq(active).addClass(CLASSES.ACTIVE);
    	}
    	
    	function wrap_selection() {
    		if (active < 0) {
    			active = list_items.size() - 1;
    		} else if (active >= list_items.size()) {
    			active = 0;
    		}
    	}
    	
    	function limit_number_of_items(available) {
    		return (options.result_limit > 0) && (options.result_limit < available)
    			? options.result_limit
    			: available;
    	}
    	
    	function data_to_dom() {
    		var num = limit_number_of_items(data.length);
    		for (var i=0; i < num; i++) {
    			if (! data[i]) {
    				continue;
    			}
    			function highlight(value) {
    				return value.replace(new RegExp("(" + term + ")", "gi"), '<span style="font-weight: bold;">$1</span>');
    			}
    			$('<li />').html( highlight($.ajatus.widgets.includes.tags.widget.format_item(data[i].data)) ).appendTo(list);
    		}
    		
    		list_items = list.find("li");
    		
    		if ( options.select_first ) {
    			list_items.eq(0).addClass(CLASSES.ACTIVE);
    			active = 0;
    		}
    	}
    	
    	return {
    		display: function(d, q) {
    			data = d;
    			term = q;
    			list.empty();
    			data_to_dom();
    			list.bgiframe();
    		},
    		next: function() {
    			move_select(1);
    		},
    		prev: function() {
    			move_select(-1);
    		},
    		hide: function() {
    			element.hide();
    			active = -1;
    		},
    		visible : function() {
    			return element.is(":visible");
    		},
    		current: function() {
    			return this.visible() && (list_items.filter("." + CLASSES.ACTIVE)[0] || options.select_first && list_items[0]);
    		},
    		show: function() {
    			element.css({
    				width: options.width > 0 ? options.width : ($(input).width() + 4)
    			}).show();
    		},
    		selected: function() {
    			return data && data[active];
    		}
    	};
    }
    
    $.ajatus.widgets.includes.tags.widget.selections = function(input, options) {
    	var CLASSES = {
    		HOVER: "ajatus_tags_widget_selection_item_hover",
    		DELETED: "ajatus_tags_widget_selection_item_deleted"
    	};

    	// Create selection holder element
    	var element = jQuery('<div />')
    	    .css({
    	        display: "none"
    	    })
    		.addClass('ajatus_tags_widget_selections');
    	jQuery(input).before( element );

    	var list = jQuery('<ul />').appendTo(element);

        var list_items = [],
            has_content = false;

    	function target(event) {
    		var element = event.target;		
    		while(element.tagName != "LI") {
    			element = element.parentNode;		    
    		}
    		return element;
    	}
    	
    	function gen_data_key(data) {
            var dt = data.title;
            if (dt.match(/:/)) {
                var value_parts = dt.split(':');
                data.context = value_parts[0];
                dt = value_parts[1];
            }
            if (dt.match(/\=/)) {
                var value_parts = dt.split('=');
                data.value = value_parts[1];
                dt = value_parts[0];
            }
            data.title = dt;
            
            return (typeof data.id != 'undefined' ? data.id : '') + dt + (typeof data.context != 'undefined' ? data.context : '') + (typeof data.value != 'undefined' ? data.value : '');
    	}

    	function can_add(data) {
    	    if (options.selection_limit > 0) {
    	        if (list_item.length == options.selection_limit) {
    	            return false;
    	        }
    	    }

            var existing = $.grep( list_items, function(n,i){
                if (   typeof data.id != 'undefined'
                    && data.id != '')
                {
                    return n.id == data.id;
                } else {
                    if (   typeof data.context != 'undefined'
                        && (   typeof data.value == 'undefined'
                            || typeof data.value == ''))
                    {
                        return (n.title == data.title) && (n.context == data.context)
                    }
                    if (   typeof data.context == 'undefined'
                        && (   typeof data.value != 'undefined'
                            && typeof data.value != ''))
                    {
                        return (n.title == data.title) && (n.value == data.value)
                    }
                    if (   typeof data.context != 'undefined'
                        && (   typeof data.value != 'undefined'
                            && typeof data.value != ''))
                    {
                        return (n.title == data.title) && (n.context == data.context) && (n.value == data.value)
                    }
                    
                    return n.title == data.title;
                }
            });
            
            if (existing.length > 0) {
                var key = gen_data_key(data);
                $('#tag_'+key, list).highlightFade(800, 'yellow');
                return false;
            }

    	    return true;
    	}

    	function add(data)
    	{
            // console.log('selections add');
            // console.log('data.id: '+data.id);
            // console.log('data.title: '+data.title);
            // console.log('data.color: '+data.color);
            // console.log('data.context: '+data.context);
            // console.log('data.value: '+data.value);

            if (! can_add(data)) {
                return false;
            }

            if (! has_content) {
                has_content = true;
                element.show();
            }

    	    var input_elem_name = "__tags_widget_selections";//" + data.title + "]";
            
            var key = gen_data_key(data);
            
            data.color = String(data.color).replace("#","");

    	    var li_elem = jQuery('<li />')
    	    .attr({
    	        id: 'tag_'+key
    	    }).css({
    	        'background-color': '#'+data.color
    	    }).mouseover( function(event) {
        		active = $("li", list).removeClass(CLASSES.HOVER).index(target(event));
        		$(target(event)).addClass(CLASSES.HOVER);
        	}).mouseout( function(event) {
        		$(target(event)).removeClass(CLASSES.HOVER);
        	}).click(function(event) {
        	    var li_element = target(event);
        	    var current_class = $(li_element).attr("deleted");
        	    if ($(li_element).attr("deleted") == "true") {
            		$(li_element).removeClass(CLASSES.DELETED);
            		restore(key);    	        
        	    } else {
            		$(li_element).addClass(CLASSES.DELETED);
            		remove(key);
        	    }
        		return false;
        	});
    	    var span_elem = $('<span />')
    	        .html(
    	            $.ajatus.widgets.includes.tags.widget.format_item(data)
    	        ).appendTo(li_elem);
    	    var input_elem = $('<input type="hidden" />').attr({
    	        name: input_elem_name,
    	        value: $.ajatus.converter.toJSON(data),
    	        id: 'tags_widget_tag_value_'+key
    	    }).hide().appendTo(li_elem);

    	    li_elem.appendTo(list);

    	    list_items.push(data);
    	}

    	function remove(id) {
    	    var input = $('#tag_'+id+' input', list);
    	    input.attr({ oldvalue: input.attr('value') });
    	    input.attr({ value: '' });
    	    $('#tag_'+id).attr("deleted","true");
    	}

    	function restore(id) {
    	    var input = $('#tag_'+id+' input', list);
    	    input.attr({ value: input.attr('oldvalue') });
    	    $('#tag_'+id).attr("deleted","false");
        }

    	return {
    	    add_item: function(item, is_raw) {
    	        if (typeof is_raw == 'undefined') {
    	            var is_raw = false;
    	        }
    	        
    	        if (is_raw) {
    	            var parsed = $.ajatus.widgets.includes.tags.widget.parse_raw_data([item]);
        	        add(parsed[0].data);
    	        } else {
        	        add(item);    	            
    	        }
    	    },
    	    del_item: function(id) {
    	        remove(id);
    	    }
    	}
    }
    
    $.ajatus.widgets.includes.tags.widget.move_selection = function(field, start, end) {
    	if (field.createTextRange) {
    		var selRange = field.createTextRange();
    		selRange.collapse(true);
    		selRange.moveStart("character", start);
    		selRange.moveEnd("character", end);
    		selRange.select();
    	} else if (field.setSelectionRange) {
    		field.setSelectionRange(start, end);
    	} else {
    		if (field.selectionStart) {
    			field.selectionStart = start;
    			field.selectionEnd = end;
    		}
    	}
    	field.focus();
    }
    
    $.ajatus.widgets.includes.tags.widget.format_item = function(item) {
        var formatted = '';
        
        if (   typeof item.context != 'undefined'
            && item.context != '')
        {
            formatted += item.context + ':';
        }
        
        formatted += item.title;
        
        if (   typeof item.value != 'undefined'
            && item.value != '')
        {
            formatted += '=' + item.value;
        }        
        
        return formatted;   
    }
    
    $.fn.extend({
        tags: function(options) {
            options = $.extend({}, $.ajatus.widgets.includes.tags.defaults, options);
            return this.each(function(){
                new $.ajatus.widgets.includes.tags.widget(this, options);
            });
        },
        tags_add_item: function(item, is_raw) {
            return this.trigger("add_selection_item", [item, is_raw]);
        },
        tags_remove_item: function(id) {
            return this.trigger("remove_selection_item", [id]);
        }
    });    
})(jQuery);



