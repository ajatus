/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;

    $.ajatus.widgets.core.tags = {
        name: 'tags',
        settings: {
            separator: ' ',
            context_key: '',
            value: ''
        },
        assign_tags: null,
        _force_data_change: false,
        _missed_tag_arr: null,
        
        get_create_tpl: function(name, default_value) {
            if (this.assign_tags == null) {
                this.assign_tags = [];
            }
            
            var idx = 0;
            if (typeof default_value == 'object') {
                $.each(default_value, function(i,dv){
                    if (typeof dv.id != 'undefined') {
                        var tag = $.ajatus.tags.get(dv.id);
                        if (tag) {
                            this.assign_tags[idx] = tag;
                            idx += 1;
                        }
                    }
                });
            }
            if (typeof $.ajatus.tags.active == 'object') {
                this.assign_tags[idx] = $.ajatus.tags.active;
            }
            
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'text', name: name, value: '' }, ''
            ];
        },
        get_edit_tpl: function(name, data) {
            data.val = this.value_on_edit(data.val);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: $.ajatus.converter.toJSON(data.val) }, '',
                'input', { type: 'text', name: name, value: '' }, ''
            ];
        },
        get_view_tpl: function(name, data) {
            data.val = this.value_on_view(data.val);

            return [
                'div', { className: data._id+'_element_'+name+'_value' }, [
                    'ul', { className: 'tag_list' }, data.val,
                    'br', { className: 'clear_fix' }, ''
                ]
            ];
        },
        set_config: function(config) {
            this.settings = $.extend(this.settings, config);
        },
        validate: function(name, value) {            
            if (value != '') {                
                var tag_data = {
                    title: value
                };
                this._missed_tag_arr = this._update_system_tags([tag_data], []);
                
                this._force_data_change = true;
            }

            return true;
        },
        value_on_save: function(value, prev_val) {
            var tag_arr = [];
            var tag_ids = [];
            
            var id_inputs = $(':input[name*=__tags_widget_selections]');
            
            $.each(id_inputs, function(i,n){                
                tag_arr.push($.ajatus.converter.parseJSON(n.value))
            });
            
            if (   typeof prev_val == 'undefined'
                || !prev_val)
            {
                var prev_val = [];
            }

            $.each(tag_arr, function(i,t){
                if (typeof t.id != 'undefined') {
                    tag_ids.push(t.id);
                }
            });
            
            tag_arr = $.grep(tag_arr, function(n,i){
                return n != '' && (typeof n.id == 'undefined' || n.id == '');
            });
            
            var saved_tag_ids = this._update_system_tags(tag_arr, prev_val);
            
            $.each(saved_tag_ids, function(i,t){
                tag_ids.push(t);
            });
            
            if (this._missed_tag_arr != null) {
                $.each(this._missed_tag_arr, function(i,t){
                    tag_ids.push(t);
                });
            }
            
            return tag_ids;
        },
        has_data_changed: function(value, prev_objects) {            
            if (this._force_data_change) {
                return true;
            }
            
            var prev_value = [];
            $.each(prev_objects, function(i, n){
                prev_value.push(n.id);
            });
            
            var data_changed = false;
            
            if (   value.length == 0
                && prev_value.length == 0)
            {
                return false;
            }
            
            
            
            if (   value.length > 0
                && prev_value.length == 0)
            {
                return true;
            }
            if (   prev_value.length > 0
                && value.length == 0)
            {
                return true;
            }
            
            $.each(value, function(i,n){
                var match_key = $.inArray(n, prev_value);
                if (match_key == -1) {
                    data_changed = true;
                }
            });
            
            if (data_changed) {
                return true;
            }
            
            $.each(prev_value, function(i,n){
                var match_key = $.inArray(n, value);
                if (match_key == -1) {
                    data_changed = true;
                }
            });
            
            return data_changed;
        },
        value_on_edit: function(value) {            
            var self = this;
            if (self.assign_tags == null) {
                self.assign_tags = [];
            }
            $.each(value, function(i,t){                
                self.assign_tags[self.assign_tags.length || i] = $.ajatus.tags.get(t);
            });
            
            return self.assign_tags;
        },
        value_on_view: function(value, type) {
            if (typeof type == 'undefined') {
                var type = 'item';
            }
            
            var has_missing = false;
            
            var tags = [];
            var tags_str = '';
            var _self = this;
            if (value.length > 0) {
                $.each(value, function(i,v){
                    var tag = $.ajatus.tags.get(v);
                    if (tag == false) {
                        has_missing = true;
                        return;
                    }
                    var tag_title = $.ajatus.tags.render_title(tag);
                                        
                    if (type == 'plain') {
                        tags_str += tag_title + _self.settings.separator;
                    } else {
                        var tpl = _self.style_tag_on_view(tag_title, tag.value.title.widget.config.color, type);
                        $.each(tpl, function(tidx, tp){
                            tags.push(tp);
                        });                        
                    }
                });
            }
            
            if (   has_missing
                && type == 'item')
            {
                $.ajatus.elements.messages.create($.ajatus.i10n.get('Missing tags'), $.ajatus.i10n.get('Object has missing tags'));
            }
            
            if (type == 'plain') {
                return tags_str;
            } else {
                return tags;
            }
        },
        style_tag_on_view: function(title, color, type) {            
            if (   typeof color == 'undefined'
                || color == '')
            {
                var color = $.ajatus.widgets.core.tag.settings.color;
            }
            
            if (type == 'list') {
                return [
                    'span', { className: 'tag' }, title + ' '
                ];
            }
            
            return [
                'li', { className: 'tag', style: 'background-color: #'+color+';' }, title
            ];
        },
        loaded: function() {
            $.ajatus.events.lock_pool.increase();
            
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/widgets/tags.css');
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/widgets/includes/tags.css');
            
            var tags_url = $.ajatus.preferences.client.application_url + 'js/widgets/includes/tags.js';
            $.ajatus.utils.load_script(tags_url, "$.ajatus.events.lock_pool.decrease", []);
        },
        init: function(holder, form_mode) {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }
        },
        init_form_mode: function(holder) {
            var element = $('input[@type=text]', holder);
            
            if (this.assign_tags == null) {
                this.assign_tags = [];
            }
            
            if (element) {
                element.tags(this.settings.widget);

                $.each(this.assign_tags, function(i,tag) {
                    if (typeof tag == "object") {
                        element.tags_add_item(tag, true);
                    } else {
                        tag = $.ajatus.tags.get(tag);
                        if (tag) {
                            element.tags_add_item(tag, true);
                        }                        
                    }
                });
            }
        },
        init_view_mode: function(holder) {
        },
        _update_system_tags: function(new_tags, prev_tags) {
            var _self = this;
            var tag_ids = [];
            
            if (   !new_tags
                || new_tags.length <= 0)
            {
                return tag_ids;
            }
            
            var on_success = function(data) {
                if (data.ok) {
                    var msg = $.ajatus.elements.messages.create(
                        $.ajatus.i10n.get('New tag created'),
                        $.ajatus.i10n.get("Created new tag '%s'", [data.id])
                    );

                    $.ajatus.debug("saved tag: "+data.id);
                    tag_ids.push(data.id);
                }
                
                return data;
            };
            var dc = $.jqCouch.connection('doc', on_success);
            
            $.each(new_tags, function(i,d){                
                var title = d.title;
                var title_data = $.ajatus.tags.validate_title(title);
                
                var context_key = '';
                var value = '';
                if (typeof title_data == 'object') {
                    title = title_data.title;
                    context_key = title_data.context;
                    value = title_data.value;
                }
                
                var data = {
                    color: (typeof d.color != 'undefined') ? d.color : '8596b6',
                    context: (typeof d.context != 'undefined') ? d.context : context_key,
                    value: (typeof d.value != 'undefined') ? d.value : value
                };

                $.ajatus.tags.create(title, data, dc);
            });
            
            $.ajatus.views.on_change_actions.add('$.ajatus.tags.refresh_cache();');
            
            return tag_ids;
        }
    };

})(jQuery);