/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){    
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
    $.ajatus.widgets.core = typeof($.ajatus.widgets.core) == 'undefined' ? {} : $.ajatus.widgets.core;
    
    $.ajatus.widgets.core.text = {
        name: 'text',
        settings: {
            max_length: 0,
            width: 0,
            as_json: false,
            value_prefix: '',
            value_suffix: ''
        },
        
        get_create_tpl: function(name, default_value)
        {
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'span', { className: 'value_prefix' }, $.ajatus.i10n.get(this.settings.value_prefix),
                'input', { type: 'text', className: 'text', name: name, value: default_value }, '',
                'span', { className: 'value_suffix' }, $.ajatus.i10n.get(this.settings.value_suffix)
            ];
        },
        get_edit_tpl: function(name, data)
        {            
            data.val = this.value_on_edit(data.val);
            return [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: this.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(this.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':required]', value: this.required }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: data.val != '' ? $.ajatus.converter.toJSON(data.val): '' }, '',
                'span', { className: 'value_prefix' }, $.ajatus.i10n.get(this.settings.value_prefix),
                'input', { type: 'text', className: 'text', name: name, value: data.val }, '',
                'span', { className: 'value_suffix' }, $.ajatus.i10n.get(this.settings.value_suffix)
            ];
        },
        get_view_tpl: function(name, data)
        {
            data.val = this.value_on_view(data.val);
            return [
                'div', { className: data._id+'_element_'+name+'_value' }, data.val
            ];
        },
        set_config: function(config)
        {
            this.settings = $.extend(this.settings, config);
        },
        value_on_save: function(value)
        {
            return value;
        },
        value_on_edit: function(value)
        {
            return value;
        },
        value_on_view: function(value, type)
        {
            if (typeof type == 'undefined') {
                var type = 'item';
            }
            
            return $.ajatus.i10n.get(this.settings.value_prefix) + value.toString() + $.ajatus.i10n.get(this.settings.value_suffix);
        },
        loaded: function()
        {
        },
        init: function(holder, form_mode)
        {
            if (form_mode) {
                this.init_form_mode(holder);
            } else {
                this.init_view_mode(holder);                
            }            
        },
        init_form_mode: function(holder)
        {
            var self = this;
            
            var element = $('input.text', holder);
            element.css({
                width: self.settings.width > 0 ? self.settings.width : '99%'
            }).attr({
                maxlength: self.settings.max_length > 0 ? self.settings.max_length : null
            });
        },
        init_view_mode: function(holder)
        {
        },
        
        create_widget_details: function(data, name) {
            var details = $.ajatus.widgets.generate_default_details(this, data, name);

            return details;
        },
        create_widget_settings: function(data) {
            var settings = $.ajatus.widgets.generate_default_settings(this, data);
            
            return settings;
        }
    };
    
})(jQuery);