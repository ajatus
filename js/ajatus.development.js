/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.development = {
    };
    // $.extend($.ajatus.development, {
    // });
    
    $.ajatus.development.view = {
        title: 'Development',
        icon: 'development.png',
        options: {},
        application_view: true,
        history_support: true,
        has_additional_views: true,
        icongrid: null,
        
        generate_additional_views: function(add, icongrid) {
            var views = {};
            
            if (typeof add == 'undefined') {
                var add = false;
            }
            if (typeof icongrid == 'undefined') {
                var icongrid = true;
            }
            
            $.each($.ajatus.development.subview.available, function(i, name){
                var view = $.ajatus.development.subview[name];
                
                var vhash = '#view.development.'+name;
                if (typeof view.view_hash != 'undefined') {
                    vhash = view.view_hash;
                }
                
                var view_data = {
                    title: view.title,
                    view_hash: vhash
                };
                
                if (typeof view.icon != 'undefined') {
                    view_data.icon = view.icon;
                }
                
                views[name] = view_data;
            });
            
            if (add) {
                var app_tab_holder = $('#tabs-application ul');
                if (icongrid) {
                    $.ajatus.development.view.icongrid = new $.ajatus.renderer.icongrid({
                        title: 'Development tools',
                        width: 50,
                        attach: false
                    });
                }
                
                app_tab_holder.html('');
                $.each(views, function(name, data){                        
                    var tab = $('<li><a href="'+data.view_hash+'"><span>'+$.ajatus.i10n.get(data.title)+'</span></a></li>');
                    if (typeof data.icon != 'undefined') {
                        tab = $('<li class="iconified"><a href="'+data.view_hash+'" title="'+$.ajatus.i10n.get(data.title)+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + data.icon + '" /></a></li>');
                        
                        if (icongrid) {
                            $.ajatus.development.view.icongrid.add_item({
                                title: $.ajatus.i10n.get(data.title),
                                icon: data.icon,
                                hash: data.view_hash
                            });
                        }
                    }
                    tab.appendTo(app_tab_holder);
                });
            }
            
            return views;
        },
        
        render: function(view) {            
            if (   typeof view == 'undefined'
                || view == 'frontpage')
            {
                view = 'frontpage';
                $.ajatus.development.view.generate_additional_views(true);
            } else {
                $.ajatus.development.view.generate_additional_views(false);
            }
            
            if (   view != 'frontpage'
                && typeof $.ajatus.development.subview[view] != 'undefined')
            {
                return $.ajatus.development.subview[view].render();
            }
            
            $.ajatus.application_content_area.html('');
            
            var body_class = 'development';
            var view_hash = '#view.development';
            var view_title = $.ajatus.i10n.get('Development');
            
            $.ajatus.layout.title.update({
                view: view_title
            });            
            $.ajatus.layout.body.set_class(body_class);     
            $.ajatus.views.on_change(view_hash, true);

            $.ajatus.toolbar.init();
            
            if ($.ajatus.development.view.icongrid != null) {
                $.ajatus.development.view.icongrid.render();
            }
            
            // if (view == 'frontpage') {
            //     $.ajatus.views.system.archive.get_items();
            // } else {
            //     $.ajatus.views.system.archive.get_view_items(view);
            // }
        }
    };
    
    $.ajatus.development.subview = {
        base_class: 'development',
        available: [
            'generator', 'templating'
        ]
    };

    $.ajatus.development.subview.generator = {
        title: 'Object generator',
        view_hash: '#view.development.generator',
        icon: 'generator.png',
        
        render: function() {
            $.ajatus.application_content_area.html('');

            var body_class = $.ajatus.development.subview.base_class + ' generator';
            var view_title = $.ajatus.i10n.get('Development') + ' - ' + $.ajatus.i10n.get($.ajatus.development.subview.generator.title);
            
            $.ajatus.layout.title.update({
                view: view_title
            });            
            $.ajatus.layout.body.set_class(body_class);
            $.ajatus.views.on_change($.ajatus.development.subview.generator.view_hash, true);

            $.ajatus.toolbar.init();
            
            
        }
    };
    
    $.ajatus.development.subview.templating = {
        title: 'Templating',
        view_hash: '#view.development.templating',
        //icon: 'templating.png',
        
        render: function() {
            $.ajatus.application_content_area.html('');

            var body_class = $.ajatus.development.subview.base_class + ' templating';
            var view_title = $.ajatus.i10n.get('Development') + ' - ' + $.ajatus.i10n.get($.ajatus.development.subview.templating.title);
            
            $.ajatus.layout.title.update({
                view: view_title
            });            
            $.ajatus.layout.body.set_class(body_class);
            $.ajatus.views.on_change($.ajatus.development.subview.templating.view_hash, true);

            $.ajatus.toolbar.init();

            var type = $.ajatus.preferences.client.content_types['note'];
            
            var renderer = new $.ajatus.renderer.list_tpl(
                $.ajatus.application_content_area,
                type,
                {
                    id: type.name + '_list_holder',
                    pool: {
                        enabled: true,
                        settings: {
                            type: 'scroll',
                            use_db: true,
                            db: {
                                'static': type.name+'/list'
                            }
                        }
                    }
                }
            );
            
            var doc_count = $.jqCouch.connection('view').get($.ajatus.preferences.client.content_database, type.name+'/list', {
                count: 0
            }).total_rows;
            
            var on_success = function(data) {
                $.each(data.rows, function(i,doc){
                    var doc = new $.ajatus.document(doc);
                    renderer.add_item(doc);
                });
                
                renderer.add_total_count(doc_count);
                
                return data;
            };
            
            $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, type.name+'/list', {
                count: 40,
                descending: true
            });
            
            renderer.render();
        }
    };
    
})(jQuery);