/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.views = $.ajatus.views || {};
    
    $.ajatus.views.system = {
        available: [
            'frontpage', 'edit', 'create', 'item', 'tags', 'trash', 'archive'
        ]
    };
    $.ajatus.views.system.frontpage = {
        title: 'Frontpage',
        options: {
            group_item_count: 3,
            max_items_before_pool: 3
        },
        in_tabs: true,
        history_support: true,
        
        pool_settings: {
            enabled: false,
            settings: {
                tick_limit: 4
            }
        },
                
        tab: {
            on_click: function(e)
            {
                $.ajatus.tabs.on_click(e);
                $.ajatus.views.system.frontpage.render();
            }
        },
    
        render: function()
        {
            $.ajatus.application_content_area.html('');
            $.ajatus.layout.body.set_class('frontpage');
            $.ajatus.views.on_change("#view.frontpage");

            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.get('Frontpage')
            });
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (   typeof type.views['list'] == 'undefined'
                    || type.on_frontpage == false) {
                    return false;
                }
            
                var on_success = function(data) {
                    if (data.total_rows == 0) {
                        return;
                    }
                    
                    var pool_settings = $.ajatus.views.system.frontpage.pool_settings;
                    if (data.total_rows >= $.ajatus.views.system.frontpage.options.max_items_before_pool) {
                        pool_settings.enabled = true;
                    }                    
                    
                    var renderer = new $.ajatus.renderer.list(
                        $.ajatus.application_content_area,
                        type,
                        {
                            id: type.name + '_list_holder',
                            append: true,
                            pool: pool_settings
                        }
                    );

                    $.each(data.rows, function(i,doc){
                        var doc = new $.ajatus.document(doc);
                        renderer.render_item(doc, i);
                    });
                    
                    return data;
                };

                if (   $.ajatus.tags.active != ''
                    && typeof type.views['list_at'] != 'undefined')
                {
                    $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, type.views['list_at'], {
                        count: $.ajatus.views.system.frontpage.options.group_item_count,
                        descending: true
                    });
                } else {
                    $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, type.name+'/list', {
                        count: $.ajatus.views.system.frontpage.options.group_item_count,
                        descending: true
                    });                        
                }
            });
        }
    };
    $.ajatus.views.system.tags = {
        title: 'Tags',
        options: {            
        },
        in_tabs: true,
        application_view: true,
        history_support: true,
        icon: 'tag.png',
                
        tab: {
            on_click: function(e) {
                $.ajatus.tabs.on_click(e);
                $.ajatus.views.system.tags.render();
            }
        },
        
        render: function() {
            $.ajatus.application_content_area.html('');
            $.ajatus.layout.body.set_class('tags');
            $.ajatus.views.on_change("#view.tags");
            
            $.ajatus.toolbar.init();

            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.plural($.ajatus.i10n.get('Tag'))
            });
  
            $.ajatus.views.system.tags.get_items();
        },

        render_results: function(tags, items, total_items) {
            var type = $.ajatus.preferences.client.content_types['tag'];

            $.ajatus.toolbar.add_item($.ajatus.i10n.get('New %s', [type.name]), {
                icon: type.name+'-new.png',
                action: function(){
                    $.ajatus.views.system.create.render(type);
                },
                access_key: 'Ctrl+n'
            });
            $.ajatus.toolbar.show();
            
            var renderer = new $.ajatus.renderer.list(
                $.ajatus.application_content_area,
                type,
                {
                    id: 'tags_list_holder',
                    headers: [
                        'title', 'context', 'value', 'created', 'creator', 'used'
                    ],
                    pool: {
                        type: 'scroll'
                    }
                }
            );
            
            var x = 0;
            $.each(tags, function(i,doc){
                var doc = new $.ajatus.document(doc);
                doc.value['used'] = {
                    val: (typeof items[doc._id] != 'undefined' ? items[doc._id].length : 0)
                };
                renderer.render_item(doc, x);
                x+=1;
            });

            renderer.items_added(total_items);            
            renderer.enable_sorting();
            
            $.ajatus.views.system.tags.get_duplicates(items);
        },
        
        render_duplicates: function(rows, items) {
            var type = $.ajatus.preferences.client.content_types['tag'];
            
            var duplicates = [];
            var duplicate_ids = [];
            
            var tags = {};
            
            var hashes = [];
            var real_hashes = [];
            var false_hashes = {};
            var broken_hashes = {};
            var ids = [];
            
            var real_hashes_cnt = 0;
            var false_hashes_cnt = 0;
            
            $.each (rows, function(i,doc){
                var doc = new $.ajatus.document(doc);
                
                var tag_hash = $.ajatus.utils.md5.encode(doc.value.context.val + ':' + doc.key + '=' + doc.value.value.val);
                
                hashes.push(tag_hash);
                ids.push(doc._id);

                if (tag_hash == doc._id) {
                    real_hashes.push(tag_hash);
                    real_hashes_cnt += 1;
                } else {
                    if (typeof false_hashes[tag_hash] == 'undefined') {
                        false_hashes[tag_hash] = [doc._id];
                        false_hashes_cnt += 1;
                    } else {
                        false_hashes[tag_hash].push(doc._id);
                        false_hashes_cnt += 1;
                    }
                }

                tags[doc._id] = doc;
            });
            
            var duplicates_with = {};            
            var broken_hashes_cnt = 0;
            var dublicate_cnt = 0;
            
            $.each(false_hashes, function(hash,hids){
                if (typeof duplicates_with[hash] == 'undefined') {
                    duplicates_with[hash] = [];
                }
                
                if (hids.length > 1) {
                    if ($.inArray(hash, real_hashes) != -1) {
                        duplicates_with[hash] = hids;
                        dublicate_cnt += 1;
                    } else {
                        broken_hashes[hash] = hids;
                        broken_hashes_cnt += 1;
                    }
                } else {
                    if ($.inArray(hash, real_hashes) != -1) {
                        duplicates_with[hash].push(hids[0]);
                        dublicate_cnt += 1;
                    } else {
                        broken_hashes[hash] = hids;
                        broken_hashes_cnt += 1;
                    }
                }
            });
            
            if (dublicate_cnt > 0) {
                var drenderer = new $.ajatus.renderer.list(
                    $.ajatus.application_content_area,
                    type,
                    {
                        id: 'tag_dups_list_holder',
                        title: $.ajatus.i10n.get("Duplicate"),
                        headers: [
                            'title', 'context', 'value', 'creator', 'used'
                        ],
                        append: true,
                        item_id_suffix: '_duplicate',
                        show_actions: false,
                        className: 'list_holder duplicates_list'
                    }
                );

                var dub_tags = [];
                var x = 0;            
                $.each(duplicates_with, function(hash, dids){
                    for (var i=0; i<dids.length; i++) {
                        if (typeof tags[dids[i]] != 'undefined') {
                            var d = tags[dids[i]];
                            d.value['used'] = {
                                val: (typeof items[d._id] != 'undefined' ? items[d._id].length : 0)
                            };
                            d.value['real_hash'] = {
                                val: hash
                            };
                            drenderer.render_item(d, x);
                            dub_tags.push(d);
                            x+=1;
                        } else {
                            $.ajatus.debug("Couldn't find tag with id: "+dids[i]);
                        }
                    }
                });
                drenderer.enable_sorting();

                var merger = new $.ajatus.tags.merger(dub_tags, duplicates_with, tags);                
            }
            
            if (broken_hashes_cnt > 0) {
                var brenderer = new $.ajatus.renderer.list(
                    $.ajatus.application_content_area,
                    type,
                    {
                        id: 'tag_broken_list_holder',
                        title: $.ajatus.i10n.get("Broken"),
                        plural_title: false,
                        headers: [
                            'title', 'context', 'value', 'curr_hash', 'real_hash', 'used'
                        ],
                        schema: {
                            'curr_hash': {
                                label: 'Current hash'
                            },
                            'real_hash': {
                                label: 'Real hash'
                            }
                        },
                        append: true,
                        item_id_suffix: '_broken',
                        show_actions: false,
                        className: 'list_holder broken_list'
                    }
                );
                
                var broken_tags = {};
                var x = 0;
                $.each(broken_hashes, function(hash, dids){
                    for (var i=0; i<dids.length; i++) {
                        if (typeof tags[dids[i]] != 'undefined') {
                            var d = tags[dids[i]];
                            d.value['used'] = {
                                val: (typeof items[d._id] != 'undefined' ? items[d._id].length : 0)
                            };
                            d.value['curr_hash'] = {
                                val: d._id
                            };
                            d.value['real_hash'] = {
                                val: hash
                            };
                            brenderer.render_item(d, x);
                            broken_tags[d._id] = d;
                            x+=1;
                        } else {
                            $.ajatus.debug("Couldn't find tag with id: "+dids[i]);
                        }
                    }
                });
                brenderer.enable_sorting();

                var fixer = new $.ajatus.tags.fixer(broken_tags, broken_hashes, tags, {
                    show_toolbar: true,
                    refresh_on_finish: true
                });                
            }
        },
        
        get_items: function(on_success) {
            var conn_on_success = function(data) {                
                if (data.total_rows == 0) {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get('tag'));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }
                
                var tags = {};
                var items = {};
                
                $.each(data.rows, function(i,n){
                    if (n.key[1] == 0) {
                        tags[n.key[0]] = n;
                    } else {
                        if (typeof items[n.key[0]] == 'undefined') {
                            items[n.key[0]] = [];
                        }
                        items[n.key[0]].push(n);
                    }
                });

                if (typeof on_success == 'undefined') {
                    on_success = $.ajatus.views.system.tags.render_results;
                }
                
                on_success(tags, items, data.total_rows);
                
                return data;
            };

            $.jqCouch.connection('view', conn_on_success).get($.ajatus.preferences.client.content_database, 'tag/list_with_docs', {
                // startkey: ["ED862081B2FBF46FB0CDBBE848ED6856", 0],
                // endkey: ["ED862081B2FBF46FB0CDBBE848ED6856", 1]
            });
        },
        
        get_duplicates: function(items) {
            var conn_on_success = function(data) {                
                if (data.total_rows == 0) {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get('dublicate'));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }

                if (typeof on_success == 'undefined') {
                    on_success = $.ajatus.views.system.tags.render_duplicates;
                }
                
                on_success(data.rows, items);
                
                return data;
            };

            $.jqCouch.connection('view', conn_on_success).get($.ajatus.preferences.client.content_database, 'tag/list_by_title', {
            });
        }

    };
    $.ajatus.views.system.trash = {
        title: 'Trash',
        options: {
            group_item_count: 3
        },
        in_tabs: true,
        application_view: true,
        history_support: true,
        icon: 'trash.png',
        
        tab: {
            on_click: function(e)
            {
                $.ajatus.tabs.on_click(e);
                $.ajatus.views.system.trash.render();
            }
        },
        
        get_items: function(on_success, limit) {
            var trash_view = 'if (doc.value.metadata.deleted.val == true) {';
            trash_view += 'map( doc.value.metadata.revised, {"_type": doc.value._type,';
            trash_view += '"title": doc.value.title,';
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (type['title_item']) {
                    $.each(type['title_item'], function(i,part){
                        if (type.schema[part]) {
                            trash_view += '"'+part+'": doc.value.'+part+',';
                        }
                    });
                }
            });

            trash_view += '"revised": doc.value.metadata.revised,"by": doc.value.metadata.revisor,"tags": doc.value.tags})}';
            
            var conn_on_success = function(data) {
                if (   data.total_rows == 0
                    || data.rows.length == 0)
                {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get('deleted item'));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }
                
                if (typeof on_success == 'undefined') {
                    on_success = $.ajatus.views.system.trash.render_results;
                }
                
                on_success(data.rows);
                return data;
            };
            
            $.jqCouch.connection('view', conn_on_success).temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+trash_view+"')", {
                descending: true
            });
        },
        
        render: function() {
            $.ajatus.application_content_area.html('');
            $.ajatus.layout.body.set_class('trash');
            $.ajatus.views.on_change("#view.trash");

            $.ajatus.toolbar.init();
            
            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.get('Trash')
            });
            
            $.ajatus.views.system.trash.get_items();
        },
        
        render_results: function(rows) {
            var result_count = {};
            var show_empty_trash = false;
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (!type.views['list']) {
                    return false;
                }
                
                var trash_type_count_view = 'if (doc.value.metadata.deleted.val == true && doc.value._type == "'+type.name+'") {';
                trash_type_count_view += 'map( doc._id, {"_type": doc.value._type});}';
                
                total_trashed_count = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+trash_type_count_view+"')", {
                    count: 0
                }).total_rows;
                
                result_count[type.name] = 0;
                
                $.each(rows, function(i,doc){
                    if (doc.value._type == type.name) {
                        result_count[type.name] += 1;
                    }
                });
                
                if (result_count[type.name] == 0) {
                    return;
                }
                
                show_empty_trash = true;
                
                var renderer = new $.ajatus.renderer.list(
                    $.ajatus.application_content_area,
                    false,
                    {
                        id: type.name + '_list_holder',
                        append: true,
                        title: type.title,
                        title_suffix: '('+total_trashed_count+')',
                        headers: [
                            'title', 'revised', 'by'
                        ],
                        schema: $.extend({
                            by: {
                                label: 'By'
                            }
                        }, type.schema)
                    }
                );
                
                var rendered_count = 0;
                $.each(rows, function(i,doc){
                    if (   doc.value._type == type.name
                        && rendered_count < $.ajatus.views.system.trash.options.group_item_count)
                    {
                        rendered_count += 1;
                        var doc = new $.ajatus.document(doc);
                        
                        renderer.set_actions(
                            [
                                {
                                    name: 'view',
                                    title: $.ajatus.i10n.get('View'),
                                    icon: 'view.png',
                                    click_action: '$.ajatus.views.system.item.render(doc);'
                                },
                                {
                                    name: 'undelete',
                                    title: $.ajatus.i10n.get('Undelete'),
                                    icon: 'undo.png',
                                    click_action: '$.ajatus.document.actions.execute("undelete", doc);'
                                },
                                {
                                    name: 'delete_final',
                                    title: $.ajatus.i10n.get('Delete'),
                                    icon: 'trash.png',
                                    click_action: '$.ajatus.document.actions.execute("delete_final", doc);'
                                }
                            ]
                        );
                        renderer.render_item(doc, i);
                    }
                });
            
                renderer.enable_sorting();
            });
            
            if (show_empty_trash) {
                $.ajatus.toolbar.add_item($.ajatus.i10n.get('Empty trash'), 'trash.png', function(){
                    $.ajatus.views.system.trash.empty();
                });
                $.ajatus.toolbar.show();
            }
        },
        empty: function() {
            var answer = confirm($.ajatus.i10n.get("Deleting all items in trash. Are you sure?"));
            if (answer) {
                $.ajatus.views.system.trash.get_items($.ajatus.views.system.trash.execute_empty);
            }
        },
        execute_empty: function(rows) {            
            $.each(rows, function(i,doc){
                doc = new $.ajatus.document(doc);
                $.ajatus.document.actions.execute("delete_final", doc);
            });
        }
    };
    $.ajatus.views.system.archive = {
        title: 'Archive',
        options: {
            group_item_count: 3
        },
        in_tabs: true,
        application_view: true,
        history_support: true,
        icon: 'archive.png',
        has_additional_views: true,
        
        tab: {
            on_click: function(e)
            {
                $.ajatus.tabs.on_click(e);
                $.ajatus.views.system.trash.render();
            }
        },
        
        generate_additional_views: function(add) {
            var views = {};
            
            if (typeof add == 'undefined') {
                var add = false;
            }
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                var view = {
                    title: type.title,
                    view_hash: '#view.archive.'+type.name,
                    icon: type.name+'.png'
                };
                
                views[type.name] = view;
            });
            
            if (add) {
                var app_tab_holder = $('#tabs-application ul');
                
                app_tab_holder.html('');
                $.each(views, function(name, data){                        
                    var tab = $('<li><a href="'+data.view_hash+'"><span>'+$.ajatus.i10n.get(data.title)+'</span></a></li>');
                    if (typeof data.icon != 'undefined') {
                        tab = $('<li class="iconified"><a href="'+data.view_hash+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + data.icon + '" alt="'+$.ajatus.i10n.get(data.title)+'"/></a></li>');
                    }
                    tab.appendTo(app_tab_holder);

                    $.ajatus.history.add_map(data.view_hash, '$.ajatus.history.update("'+data.view_hash+'");$.ajatus.views.system.archive.render("'+name+'");');
                });                
            }
            
            return views;
        },
        
        render: function(view) {
            if (   typeof view == 'undefined'
                || view == 'list')
            {
                $.ajatus.views.system.archive.generate_additional_views(true);
                var view = 'summary';
            }
            
            $.ajatus.application_content_area.html('');
            
            var body_class = 'archive';
            var view_hash = '#view.archive';
            var view_title = $.ajatus.i10n.get('Archive');
            
            if (view != 'summary') {
                body_class += ' list_' + view;
                view_hash += '.' + view;
                view_title += ' - ' + $.ajatus.i10n.plural($.ajatus.i10n.get($.ajatus.preferences.client.content_types[view].title));
            }
            
            $.ajatus.layout.title.update({
                view: view_title
            });
            
            $.ajatus.layout.body.set_class(body_class);            
            $.ajatus.views.on_change(view_hash, true);

            $.ajatus.toolbar.init();
            
            if (view == 'summary') {
                $.ajatus.views.system.archive.get_items();
            } else {
                $.ajatus.views.system.archive.get_view_items(view);
            }            
        },
        
        get_items: function() {
            var archive_view = 'if (doc.value.metadata.deleted.val == false && doc.value.metadata.archived.val == true) {';
            archive_view += 'map( doc.value.metadata.revised, {"_type": doc.value._type,';
            archive_view += '"title": doc.value.title,';
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (type['title_item']) {
                    $.each(type['title_item'], function(i,part){
                        if (type.schema[part]) {
                            archive_view += '"'+part+'": doc.value.'+part+',';
                        }
                    });
                }
            });

            archive_view += '"revised": doc.value.metadata.revised,"by": doc.value.metadata.revisor,"tags": doc.value.tags})}';
            
            var conn_on_success = function(data) {
                if (   data.total_rows == 0
                    || data.rows.length == 0)
                {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get('archived item'));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }
                
                $.ajatus.views.system.archive.render_summary(data.rows);
                
                return data;
            };
            
            $.jqCouch.connection('view', conn_on_success).temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+archive_view+"')", {
                descending: true
            });
        },
        
        render_summary: function(rows) {
            var result_count = {};
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (!type.views['list']) {
                    return false;
                }
                
                var archive_type_count_view = 'if (doc.value.metadata.deleted.val == false && doc.value.metadata.archived.val == true && doc.value._type == "'+type.name+'") {';
                archive_type_count_view += 'map( doc._id, {"_type": doc.value._type});}';
                
                total_archived_count = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+archive_type_count_view+"')", {
                    count: 0
                }).total_rows;
                
                result_count[type.name] = 0;
                
                $.each(rows, function(i,doc){
                    if (doc.value._type == type.name) {
                        result_count[type.name] += 1;
                    }
                });
                
                if (result_count[type.name] == 0) {
                    return;
                }
                
                var renderer = new $.ajatus.renderer.list(
                    $.ajatus.application_content_area,
                    false,
                    {
                        id: type.name + '_list_holder',
                        append: true,
                        title: type.title,
                        title_suffix: '('+total_archived_count+')',
                        headers: [
                            'title', 'revised', 'by'
                        ],
                        schema: $.extend({
                            by: {
                                label: 'By'
                            }
                        }, type.schema)
                    }
                );
                
                var rendered_count = 0;
                $.each(rows, function(i,doc){
                    if (   doc.value._type == type.name
                        && rendered_count < $.ajatus.views.system.archive.options.group_item_count)
                    {
                        rendered_count += 1;
                        var doc = new $.ajatus.document(doc);
                        
                        renderer.set_actions(
                            [
                                {
                                    name: 'view',
                                    title: $.ajatus.i10n.get('View'),
                                    icon: 'view.png',
                                    click_action: '$.ajatus.views.system.item.render(doc);'
                                },
                                {
                                    name: 'unarchive',
                                    title: $.ajatus.i10n.get('Unarchive'),
                                    icon: 'undo.png',
                                    click_action: '$.ajatus.document.actions.execute("unarchive", doc);'
                                }
                            ]
                        );
                        renderer.render_item(doc, i);
                    }
                });
            
                renderer.enable_sorting();
            });
        },
        
        get_view_items: function(view) {            
            var type = $.ajatus.preferences.client.content_types[view];
            
            var archive_view = 'if (doc.value.metadata.deleted.val == false && doc.value.metadata.archived.val == true && doc.value._type == "'+type.name+'") {';
            archive_view += 'map( doc.value.metadata.revised, {"_type": doc.value._type,';
            archive_view += '"title": doc.value.title,';
            
            if (type['title_item']) {
                $.each(type['title_item'], function(i,part){
                    if (type.schema[part]) {
                        archive_view += '"'+part+'": doc.value.'+part+',';
                    }
                });
            }

            archive_view += '"revised": doc.value.metadata.revised,"by": doc.value.metadata.revisor,"tags": doc.value.tags})}';
            
            var conn_on_success = function(data) {
                if (data.total_rows == 0) {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get('archived %s', [type.title]));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }
                
                $.ajatus.views.system.archive.render_items(type, data.rows);
                
                return data;
            };
            
            $.jqCouch.connection('view', conn_on_success).temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+archive_view+"')", {
                descending: true
            });
        },
        
        render_items: function(type, rows) {
            if (! type.views['list']) {
                return false;
            }
            
            if (rows.total_rows == 0) {
                return;
            }
            
            var renderer = new $.ajatus.renderer.list(
                $.ajatus.application_content_area,
                false,
                {
                    id: type.name + '_list_holder',
                    title: type.title,
                    headers: [
                        'title', 'revised', 'by'
                    ],
                    schema: $.extend({
                        by: {
                            label: 'By'
                        }
                    }, type.schema)
                }
            );

            renderer.set_actions(
                [
                    {
                        name: 'view',
                        title: $.ajatus.i10n.get('View'),
                        icon: 'view.png',
                        click_action: '$.ajatus.views.system.item.render(doc);'
                    },
                    {
                        name: 'unarchive',
                        title: $.ajatus.i10n.get('Unarchive'),
                        icon: 'undo.png',
                        click_action: '$.ajatus.document.actions.execute("unarchive", doc);'
                    }
                ]
            );

            $.each(rows, function(i,doc){
                if (doc.value._type == type.name) {
                    var doc = new $.ajatus.document(doc);
                    
                    renderer.render_item(doc, i);
                }
            });
        
            renderer.enable_sorting();
        }
    };
    $.ajatus.views.system.edit = {
        title: 'Edit',
        options: {
        },
        in_tabs: false,
        dynamic_history: true
    };
    $.extend($.ajatus.views.system.edit, {
        render: function(type, doc, use_given_doc)
        {
            if (typeof use_given_doc == 'undefined') {
                use_given_doc = false;
            }
            
            var content_type = type;
                        
            if (! use_given_doc) {
                var doc = new $.ajatus.document.loader(doc._id, doc._rev);                
            }
            
            if (! type) {
                content_type = $.ajatus.preferences.client.content_types[doc.value._type];
            }
                        
            if (typeof(type) == 'string') {
                content_type = $.ajatus.preferences.client.content_types[type];
            }
            
            $.ajatus.layout.body.set_class('edit '+content_type.name);

            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.get('Edit %s', [content_type.title])
            });
            
            var view_hash = '#edit.'+doc.value._type+'.'+doc._id+'.'+doc._rev;
            $.ajatus.history.update(view_hash);
            $.ajatus.views.on_change(view_hash);
            
            var latest_rev = $.ajatus.document.revisions.get(doc).active.rev;
            
            if (   content_type.custom_renderer
                && typeof(content_type.custom_renderer['edit']) == 'function')
            {
                var renderer = content_type.custom_renderer.edit(doc, latest_rev);
            } else {            
                var renderer = new $.ajatus.renderer.form(content_type, doc, false, latest_rev);
                $.ajatus.application_content_area.html(renderer.form);
                $.ajatus.forms.register.normal(renderer.form, 'edit');
            }
        },
        history_register: function() {
            $.ajatus.history.add_dynamic_map(['edit'], 3, '$.ajatus.views.system.edit.history_render');
        },
        history_render: function() {
            var type = arguments[0];
            var id = arguments[1];
            var rev = false;

            if (   typeof arguments[2] != 'undefined'
                && arguments[2] != '')
            {
                rev = arguments[2];
            }
            
            var doc = new $.ajatus.document.loader(id, rev);
            $.ajatus.views.system.edit.render(type, doc);
        }
    });
    
    $.ajatus.views.system.create = {
        title: 'Create',
        options: {
        },
        in_tabs: false,
        dynamic_history: true
    };
    $.extend($.ajatus.views.system.create, {
        render: function(type, doc)
        {
            var content_type = type; 
            
            if (typeof(type) == 'string') {
                content_type = $.ajatus.preferences.client.content_types[type];
            }
            
            $.ajatus.layout.body.set_class('create '+content_type.name);

            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.get('Create %s', [content_type.title])
            });
            
            var view_hash = '#create.'+content_type.name;
            
            $.ajatus.history.update(view_hash);
            
            $.ajatus.views.on_change(view_hash);
            
            if (   content_type.custom_renderer
                && typeof(content_type.custom_renderer['create']) == 'function')
            {
                var renderer = content_type.custom_renderer.create();
            } else {
                var renderer = new $.ajatus.renderer.form(content_type, doc, true);
                $.ajatus.application_content_area.html(renderer.form);
                $.ajatus.forms.register.normal(renderer.form, 'create');
            }
        },
        history_register: function() {
            $.ajatus.history.add_dynamic_map(['create'], 2, '$.ajatus.views.system.create.history_render');
        },
        history_render: function() {            
            var type = arguments[0];
                        
            $.ajatus.views.system.create.render(type);
        }
    });

    $.ajatus.views.system.item = {
        title: 'View item',
        options: {
        },
        in_tabs: false,
        dynamic_history: true
    };
    $.extend($.ajatus.views.system.item, {
        render: function(doc)
        {            
            if (typeof doc._rev != 'undefined') {
                var doc = new $.ajatus.document.loader(doc._id, doc._rev);
            } else {
                var doc = new $.ajatus.document.loader(doc._id);
            }
            
            var content_type = $.ajatus.preferences.client.content_types[doc.value._type];

            $.ajatus.layout.title.update({
                view: $.ajatus.i10n.get('View %s', [content_type.title])
            });
            
            $.ajatus.layout.body.set_class('view '+doc.value._type);
            
            var view_hash = '#view.'+doc.value._type+'.'+doc._id+'.'+doc._rev;
            $.ajatus.history.update(view_hash);
            $.ajatus.views.on_change(view_hash);
            
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Edit'), {
                icon: 'edit.png',
                action: function(){
                    $.ajatus.views.system.edit.render(content_type, doc);
                },
                access_key: 'Ctrl+e'
            });

            if (doc.value.metadata.archived.val) {
                var unarch_id = $.ajatus.toolbar.add_item($.ajatus.i10n.get('Restore'), 'undo.png', function(item, doc){
                    doc._rev = null;
                    $.ajatus.document.actions.execute("unarchive", doc);
                    
                    $.ajatus.toolbar.hide_item(item.id);
                    $.ajatus.toolbar.add_item($.ajatus.i10n.get('Archive'), 'archive.png', function(item, doc, unarch_item_id){
                        doc._rev = null;
                        $.ajatus.document.actions.execute("archive", doc);
                        
                        $.ajatus.toolbar.hide_item(item.id);
                        $.ajatus.toolbar.show_item(unarch_item_id);
                    }, [doc, unarch_id], unarch_id);
                }, [doc]);
            } else {
                var arch_id = $.ajatus.toolbar.add_item($.ajatus.i10n.get('Archive'), 'archive.png', function(item, doc){
                    doc._rev = null;
                    $.ajatus.document.actions.execute("archive", doc);
                    
                    $.ajatus.toolbar.hide_item(item.id);
                    $.ajatus.toolbar.add_item($.ajatus.i10n.get('Restore'), 'undo.png', function(item, doc, arch_item_id){
                        doc._rev = null;
                        $.ajatus.document.actions.execute("unarchive", doc);
                        
                        $.ajatus.toolbar.hide_item(item.id);
                        $.ajatus.toolbar.show_item(arch_item_id);
                    }, [doc, arch_id], arch_id);
                }, [doc]);
            }
            
            if (doc.value.metadata.deleted.val) {
                var undel_id = $.ajatus.toolbar.add_item($.ajatus.i10n.get('Undelete'), 'undo.png', function(item, doc){
                    doc._rev = null;
                    $.ajatus.document.actions.execute("undelete", doc);
                    
                    var d = doc;
                    $.ajatus.toolbar.hide_item(item.id);
                    $.ajatus.toolbar.add_item($.ajatus.i10n.get('Delete'), 'trash.png', function(item, doc, undel_item_id){
                        doc._rev = null;
                        $.ajatus.document.actions.execute("delete", doc);
                        
                        $.ajatus.toolbar.hide_item(item.id);
                        $.ajatus.toolbar.show_item(undel_item_id);
                    }, [d, undel_id], undel_id);
                }, [doc]);                
            } else {                
                var del_id = $.ajatus.toolbar.add_item($.ajatus.i10n.get('Delete'), 'trash.png', function(item, doc){
                    doc._rev = null;
                    $.ajatus.document.actions.execute("delete", doc);

                    var d = doc;                    
                    $.ajatus.toolbar.hide_item(item.id);
                    $.ajatus.toolbar.add_item($.ajatus.i10n.get('Undelete'), 'undo.png', function(item, doc, del_item_id){
                        doc._rev = null;
                        $.ajatus.document.actions.execute("undelete", doc);
                        
                        $.ajatus.toolbar.hide_item(item.id);
                        $.ajatus.toolbar.show_item(del_item_id);
                    }, [d, del_id], del_id);
                }, [doc]);
            }

            $.ajatus.toolbar.show();
            
            var metadata_sb = new $.ajatus.renderer.blocks.sub('metadata_for_'+doc._id);
            metadata_sb.set_title($.ajatus.i10n.get('Metadata'));
            $.ajatus.views.on_change_actions.add('$.ajatus.renderer.blocks.clear_sub("'+metadata_sb.id+'");');
            
            var md_renderer = new $.ajatus.renderer.metadata(doc, metadata_sb.element);            

            if (   typeof doc.value.tags != 'undefined'
                && doc.value.tags.val.length > 0)
            {
                var rel_docs = $.ajatus.tags.related(doc.value.tags.val, [doc._id]);

                var related_sb = new $.ajatus.renderer.blocks.sub('related_objects');
                related_sb.set_title($.ajatus.i10n.get('Related Objects'));
                $.ajatus.views.on_change_actions.add('$.ajatus.renderer.blocks.clear_sub("'+related_sb.id+'");');

                var tinylist = new $.ajatus.renderer.tinylist(related_sb.element);
                tinylist.render_items(rel_docs);
            } else {
                if (content_type.name == 'tag') {
                    var rel_docs = $.ajatus.tags.related([doc._id]);
                    
                    var related_sb = new $.ajatus.renderer.blocks.sub('related_objects');
                    related_sb.set_title($.ajatus.i10n.get('Related Objects'));
                    $.ajatus.views.on_change_actions.add('$.ajatus.renderer.blocks.clear_sub("'+related_sb.id+'");');
                    
                    var tinylist = new $.ajatus.renderer.tinylist(related_sb.element);
                    tinylist.render_items(rel_docs);
                }
            }
            
            if (   content_type.custom_renderer
                && typeof(content_type.custom_renderer['item']) == 'function')
            {
                var renderer = content_type.custom_renderer.item();
            } else {
                var renderer = new $.ajatus.renderer.item(doc);
            }
        },
        history_register: function() {
            $.ajatus.history.add_dynamic_map(['view'], 3, '$.ajatus.views.system.item.history_render');
        },
        history_render: function() {
            var type = arguments[0];
            var id = arguments[1];
            var rev = false;

            if (   typeof arguments[2] != 'undefined'
                && arguments[2] != '')
            {
                rev = arguments[2];
            }

            if (rev) {
                var data = {
                    _id: id,
                    _rev: rev,
                    value: {
                        _type: type
                    }
                };                
            } else {
                var data = {
                    _id: id,
                    value: {
                        _type: type
                    }
                };
            }

            if (typeof data['toSource'] != 'undefined') {
                data = data.toSource();
            }

            setTimeout("$.ajatus.views.system.item.render("+data+");", 200);
        }
    });

})(jQuery);