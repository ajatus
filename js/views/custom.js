/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.views = $.ajatus.views || {};
    
    $.ajatus.views.custom = {
        available: []
    };
    
    $.ajatus.views.custom.init = function(view_name)
    {
        if ($.ajatus.views.custom.available[view_name] == 'undefined') {
            return false;
        }
        
        var view = $.ajatus.views.custom.available[view_name];
        if (   view.in_tabs
            && view.tab)
        {
            var views_tab_holder = $('#tabs-views ul');
            var tab_hash = '#view.'+view_name;
            var tab = $('<li><a href="'+tab_hash+'"><span>'+$.ajatus.i10n.get(view.title)+'</span></a></li>');
            tab.appendTo(views_tab_holder);
            $.ajatus.tabs.prepare(tab);
            
            if (   $.ajatus.history
                && view.history_support) {
                $.ajatus.history.add_map(tab_hash, '$.ajatus.views.custom.'+view_name+'.render();');
            } else {
                tab.bind('click',view.tab.on_click);                    
            }
        }
        if (view['statics']) {
            $.ajatus.views.install_statics(view_name, view);
        }
    };
    
})(jQuery);