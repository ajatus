/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){

    $.ajatus.views.custom.test = {
        title: 'Test custom view',
        in_tabs: true,
        options: {
        },
        update_statics: true,
        history_support: true,
        
        tab: {
            on_click: function(e)
            {
                $.ajatus.tabs.on_click(e);
                $.ajatus.views.custom.test.render();
            }
        },
        
        render: function() {
            $.ajatus.layout.body.set_class('test_custom_view');
            
            var on_success = function(data) {
                if (data.total_rows > 0) {
                    $.ajatus.application_content_area.html('');                    
                }
                var type = $.ajatus.preferences.client.content_types['note'];
                var renderer = new $.ajatus.renderer.list(
                    $.ajatus.application_content_area,
                    type,
                    {
                        id: type.name + '_list_holder',
                        headers: [
                            'id', 'title', 'creator', 'created'
                        ],
                        schema: $.extend({
                            id: {
                                label: 'Id'
                            }
                        }, $.ajatus.document.metadata)
                    }
                );
                
                $.each(data.rows, function(i,doc){
                    var doc = new $.ajatus.document(doc);
                    renderer.render_item(doc, i);
                });
                
                renderer.enable_sorting();
                
                return data;
            };
            
            $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, 'test/funny', {
                descending: true
            });            
        },
        
        gen_dynamic_view: function() {
            var dynamic_view = 'map( doc.value.metadata.created, {"_type": doc.value._type, "id": {val: doc._id},';
            dynamic_view += '"title": doc.value.title,';

            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (type['title_item']) {
                    $.each(type['title_item'], function(i,part){
                        if (type.schema[part]) {
                            dynamic_view += '"'+part+'": doc.value.'+part+',';
                        }
                    });
                }
            });

            dynamic_view += '"creator": doc.value.metadata.creator, "created": doc.value.metadata.created});';
            
            return dynamic_view;
        },
        
        statics: {
            funny: '$.ajatus.views.custom.test.gen_dynamic_view()'
        }
    };

})(jQuery);