/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.content_type = $.ajatus.content_type || {};
    
    $.ajatus.content_type.contact = function()
    {
        var self = this;
        this.name = 'contact';
        this.title = 'Contact';
        this.in_tabs = true;
        this.history_support = true;
        
        this.schema = {
            firstname: {
                label: 'Firstname',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            lastname: {
                label: 'Lastname',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            email: {
                label: 'Email',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            phone: {
                label: 'Phone',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            organization: {
                label: 'Oranization',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            xmpp: {
                label: 'XMPP',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            skype: {
                label: 'Skype',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            description: {
                label: 'Description',
                widget: {
                    name: 'wiki',
                    config: {}
                },
                def_val: ''
            },
            tags: {
                label: 'Tags',
                widget: {
                    name: 'tags',
                    config: {}
                },
                def_val: []
            }
        };

        this.title_item = [
            'firstname', ' ', 'lastname'
        ];
        
        this.list_map = 'map( doc.value.metadata.created.val, {"_type": doc.value._type,';
        this.list_map += '"report_id": doc.value.report_id,';
        this.list_map += '"firstname": doc.value.firstname,';
        this.list_map += '"lastname": doc.value.lastname,';
        this.list_map += '"email": doc.value.email,';
        this.list_map += '"phone": doc.value.phone,';
        this.list_map += '"tags": doc.value.tags,';
        this.list_map += '"creator": doc.value.metadata.creator,';
        this.list_map += '"created": doc.value.metadata.created';
        this.list_map += '});';

        this.gen_views = function() {
            this.views = {
                list: $.ajatus.views.generate(this.view_header+this.list_map+this.view_footer),
                list_at: ''
            };        
            this.statics = {
                list: this.views.list
            };
        };
        
        this.list_headers = [
            'title', 'email', 'phone'
        ];
        
        this.tab = {
            on_click: function(e)
            {   
                $.ajatus.tabs.on_click(e);                
                this.render();
            }
        };
    };
    
    $.ajatus.preferences.client_defaults.content_types['contact'] = new $.ajatus.content_type.contact();

})(jQuery);