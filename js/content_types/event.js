/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){    
    $.ajatus = $.ajatus || {};
    $.ajatus.content_type = $.ajatus.content_type || {};
    
    $.ajatus.content_type.event = function()
    {
        var self = this;
        this.name = 'event';
        this.title = 'Event';
        this.in_tabs = true;
        this.history_support = true;
        this.update_statics = false;
        
        this.schema = {
            title: {
                label: 'Title',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            description: {
                label: 'Description',
                widget: {
                    name: 'wiki',
                    config: {}
                },
                def_val: ''
            },
            start: {
                label: 'Start',
                widget: {
                    name: 'date',
                    config: {
                        use_time: true
                    }
                },
                def_val: $.ajatus.formatter.date.js_to_iso8601(new Date())
            },
            end: {
                label: 'End',
                widget: {
                    name: 'date',
                    config: {
                        use_time: true
                    }
                },
                def_val: $.ajatus.formatter.date.js_to_iso8601(new Date())
            },
            location: {
                label: 'Location',
                widget: {
                    name: 'text',
                    config: {}
                },
                def_val: ''
            },
            tags: {
                label: 'Tags',
                widget: {
                    name: 'tags',
                    config: {}
                },
                def_val: []
            }
        };

        this.list_map = 'map( doc.value.start.val, {"_type": doc.value._type,';
        this.list_map += '"report_id": doc.value.report_id,';
        this.list_map += '"title": doc.value.title,';
        this.list_map += '"start": doc.value.start,';
        this.list_map += '"end": doc.value.end,';
        this.list_map += '"location": doc.value.location,';
        this.list_map += '"tags": doc.value.tags,';
        this.list_map += '"creator": doc.value.metadata.creator,';
        this.list_map += '"created": doc.value.metadata.created';
        this.list_map += '});';

        this.gen_views = function() {
            this.views = {
                list: $.ajatus.views.generate(this.view_header+this.list_map+this.view_footer),
                list_at: ''
            };        
            this.statics = {
                list: this.views.list
            };
        };
        
        this.list_headers = [
            'title', 'start', 'end', 'location'
        ];
        
        this.tab = {
            on_click: function(e) {
                $.ajatus.tabs.on_click(e);                
                this.render();
            }
        };

    };
    
    $.ajatus.preferences.client_defaults.content_types['event'] = new $.ajatus.content_type.event();

})(jQuery);