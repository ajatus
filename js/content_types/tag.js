/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.content_type = $.ajatus.content_type || {};
    
    $.ajatus.content_type.tag = function()
    {
        var self = this;
        this.name = 'tag';
        this.title = 'Tag';
        this.in_tabs = false;
        this.history_support = true;
        this.on_frontpage = false;
        
        this.schema = {
            title: {
                label: 'Title',
                widget: {
                    name: 'tag',
                    config: {}
                },
                def_val: '',
                read_only: false,
                required: true
            }
        };
        
        this.listen_signals = [
            'object_saved'
        ];
        this.on_signal = {
            object_saved: function(signal_data) {
                if (signal_data.content_type.name == 'tag') {                    
                    var fixer = new $.ajatus.tags.fixer();
                    var fixed = fixer.fix_single(signal_data.doc);
                    if (fixed) {
                        $.ajatus.elements.messages.create($.ajatus.i10n.get("Tag fixed"), $.ajatus.i10n.get("%s fixed successfully", [signal_data.doc.value.title.val]));
                        setTimeout('location.hash = "#edit.tag.'+fixed._id+'";', 1000);
                    }
                }
            }
        };
        
        this.list_map = 'map( doc._id, {"_type": doc.value._type,';
        this.list_map += '"title": doc.value.title,';
        this.list_map += '"color": { val: doc.value.title.widget.config.color },';
        this.list_map += '"context": { val: doc.value.title.widget.config.context },';
        this.list_map += '"value": { val: doc.value.title.widget.config.value },';
        this.list_map += '"creator": doc.value.metadata.creator,';
        this.list_map += '"created": doc.value.metadata.created';
        this.list_map += '});';
        
        this.list_map_by_title = 'map( doc.value.title.val, {"_type": doc.value._type,';
        this.list_map_by_title += '"title": doc.value.title,';
        this.list_map_by_title += '"color": { val: doc.value.title.widget.config.color },';
        this.list_map_by_title += '"context": { val: doc.value.title.widget.config.context },';
        this.list_map_by_title += '"value": { val: doc.value.title.widget.config.value },';
        this.list_map_by_title += '"creator": doc.value.metadata.creator,';
        this.list_map_by_title += '"created": doc.value.metadata.created';
        this.list_map_by_title += '});';
        
        this.list_map_with_docs = 'map( [doc._id, 0], {"_type": doc.value._type,';
        this.list_map_with_docs += '"title": doc.value.title,';
        this.list_map_with_docs += '"color": { val: doc.value.title.widget.config.color },';
        this.list_map_with_docs += '"context": { val: doc.value.title.widget.config.context },';
        this.list_map_with_docs += '"value": { val: doc.value.title.widget.config.value },';
        this.list_map_with_docs += '"created": doc.value.metadata.created,';
        this.list_map_with_docs += '"creator": doc.value.metadata.creator';
        this.list_map_with_docs += '});}';
        this.list_map_with_docs += 'if (doc.value.metadata.deleted.val == false && doc.value.metadata.archived.val == false && doc.value._type != "tag") {';
        this.list_map_with_docs += 'for (var i=0;i<doc.value.tags.val.length;i++){';
        this.list_map_with_docs += 'map( [doc.value.tags.val[i], 1], {"_type": doc.value._type,';
        this.list_map_with_docs += '"id": doc._id';
        this.list_map_with_docs += '});}';
        
        this.gen_views = function() {
            this.views = {
                list: $.ajatus.views.generate(this.view_header+this.list_map+this.view_footer),
                list_by_title: $.ajatus.views.generate(this.view_header+this.list_map_by_title+this.view_footer),
                list_at: '',
                list_with_docs: $.ajatus.views.generate(this.view_header+this.list_map_with_docs+this.view_footer)
            };
            
            this.statics = {
                list: this.views.list,
                list_by_title: this.views.list_by_title,
                list_with_docs: this.views.list_with_docs
            };
        };
        
        this.list_headers = [
            'title', 'context', 'value'
        ];
        this.list_schema = $.extend({
            context: {
                label: 'Context'
            },
            value: {
                label: 'Value'
            },
            used: {
                label: 'Used'
            }
        }, this.original_schema)
        
        this.tab = {
            on_click: function(e) {   
                $.ajatus.tabs.on_click(e);
                this.render();
            }
        };
        
        // this.custom_renderer = {
        //     edit: function(doc, latest_rev) {
        //         console.log("Tag edit renderer");
        //         
        //         var form = $('<form id="edit_'+type_name+'" name="edit_'+type_name+'" />');
        //         var form_actions = $('<div class="form_actions"><input type="submit" name="save" value="' + $.ajatus.i10n.get('Save') + '" /><input type="submit" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" /></div>');
        //      var form_struct = $('<div class="form_structure" />');
        //      var form_errors = $('<div class="form_errors" />').hide();
        //      var row_holder = $('<ul class="row_holder" />');
        // 
        //         var type_title = self.title;
        //      $('<h2>' + $.ajatus.i10n.get('Edit %s', [type_title]) + '</h2>').appendTo(form_struct);
        //      
        //      if (typeof $.ajatus.forms.process.has_errors != 'undefined') {
        //          var error_holder = $('<ul />');
        //          $.each($.ajatus.forms.process.error_fields, function(field, data){
        //              $('<li class="error" />').html(data.msg).appendTo(error_holder);
        //          });
        //          error_holder.appendTo(form_errors);
        //          form_errors.appendTo(form_struct).show();
        //      }
        //      
        //         $.ajatus.toolbar.add_item($.ajatus.i10n.get('Save'), {
        //             icon: 'save.png',
        //             action: function(){
        //                 $('#edit_'+self.name+' input[@type=submit][name*=save]').trigger("click", []);
        //             },
        //             access_key: 'Ctrl+s'
        //         });
        //         $.ajatus.toolbar.add_item($.ajatus.i10n.get('Cancel'), 'cancel.png', function(){
        //             $('#edit_'+self.name+' input[@type=submit][name*=cancel]').trigger("click", []);
        //         });
        //         $.ajatus.toolbar.add_item($.ajatus.i10n.get('View'), {
        //             icon: 'view.png',
        //             action: function(){
        //                 $.ajatus.views.system.item.render(doc);
        //             },
        //             access_key: 'Ctrl+v'
        //         });
        //         
        //         
        //     }
        // }
    };
    
    $.ajatus.preferences.client_defaults.content_types['tag'] = new $.ajatus.content_type.tag();

})(jQuery);