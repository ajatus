/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.content_type = $.ajatus.content_type || {};
    
    $.ajatus.content_type.expense = function()
    {
        var self = this;
        this.name = 'expense';
        this.title = 'Expense report';
        this.in_tabs = true;
        this.history_support = true;
        this.enable_additionals = true;
        
        this.auto_show_calendar = false;
        
        this.schema = {
            report_id: {
                label: 'Report ID',
                widget: {
                    name: 'text',
                    config: {
                        value_prefix: '#'
                    }
                },
                read_only: true,
                def_val: $.ajatus.utils.generate_id()
            },
            date: {
                label: 'Date',
                widget: {
                    name: 'date',
                    config: {}
                },
                def_val: $.ajatus.formatter.date.js_to_iso8601(new Date())
            },
            amount: {
                label: 'Amount',
                widget: {
                    name: 'integer',
                    config: {
                        width: 50,
                        value_suffix: '%_CURR_S'
                    }
                },
                def_val: ''
            },
            vat: {
                label: 'VAT',
                widget: {
                    name: 'integer',
                    config: {
                        width: 50,
                        value_suffix: ' %'
                    }
                },
                def_val: '22'
            },
            description: {
                label: 'Description',
                widget: {
                    name: 'wiki',
                    config: {
                        width: 180,
                        height: 40,
                        livepreview: false
                    }
                },
                def_val: ''
            },
            tags: {
                label: 'Tags',
                widget: {
                    name: 'tags',
                    config: {}
                },
                def_val: []
            }
        };
        
        this.title_item = [
            'amount', ' ', 'Expense report for ', 'tags'
        ];
        
        this.list_map = 'map( doc.value.metadata.created.val, {"_type": doc.value._type,';
        this.list_map += '"report_id": doc.value.report_id,';
        this.list_map += '"title": doc.value.title,';
        this.list_map += '"amount": doc.value.amount,';
        this.list_map += '"date": doc.value.date,';
        this.list_map += '"tags": doc.value.tags,';
        this.list_map += '"creator": doc.value.metadata.creator,';
        this.list_map += '"created": doc.value.metadata.created';
        this.list_map += '});';
        
        this.gen_views = function() {
            this.views = {
                list: $.ajatus.views.generate(this.view_header+this.list_map+this.view_footer),
                list_at: ''
            };        
            this.statics = {
                list: this.views.list
            };
        };
        
        this.additional_views = {
            "export": {
                title: 'Export',
                hash_key: 'export'
            }
        };
        
        this.list_headers = [
            'report_id', 'date', 'title', 'creator'
        ];
        this.export_list_headers = [
            'report_id', 'date', 'amount', 'tags', 'creator'
        ];
        
        this.tab = {
            on_click: function(e) {
                $.ajatus.tabs.on_click(e);
                this.render();
            }
        };
        
        this.create_er_form = function(idx, reports_holder) {
            var schema_fields = self.original_schema;
            var type_name = self.name;
            var form_id = 'create_'+type_name+'_'+idx;
            
            var form = $('<form id="'+form_id+'" name="create_'+type_name+'_'+idx+'" />');
    	    var form_struct = $('<div class="form_structure" />');
    	    var rows_holder = $('<ul />');
    	    
    	    var hidden_data = {
    	        _type: type_name
    	    };

            var hidden_fields = function() {
                return [
                    'input', { type: 'hidden', id: '_type', name: '_type', value: this._type }, ''
                ];
            };
            $(form).tplAppend(hidden_data, hidden_fields);
            
            var row_class = idx % 2 == 0 ? 'odd' : 'even';
            var row_holder = $('<li/>').attr({
                'class': row_class,
                id: 'row_'+idx
            });
            row_holder.appendTo(rows_holder);
            
            var row_actions = $('<div class="row_actions"><input type="submit" name="save" value="Create" /></div>');
            var row_breaker = $('<div />').css({
                clear: 'both'
            });
            
    	    $.each(schema_fields, function(i,n){
    	        
    	        if (i == 'report_id') {
    	            n.def_val = $.ajatus.utils.generate_id();
    	        }
    	        
    	        var widget = new $.ajatus.widget(n.widget.name, n.widget.config);
    	        var wdgt_tpl = widget.get_create_tpl(i, n.def_val);

    	        if ( typeof n['read_only'] != 'undefined'
    	            && n.read_only == true)
    	        {
    	            wdgt_tpl = $.ajatus.widgets.to_readonly('create', i, n.def_val, widget);
    	        }

                $(row_holder).createAppend(
                    'div', { id: '_'+idx+'_element_'+i, className: 'row_item '+i }, [
                        'label', { id: '_'+idx+'_element_'+i+'_label' }, n.label,
                        'div', { id: '_'+idx+'_element_'+i+'_widget', className: widget.name }, wdgt_tpl
                    ]
                );

                widget.init($('#_'+idx+'_element_'+i+'_widget',row_holder), true);
    	    });

    	    row_actions.appendTo(row_holder);
    	    row_breaker.appendTo(row_holder);
    	    
            rows_holder.appendTo(form_struct);
            form_struct.appendTo(form);            	    
            form.appendTo(reports_holder);
            
            if (this.auto_show_calendar) {
                setTimeout("$('#"+form_id+" :input:not(:hidden):eq(0)').focus();", 200);
            } else {
                setTimeout("$('#"+form_id+" :input:not(:hidden):eq(1)').focus();", 200);
            }
            
            return form;
        };
        
        this.custom_renderer = {
            create: function() {
                $.ajatus.layout.body.append_class('report');
                
                var forms = [];
                var current_form = 0;
                
                var reports_holder = $('<div class="expense_reports_holder" />');
                $.ajatus.application_content_area.html(reports_holder);
                
                var saved_holder = $('<div class="saved_expense_reports_holder" />');
                saved_holder.prependTo(reports_holder);
                
                var saved_list = new $.ajatus.renderer.tinylist(saved_holder, {
                    show_headers: true,
                    headers: ['report_id', 'date', 'title'],
                    schema: self.original_schema
                });
                
                var type_title = self.title;
        	    $('<h2>Create ' + type_title + '</h2>').appendTo(reports_holder);
                
                var form = self.create_er_form(0, reports_holder);
                forms.push(form);
                
                var on_ready = function(data){
                    var prev_form = forms[current_form];
                    prev_form.remove();
                    
                    current_form += 1;
                    var form = self.create_er_form(current_form, reports_holder);
                    forms.push(form);

                    saved_list.render_item(data, current_form);
                    
                    $.ajatus.forms.register.ajax(form, on_ready);
                };
                
                $.ajatus.forms.register.ajax(forms[current_form], on_ready, 'create');
            }
        };        
    };
    
    $.ajatus.preferences.client_defaults.content_types['expense'] = new $.ajatus.content_type.expense();

})(jQuery);