/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};
    
    $.ajatus.i10n.currency = {
        name: 'US Dollar',
        symbol: '$'
    };
    
    $.ajatus.i10n.datetime = {
        date: 'MDY/',
        time: 'HMS:'
    };
    
    $.ajatus.i10n.dict = {
        "en_GB": "English",
        
        "distributed crm": "Distributed CRM",
        "loading ajatus": "Loading Ajatus",
        
        "failed": "failed",
        "ok": "ok",
        "continue": "Continue",
        "type": "Type",
        "undo": "Undo",
        "archive": "Archive",
        "archived": "Archived",
        "archived item": "Archived item",
        "archived %": "Archived %s",
        "unarchive": "Unarchive",
        "delete": "Delete",
        "deleted": "Deleted",
        "undelete": "Undelete",
        "restore": "Restore",
        "view": "View",
        "view %s": "View %s",
        "edit": "Edit",
        "edit %s": "Edit %s",
        "save": "Save",
        "create": "Create",
        "create %s": "Create %s",
        "cancel": "Cancel",
        "new": "New",
        "new %s": "New %s",
        "add field": "Add field",
        "actions": "Actions",
        "settings": "Settings",
        "enabled": "Enabled",
        "interval": "Interval",
        "label": "Label",
        "default value": "default value",
        "required": "required",
        "auto saving in": "Auto saving in",
        "first": "First",
        "next": "Next",
        "previous": "Previous",
        "last": "Last",
        "%d of %d": "%d of %d",
        "duplicate": "Duplicate",
        "broken": "Broken",
        "related objects": "Related objects",
                
        "title": "Title",
        "description": "Description",
        "date": "Date",
        "firstname": "Firstname",
        "lastname": "Lastname",
        "name": "Name",
        "email": "Email",
        "phone": "Phone",
        "hours": "Hours",
        "amount": "Amount",
        "vat": "VAT",
        "start": "Start", // This is bit misleading currently (Means start date)
        "end": "End", // This is bit misleading currently (Means end date)
        "location": "Location",
        "used": "Used",
        
        // Field types
        "wiki": "Wiki",
        "text": "Text",
        "boolean": "Boolean",
        "integer": "Integer",
        "selection": "Selection",
        
        "global tag": "Global Tag",
        "new tag created": "New tag created",
        "created new tag '%s'": "Created new tag '%s'",
        "tag %s removed from ajatus": "Tag %s removed from Ajatus",
        "merge selected": "Merge selected",
        "merging tag %s": "Merging tag %s",
        "merging &d tag": "Merging %d tag",
        "fix all broken tags": "Fix all broken tags",
        "tag fixed": "Tag fixed",
        "%s fixed successfully": "%s fixed successfully",
        "missing tags": "Missing tags",
        "object has missing tags": "Object has missing tags",
                
        "related objects": "Related Objects",
        "widget": "Widget",
        "select one": "Select one",
        "edit mode": "Edit",
        "preview mode": "Preview",
        
        "empty results": "Empty results",
        "no %s found": "No %s found",
        "nothing to export": "Nothing to export",
        "export %s": "Export %s",
        
        "field %s is required": "Field %s is required",
        
        // Installer
        "installing": "Installing",
        "uninstalling": "Uninstalling",
        "installing application database": "Installing application database",
        "uninstalling application database": "Uninstalling application database",
        "preparing application database": "Preparing application database",
        "installing application content database": "Installing application content database",
        "uninstalling application content database": "Uninstalling application content database",
        
        // Metadata
        "metadata": "Metadata",
        "creator": "Creator",
        "created": "Created",
        "revisor": "Revisor",
        "revised": "Revised",
        "archiver": "Archiver",
        "archived": "Archived",
        "deleted": "Deleted",
        
        // System views
        "frontpage": "Frontpage",
        "trash": "Trash",
        "preferences": "Preferences",
        "archive": "Archive",
        "tags": "Tags",
        "view item": "View item",
        
        // Content types
        "note": "Note",
        "hour report": "Hour report",
        "event": "Event",
        "tag": "Tag",
        "contact": "Contact",
        "expense report": "Expense report",
        
        "hour report for": "Hour report for ",
        "expense report for": "Expense report for ",
        
        // Document actions
        "unknown document action": "Unknown document action",
        "unknown action %s requested for object %s!": "Unknown action %s requested for object %s!",
        "object saved": "Object saved",
        "%s saved successfully": "%s saved successfully",
        "object deleted": "Object deleted",
        "object %s moved to trash.": "Object %s moved to trash.",
        "object %s removed from ajatus.": "Object %s removed from Ajatus.",
        "object restored": "Object restored",
        "object %s restored succesfully.": "Object %s restored succesfully.",
        "emptying action pool": "Emptying action pool",
        "running %s pooled actions.": "Running %s pooled actions.",

        "archive visible": "Archive visible",
        "archiving all items in view. are you sure?": "Archiving all items in view. Are you sure?",
        
        "empty trash": "Empty trash",
        "deleting all items in trash. are you sure?": "Deleting all items in trash. Are you sure?",
        
        "nothing to update": "Nothing to update",
        "object saved": "Object saved",
        "%s saved successfully": "%s saved successfully",
        "you have unsaved changes.": "You have unsaved changes.",
        
        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s doesn't support dynamic creation.",
        "widget %s doesn't support dynamic settings.": "Widget %s doesn't support dynamic settings.",
        "edit field %s": "Edit field %s",
        "add field": "Add field",
        "select one": "Select one",
        "configure widget %s": "Configure widget %s",
        "do you really want to delete item %s": "Do you really want to delete item %s",
        
        //Preferences
        "theme": "Theme",
        "language": "Language",
        "user": "User",
        "localization": "Localization",
        "layout": "Layout",
        "replication": "Replication",
        "backups": "Backups",
        "expansions": "Expansions",
        "auto save": "Auto save",
        
        "generated doc title (%s)": "generated doc title (%s)",
        
        // Strings used in testing
        "test %s": "Testing %s",
        "%d comment": "%d comment",
        "%d comment in %d doc": "%d comment in %d doc"
    };
    
    $.ajatus.i10n.inflections['en_GB'] = {
        plural: [
            [/(quiz)$/i,               "$1zes"  ],
            [/^(ox)$/i,                "$1en"   ],
            [/([m|l])ouse$/i,          "$1ice"  ],
            [/(matr|vert|ind)ix|ex$/i, "$1ices" ],
            [/(x|ch|ss|sh)$/i,         "$1es"   ],
            [/([^aeiouy]|qu)y$/i,      "$1ies"  ],
            [/(hive)$/i,               "$1s"    ],
            [/(?:([^f])fe|([lr])f)$/i, "$1$2ves"],
            [/sis$/i,                  "ses"    ],
            [/([ti])um$/i,             "$1a"    ],
            [/(buffal|tomat)o$/i,      "$1oes"  ],
            [/(bu)s$/i,                "$1ses"  ],
            [/(alias|status)$/i,       "$1es"   ],
            [/(octop|vir)us$/i,        "$1i"    ],
            [/(ax|test)is$/i,          "$1es"   ],
            [/s$/i,                    "s"      ],
            [/$/,                      "s"      ]
        ],
        singular: [
            [/(quiz)zes$/i,                                                    "$1"     ],
            [/(matr)ices$/i,                                                   "$1ix"   ],
            [/(vert|ind)ices$/i,                                               "$1ex"   ],
            [/^(ox)en/i,                                                       "$1"     ],
            [/(alias|status)es$/i,                                             "$1"     ],
            [/(octop|vir)i$/i,                                                 "$1us"   ],
            [/(cris|ax|test)es$/i,                                             "$1is"   ],
            [/(shoe)s$/i,                                                      "$1"     ],
            [/(o)es$/i,                                                        "$1"     ],
            [/(bus)es$/i,                                                      "$1"     ],
            [/([m|l])ice$/i,                                                   "$1ouse" ],
            [/(x|ch|ss|sh)es$/i,                                               "$1"     ],
            [/(m)ovies$/i,                                                     "$1ovie" ],
            [/(s)eries$/i,                                                     "$1eries"],
            [/([^aeiouy]|qu)ies$/i,                                            "$1y"    ],
            [/([lr])ves$/i,                                                    "$1f"    ],
            [/(tive)s$/i,                                                      "$1"     ],
            [/(hive)s$/i,                                                      "$1"     ],
            [/([^f])ves$/i,                                                    "$1fe"   ],
            [/(^analy)ses$/i,                                                  "$1sis"  ],
            [/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i, "$1$2sis"],
            [/([ti])a$/i,                                                      "$1um"   ],
            [/(n)ews$/i,                                                       "$1ews"  ],
            [/s$/i,                                                            ""       ]
        ],
        irregular: [
            ['move',   'moves'   ],
            ['sex',    'sexes'   ],
            ['child',  'children'],
            ['man',    'men'     ],
            ['person', 'people'  ]
        ],
        uncountable: [
            "of",
            "sheep",
            "fish",
            "series",
            "species",
            "money",
            "rice",
            "information",
            "equipment"
        ],
        ordinalize: function(number) {
            if (11 <= parseInt(number) % 100 && parseInt(number) % 100 <= 13) {
                return number + "th";
            } else {
                switch (parseInt(number) % 10) {
                    case  1:
                        return number + "st";
                    case  2:
                        return number + "nd";
                    case  3:
                        return number + "rd";
                    default:
                        return number + "th";
                }
            }
        }
    };
    
})(jQuery);