/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};

    $.ajatus.i10n.currency = {
        name: 'Euro',
        symbol: '€'
    };

    $.ajatus.i10n.datetime = {
        date: 'MDY/',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "pl_PL": "Polski",
        
        "distributed crm": "CRM",
        "loading ajatus": "Ajatus - trwa uruchamianie",

        "failed": "Nie powiodło się",
        "ok": "ok",
        "continue": "Kontynuuj",
        "type": "Typ",
        "undo": "Cofnij",
        "archive": "Archiwum",
        "archive": "Archiwum",
        "delete": "Usuń",
        "deleted": "Usunięty",
        "undelete": "Odtwórz", /* ??? */
        "view": "Widok",
        "view item": "Widok",
        "edit": "Edytuj",
        "edit %s": "Edytuj %s",
        "save": "Zapisz",
        "cancel": "Cancel",
        "create": "Utwórz",
        "create %s": "Utwórz %s",
        "add field": "Add field",
        "actions": "Akcje",
                
        "title": "Tytuł",
        "description": "Opis",
        "date": "Data",
        "firstname": "Imię",
        "lastname": "Nazwisko",
        "email": "Email",
        "phone": "Teleofon",
        "hours": "Godziny",
        "start": "Start", // This is bit misleading currently (Means start date)
        "end": "End", // This is bit misleading currently (Means end date)
        "location": "Location",

        // Field types
        "wiki": "Wiki",
        "text": "Text",
        "boolean": "Yes/no",
        "integer": "Integer",
        "selection": "Selection",
                                                
        "global tag": "Tag globalny",
        "related objects": "Obiekty powiązane",
        "widget": "Widget",
        "select one": "Select one",
        "edit mode": "Edit",
        "preview mode": "Preview",
        
        "empty results": "Pusty wynik",
        "no %s found": "%s nie znaleziony.",
        
        // Installer
        "installing": "Instaluje",
        "uninstalling": "Usuwam",
        "installing application database": "Instaluje baze danych programu",
        "uninstalling application database": "Usuwam baze danych programu",
        "preparing application database": "Przygotowanie bazy danych programu",
        "installing application tags database": "Instaluje tagi bazy danych",
        "uninstalling application tags database": "Usuwam tagi bazy danych",
        "installing application content database": "Instaluje baze danych programu",
        "uninstalling application content database": "Usuwam baze danych programu",
        
        // Metadata
        "creator": "Utzworzył",
        "created": "Utworzono",
        "revisor": "Edytował",
        "revised": "Edytowano",
        "archiver": "Zarchiwizował",
        "archived": "Zarchiwizowano",
        "deleted": "Usunięty",
        
        // System views
        "frontpage": "Strona domowa",
        "trash": "Kosz",
        "preferences": "Preferencje",
        "tags": "Tagi",
        
        // Content types
        "note": "Notatka",
        "hour report": "Raport godzinowy",
        "event": "Wydarzenie",
        "tag": "Tag",
        "contact": "Kontakt",
        "expense report": "Expense report",

        "hour report for": "Hour report for ",
        "expense report for": "Expense report for ",

        // Document actions
        "unknown document action": "Nieznana akcja dla dokumentu",
        "unknown action %s requested for object %s!": "Nieznana akcja %s dla obiektu %s!",
        "object deleted": "Obiekt usunięty",
        "object %s moved to trash.": "Obiekt %s przeniesiony do kosza.",
        "object %s removed from ajatus.": "Obiekt %s usunięty z Ajatus.",
        "object restored": "Obiekt odtworzony",
        "object %s restored succesfully.": "Obiek %s pomyślnie odtworzony.",
        "emptying action pool": "Usuwam kolejki akcji",
        "running %s pooled actions.": "Uruchamiam %s akcji z kolejki.",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s doesn't support dynamic creation.",
        "widget %s doesn't support dynamic settings.": "Widget %s doesn't support dynamic settings.",
        "settings": "Settings",
        "edit field %s": "Edit field %s",

        // Strings used in testing
        "test %s": "Testuje %s",
        "%d comment": "%d komentarz",
        "%d comment in %d doc": "%d komentarz w %d "
    };
    
    $.ajatus.i10n.inflections['pl_PL'] = {
        plural: [],
        singular: [],
        irregular: [],
        uncountable: []
    };
})(jQuery);
