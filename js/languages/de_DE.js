/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 *
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 *
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};

    $.ajatus.i10n.currency = {
        name: 'Euro',
        symbol: '€'
    };

    $.ajatus.i10n.datetime = {
        date: 'DMY.',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "de_DE": "Deutsch",

        "distributed crm": "Verteiltes CRM",
        "loading ajatus": "Lade Ajatus",

        "failed": "missglückt",
        "ok": "ok",
        "continue": "Fortfahren",
        "type": "Type",
        "undo": "Rückgängig",
        "archive": "Archiv",
        "archived": "Archiviert",
        "delete": "Löschen",
        "deleted": "Gelöscht",
        "undelete": "Löschen rückgängig",
        "view": "Ansicht",
        "edit": "Bearbeiten",
        "edit %s": "%s bearbeiten",
        "save": "Speichern",
        "create": "Erzeugen",
        "create %s": "Erzeuge %s",
        "cancel": "Abbrechen",
        "new": "Neu",
        "add field": "Feld hinzufügen",
        "actions": "Optionen",

        "title": "Titel",
        "description": "Beschreibung",
        "date": "Datum",
        "firstname": "Vorname",
        "lastname": "Nachname",
        "email": "E-Mail",
        "phone": "Telefon",
        "hours": "Stunden",
        "amount": "Anzahl",
        "vat": "Mehrwertsteuer",
        "start": "Beginn", // This is bit misleading currently (Means start date)
        "end": "Ende", // This is bit misleading currently (Means end date)
        "location": "Ort",
        "used": "verbraucht",

        // Field types
        "wiki": "Wiki",
        "text": "Text",
        "boolean": "Boolean",
        "integer": "Ganzahl",
        "selection": "Auswahl",

        "global tag": "Allgemeiner Tag",
        "new tag created": "Neuer Tag angelegt",
        "created new tag '%s'": "Tag '%s' angelegt",

        "related objects": "Ähnliche Objekte",
        "widget": "Widget",
        "select one": "Wählen Sie eines",
        "edit mode": "Editieren",
        "preview mode": "Voransicht",

        "empty results": "Keine Ergebnisse",
        "no %s found": "%s nicht gefunden",

        "field %s is required": "Feld %s muss angegeben werden",

        // Installer
        "installing": "Installiere",
        "uninstalling": "Deinstalliere",
        "installing application database": "Installiere Datenbank",
        "uninstalling application database": "Deinstalliere Datenbank",
        "preparing application database": "Bereite Datenbank vor",
        "installing application tags database": "Installiere Tag-Datenbank",
        "uninstalling application tags database": "Deinstalliere Tag-Datenbank",
        "installing application content database": "Installiere Inhalts-Datenbank",
        "uninstalling application content database": "Deinstalliere Inhaltsdatenbank",

        // Metadata
        "creator": "Autor",
        "created": "angelegt",
        "revisor": "Kontrollor",
        "revised": "kontrolliert",
        "archiver": "Archivar",
        "archived": "Archiviert",
        "deleted": "Gelöscht",

        // System views
        "frontpage": "Startseite",
        "trash": "Mülleimer",
        "preferences": "Voreinstellungen",
        "tags": "Tags",
        "view item": "Ansicht",

        // Content types
        "note": "Notiz",
        "hour report": "Stundennachweis",
        "event": "Ereignis",
        "tag": "Tag",
        "contact": "Kontakt",
        "expense report": "Spesenabrechnung",

        "hour report for": "Stundennachweis für ",
        "expense report for": "Spesenabrechnung für ",

        // Document actions
        "unknown document action": "Unbekannte Option für das Dokument",
        "unknown action %s requested for object %s!": "UUnbekannte Option %s für das Objekt %s!",
        "object saved": "Objekt gespeichert",
        "%s saved successfully": "%s erfolgreich gespeichert",
        "object deleted": "Objekt gelöscht",
        "object %s moved to trash.": "Objekt %s wurde in den Mülleimer verschoben",
        "object %s removed from ajatus.": "Objekt %s aus Ajatus entfernt",
        "object restored": "Objekt wiederhergestellt",
        "object %s restored succesfully.": "Objekt %s erfolgreich wiederhergestellt",
        "emptying action pool": "Lösche Aktionspool",
        "running %s pooled actions.": "Führe %s gespeicherte Aktionen aus",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s kann nicht dynamisch angelegt werden",
        "widget %s doesn't support dynamic settings.": "Widget %s unterstützt keine dynamischen Parameter",
        "settings": "Einstellungen",
        "edit field %s": "Editiere Feld %s",

        // Strings used in testing
        "test %s": "Teste %s",
        "%d comment": "%d Kommentar",
        "%d comment in %d doc": "%d Kommentar im %d Dokument"
    };

    $.ajatus.i10n.inflections['en_GB'] = {
        plural: [
            [/(quiz)$/i,               "$1zes"  ],
            [/^(ox)$/i,                "$1en"   ],
            [/([m|l])ouse$/i,          "$1ice"  ],
            [/(matr|vert|ind)ix|ex$/i, "$1ices" ],
            [/(x|ch|ss|sh)$/i,         "$1es"   ],
            [/([^aeiouy]|qu)y$/i,      "$1ies"  ],
            [/(hive)$/i,               "$1s"    ],
            [/(?:([^f])fe|([lr])f)$/i, "$1$2ves"],
            [/sis$/i,                  "ses"    ],
            [/([ti])um$/i,             "$1a"    ],
            [/(buffal|tomat)o$/i,      "$1oes"  ],
            [/(bu)s$/i,                "$1ses"  ],
            [/(alias|status)$/i,       "$1es"   ],
            [/(octop|vir)us$/i,        "$1i"    ],
            [/(ax|test)is$/i,          "$1es"   ],
            [/s$/i,                    "s"      ],
            [/$/,                      "s"      ]
        ],
        singular: [
            [/(quiz)zes$/i,                                                    "$1"     ],
            [/(matr)ices$/i,                                                   "$1ix"   ],
            [/(vert|ind)ices$/i,                                               "$1ex"   ],
            [/^(ox)en/i,                                                       "$1"     ],
            [/(alias|status)es$/i,                                             "$1"     ],
            [/(octop|vir)i$/i,                                                 "$1us"   ],
            [/(cris|ax|test)es$/i,                                             "$1is"   ],
            [/(shoe)s$/i,                                                      "$1"     ],
            [/(o)es$/i,                                                        "$1"     ],
            [/(bus)es$/i,                                                      "$1"     ],
            [/([m|l])ice$/i,                                                   "$1ouse" ],
            [/(x|ch|ss|sh)es$/i,                                               "$1"     ],
            [/(m)ovies$/i,                                                     "$1ovie" ],
            [/(s)eries$/i,                                                     "$1eries"],
            [/([^aeiouy]|qu)ies$/i,                                            "$1y"    ],
            [/([lr])ves$/i,                                                    "$1f"    ],
            [/(tive)s$/i,                                                      "$1"     ],
            [/(hive)s$/i,                                                      "$1"     ],
            [/([^f])ves$/i,                                                    "$1fe"   ],
            [/(^analy)ses$/i,                                                  "$1sis"  ],
            [/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i, "$1$2sis"],
            [/([ti])a$/i,                                                      "$1um"   ],
            [/(n)ews$/i,                                                       "$1ews"  ],
            [/s$/i,                                                            ""       ]
        ],
        irregular: [
            ['move',   'moves'   ],
            ['sex',    'sexes'   ],
            ['child',  'children'],
            ['man',    'men'     ],
            ['person', 'people'  ]
        ],
        uncountable: [
            "of",
            "sheep",
            "fish",
            "series",
            "species",
            "money",
            "rice",
            "information",
            "equipment"
        ],
        ordinalize: function(number) {
            if (11 <= parseInt(number) % 100 && parseInt(number) % 100 <= 13) {
                return number + ".";
            } else {
                switch (parseInt(number) % 10) {
                    case  1:
                        return number + ".";
                    case  2:
                        return number + ".";
                    case  3:
                        return number + ".";
                    default:
                        return number + ".";
                }
            }
        }
    };

})(jQuery);
