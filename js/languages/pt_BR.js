/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};
    
    $.ajatus.i10n.currency = {
        name: 'Real',
        symbol: 'R$'
    };
    
    $.ajatus.i10n.datetime = {
        date: 'MDY/',
        time: 'HMS:'
    };
    
    $.ajatus.i10n.dict = {
        "pt_BR": "Brazilian Portuguese",
        
        "distributed crm": "O CRM Distribuído",
        "loading ajatus": "Carregando Ajatus",
        
        "failed": "falhou",
        "ok": "ok",
        "continue": "Continuar",
        "type": "Tipo",
        "undo": "Desfazer",
        "archive": "Arquivar",
        "archived": "Arquivado",
        "delete": "Excluir",
        "deleted": "Excluído",
        "undelete": "Recuperar",
        "view": "Visualizar",
        "edit": "Editar",
        "edit %s": "Editar %s",
        "save": "Salvar",
        "create": "Criar",
        "create %s": "Criado %s",
        "cancel": "Cancelar",
        "new": "Novo",
        "add field": "Adicionar campo",
        "actions": "Ações",
                
        "title": "Título",
        "description": "Descrição",
        "date": "Data",
        "firstname": "Nome",
        "lastname": "Sobrenome",
        "email": "Email",
        "phone": "Telefone",
        "hours": "Hours",
        "amount": "Valor",
        "vat": "VAT",
        "start": "Inicio", // This is bit misleading currently (Means start date)
        "end": "Fim", // This is bit misleading currently (Means end date)
        "location": "Localização",
        
        // Field types
        "wiki": "Wiki",
        "text": "Texto",
        "boolean": "Boleano",
        "integer": "Inteiro",
        "selection": "Seleção",
        
        "global tag": "Marcador Global",
        "new tag created": "Novo marcador criado",
        "created new tag '%s'": "Criado novo marcador '%s'",
                
        "related objects": "Objetos Relacionados",
        "widget": "Widget",
        "select one": "Selecione um",
        "edit mode": "Editar",
        "preview mode": "Visualizar",
        
        "empty results": "Vazio",
        "no %s found": "Nenhum %s encontrado.",
        
        "field %s is required": "Campo %s é obrigatório",
        
        // Installer
        "installing": "Instalando",
        "uninstalling": "Desinstalando",
        "installing application database": "Instalando banco de dados da aplicação",
        "uninstalling application database": "Desinstalando banco de dados da aplicação",
        "preparing application database": "Preparing banco de dados da aplicação",
        "installing application tags database": "Instalando marcadores",
        "uninstalling application tags database": "Desinstalando marcadores",
        "installing application content database": "Instalando conteúdo",
        "uninstalling application content database": "Desinstalando conteúdo",
        
        // Metadata
        "creator": "Autor",
        "created": "Criado",
        "revisor": "Revisor",
        "revised": "Revisado",
        "archiver": "Arquivador",
        "archived": "Arquivado",
        "deleted": "Excluído",
        
        // System views
        "frontpage": "Principal",
        "trash": "Lixeira",
        "preferences": "Preferências",
        "tags": "Marcadores",
        "view item": "Visualizar item",
        
        // Content types
        "note": "Anotações",
        "hour report": "Relatório de Horas",
        "event": "Evento",
        "tag": "Marcador",
        "contact": "Contato",
        "expense report": "Relatório de Despesas",
        
        "hour report for": "Relatório de horas para ",
        "expense report for": "Relatório de despesas para ",
        
        // Document actions
        "unknown document action": "Unknown document action",
        "unknown action %s requested for object %s!": "Unknown action %s requested for object %s!",
        "object saved": "Objeto salvo",
        "%s saved successfully": "%s saved successfully",
        "object deleted": "Object deleted",
        "object %s moved to trash.": "Object %s moved to trash.",
        "object %s removed from ajatus.": "Object %s removed from Ajatus.",
        "object restored": "Object restored",
        "object %s restored succesfully.": "Object %s restored succesfully.",
        "emptying action pool": "Emptying action pool",
        "running %s pooled actions.": "Running %s pooled actions.",
        
        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s doesn't support dynamic creation.",
        "widget %s doesn't support dynamic settings.": "Widget %s doesn't support dynamic settings.",
        "settings": "Settings",
        "edit field %s": "Edit field %s",
        
        // Strings used in testing
        "test %s": "Testing %s",
        "%d comment": "%d comment",
        "%d comment in %d doc": "%d comment in %d doc"
    };
    
    $.ajatus.i10n.inflections['en_GB'] = {
        plural: [
            [/(quiz)$/i,               "$1zes"  ],
            [/^(ox)$/i,                "$1en"   ],
            [/([m|l])ouse$/i,          "$1ice"  ],
            [/(matr|vert|ind)ix|ex$/i, "$1ices" ],
            [/(x|ch|ss|sh)$/i,         "$1es"   ],
            [/([^aeiouy]|qu)y$/i,      "$1ies"  ],
            [/(hive)$/i,               "$1s"    ],
            [/(?:([^f])fe|([lr])f)$/i, "$1$2ves"],
            [/sis$/i,                  "ses"    ],
            [/([ti])um$/i,             "$1a"    ],
            [/(buffal|tomat)o$/i,      "$1oes"  ],
            [/(bu)s$/i,                "$1ses"  ],
            [/(alias|status)$/i,       "$1es"   ],
            [/(octop|vir)us$/i,        "$1i"    ],
            [/(ax|test)is$/i,          "$1es"   ],
            [/s$/i,                    "s"      ],
            [/$/,                      "s"      ]
        ],
        singular: [
            [/(quiz)zes$/i,                                                    "$1"     ],
            [/(matr)ices$/i,                                                   "$1ix"   ],
            [/(vert|ind)ices$/i,                                               "$1ex"   ],
            [/^(ox)en/i,                                                       "$1"     ],
            [/(alias|status)es$/i,                                             "$1"     ],
            [/(octop|vir)i$/i,                                                 "$1us"   ],
            [/(cris|ax|test)es$/i,                                             "$1is"   ],
            [/(shoe)s$/i,                                                      "$1"     ],
            [/(o)es$/i,                                                        "$1"     ],
            [/(bus)es$/i,                                                      "$1"     ],
            [/([m|l])ice$/i,                                                   "$1ouse" ],
            [/(x|ch|ss|sh)es$/i,                                               "$1"     ],
            [/(m)ovies$/i,                                                     "$1ovie" ],
            [/(s)eries$/i,                                                     "$1eries"],
            [/([^aeiouy]|qu)ies$/i,                                            "$1y"    ],
            [/([lr])ves$/i,                                                    "$1f"    ],
            [/(tive)s$/i,                                                      "$1"     ],
            [/(hive)s$/i,                                                      "$1"     ],
            [/([^f])ves$/i,                                                    "$1fe"   ],
            [/(^analy)ses$/i,                                                  "$1sis"  ],
            [/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i, "$1$2sis"],
            [/([ti])a$/i,                                                      "$1um"   ],
            [/(n)ews$/i,                                                       "$1ews"  ],
            [/s$/i,                                                            ""       ]
        ],
        irregular: [
            ['move',   'moves'   ],
            ['sex',    'sexes'   ],
            ['child',  'children'],
            ['man',    'men'     ],
            ['person', 'people'  ]
        ],
        uncountable: [
            "of",
            "sheep",
            "fish",
            "series",
            "species",
            "money",
            "rice",
            "information",
            "equipment"
        ],
        ordinalize: function(number) {
            if (11 <= parseInt(number) % 100 && parseInt(number) % 100 <= 13) {
                return number + "th";
            } else {
                switch (parseInt(number) % 10) {
                    case  1:
                        return number + "st";
                    case  2:
                        return number + "nd";
                    case  3:
                        return number + "rd";
                    default:
                        return number + "th";
                }
            }
        }
    };
    
})(jQuery);
