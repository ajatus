/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};

    $.ajatus.i10n.currency = {
        name: 'Euro',
        symbol: '€'
    };
    
    $.ajatus.i10n.datetime = {
        date: 'DMY.',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "fi_FI": "Suomi",
        
        "distributed crm": "Jaettu CRM",
        "loading ajatus": "Ladataan Ajatus",

        "failed": "epäonnistui",
        "ok": "ok",
        "continue": "Jatka",
        "type": "Tyyppi",
        "undo": "Kumoa",
        "archive": "Arkistoi",
        "archived": "Arkistoitu",
        "delete": "Poista",
        "deleted": "Poistettu",
        "undelete": "Palauta",
        "view": "Katso",
        "view item": "Näytä objekti",
        "edit": "Muokkaa",
        "edit %s": "Muokkaa %s",
        "save": "Tallenna",
        "create": "Luo",
        "create %s": "Luo %s",
        "cancel": "Peruuta",
        "new": "Uusi",
        "add field": "Lisää kenttä",
        "actions": "Toiminnot",
                
        "title": "Otsikko",
        "description": "Kuvaus",
        "date": "Päivämäärä",
        "firstname": "Etunimi",
        "lastname": "Sukunimi",
        "email": "Sähköposti",
        "phone": "Puhelin",
        "hours": "Tunnit",
        "amount": "Summa",
        "vat": "ALV",
        "start": "Alkaa", // This is bit misleading currently (Means start date)
        "end": "Päättyy", // This is bit misleading currently (Means end date)
        "location": "Location",
        "used": "Käytetty",
                
        // Field types
        "wiki": "Wiki",
        "text": "Teksti",
        "boolean": "Kyllä/ei",
        "integer": "Numero",
        "selection": "Valinta",
        
        "global tag": "Yleinen Tag-merkintä",
        "related objects": "Liittyvät objektit",
        "widget": "Kenttä",
        "select one": "Valitse yksi",
        "edit mode": "Muokkaus",
        "preview mode": "Esikatselu",
        
        "empty results": "Ei osumia",
        "no %s found": "Yhtään %s ei löytynyt.",
        
        "field %s is required": "Kenttä %s on pakollinen",
        
        // Installer
        "installing": "Asennetaan",
        "uninstalling": "Poistetaan",
        "installing application database": "Asennetaan ohjelman tietokanta",
        "uninstalling application database": "Poistetaan ohjelman tietokanta",
        "preparing application database": "Valmistellaan ohjelman tietokantaa",
        "installing application tags database": "Asennetaan ohjelman tagi tietokantaa",
        "uninstalling application tags database": "Poistetaan ohjelman tagi tietokantaa",
        "installing application content database": "Asennetaan ohjelman sisältö tietokantaa",
        "uninstalling application content database": "Poistetaan ohjelman sisältö tietokantaa",
        
        // Metadata
        "creator": "Luoja",
        "created": "Luotu",
        "revisor": "Muokkaaja",
        "revised": "Muokattu",
        "archiver": "Arkistoija",
        "archived": "Arkistoitu",
        "deleted": "Poistettu",
        
        // System views
        "frontpage": "Etusivu",
        "trash": "Roskakori",
        "preferences": "Asetukset",
        "tags": "Tagit",
        
        // Content types
        "note": "Muistiinpano",
        "hour report": "Tuntiraportti",
        "event": "Tapahtuma",
        "tag": "Tag-merkintä",
        "contact": "Yhteystieto",
        "expense report": "Kuluraportti",

        "hour report for": "Tuntiraportti koskien ",
        "expense report for": "Kuluraportti koskien ",

        // Document actions
        "unknown document action": "Ei-tuettu toimenpide",
        "unknown action %s requested for object %s!": "Ei-tuettu toimenpide %s pyydetty tietueelle %s!",
        "object deleted": "Tietue poistettu",
        "object %s moved to trash.": "Tietue %s on siirretty roskakoriin.",
        "object %s removed from ajatus.": "Tietue %s on poistettu Ajatuksesta.",
        "object restored": "Tietue palautettu",
        "object %s restored succesfully.": "Tietue %s on palautettu onnistuneesti.",
        "emptying action pool": "Toimenpidejonoa tyhjennetään",
        "running %s pooled actions.": "Ajetaan %s toimenpidettä jonosta.",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Kenttä %s ei tue dynaamista luontia.",
        "widget %s doesn't support dynamic settings.": "Kenttä %s ei tue dynaamisia asetuksia.",
        "settings": "Asetukset",
        "edit field %s": "Muokkaa kenttää %s",
        
        // Strings used in testing
        "test %s": "Testataan %s",
        "%d comment": "%d kommentti",
        "%d comment in %d doc": "%d komentti tietueelle %d"
    };
    
    $.ajatus.i10n.inflections['fi_FI'] = {
        plural: [
            [/(uistiinpano)$/i,     "$1t"   ],
            [/(hteystie)to$/i,      "$1dot" ],
            [/(unti raport)ti$/i,   "$1tit" ],
            [/(apahtuma)$/i,        "$1t"   ]
        ],
        singular: [
            [/(uistiinpano)t$/i,    "$1"    ],
            [/(hteystie)dot$/i,     "$1to"  ],
            [/(unti raport)tit$/i,  "$1tti" ],
            [/(apahtuma)t$/i,       "$1"    ]
        ],
        irregular: [],
        uncountable: []
    };
})(jQuery);