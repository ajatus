/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};
    
    $.ajatus.i10n.currency = {
        name: 'Forint',
        symbol: 'Ft'
    };

    $.ajatus.i10n.datetime = {
        date: 'YMD.',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "hu_HU": "Magyar",
        
        "distributed crm": "Ügyfélkapcsolatok",
        "loading ajatus": "Az Ajatus betöltése folyamatban",
        
        "failed": "sikertelen",
        "ok": "ok",
        "continue": "Tovább",
        "type": "Típus",
        "undo": "Visszafordít",
        "archive": "Archívum",
        "archived": "Archivál",
        "delete": "Töröl",
        "deleted": "Törölve",
        "undelete": "Visszaállít",
        "view": "Nézet",
        "edit": "Modosítás",
        "edit %s": "%s módosítása",
        "save": "Ment",
        "create": "Létrehoz",
        "create %s": "%s létrehozása",
        "cancel": "Mégsem",
        "new": "Új",
        "add field": "Add field",
        "actions": "Feladatok",
        
        "title": "Cím",
        "description": "Leírás",
        "date": "Dátum",
        "firstname": "Keresztnév",
        "lastname": "Vezetéknév",
        "email": "Email",
        "phone": "Telefon",
        "hours": "Órák",
        "amount": "Mennyiség",
        "vat": "ÁFA",
        "start": "Start", // This is bit misleading currently (Means start date)
        "end": "End", // This is bit misleading currently (Means end date)
        "location": "Location",
        
        // Field types
        "wiki": "Wiki",
        "text": "Szöveg",
        "boolean": "Igen/Nem",
        "integer": "Egész szám",
        "selection": "Kiválasztás",
                        
        "global tag": "Cimke",
        "related objects": "Kapcsolodó objektumok",
        "widget": "Widget",
        "select one": "Jelölj ki egyet",
        "edit mode": "Szerkesztés",
        "preview mode": "Előnézet",
        
        "empty results": "Nincsen találat",
        "no %s found": "Az adatbázisban %s nem található.",
                
        // Installer
        "installing": "Telepítés",
        "uninstalling": "Eltávolítás",
        "installing application database": "Az Ajatus adatbázis telepítése",
        "uninstalling application database": "Az Ajatus adatbázis eltávolítása",
        "preparing application database": "Az Ajatus adatbázis előkészítése",
        "installing application tags database": "A cimke adatbázis telepítése",
        "uninstalling application tags database": "A cimke adatbázis eltávolítása",
        "installing application content database": "A tartalom adatbázis telepítése",
        "uninstalling application content database": "A tartalom adatbázis eltávolítása",
        
        // Metadata
        "creator": "Készítő",
        "created": "Elkészítetve",
        "revisor": "Ellenőr",
        "revised": "Ellenőrizve",
        "archiver": "Archiváló",
        "archived": "Archiválva",
        "deleted": "Törölve",
        
        // System views
        "frontpage": "Kezdőlap",
        "trash": "Szemetes",
        "preferences": "Beállítások",
        "tags": "Cimkék",
        "edit": "Szerkeszt",
        "create": "Létrehoz",
        "view item": "Megtekintés",
        
        // Content types
        "note": "Jegyzet",
        "hour report": "Óra jelentés",
        "event": "Esemény",
        "tag": "Cimke",
        "contact": "Kapcsolat",
        "expense report": "Kiadás",

        "hour report for": "Hour report for ",
        "expense report for": "Expense report for ",

        // Document actions
        "unknown document action": "Ismeretlen feladat",
        "unknown action %s requested for object %s!": "Ismeretlen feladat (%s) kérése a(z) %s objektumon!",
        "object deleted": "Objektum törölve",
        "object %s moved to trash.": "A(z) %s objektum a kukába helyezve.",
        "object %s removed from ajatus.": "A(z) %s objektum törölve.",
        "object restored": "Az objektum visszaállítva",
        "object %s restored succesfully.": "A(z) %s objektum sikeresen visszaállítva.",
        "emptying action pool": "Az feladat törlése",
        "running %s pooled actions.": "A(z) %s feladat végrehajtása.",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s doesn't support dynamic creation.",
        "widget %s doesn't support dynamic settings.": "Widget %s doesn't support dynamic settings.",
        "settings": "Settings",
        "edit field %s": "Edit field %s",

        // Strings used in testing        
        "test %s": "Tesztelés %s",
        "%d comment": "%d megjegyzés",
        "%d comment in %d doc": "%d megjegyzés %d dokumentumban"                
    };
    
    $.ajatus.i10n.inflections['hu_HU'] = {
        plural: [
        ],
        singular: [
        ],
        irregular: [
        ],
        uncountable: [
        ],
        ordinalize: function(number) {
            return number + ".";
        }
    };
    
})(jQuery);
