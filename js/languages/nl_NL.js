/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};

    $.ajatus.i10n.currency = {
        name: 'Euro',
        symbol: '€'
    };

    $.ajatus.i10n.datetime = {
        date: 'MDY/',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "nl_NL": "Nederlands",
        
        "distributed crm": "Gedistribueerd CRM",
        "loading ajatus": "Bezig met het laden van Ajatus",

        "failed": "mislukt",
        "ok": "ok",
        "continue": "Doorgaan",
        "type": "Type",
        "undo": "Ongedaan maken",
        "archive": "Archiveren",
        "archive": "Archief",
        "delete": "Verwijderen",
        "deleted": "Verwijderd",
        "undelete": "Terugzetten",
        "view": "Bekijk",
        "view item": "Bekijk onderdeel",
        "edit": "Bewerk",
        "edit %s": "Bewerk %s",
        "save": "Opslaan",
        "cancel": "Ongedaan maken",
        "create": "Maak aan",
        "create %s": "Maak %s aan",
        "add field": "Veld toevoegen",
        "actions": "Acties",
                
        "title": "Titel",
        "description": "Beschrijving",
        "date": "Datum",
        "firstname": "Voornaam",
        "lastname": "Achternaam",
        "email": "Emailadres",
        "phone": "Telefoonnummer",
        "hours": "Uren",
        "start": "Start", // This is bit misleading currently (Means start date)
        "end": "Einde", // This is bit misleading currently (Means end date)
        "location": "Locatie",

        // Field types
        "wiki": "Wiki",
        "text": "Tekst",
        "boolean": "Ja/Nee",
        "integer": "Getal",
        "selection": "Selectie",
                                                
        "global tag": "Globale Tag",
        "related objects": "Gerelateerde objecten",
        "widget": "Widget",
        "select one": "Selecteer een",
        "edit mode": "Edit",
        "preview mode": "Preview",
        
        "empty results": "Niets gevonden",
        "no %s found": "Geen %s gevonden.",
        
        // Installer
        "installing": "Bezig met installeren",
        "uninstalling": "Bezig met verwijderen installatie",
        "installing application database": "Bezig met het installeren van de applicatie database",
        "uninstalling application database": "Bezig met het verwijderen van de applicatie database",
        "preparing application database": "Voorbereiden van applicatie database",
        "installing application tags database": "Bezig met installeren van de applicatie tags database",
        "uninstalling application tags database": "Bezig met het verwijderen van de applicatie tags database",
        "installing application content database": "Bezig met het installeren van de applicatie content database",
        "uninstalling application content database": "Bezig met het verwijderen van de applicatie content database",
        
        // Metadata
        "creator": "Auteur",
        "created": "Aangemaakt",
        "revisor": "Aangepast door",
        "revised": "Aangepast",
        "archiver": "Archiveerder",
        "archived": "Gearchiveerd",
        "deleted": "Verwijderd",
        
        // System views
        "frontpage": "Voorpagina",
        "trash": "Vuilnisbak",
        "preferences": "Voorkeuren",
        "tags": "Tags",
        
        // Content types
        "note": "Opmerking",
        "hour report": "Urenverslag",
        "event": "Gebeurtenis",
        "tag": "Tag",
        "contact": "Contactpersoon",
        "expense report": "Uitgavenverslag",

        "hour report for": "Urenverslag voor ",
        "expense report for": "Uitgavenverslag voor ",
                
        // Document actions
        "unknown document action": "Onbekende actie voor document",
        "unknown action %s requested for object %s!": "Onbekende actie %s aangevraagd voor object %s!",
        "object deleted": "Object is verwijderd",
        "object %s moved to trash.": "Object %s is naar de vuilnisbak verplaats.",
        "object %s removed from ajatus.": "Object %s is verwijderd uit Ajatus.",
        "object restored": "Object teruggezet",
        "object %s restored succesfully.": "Object %s is teruggezet.",
        "emptying action pool": "Bezig met het leegmaken van de actie pool",
        "running %s pooled actions.": "Bezig met het draaien van %s acties in deze pool.",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s ondersteunt geen dynamische aanmaak.",
        "widget %s doesn't support dynamic settings.": "Widget %s ondersteunt geen dynamische instellingen.",
        "settings": "Instellingen",
        "edit field %s": "Bewerk veld %s",

        // Strings used in testing
        "test %s": "Bezig met het testen van %s",
        "%d comment": "%d opmerking",
        "%d comment in %d doc": "%d opmerking in %d doc"
    };

    $.ajatus.i10n.inflections['nl_NL'] = {
        plural: [
            [/$/,                      "en"      ]
        ],
        singular: [],
        irregular: [],
        uncountable: [],
        ordinalize: function(number) {
            if (20 <= parseInt(number) % 100) {
                return number + "ste";
            } else {
                switch (parseInt(number)) {
                    case  1:
                        return number + "ste";
                    case  8:
                        return number + "ste";
                    default:
                        return number + "de";
                }
            }
        }
    };

})(jQuery);
