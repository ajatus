/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.i10n = $.ajatus.i10n || {};

    $.ajatus.i10n.currency = {
        name: 'Euro',
        symbol: '€'
    };

    $.ajatus.i10n.datetime = {
        date: 'DMY.',
        time: 'HMS:'
    };

    $.ajatus.i10n.dict = {
        "sv_SE": "Sverige",
        
        "distributed crm": "Distribuerad CRM",
        "loading ajatus": "Laddar Ajatus",

        "failed": "Misslyckades",
        "ok": "Ok",
        "continue": "Fortsätt",
        "type": "Mata in",
        "undo": "Ångra",
        "archive": "Arkiv",
        "archive": "Arkivera",
        "delete": "Ta bort",
        "deleted": "Borttaget",
        "undelete": "Ångra borttagning",
        "view": "Visa",
        "view item": "Visa sak",
        "edit": "Redigera",
        "edit %s": "Redigera %s",
        "save": "Spara",
        "cancel": "Cancel",
        "create": "Skapa",
        "create %s": "Skapa %s",
        "add field": "Add field",
        "actions": "Funktioner",
        
        "title": "Titel",
        "description": "Beskrivning",
        "date": "Datum",
        "firstname": "Förnamn",
        "lastname": "Efternamn",
        "email": "E-post",
        "phone": "Telefon",
        "hours": "Timmar",
        "amount": "Amount",
        "vat": "VAT",
        "start": "Start", // This is bit misleading currently (Means start date)
        "end": "End", // This is bit misleading currently (Means end date)
        "location": "Location",
                                                
        "global tag": "Global tagg",
        "related objects": "Relaterade objekt",
        "widget": "Widget",
        "select one": "Select one",
        "edit mode": "Edit",
        "preview mode": "Preview",
        
        "empty results": "Tomma resultat",
        "no %s found": "Kunde inte finna %s.",
        
        // Installer
        "installing": "Installera",
        "uninstalling": "Avistallera",
        "installing application database": "Installera programdatabas",
        "uninstalling application database": "Avistallera programdatabas",
        "preparing application database": "Förbereder programdatabas",
        "installing application tags database": "Installera programdatabastaggar",
        "uninstalling application tags database": "Avinstallera programdatabastaggar",
        "installing application content database": "Installera programdatabasinnehåll",
        "uninstalling application content database": "Installera programdatabasinnehåll",
        
        // Metadata
        "creator": "Skapare",
        "created": "Skapad",
        "revisor": "Muokkaaja",
        "revised": "Muokattu",
        "archiver": "Paketerad av",
        "archived": "Paketerad",
        "deleted": "Borttagen",
        
        // System views
        "frontpage": "framsida",
        "trash": "papperskorg",
        "preferences": "inställningar",
        "tags": "taggar",
        "view item": "View item",
        
        // Content types
        "note": "Anteckning",
        "hour report": "Timrapport",
        "event": "Händelse",
        "tag": "Tagg",
        "contact": "Kontakt",
        "expense report": "Expense report",
        
        "hour report for": "Hour report for ",
        "expense report for": "Expense report for ",
        
        // Document actions
        "unknown document action": "Okänd dokumentfunktion",
        "unknown action %s requested for object %s!": "Okänd funktion %s begärd för %s!",
        "object deleted": "Objektet borttaget",
        "object %s moved to trash.": "Objektet %s borttaget från papperskorgen.",
        "object %s removed from ajatus.": "Objektet %s borttaget från Ajatus.",
        "object restored": "Objektet återställt",
        "object %s restored succesfully.": "Objektet %s framgångsrikt återställt.",
        "emptying action pool": "Tömmer händelsepool",
        "running %s pooled actions.": "Kör %s poolade händelser.",

        // Add field -widget titles and messages
        "widget %s doesn't support dynamic creation.": "Widget %s doesn't support dynamic creation.",
        "widget %s doesn't support dynamic settings.": "Widget %s doesn't support dynamic settings.",
        "settings": "Settings",
        "edit field %s": "Edit field %s",

        // Strings used in testing
        "test %s": "testa %s",
        "%d comment": "%d kommentar",
        "%d comment in %d doc": "%d kommentar i %d"
    };
    
    $.ajatus.i10n.inflections['fi_FI'] = {
        plural: [],
        singular: [],
        irregular: [],
        uncountable: []
    };
})(jQuery);
