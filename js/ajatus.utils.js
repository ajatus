/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.utils = {        
    };
    
    $.ajatus.utils.merge_configs = function(a,b) {
        var c = {};
        
        if (typeof a == 'undefined') {
            return c;
        }        
        if (typeof b == 'undefined') {
            var b = {};
        }
        
        for (var ak in a) {
            if (typeof a[ak] != 'object') {
                c[ak] = a[ak];
                if (   typeof b[ak] != 'undefined'
                    && typeof b[ak] != 'object')
                {
                    c[ak] = b[ak];
                }
            } else {
                if (typeof b[ak] == 'undefined') {
                    c[ak] = $.ajatus.utils.merge_configs(a[ak], {});
                } else {
                    c[ak] = $.ajatus.utils.merge_configs(a[ak], b[ak]);
                }                
            }
        }
        
        return c;
    };
    
    $.ajatus.utils.version = {
        differs: function(v1, v2) {
            var differs = false;
            for (var i=0; i<3; i++) {
                if (v1[i] != v2[i]) {
                    differs = true;
                }
            }
            
            return differs;
        }
    };
    
    $.ajatus.utils.to_boolean = function(value) {
        if (typeof value == 'boolean') {
            return value;
        }
        
        if (typeof value == 'string') {
            if (value === 'true') {
                return true;
            } else if (value === '1') {
                return true;
            }
            return false;
        }

        if (typeof value == 'number') {
            if (value === 1) {
                return true;
            }
            return false;
        }
        
        return false;
    };

    /**
     * Renders pretty printed version from given value
     * Original pretty_print function by Damien Katz <damien_katz@yahoo.com>
     * Modified to work with Ajatus by Jerry Jalava <jerry.jalava@gmail.com>
     * @param {Mixed} val Value to render
     * @param {Number} indent Current indent level (Default: 4)
     * @param {String} linesep Line break to be used (Default: "\n")
     * @param {Number} depth Current line depth (Default: 1)
     * @returns Pretty printed value
     * @type String
     */
    $.ajatus.utils.pretty_print = function(val, indent, linesep, depth) {
        var indent = typeof indent != 'undefined' ? indent : 4;
        var linesep = typeof linesep != 'undefined' ? linesep : "\n";
        var depth = typeof depth != 'undefined' ? depth : 1;
        
        var propsep = linesep.length ? "," + linesep : ", ";

        var tab = [];

        for (var i = 0; i < indent * depth; i++) {
            tab.push("")
        };
        tab = tab.join(" ");

        switch (typeof val) {
            case "boolean":
            case "number":
            case "string":
                return $.ajatus.converter.toJSON(val);
            case "object":
                if (val === null) {
                    return "null";
                }
                if (val.constructor == Date) {
                    return $.ajatus.converter.toJSON(val);
                }

                var buf = [];
                if (val.constructor == Array) {
                    buf.push("[");
                    for (var index = 0; index < val.length; index++) {
                        buf.push(index > 0 ? propsep : linesep);
                        buf.push(
                            tab, $.ajatus.utils.pretty_print(val[index], indent, linesep, depth + 1)
                        );
                    }
                    
                    if (index >= 0) {
                        buf.push(linesep, tab.substr(indent))
                    };
                    
                    buf.push("]");
                } else {
                    buf.push("{");
                    var index = 0;
                    for (var key in val) {                        
                        if (! val.hasOwnProperty(key)) {
                            continue;
                        };
                        
                        buf.push(index > 0 ? propsep : linesep);
                        buf.push(
                            tab, $.ajatus.converter.toJSON(key), ": ",
                            $.ajatus.utils.pretty_print(val[key], indent, linesep, depth + 1)
                        );
                        index++;
                    }
                    
                    if (index >= 0) {
                        buf.push(linesep, tab.substr(indent));
                    };
                    
                    buf.push("}");
                }
                
                return buf.join("");
            break;
        }
    };

    /**
     * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
     * Digest Algorithm, as defined in RFC 1321.
     * Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
     * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
     * Distributed under the BSD License
     * See http://pajhome.org.uk/crypt/md5 for more info.
     *
     * Modified to work with Ajatus by Jerry Jalava <jerry.jalava@gmail.com>
     * @param {Mixed} val Value to render
     * @param {Number} indent Current indent level (Default: 4)
     * @param {String} linesep Line break to be used (Default: "\n")
     * @param {Number} depth Current line depth (Default: 1)
     * @returns Pretty printed value
     * @type String
     */
    $.ajatus.utils.md5 = {
        _hexcase: 0,  /* hex output format. 0 - lowercase; 1 - uppercase */
        _b64pad: "", /* base-64 pad character. "=" for strict RFC compliance */
        _chrsz: 8,  /* bits per input character. 8 - ASCII; 16 - Unicode */
        
        /*
         * Calculate the MD5 of an array of little-endian words, and a bit length
         */
        _core_md5: function(x, len) {
            /* append padding */
            x[len >> 5] |= 0x80 << ((len) % 32);
            x[(((len + 64) >>> 9) << 4) + 14] = len;

            var a =  1732584193;
            var b = -271733879;
            var c = -1732584194;
            var d =  271733878;

            for (var i = 0; i < x.length; i += 16) {
                var olda = a;
                var oldb = b;
                var oldc = c;
                var oldd = d;

                a = $.ajatus.utils.md5._md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
                d = $.ajatus.utils.md5._md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
                c = $.ajatus.utils.md5._md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
                b = $.ajatus.utils.md5._md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
                a = $.ajatus.utils.md5._md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
                d = $.ajatus.utils.md5._md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
                c = $.ajatus.utils.md5._md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
                b = $.ajatus.utils.md5._md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
                a = $.ajatus.utils.md5._md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
                d = $.ajatus.utils.md5._md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
                c = $.ajatus.utils.md5._md5_ff(c, d, a, b, x[i+10], 17, -42063);
                b = $.ajatus.utils.md5._md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
                a = $.ajatus.utils.md5._md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
                d = $.ajatus.utils.md5._md5_ff(d, a, b, c, x[i+13], 12, -40341101);
                c = $.ajatus.utils.md5._md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
                b = $.ajatus.utils.md5._md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

                a = $.ajatus.utils.md5._md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
                d = $.ajatus.utils.md5._md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
                c = $.ajatus.utils.md5._md5_gg(c, d, a, b, x[i+11], 14,  643717713);
                b = $.ajatus.utils.md5._md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
                a = $.ajatus.utils.md5._md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
                d = $.ajatus.utils.md5._md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
                c = $.ajatus.utils.md5._md5_gg(c, d, a, b, x[i+15], 14, -660478335);
                b = $.ajatus.utils.md5._md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
                a = $.ajatus.utils.md5._md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
                d = $.ajatus.utils.md5._md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
                c = $.ajatus.utils.md5._md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
                b = $.ajatus.utils.md5._md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
                a = $.ajatus.utils.md5._md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
                d = $.ajatus.utils.md5._md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
                c = $.ajatus.utils.md5._md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
                b = $.ajatus.utils.md5._md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

                a = $.ajatus.utils.md5._md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
                d = $.ajatus.utils.md5._md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
                c = $.ajatus.utils.md5._md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
                b = $.ajatus.utils.md5._md5_hh(b, c, d, a, x[i+14], 23, -35309556);
                a = $.ajatus.utils.md5._md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
                d = $.ajatus.utils.md5._md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
                c = $.ajatus.utils.md5._md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
                b = $.ajatus.utils.md5._md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
                a = $.ajatus.utils.md5._md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
                d = $.ajatus.utils.md5._md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
                c = $.ajatus.utils.md5._md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
                b = $.ajatus.utils.md5._md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
                a = $.ajatus.utils.md5._md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
                d = $.ajatus.utils.md5._md5_hh(d, a, b, c, x[i+12], 11, -421815835);
                c = $.ajatus.utils.md5._md5_hh(c, d, a, b, x[i+15], 16,  530742520);
                b = $.ajatus.utils.md5._md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

                a = $.ajatus.utils.md5._md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
                d = $.ajatus.utils.md5._md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
                c = $.ajatus.utils.md5._md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
                b = $.ajatus.utils.md5._md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
                a = $.ajatus.utils.md5._md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
                d = $.ajatus.utils.md5._md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
                c = $.ajatus.utils.md5._md5_ii(c, d, a, b, x[i+10], 15, -1051523);
                b = $.ajatus.utils.md5._md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
                a = $.ajatus.utils.md5._md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
                d = $.ajatus.utils.md5._md5_ii(d, a, b, c, x[i+15], 10, -30611744);
                c = $.ajatus.utils.md5._md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
                b = $.ajatus.utils.md5._md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
                a = $.ajatus.utils.md5._md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
                d = $.ajatus.utils.md5._md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
                c = $.ajatus.utils.md5._md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
                b = $.ajatus.utils.md5._md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

                a = $.ajatus.utils.md5._safe_add(a, olda);
                b = $.ajatus.utils.md5._safe_add(b, oldb);
                c = $.ajatus.utils.md5._safe_add(c, oldc);
                d = $.ajatus.utils.md5._safe_add(d, oldd);
            }
          
            return Array(a, b, c, d);
        },
        
        /*
         * These functions implement the four basic operations the algorithm uses.
         */
        _md5_cmn: function(q, a, b, x, s, t) {
            return $.ajatus.utils.md5._safe_add($.ajatus.utils.md5._bit_rol($.ajatus.utils.md5._safe_add($.ajatus.utils.md5._safe_add(a, q), $.ajatus.utils.md5._safe_add(x, t)), s),b);
        },
        _md5_ff: function(a, b, c, d, x, s, t) {
            return $.ajatus.utils.md5._md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
        },
        _md5_gg: function(a, b, c, d, x, s, t) {
            return $.ajatus.utils.md5._md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
        },
        _md5_hh: function(a, b, c, d, x, s, t) {
            return $.ajatus.utils.md5._md5_cmn(b ^ c ^ d, a, b, x, s, t);
        },
        _md5_ii: function(a, b, c, d, x, s, t) {
            return $.ajatus.utils.md5._md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
        },
        
        /*
         * Calculate the HMAC-MD5, of a key and some data
         */
        _core_hmac_md5: function(key, data) {
            var bkey = $.ajatus.utils.md5._str2binl(key);
            if(bkey.length > 16) bkey = $.ajatus.utils.md5._core_md5(bkey, key.length * $.ajatus.utils.md5._chrsz);

            var ipad = Array(16), opad = Array(16);
            for(var i = 0; i < 16; i++) {
                ipad[i] = bkey[i] ^ 0x36363636;
                opad[i] = bkey[i] ^ 0x5C5C5C5C;
            }

            var hash = $.ajatus.utils.md5._core_md5(ipad.concat($.ajatus.utils.md5._str2binl(data)), 512 + data.length * $.ajatus.utils.md5._chrsz);

            return $.ajatus.utils.md5._core_md5(opad.concat(hash), 512 + 128);
        },
        
        /*
         * Add integers, wrapping at 2^32. This uses 16-bit operations internally
         * to work around bugs in some JS interpreters.
         */
        _safe_add: function(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        },

        /*
         * Bitwise rotate a 32-bit number to the left.
         */
        _bit_rol: function(num, cnt) {
            return (num << cnt) | (num >>> (32 - cnt));
        },

        /*
         * Convert a string to an array of little-endian words
         * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
         */
        _str2binl: function(str) {
            var bin = Array();
            var mask = (1 << $.ajatus.utils.md5._chrsz) - 1;
            for (var i = 0; i < str.length * $.ajatus.utils.md5._chrsz; i += $.ajatus.utils.md5._chrsz) {
                bin[i>>5] |= (str.charCodeAt(i / $.ajatus.utils.md5._chrsz) & mask) << (i%32);
            }
            return bin;
        },

        /*
         * Convert an array of little-endian words to a string
         */
        _binl2str: function(bin) {
            var str = "";
            var mask = (1 << $.ajatus.utils.md5._chrsz) - 1;
            for(var i = 0; i < bin.length * 32; i += $.ajatus.utils.md5._chrsz) {
                str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);                
            }
            return str;
        },

        /*
         * Convert an array of little-endian words to a hex string.
         */
        _binl2hex: function(binarray) {
            var hex_tab = $.ajatus.utils.md5._hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
            var str = "";
            for (var i = 0; i < binarray.length * 4; i++) {
                str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) + hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
            }
            return str;
        },

        /*
         * Convert an array of little-endian words to a base-64 string
         */
        _binl2b64: function(binarray) {
            var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            var str = "";
            for (var i = 0; i < binarray.length * 4; i += 3) {
                var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16) | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 ) | ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
                for (var j = 0; j < 4; j++) {
                    if (i * 8 + j * 6 > binarray.length * 32) {
                        str += $.ajatus.utils.md5._b64pad;
                    } else {
                        str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
                    }
                }
            }
            return str;
        },
        
        _hex_md5: function(s) {
            return $.ajatus.utils.md5._binl2hex($.ajatus.utils.md5._core_md5($.ajatus.utils.md5._str2binl(s), s.length * $.ajatus.utils.md5._chrsz));
        },
        _b64_md5: function(s) {
            return $.ajatus.utils.md5._binl2b64($.ajatus.utils.md5._core_md5($.ajatus.utils.md5._str2binl(s), s.length * $.ajatus.utils.md5._chrsz));
        },
        _str_md5: function(s) {
            return $.ajatus.utils.md5._binl2str($.ajatus.utils.md5._core_md5($.ajatus.utils.md5._str2binl(s), s.length * $.ajatus.utils.md5._chrsz));
        },
        _hex_hmac_md5: function(key, data) {
            return $.ajatus.utils.md5._binl2hex($.ajatus.utils.md5._core_hmac_md5(key, data));
        },
        _b64_hmac_md5: function(key, data) {
            return $.ajatus.utils.md5._binl2b64($.ajatus.utils.md5._core_hmac_md5(key, data));
        },
        _str_hmac_md5: function(key, data) {
            return $.ajatus.utils.md5._binl2str($.ajatus.utils.md5._core_hmac_md5(key, data));
        },
        
        encode: function(string, encoding, data) {
            var encoded_string = '';
            if (typeof encoding == 'undefined') {
                var encoding = 'hex';
            }
            
            switch(encoding) {
                case 'b64':
                    encoded_string = $.ajatus.utils.md5._b64_md5(string);
                break;
                case 'str':
                    encoded_string = $.ajatus.utils.md5._str_md5(string);
                break;
                case 'hex_hmac':
                    encoded_string = $.ajatus.utils.md5._hex_hmac_md5(string, data);
                break;
                case 'b64_hmac':
                    encoded_string = $.ajatus.utils.md5._b64_hmac_md5(string, data);
                break;
                case 'str_hmac':
                    encoded_string = $.ajatus.utils.md5._str_hmac_md5(string, data);
                break;
                case 'hex':
                default:
                    encoded_string = $.ajatus.utils.md5._hex_md5(string);
                break;
            }
            
            return encoded_string;
        },
        match: function(string, md5, encoding) {
            var encoded_string = $.ajatus.utils.md5.encode(string, encoding);
            
            if (encoded_string === md5) {
                return true;
            }
            
            return false;
        }
    };
    
    $.ajatus.utils.base64 = {
        END_OF_INPUT: -1,
        base64Chars: [
            'A','B','C','D','E','F','G','H',
            'I','J','K','L','M','N','O','P',
            'Q','R','S','T','U','V','W','X',
            'Y','Z','a','b','c','d','e','f',
            'g','h','i','j','k','l','m','n',
            'o','p','q','r','s','t','u','v',
            'w','x','y','z','0','1','2','3',
            '4','5','6','7','8','9','+','/'
        ],
        
        reverseBase64Chars: null,

        encode: function(value) {
            var base64Str;
            var base64Count;
            
            function setBase64Str(str) {
                base64Str = str;
                base64Count = 0;
            }
            
            function readBase64() {
                if (! base64Str) {
                    return $.ajatus.utils.base64.END_OF_INPUT;
                }
                if (base64Count >= base64Str.length) {
                    return $.ajatus.utils.base64.END_OF_INPUT;
                }
                var c = base64Str.charCodeAt(base64Count) & 0xff;
                base64Count++;
                
                return c;
            }

            setBase64Str(value);
            var result = '';
            var inBuffer = new Array(3);
            var lineCount = 0;
            var done = false;
            while (!done && (inBuffer[0] = readBase64()) != $.ajatus.utils.base64.END_OF_INPUT) {
                inBuffer[1] = readBase64();
                inBuffer[2] = readBase64();
                result += ($.ajatus.utils.base64.base64Chars[ inBuffer[0] >> 2 ]);
                if (inBuffer[1] != $.ajatus.utils.base64.END_OF_INPUT) {
                    result += ($.ajatus.utils.base64.base64Chars[((inBuffer[0] << 4) & 0x30) | (inBuffer[1] >> 4)]);
                    if (inBuffer[2] != $.ajatus.utils.base64.END_OF_INPUT) {
                        result += ($.ajatus.utils.base64.base64Chars[((inBuffer[1] << 2) & 0x3c) | (inBuffer[2] >> 6) ]);
                        result += ($.ajatus.utils.base64.base64Chars[inBuffer[2] & 0x3F]);
                    } else {
                        result += ($.ajatus.utils.base64.base64Chars[((inBuffer[1] << 2) & 0x3c)]);
                        result += ('=');
                        done = true;
                    }
                } else {
                    result += ($.ajatus.utils.base64.base64Chars[((inBuffer[0] << 4) & 0x30)]);
                    result += ('=');
                    result += ('=');
                    done = true;
                }
                lineCount += 4;
                if (lineCount >= 76) {
                    result += ('\n');
                    lineCount = 0;
                }
            }
            
            return result;
        },
        decode: function(value) {
            var base64Str;
            var base64Count;
            
            if ($.ajatus.utils.base64.reverseBase64Chars == null) {
                $.ajatus.utils.base64.reverseBase64Chars = [];
                for (var i=0; i < $.ajatus.utils.base64.base64Chars.length; i++) {
                    $.ajatus.utils.base64.reverseBase64Chars[$.ajatus.utils.base64.base64Chars[i]] = i;
                }                
            }
            
            function setBase64Str(str) {
                base64Str = str;
                base64Count = 0;
            }

            function readReverseBase64() {   
                if (! base64Str) {
                    return $.ajatus.utils.base64.END_OF_INPUT;
                }
                
                while (true) {
                    if (base64Count >= base64Str.length) {
                        return $.ajatus.utils.base64.END_OF_INPUT;
                    }
                    var nextCharacter = base64Str.charAt(base64Count);
                    base64Count++;
                    if ($.ajatus.utils.base64.reverseBase64Chars[nextCharacter]){
                        return $.ajatus.utils.base64.reverseBase64Chars[nextCharacter];
                    }
                    if (nextCharacter == 'A') {
                        return 0;
                    }
                }
                return $.ajatus.utils.base64.END_OF_INPUT;
            }

            function ntos(n) {
                n = n.toString(16);
                if (n.length == 1) {
                    n = "0" + n;
                }
                n = "%" + n;
                
                return unescape(n);
            }
            
            setBase64Str(value);
            var result = "";
            var inBuffer = new Array(4);
            var done = false;
            while (!done && (inBuffer[0] = readReverseBase64()) != $.ajatus.utils.base64.END_OF_INPUT
                && (inBuffer[1] = readReverseBase64()) != $.ajatus.utils.base64.END_OF_INPUT)
            {
                inBuffer[2] = readReverseBase64();
                inBuffer[3] = readReverseBase64();
                result += ntos((((inBuffer[0] << 2) & 0xff)| inBuffer[1] >> 4));
                if (inBuffer[2] != $.ajatus.utils.base64.END_OF_INPUT){
                    result +=  ntos((((inBuffer[1] << 4) & 0xff)| inBuffer[2] >> 2));
                    if (inBuffer[3] != $.ajatus.utils.base64.END_OF_INPUT){
                        result +=  ntos((((inBuffer[2] << 6)  & 0xff) | inBuffer[3]));
                    } else {
                        done = true;
                    }
                } else {
                    done = true;
                }
            }
            
            return result;            
        }
    };
    
    /**
     *
     * UTF-8 data encode / decode
     * http://www.webtoolkit.info/
     *
     * @returns Encoded/Decoded string
     * @type String
     */
    $.ajatus.utils.utf8 = {
        encode: function(string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";

            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if (   (c > 127)
                           && (c < 2048))
                {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }

            return utftext;
        },
        decode: function(utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;

            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);

                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if (   (c > 191)
                           && (c < 224))
                {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }

            return string;
        }
    };
    
    $.ajatus.utils.array = {
        has_match: function(needles, haystack) {
            if (   typeof haystack != 'object'
                || haystack.length <= 0)
            {
                return false;
            }

            if (typeof needles == 'object') {
                var matched = false;
                $.each(needles, function(i,needle){
                    if ($.inArray(needle, haystack) != -1) {
                        matched = true;
                    }
                    // if (typeof(haystack[needle]) != 'undefined') {
                    //                         matched = true;
                    //                     }
                });
                return matched;
            } else if (typeof needles == 'string') {
                var matched = false;
                if ($.inArray(needles, haystack) != -1) {
                    matched = true;
                }
                return matched;
            }
            
            return false;
        },
        copy: function(source) {
            var cp = [];
            
            for (var k in source) {
                cp [k] = source[k];
            }
            
            return cp;
        }
    };
    
    $.ajatus.utils.object = {
        clone: function(obj) {
            if(obj == null || typeof(obj) != 'object') {
                return obj;                
            }
            var temp = {};
            for(var key in obj) {
                temp[key] = $.ajatus.utils.object.clone(obj[key]);                
            }

            return temp;
        }
    };
    
    $.ajatus.utils.pause = function(ms) {
        var date = new Date();
        var currDate = null;

        do { currDate = new Date(); }
        while(currDate - date < ms);
    };
        
    $.ajatus.utils._random_key = 0;
    $.ajatus.utils.generate_id = function() {
        $.ajatus.utils._random_key += 1;
        
        var date = new Date();
        var random_key = Math.floor(Math.random()*4013);
        var random_key2 = Math.floor(Math.random()*3104);
        
        return $.ajatus.utils.md5.encode(Math.floor(((date.getTime()/1000)+random_key2) + (10016486 + (random_key * 22423)) * random_key / random_key2).toString() + "_" + $.ajatus.utils._random_key).toString().substr(0,8);
    };
    
    $.ajatus.utils.get_url_arg = function(name) {
        var value = null;
        
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var exp = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( exp );
        var results = regex.exec(window.location.href);
        if (results != null) {
            value = results[1];
        }
        
        return value;
    };
    
    $.ajatus.utils.load_script = function(url, callback, cb_args) {
        $('head').append('<script type="text/javascript" charset="utf-8" src="'+url+'"></script>');
        if (typeof callback == 'string') {
            if (   typeof cb_args == 'undefined'
                || typeof cb_args != 'object')
            {
                var cb_args = [];
            }
            
            setTimeout('eval("var fn = eval('+callback+'); fn.apply(fn, [\''+cb_args.join("','")+'\']);")', 300);
        }
    };
    
    $.ajatus.utils.doc_generator = function(prefix, count, type, data, value_tpl) {
        var doc = {
            _id: '',
            value: {
            }
        };
        if (typeof value_tpl != 'undefined') {
            doc.value = value_tpl;
        }
        if (typeof type == 'undefined') {
            var type = $.ajatus.preferences.client.content_types['note'];
        }
        if (typeof data == 'undefined') {
            var data = {
            }
        }
        
        if (   typeof type == 'string'
            && typeof $.ajatus.preferences.client.content_types[type] != 'undefined')
        {
            type = $.ajatus.preferences.client.content_types[type];
        }
        
        doc.value._type = type.name;
                
        $.each(type.original_schema, function(i,n){
            doc.value[i] = {
                val: n.def_val,
                widget: n.widget
            };
        });
        
        var docs = [];
        
        if (typeof prefix == 'undefined') {
            var prefix = 'ajatus';
        }
        if (typeof count == 'undefined') {
            count = 10;
        }
        
        function prepare_title(title, id) {
            var full_title = title + " ";
            full_title += (id).toString();
            return full_title;
        }
        
        for (var i=0; i<count; i++) {
    	    var new_doc = new $.ajatus.document(doc);
            
            if (   typeof data.title == 'undefined'
                && typeof new_doc.value['title'] != 'undefined')
            {
                new_doc.value['title'].val = $.ajatus.i10n.get("Generated doc title (%s)", [prefix]) + " " + i;
            }
            
            $.each(data, function(x,n){
                if (typeof new_doc.value[x] != 'undefined') {
                    new_doc.value[x].val = n;
                }
            });

    	    var now = $.ajatus.formatter.date.js_to_iso8601(new Date());

    	    var new_metadata = {
    	        created: now,
    	        creator: $.ajatus.preferences.local.user.email,
    	        revised: now,
    	        revisor: $.ajatus.preferences.local.user.email
    	    };

    	    new_doc = $.ajatus.document.modify_metadata(new_doc, new_metadata);
            
            new_doc._id = prefix + "_gen_doc_" + (i).toString();
            docs.push($.ajatus.utils.object.clone(new_doc));
        }
        
        return docs;
    };

    // Put focus on given form element (0 == first) (skips hidden fields)
    $.fn.set_focus_on = function(eid) {
        var elem = $('input:visible:not(:hidden)', this).get(eid);
        var select = $('select:visible', this).get(eid);

        if (select && elem) {
            if (select.offsetTop < elem.offsetTop) {
                elem = select;
            }
        }

        var textarea = $('textarea:visible', this).get(eid);
        if (textarea && elem) {
            if (textarea.offsetTop < elem.offsetTop) {
                elem = textarea;
            }
        }

        if (elem) {
            elem.focus();
        }

        return this;
    };
    
    /**
     * Following functions are taken from the jquery form plugin.
     * Plugin can be found from http://www.malsup.com/jquery/form/
     */
    $.fieldValue = function(el, successful) {
        var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
        if (typeof successful == 'undefined') successful = true;

        if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
            (t == 'checkbox' || t == 'radio') && !el.checked ||
            (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
            tag == 'select' && el.selectedIndex == -1))
                return null;

        if (tag == 'select') {
            var index = el.selectedIndex;
            if (index < 0) return null;
            var a = [], ops = el.options;
            var one = (t == 'select-one');
            var max = (one ? index+1 : ops.length);
            for(var i=(one ? index : 0); i < max; i++) {
                var op = ops[i];
                if (op.selected) {
                    // extra pain for IE...
                    var v = $.browser.msie && !(op.attributes['value'].specified) ? op.text : op.value;
                    if (one) return v;
                    a.push(v);
                }
            }
            return a;
        }
        return el.value;
    };
	$.fn.formToArray = function(semantic) {
        var a = [];
        if (this.length == 0) return a;

        var form = this[0];
        var els = semantic ? form.getElementsByTagName('*') : form.elements;
        if (!els) return a;
        for(var i=0, max=els.length; i < max; i++) {
            var el = els[i];
            var n = el.name;
            if (!n) continue;

            if (semantic && form.clk && el.type == "image") {
                // handle image inputs on the fly when semantic == true
                if(!el.disabled && form.clk == el)
                    a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
                continue;
            }

            var v = $.fieldValue(el, true);
            if (v && v.constructor == Array) {
                for(var j=0, jmax=v.length; j < jmax; j++)
                    a.push({name: n, value: v[j]});
            }
            else if (v !== null && typeof v != 'undefined')
                a.push({name: n, value: v});
        }

        if (!semantic && form.clk) {
            // input type=='image' are not found in elements array! handle them here
            var inputs = form.getElementsByTagName("input");
            for(var i=0, max=inputs.length; i < max; i++) {
                var input = inputs[i];
                var n = input.name;
                if(n && !input.disabled && input.type == "image" && form.clk == input)
                    a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
            }
        }
        return a;
    };
    /**
     * Form plugin functions end
     */
    
})(jQuery);