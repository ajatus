/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.widgets = $.ajatus.widgets || {};
        
    $.ajatus.widgets.core = {
        available: [
            'text',
            'wiki',
            'tags',
            'date',
            'boolean',
            'integer',
            'tag',
            'selection'
        ]
    };
    $.ajatus.widgets.loaded_widgets = [];
            
    $.ajatus.widget = function(widget, config)
    {
        if (typeof($.ajatus.widgets[widget]) == 'undefined') {
            $.ajatus.debug("widget '"+widget+"' not installed! Using text widget");
            wdgt = $.ajatus.utils.object.clone($.ajatus.widgets.text);
        } else {
            wdgt = $.ajatus.utils.object.clone($.ajatus.widgets[widget]);
        }
        
        if (typeof wdgt == 'undefined') {
            throw("FATAL: Couldn't init widget "+widget);
        }
        
        if (typeof config != 'undefined') {
            wdgt.set_config(config);            
        }
        
        if (typeof wdgt['required'] == 'undefined') {
            wdgt.required = false;
        }
        if (typeof wdgt['validate'] == 'undefined') {
            wdgt.validate = function(name, value) {
                if (   wdgt.required === true
                    && value == '')
                {
                    return {
                        msg: $.ajatus.i10n.get("Field %s is required", [name])
                    };
                }

                return true;
            };
        }
        
        if (typeof wdgt['has_data_changed'] == 'undefined') {
            wdgt.has_data_changed = function(value, prev_value) {
                if ($.ajatus.utils.md5.encode(value) != $.ajatus.utils.md5.encode(prev_value)) {
                    return true;
                }
                return false;
            }
        }
        
        return wdgt;
    };
    
    $.ajatus.widgets.widget_loaded = function(wdgt, try_count) {
        if (typeof try_count == 'undefined') {
            var try_count = 0;
        }
        
        $.ajatus.widgets[wdgt] = $.ajatus.widgets.core[wdgt];
        
        if (typeof $.ajatus.widgets[wdgt] != 'undefined') {
            $.ajatus.widgets[wdgt].loaded();
            $.ajatus.widgets.loaded_widgets.push(wdgt);
            
             $.ajatus.events.lock_pool.decrease();
        } else {
            try_count += 1;
            if (try_count < 2) {
                setTimeout("$.ajatus.widgets.widget_loaded('"+wdgt+"', "+try_count+");", 200);
            }
        }
    }
    
    $.ajatus.widgets.custom_widget_loaded = function(wdgt, try_count) {
        if (typeof try_count == 'undefined') {
            var try_count = 0;
        }
        
        $.ajatus.widgets[wdgt] = $.ajatus.widgets.custom[wdgt];
        
        if (typeof $.ajatus.widgets[wdgt] != 'undefined') {
            $.ajatus.widgets[wdgt].loaded();
            $.ajatus.widgets.loaded_widgets.push(wdgt);            
        } else {
            try_count += 1;
            if (try_count < 2) {
                setTimeout("$.ajatus.widgets.widget_loaded('"+wdgt+"', "+try_count+");", 200);
            }
        }
        
        $.ajatus.events.lock_pool.decrease();
    }
    
    $.ajatus.widgets.init = function()
    {
        $.ajatus.widgets.loaded_widgets = new Array();
        
        if ($.browser.safari) {
            $.ajatus.widgets.core.available = $.ajatus.widgets.core.available.reverse();
        }
        
        $.each($.ajatus.widgets.core.available, function(i,cw){
            if (   cw == ''
                || typeof $.ajatus.widgets[cw] != 'undefined')
            {
                return;
            }
            
            var widget_url = $.ajatus.preferences.client.application_url + 'js/widgets/'+cw+'.js';
            
            $.ajatus.events.lock_pool.increase();
            
            $.ajatus.utils.load_script(widget_url, "$.ajatus.widgets.widget_loaded", [cw]);
        });
        
        // console.log("Loaded widgets:");
        // console.log($.ajatus.widgets.loaded_widgets);

        if ($.ajatus.preferences.client.custom_widgets.length > 0)
        {
            $.each($.ajatus.preferences.client.custom_widgets, function(i,cw){
                if (   cw == ''
                    || typeof $.ajatus.widgets[cw] != 'undefined')
                {
                    return;
                }
                
                var widget_url = $.ajatus.preferences.client.application_url + 'js/widgets/custom/'+cw+'.js';
            
                $.ajatus.events.lock_pool.increase();
                
                $.ajatus.utils.load_script(widget_url, "$.ajatus.widgets.custom_widget_loaded", [cw]);
            });
        }
    }
    
    $.ajatus.widgets.to_readonly = function(view_type, name, default_value, widget) {
        var tpl = false;
        
        if (typeof widget['get_readonly_tpl'] == 'function') {
            tpl = widget.get_readonly_tpl(view_type, name, default_value);
        } else {
            tpl = [
                'input', { type: 'hidden', name: 'widget['+name+':name]', value: widget.name }, '',
                'input', { type: 'hidden', name: 'widget['+name+':config]', value: $.ajatus.converter.toJSON(widget.settings) }, '',
                'input', { type: 'hidden', name: 'widget['+name+':prev_val]', value: $.ajatus.converter.toJSON(default_value) }, '',
                'input', { type: 'hidden', name: name, value: default_value }, '',
                'span', { className: 'value_prefix' }, $.ajatus.i10n.get(widget.settings.value_prefix),
                'span', { name: name }, default_value.toString(),
                'span', { className: 'value_suffix' }, $.ajatus.i10n.get(widget.settings.value_suffix)
            ];
        }
        
        return tpl;
    }
    
    $.ajatus.widgets.generate_settings_form = function(wdgt_sett, curr_sett)
    {
        var settings = $.extend({}, wdgt_sett, curr_sett);
        var holder = $('<div class="settings_holder"/>');
        var rows = $('<ul/>');
                
        $.each(settings, function(i,s){
            // console.log("i: "+i+" s: ");
            // console.log(s);
            var s_type = typeof(s);
            // console.log('type: '+s_type);
            var wdgt = null;
            var normalized_name = i.toString().replace('_', ' ');

            switch(s_type) {
                case 'boolean':
                    wdgt = $.ajatus.widget('boolean');
                break;
                case 'number':
                    wdgt = $.ajatus.widget('integer');
                break;
                case 'string':
                default:
                    wdgt = $.ajatus.widget('text');
            };
            
            rows.createAppend(
                'li', { id: '_setting_'+i, className: 'row' }, [
                    'label', { id: '_setting_'+i+'_label' }, $.ajatus.i10n.get(normalized_name),
                    'div', { id: '_setting_'+i+'_widget', className: wdgt.name }, wdgt.get_create_tpl(i, s),
                    'br', { className: 'clear_fix' }, ''
                ]
            );            
            wdgt.init($('#_setting_'+i+'_widget',rows), true);
        });
        rows.appendTo(holder);

        $('li:first', rows).addClass('first');
        $('li:last', rows).addClass('last');
        
        return holder;
    }
    
    $.ajatus.widgets.generate_default_details = function(widget, data, name)
    {        
        var holder = $('<div class="widget_details" />');
        
        var saved_settings = $('<div class="saved_settings" />');
        saved_settings.appendTo(holder);

        $.each(widget.settings, function(k,val){
            if (typeof data['widget'] != 'undefined') {
                val = data.widget.config[k];
            }
            var field = $('<input type="hidden" />').attr({
                name: 'config;'+k,
                value: $.ajatus.converter.toJSON(val)
            });
            field.appendTo(saved_settings);
        });
        
        var actions = $('<div class="actions" />');
        var pref_btn = $('<div class="widget_settings_btn"><img src="' + $.ajatus.preferences.client.theme_icons_url + 'preferences.png" alt="' + $.ajatus.i10n.get('Settings') + '" /></div>');            
        pref_btn.appendTo(actions);
        pref_btn.bind('click', function(){
            var prev_vals = {};
            var saved_inputs = $('input', saved_settings);
            if (saved_inputs.length > 0) {
                $.each(saved_inputs, function(i,n){
                    var key = n.name.split(";")[1];
                    prev_vals[key] = $.ajatus.converter.parseJSON(n.value);
                });
            }
            $.ajatus.document.additionals.config(widget, prev_vals);
        });
        
        actions.appendTo(holder);
        
        var field_names = {
            'name': {
                title: $.ajatus.i10n.get('name'),
                type: 'string',
                value: name || '',
                read_only: typeof name == 'undefined' ? false : true,
                required: true
            },
            'label': {
                title: $.ajatus.i10n.get('label'),
                type: 'string',
                value: typeof data.label != 'undefined' ? data.label : '',
                required: true
            },
            'def_val': {
                title: $.ajatus.i10n.get('default value'),
                type: 'string',
                value: typeof data.def_val != 'undefined' ? data.def_val : ''
            },
            'required': {
                title: $.ajatus.i10n.get('required'),
                type: 'boolean',
                value: typeof data.required != 'undefined' ? data.required : false
            }
        };
        
        var fields = $('<ul class="fields_holder" />');
        
        $.each(field_names, function(fn,fd){
            var wdgt = null;
            switch (fd.type) {
                case 'boolean':
                    wdgt = $.ajatus.widget('boolean');
                break;
                case 'integer':
                    wdgt = $.ajatus.widget('integer');
                break;
                case 'string':
                default:
                    wdgt = $.ajatus.widget('text');
            };
            
	        if (   typeof fd.required != 'undefined'
	            && fd.required)
	        {
	            wdgt.required = true;
	        }
            
            var def_val = '';
            if (   typeof fd.value != 'undefined'
                && fd.value != '')
            {
                def_val = fd.value;
            }
            
            var wdgt_tpl = wdgt.get_create_tpl(fn, def_val);
            
	        if ( typeof fd['read_only'] != 'undefined'
	            && fd.read_only == true)
	        {
	            wdgt_tpl = $.ajatus.widgets.to_readonly(fd.type, fn, def_val, wdgt);
	        }
            
            fields.createAppend(
                'li', { id: '_field_'+fn, className: 'row' }, [
                    'label', { id: '_field_'+fn+'_label' }, fd.title + (wdgt.required ? ' *' : ''),
                    'div', { id: '_field_'+fn+'_widget', className: wdgt.name }, wdgt_tpl,
                    'br', { className: 'clear_fix' }, ''
                ]
            );            
            wdgt.init($('#_field_'+fn+'_widget',fields), true);
        });
        
        fields.appendTo(holder);
        
        return holder;
    }
    
    $.ajatus.widgets.generate_default_settings = function(widget, data)
    {
        var holder = $('<div class="widget_settings" />');
        
        var ext_sett = {};
        if (typeof(data) == 'object') {
            ext_sett = data;
        }
        
        var settings = $.ajatus.widgets.generate_settings_form(widget.settings, ext_sett);
        
        settings.appendTo(holder);
        
        return holder;
    }
    
})(jQuery);