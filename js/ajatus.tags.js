/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    /**
     * Holds all tag methods
     * @accessor {String} cache Holds all cached tag names
     * @accessor {String} active Currently active tag name
     * @constructor
     */
    $.ajatus.tags = {
        cache: [],
        used_contexts: [],
        active: ''
    };
    $.extend($.ajatus.tags, {

        /**
         * Initializes the tags
         * @see ajatus.core.init
         * @see ajatus.tags.load
         * @see ajatus.tags.update_app_tag_list
         */
        init: function() {
            $.ajatus.events.lock_pool.increase();
            $.ajatus.tags.load();
            $.ajatus.events.lock_pool.decrease();
            $.ajatus.tags.update_app_tag_list();
            $.ajatus.tags.get_active();
        },
        load: function() {
            $.ajatus.tags.used_contexts = [];
            $.ajatus.tags.cache = [];
            
            var legacy_tags_db = $.ajatus.preferences.client.application_database + '_tags';
            if ($.jqCouch.connection('db').exists(legacy_tags_db)) {
                $.ajatus.events.lock_pool.increase();
                
                var on_success = function(data) {
                    if (data.total_rows == 0) {
                        return data;
                    }
                    
                    $.each(data.rows, function(i,doc){
                        var doc = new $.ajatus.document(doc);
                        if (! $.ajatus.tags.exists(doc.value.title.val, doc.value.title.widget.config.context, doc.value.title.widget.config.value)) {
                            var tag = {
                                value: {
                                    _type: 'tag',
                                    title: {
                                        val: doc.value.title.val,
                                        widget: {
                                            name: 'tag',
                                            config: {
                                                color: (doc.value.title.widget.config.color || '8596b6'),
                                                context: (doc.value.title.widget.config.context || ''),
                                                value: (doc.value.title.widget.config.value || '')
                                            }
                                        }
                                    }
                                }
                            };
                            tag.value.metadata = doc.value.metadata;
                            $.jqCouch.connection('doc').post($.ajatus.preferences.client.content_database, tag);
                        }
                    });
                };
                
                var list_map = function(doc) {
                    if (   doc.value.metadata.archived.val != false
                        && doc.value.metadata.deleted.val != false)
                    {
                        map(doc._id, doc);
                    }
                };
                $.jqCouch.connection('view', on_success).temp(legacy_tags_db, list_map);
                
                $.jqCouch.connection('db').del(legacy_tags_db);                
                $.ajatus.events.lock_pool.decrease();
            }
            
            var on_success = function(data) {
                if (data.total_rows <= 0) {
                    return false;
                }

                $.each(data.rows, function(i,n){
                    if (   typeof(n.value.title.widget.config['context']) != 'undefined'
                        && n.value.title.widget.config['context'] != '')
                    {
                        var found = $.inArray(n.value.title.widget.config['context'], $.ajatus.tags.used_contexts);
                        if (found == -1) {
                            $.ajatus.tags.used_contexts.push(n.value.title.widget.config['context']);
                        }
                    }
                    $.ajatus.tags.cache.push(n);
                });
                
                return data;
            };
            
            $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, function(doc){
                if (   doc.value._type == 'tag'
                    && doc.value.metadata.archived.val == false
                    && doc.value.metadata.deleted.val == false)
                {
                    map(doc._id, doc.value);
                }
            });
        },
        refresh_cache: function() {
            $.ajatus.tags.load();
            $.ajatus.tags.update_cache();
        },
        update_cache: function() {
            $.ajatus.tags.update_app_tag_list();
            if ($.ajatus.tags.active != '') {
                $.ajatus.tags.set_active($.ajatus.tags.active);
            }
        },
        set_active: function(active) {
            var id = active;
            if (typeof active == 'object') {
                id = active.id || active._id;
            }
            $.cookie("ajatus.tags.active", active);
            window.location.reload();
            // $.ajatus.tags.get_active();
        },
        get_active: function() {
            var id = $.cookie("ajatus.tags.active");
            
            if (   typeof id == 'undefined'
                || id == ''
                || id == null)
            {
                return;
            }
            
            var select = $('#application-tags select');
            $('option', select).each(function(i,o){
                if (o.value == id) {
                    o.selected = 'selected';
                }
            });
                        
            if (id == '') {
                $.ajatus.tags.active = '';
                return;
            }
            
            var tag = $.ajatus.tags.get(id);
            if (tag) {                
                $.ajatus.tags.active = tag;
                $.ajatus.events.signals.trigger('active_tag_changed', $.ajatus.tags.active);
            }
        },
        update_app_tag_list: function() {            
            var select = $('#application-tags select');
            select.html('');
            
            var opt_groups = {};                        
            var def_opt_grp = $('<optgroup label="' + $.ajatus.i10n.get('No context') + '" />');
            def_opt_grp.appendTo(select);
            
            $.each($.ajatus.tags.used_contexts, function(i){
                var opt_grp = $('<optgroup label="' + this + '" />');
                opt_grp.prependTo(select);
                opt_groups[this] = opt_grp;
            });
            
            $.each($.ajatus.tags.cache, function(i){
                var title = this.value.title.val;
                if (   typeof this.value.title.widget.config.value != 'undefined'
                    && this.value.title.widget.config.value != '')
                {
                    title += " = " + this.value.title.widget.config.value;
                }
                var opt = $('<option>'+title+'</option>').attr({
                    value: this.id
                });
                
                if (   typeof(this.value.title.widget.config['context']) != 'undefined'
                    && this.value.title.widget.config['context'] != '')
                {
                    var context = this.value.title.widget.config.context;
                    opt.appendTo(opt_groups[context]);
                } else {
                    opt.appendTo(def_opt_grp);
                }
            });
            
            var def = $('<option />').attr({
                value: '',
                selected: 'selected'
            }).html($.ajatus.i10n.get('Global Tag') + '&nbsp;');
            def.prependTo(select);
            
            select.bind('change', function(e){
                $.ajatus.tags.set_active(e.currentTarget.value);
            });
        },
        search: function(query, context, max_res) {
            // console.log("Search tags with: "+query);
            
            var results = [];
            var context_key = false;
            
            if (typeof max_res != 'number') {
                var max_res = 20;
            }

            query = $.ajatus.tags._prepare_query(query);

            var on_success = function(data) {
                if (data.total_rows == 0) {
                    return [];
                }
                
                $.each(data.rows, function(i,n){
                    var doc = new $.ajatus.document(n);
                    results.push(doc);
                });
                
                return data;
            };
            
            var rel_view = 'if (doc.value._type == "tag" ';
            rel_view += '&& doc.value.title.val.match(/\\\\b'+query+'(.*?)\\\\b/i) ';
            if (   typeof context != 'undefined'
                && context != '')
            {
                rel_view += '&& typeof(doc.value.title.widget.config["context"]) != "undefined" ';
                rel_view += '&& doc.value.title.widget.config.context == "+context+" ';
            }
            rel_view += '&& doc.value.metadata.archived.val == false && doc.value.metadata.deleted.val == false){';
            rel_view += 'map( doc._id, doc.value );}';

            $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database,
                "$.ajatus.views.generate('"+rel_view+"')", {
                    count: max_res
                }
            );

            return results;                        
        },
        search_cache: function(query, context, max_res) {
            // console.log("Search tags cache with: "+query);

            if (typeof max_res != 'number') {
                var max_res = 20;
            }
            
            var results = [];
            query = $.ajatus.tags._prepare_query(query);
            var re = new RegExp(query+"(.*?)", "gi");
            
            $.each($.ajatus.tags.cache, function(i, tag){
                if (results.length > max_res) {
                    return;
                }
                
                if (tag.value.title.val.toString().match(re)) {
                    if (   typeof context != 'undefined'
                        && context != '')
                    {
                        if (   typeof(tag.value.title.widget.config['context']) != 'undefined'
                            && tag.value.title.widget.config['context'] == context)
                        {
                            results.push(tag);
                        }
                    } else {
                        results.push(tag);
                    }
                }
            });
            
            return results;
        },
        _prepare_query: function(query) {
            query = query.replace(/[\(|\)|\.|\?|\;|\/]/, '');
            query = query.replace('\\', '');
            query = query.replace('*', '(.*?)');
            
            return query;
        },
        get: function(tag, rev, skip_cache, callback) {
            var result = false;
            if (typeof skip_cache == 'undefined') {
                var skip_cache = false;
            }
            
            if (   typeof rev == 'undefined'
                && !skip_cache)
            {
                $.each($.ajatus.tags.cache, function(i,t){
                    if (t.id == tag) {
                        result = t;
                    }
                });

                if (result) {
                    if (typeof callback == 'function') {
                        var fn = eval('('+callback+')');
                        fn.apply(fn, [result]);
                    }
                    
                    return result;
                }
            }
            
            var on_success = function(data) {
                var doc = new $.ajatus.document(data);
                result = doc;
                result.id = doc._id;
                
                if (typeof callback == 'function') {
                    var fn = eval('('+callback+')');
                    fn.apply(fn, [result]);
                }
                
                return data;
            };

            var args = {};
            if (typeof rev != 'undefined') {
                args['_rev'] = rev;
            }

            $.jqCouch.connection('doc', on_success).get($.ajatus.preferences.client.content_database + '/' + tag, args);
            
            return result;
        },
        exists: function(tag, context, value) {            
            var tag_ids = [];
            var found = false;
            
            var match_tag_id = $.ajatus.utils.md5.encode(context + ':' + tag + '=' + value);
            
            $.each($.ajatus.tags.cache, function(i,t){
                if (t.id == match_tag_id) {
                    found = true;
                } else {
                    // Legacy matching
                    if (t.value.title.val == tag) {
                        found = true;

                        if (   typeof context != 'undefined'
                            && context != '')
                        {
                            if (t.value.title.widget.config.context == context) {
                                found = true;
                            } else {
                                found = false;
                            }
                        }

                        if (   typeof value != 'undefined'
                            && value != '')
                        {
                            if (t.value.title.widget.config.value == value) {
                                found = true;
                            } else {
                                found = false;
                            }
                        }
                    }                    
                }
            });

            return found;
        },        
        create: function(name, data, dc) {
            if (typeof data == 'undefined') {
                var data = {
                    color: '',
                    context: '',
                    value: ''
                };
            }
            if (typeof data == 'string') {
                var data = {
                    color: '',
                    context: data,
                    value: ''
                };
            }
            
            var color = data.color;
            var context = data.context;
            var value = data.value;
            
            if ($.ajatus.tags.exists(name, context, value)) {
                return false;
            }
            
            if (typeof dc == 'undefined') {
                var on_success = function(data) {
                    if (data.ok) {
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('New tag created'),
                            $.ajatus.i10n.get("Created new tag '%s'", [data.value.title.val])
                        );

                        $.ajatus.debug("saved tag: "+data.id);

                        var tag = $.ajatus.tags.get(data.id, data.rev, true);
                        $.ajatus.tags.cache.push(tag);
                    }
                    
                    return data;
                };

                var dc = $.jqCouch.connection('doc', on_success);
                $.ajatus.views.on_change_actions.add('$.ajatus.tags.update_cache();');
            }
            
            var tag_id = $.ajatus.tags.calculate_hash(name, context, value);
            
            var tag = {
                _id: tag_id,
                value: {
                    _type: 'tag',
                    title: {
                        val: name,
                        widget: {
                            name: 'tag',
                            config: {
                                color: (color || '8596b6'),
                                context: (context || ''),
                                value: value
                            }
                        }
                    }
                }
            };
            tag = new $.ajatus.document(tag);
            
            var now = $.ajatus.formatter.date.js_to_iso8601(new Date());
            var new_metadata = {
                created: now,
                creator: $.ajatus.preferences.local.user.email,
                revised: now,
                revisor: $.ajatus.preferences.local.user.email
            };
            tag = $.ajatus.document.modify_metadata(tag, new_metadata);
            dc.save($.ajatus.preferences.client.content_database, tag);
            
            return true;
        },
        remove: function(tag, dc) {
            if (typeof dc == 'undefined') {
                var on_success = function(data) {
                    var msg = $.ajatus.elements.messages.create(
                        $.ajatus.i10n.get('Object deleted'),
                        $.ajatus.i10n.get('Tag %s removed from Ajatus.', [tag.value.title.val])
                    );
                };
                var dc = $.jqCouch.connection('doc', on_success);
            }

            dc.del($.ajatus.preferences.client.content_database + '/' + tag.id);
        },
        related: function(tags, skip_docs) {
            var related = [];

            if (typeof skip_docs != 'object') {
                var skip_docs = [];
            }
            
            if (tags.length == 0) {
                return related;
            }

            var on_success = function(data) {
                if (data.total_rows == 0) {
                    return;
                }
                
                $.each(data.rows, function(i,row){
                    related.push(new $.ajatus.document(row));
                });
                
                return data;
            };
            
            var rel_view = 'if (typeof(doc.value.tags) != "undefined" && doc.value.tags.val.length > 0 ';
            rel_view += '&& doc.value.metadata.archived.val == false && doc.value.metadata.deleted.val == false){';
            
            if (skip_docs.length > 0) {
                rel_view += 'if (';
                for (var i=0; i<skip_docs.length; i++) {
                    rel_view += 'doc._id != "'+skip_docs[i]+'" ';
                    if (i < skip_docs.length-1) {
                        rel_view += '&& ';
                    }
                };
                rel_view += ') {';
            }
            
            rel_view += 'var hm = false;';
            rel_view += 'for (var i=0;i<doc.value.tags.val.length;i++){';
            rel_view += 'if (';
            for (var i=0; i<tags.length; i++) {                                
                rel_view += 'doc.value.tags.val[i] == "'+tags[i]+'" ';
                if (i < tags.length-1) {
                    rel_view += '|| ';
                }
            };
            rel_view += ') {';
            rel_view += 'hm = true;}';
            rel_view += '}';
            
            rel_view += 'if (hm) {';
            
            rel_view += 'map( doc.value._type, {"_id": doc._id, "_rev": doc._rev, "_type": doc.value._type,';
            rel_view += '"title": doc.value.title,';
            
            $.each($.ajatus.preferences.client.content_types, function(key,type){
                if (type['title_item']) {
                    $.each(type['title_item'], function(i,part){
                        if (type.schema[part]) {
                            rel_view += '"'+part+'": doc.value.'+part+',';
                        }
                    });
                }
            });

            rel_view += '"tags": doc.value.tags });';
            
            rel_view += '}';

            if (skip_docs.length > 0) {
                rel_view += '}';
            }
            
            rel_view += '}';
            
            $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, "$.ajatus.views.generate('"+rel_view+"')");
            
            return related;
        },
        render_title: function(tag) {
            var tag_title = '';

            if (typeof tag.value == 'undefined') {
                return tag_title;
            }
            
            if (   typeof(tag.value.title.widget.config['context']) != 'undefined'
                && tag.value.title.widget.config['context'] != '')
            {
                tag_title += tag.value.title.widget.config.context + ':';
            }
            
            tag_title += tag.value.title.val;
            
            if (   typeof(tag.value.title.widget.config['value']) != 'undefined'
                && tag.value.title.widget.config['value'] != '')
            {
                tag_title += '=' + tag.value.title.widget.config.value;
            }
            
            return tag_title;
        },
        calculate_hash: function() { // tag / title, context, value
            var hash = null;
            
            if (arguments.length == 1) {
                var tag = arguments[0];
                hash = $.ajatus.utils.md5.encode(tag.value.title.widget.config.context + ':' + tag.value.title.val + '=' + tag.value.title.widget.config.value);
            } else {
                var title = arguments[0];
                var context = '';
                var value = '';
                if (arguments.length > 1) {
                    context = arguments[1];
                }
                if (arguments.length > 2) {
                    value = arguments[2];
                }
                
                hash = $.ajatus.utils.md5.encode(context + ':' + title + '=' + value);
            }

            return hash;
        },
        validate_title: function(title) {
            var context_key = '';
            var value = '';
            
            if (   !title.match(/:/)
                && !title.match(/\=/))
            {
                return title;
            }
            
            if (title.match(/:/)) {
                var tag_parts = title.split(':');
                context_key = tag_parts[0];
                title = tag_parts[1];
            }
            
            if (title.match(/\=/)) {
                var tag_parts = title.split('=');
                value = tag_parts[1];
                title = tag_parts[0];
            }
            
            var data = {
                title: title,
                context: context_key,
                value: value
            };
            
            return data;
        }
    });
    
    $.ajatus.tags.merger = function(dubs, dubs_with, tags) {
        this.merge_list = {};
        this.merge_count = 0;
        this.button_generated = false;
        this.button_visible = false;
        this.button_holder = null;
        this.dub_rows = {};
        
        this.all_tags = [];
        this.dub_tags = [];
        this.dub_tags_with = {};
        
        this.init(dubs, dubs_with, tags);
    };
    $.extend($.ajatus.tags.merger.prototype, {
        init: function(dubs, dubs_with, tags) {
            this.all_tags = tags;
            this.dub_tags = dubs;
            this.dub_tags_with = dubs_with;
            
            var _self = this;
            $.each(this.dub_tags, function(i,dub){
                var row = $('#object_'+dub._id+'_duplicate');
                row.bind('click', function(){
                    _self.toggle_selection($(this), dub._id, dub.value.real_hash.val);
                });
            });            
        },
        add_to_list: function(real_hash, with_tag, source_tag, row) {
            // console.log("add_to_list source_tag: "+source_tag+" with_tag: "+with_tag+" real_hash: "+real_hash);
            
            // if (source_tag == with_tag) {
            //     this.remove_from_list(real_hash, with_tag, source_tag);
            //     return;
            // }
            
            if (typeof this.dub_rows[with_tag] == 'undefined') {
                this.dub_rows[with_tag] = row;                
            }
            
            if (typeof this.merge_list[real_hash] == 'undefined') {
                this.merge_list[real_hash] = [with_tag];
                this.merge_count += 1;
            } else {
                this.merge_list[real_hash].push(with_tag);
            }
            
            this.merge_count += 1;
            
            // console.log(this.merge_list);
            
            this._gen_button();
            this._show_button();
        },
        remove_from_list: function(real_hash, with_tag, source_tag) {
            // console.log("remove_from_list source_tag: "+source_tag+" with_tag: "+with_tag+" real_hash: "+real_hash);
            var _self = this;
            this.merge_list[real_hash] = $.grep(this.merge_list[real_hash], function(n,i){
                if (n == with_tag) {
                    _self.merge_count -= 1;
                    return false;
                }
                return true;
            });
            
            if (this.merge_list[real_hash].length == 0) {
                var tmp_list = {};
                for (var hash in this.merge_list) {
                    if (hash != real_hash) {
                        tmp_list[hash] = this.merge_list[hash];
                    }
                }
                this.merge_list = tmp_list;
                _self.merge_count -= 1;
            }
            
            // console.log(this.merge_list);
            
            if (this.merge_count <= 0) {
                this._hide_button();
            }
        },
        merge_selections: function() {
            var _self = this;
            
            var dialog = this._gen_dialog(this.merge_count);
            dialog.open();
            
            var status_holder = $('.status_holder', dialog);
            
            var row = '';
            var row_failed = '<span class="status_failed">' + $.ajatus.i10n.get('failed').toUpperCase() + '</span><br />';
            var row_ok = '<span class="status_ok">' + $.ajatus.i10n.get('ok').toUpperCase() + '</span><br />';
            
            $.each(this.merge_list, function(hash, dubs){
                
                var tag = _self.all_tags[hash];
                var tag_title = '"' + $.ajatus.tags.render_title(tag) + '"';
                
                row = $.ajatus.i10n.get('Merging tag %s', [tag_title]) + '... ';
                status_holder.append(row);
                
                row = row_failed;
                if (_self._execute_merge(tag, dubs)) {
                    row = row_ok;
                }
                status_holder.append(row);
                
                _self.toggle_selection(_self.dub_rows[dubs[0]], dubs[0], hash);
            });
            
            $('#submit_ok', dialog).show();
            
            this._destroy_button();
        },
        
        toggle_selection: function(row, dub_id, real_hash) {
            // console.log("toggle_selection dub_id: "+dub_id+" real_hash: "+real_hash);
            
            // console.log("this.dub_tags_with["+real_hash+"]: "+this.dub_tags_with[real_hash].length);
            // console.log(this.dub_tags_with[real_hash]);
            
            if (row.hasClass('duplicate_row_autoselected')) {
                return;
            }
            
            var bg_color = "#73d216";
            var fg_color = "#000";
            
            var ohdrow = $('#object_'+real_hash);
            
            row.toggleClass('duplicate_row_indicator');
            if (row.hasClass('duplicate_row_indicator')) {
                this.add_to_list(real_hash, dub_id, dub_id, row);
                row.css({
                    "backgroundColor": bg_color,
                    "color": fg_color
                });
                
                ohdrow.css({
                    "backgroundColor": bg_color,
                    "color": fg_color
                });
            } else {
                this.remove_from_list(real_hash, dub_id, dub_id);
                row.css({
                    "backgroundColor": "",
                    "color": ""
                });
                
                ohdrow.css({
                    "backgroundColor": "",
                    "color": ""
                });
            }
            
            var odrow = $('#object_'+dub_id).toggleClass('duplicate_row_indicator');
            if (odrow.hasClass('duplicate_row_indicator')) {
                odrow.css({
                    "backgroundColor": bg_color,
                    "color": fg_color
                });
            } else {
                odrow.css({
                    "backgroundColor": "",
                    "color": ""
                });                            
            }
            
            var dub_with_ids = this.dub_tags_with[real_hash];
            
            for (var i=0; i<dub_with_ids.length; i++) {
                if (dub_with_ids[i] == dub_id) {
                    continue;
                }
                var drow = $('#object_'+dub_with_ids[i]).toggleClass('duplicate_row_indicator');
                if (drow.hasClass('duplicate_row_indicator')) {
                    drow.css({
                        "backgroundColor": bg_color,
                        "color": fg_color
                    });
                } else {                    
                    drow.css({
                        "backgroundColor": "",
                        "color": ""
                    });                            
                }
                var drow2 = $('#object_'+dub_with_ids[i]+'_duplicate').toggleClass('duplicate_row_indicator').addClass('duplicate_row_autoselected');
                if (drow2.hasClass('duplicate_row_indicator')) {
                    this.add_to_list(real_hash, dub_with_ids[i], dub_id, drow2);
                    drow2.css({
                        "backgroundColor": bg_color,
                        "color": fg_color
                    });
                } else {
                    this.remove_from_list(real_hash, dub_with_ids[i], dub_id);
                    drow2.removeClass('duplicate_row_autoselected');
                    drow2.css({
                        "backgroundColor": "",
                        "color": ""
                    });                            
                }
            }
        },
        
        _gen_button: function() {
            if (this.button_generated) {
                return;
            }
            
            var _self = this;
            
            this.button_holder = $('<div class="tag_merger_button_holder" />').hide();
            var button = $('<div class="tag_merger_button" />').html($.ajatus.i10n.get('Merge selected!'));
            
            button.bind('click', function(e){
                _self.merge_selections();
            });
            
            button.appendTo(this.button_holder);
            
            $.ajatus.application_dynamic_elements.append(this.button_holder);
            
            this.button_generated = true;
        },
        _destroy_button: function() {
            if (! this.button_generated) {
                return;
            }
            
            this.button_holder.remove();
            this.button_generated = false;
            this.button_visible = false;
        },
        _show_button: function() {
            this.button_visible = true;
            this.button_holder.show();
        },
        _hide_button: function() {
            this.button_visible = false;
            this.button_holder.hide();            
        },
        
        _gen_dialog: function(total_merge_count) {
            
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Merging %d tag', [total_merge_count]), '', {
                modal: true,
                dialog: {
                    height: 280
                }
            });
            var cholder = dialog.get_content_holder();
            
            var status_holder = $('<div class="status_holder" />');
            status_holder.appendTo(cholder);
            
            var done_btn = $('<input type="submit" id="submit_ok" name="ok" value="' + $.ajatus.i10n.get('ok').toUpperCase() + '" />').hide();
            
            done_btn.appendTo(cholder);
            
            $('#submit_ok', cholder).bind('click', function(e){
                dialog.close();
                return false;
            });
            
            return dialog;
        },
        
        _hide_rows_for: function(id) {
            if (typeof this.dub_rows[id] != 'undefined') {
                var row = this.dub_rows[id];
                row.hide();                
            }
            
            $('#object_'+id).hide();
            $('#object_'+id+'_duplicate').hide();
        },
        
        _execute_merge: function(source_tag, targets) {
            var _self = this;
            
            var items = {};
            var on_success = function(data) {
                var tag_id = null;
                
                $.each(data.rows, function(i,n){
                    if (n.key[1] == 0) {
                        tag_id = n.key[0];
                    } else {
                        items[n.key[0]].push(n);
                    }
                });
                
                return data;
            };
            
            for (var i=0; i<targets.length; i++) {
                items[targets[i]] = [];
                $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, 'tag/list_with_docs', {
                    startkey: [targets[i], 0],
                    endkey: [targets[i], 1]
                });
            }
            
            var on_save_success = null;
            
            $.each(items, function(hash, objs){
                on_save_success = function(data) {
                    _self._hide_rows_for(hash);
                    
                    return data;
                };
                
                if (objs.length > 0) {
                    for (var i=0; i<objs.length; i++) {                    
                        var doc = new $.ajatus.document($.jqCouch.connection('doc').get($.ajatus.preferences.client.content_database + '/' + objs[i].id));

                        doc.value.tags.val = $.grep(doc.value.tags.val, function(t,x){
                            if (t == hash) {
                                return false;
                            }
                            return true;
                        });
                        doc.value.tags.val.push(source_tag._id);

                        doc = $.ajatus.document.modify_metadata(doc, {
                            revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                            revisor: $.ajatus.preferences.local.user.email
                        });

                        $.jqCouch.connection('doc', on_save_success).save($.ajatus.preferences.client.content_database, doc);
                    }                    
                } else {
                    _self._hide_rows_for(hash);
                }
                
                $.ajatus.document.actions.execute('delete', {
                    _id: hash
                });
            });
                        
            return true;
        }
    });
    
    $.ajatus.tags.fixer = function(btags, bhashes, tags, settings) {
        this.all_tags = [];
        this.broken_tags = [];
        this.broken_hashes = {};
        
        this.settings = $.extend({
            show_toolbar: false,
            refresh_on_finish: false            
        }, (settings || {}));
        
        this.init(btags, bhashes, tags);
    };
    $.extend($.ajatus.tags.fixer.prototype, {
        init: function(btags, bhashes, tags) {
            this.all_tags = tags;
            this.broken_tags = btags;
            this.broken_hashes = bhashes;
            
            if (this.settings.show_toolbar) {
                var _self = this;
                $.ajatus.toolbar.add_item($.ajatus.i10n.get('Fix all broken tags'), 'repair.png', function(){
                    _self.fix_all();
                });                
            }
        },
        validate: function(curr_hash, data) {            
            var hash = $.ajatus.tags.calculate_hash(data.title, data.context, data.value);
            if (curr_hash == hash) {
                return true;
            }
            
            return false;
        },
        fix_single: function(tag) {            
            var is_valid = this.validate(tag._id, {
                title: tag.value.title.val,
                context: tag.value.title.widget.config.context,
                value: tag.value.title.widget.config.value
            });
            
            if (is_valid) {
                return false;
            }

            var data = this._prepare_tag_data(tag.value.title);
            var hash = $.ajatus.tags.calculate_hash(data.title.val, data.title.widget.config.context, data.title.widget.config.value);
                        
            var real_tag = this._create_real_tag(hash, data);
            
            var items = this._fetch_orig_items(tag._id);
            this._update_items(items, tag._id, hash);
            
            $.ajatus.document.actions.execute('delete_final', {
                _id: tag._id
            });
            
            return real_tag;
        },
        fix_all: function() {
            var _self = this;
            $.each(this.broken_hashes, function(hash,ids){
                if (ids.length > 1) {
                    var data = _self._prepare_tag_data(_self.broken_tags[ids[0]].value.title);
                    var real_tag = _self._create_real_tag(hash, data);
                    
                    for (var i=0; i<ids.length; i++) {
                        var items = _self._fetch_orig_items(ids[i]);
                        _self._update_items(items, ids[i], hash);
                        
                        $.ajatus.document.actions.execute('delete', {
                            _id: ids[i]
                        });
                    }
                    
                } else {
                    var data = _self._prepare_tag_data(_self.broken_tags[ids[0]].value.title);
                    var real_tag = _self._create_real_tag(hash, data);
                    var items = _self._fetch_orig_items(ids[0]);
                    _self._update_items(items, ids[0], hash);

                    $.ajatus.document.actions.execute('delete', {
                        _id: ids[0]
                    });
                }
            });
            
            window.location.reload();
        },
        _update_items: function(items, old_hash, new_hash) {
            if (items.length > 0) {
                for (var i=0; i<items.length; i++) {                    
                    var doc = new $.ajatus.document($.jqCouch.connection('doc').get($.ajatus.preferences.client.content_database + '/' + items[i].id));

                    doc.value.tags.val = $.grep(doc.value.tags.val, function(t,x){
                        if (t == old_hash) {
                            return false;
                        }
                        return true;
                    });
                    doc.value.tags.val.push(new_hash);

                    doc = $.ajatus.document.modify_metadata(doc, {
                        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                        revisor: $.ajatus.preferences.local.user.email
                    });

                    $.jqCouch.connection('doc').save($.ajatus.preferences.client.content_database, doc);
                }                    
            }
        },
        _fetch_orig_items: function(id) {
            var items = [];
            var on_success = function(data) {
                var tag_id = null;
                
                $.each(data.rows, function(i,n){
                    if (n.key[1] == 1) {
                        items.push(n);
                    }
                });
                
                return data;
            };
            
            $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, 'tag/list_with_docs', {
                startkey: [id, 0],
                endkey: [id, 1]
            });
            
            return items;
        },
        _prepare_tag_data: function(title_value) {
            
            var title_data = $.ajatus.tags.validate_title(title_value.val);
            if (typeof title_data == 'object') {
                title_value.val = title_data.title;
                title_value.widget.config.context = title_data.context;
                title_value.widget.config.value = title_data.value;
            }
            
            var data = {
                _type: 'tag',
                title: title_value
            };
            return data;
        },
        _create_real_tag: function(hash, data) {
            var doc = {
                _id: hash,
                value: data
            };
            doc = new $.ajatus.document(doc);       
            var now = $.ajatus.formatter.date.js_to_iso8601(new Date());
    	    var new_metadata = {
    	        revised: now,
    	        revisor: $.ajatus.preferences.local.user.email,
    	        created: now,
    	        creator: $.ajatus.preferences.local.user.email
    	    };
	        doc = $.ajatus.document.modify_metadata(doc, new_metadata);
            
            $.jqCouch.connection('doc').save($.ajatus.preferences.client.content_database, doc);
            doc = new $.ajatus.document(doc);
            
            return doc;
        }
    });
    
})(jQuery);