/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.renderer_defaults = {
        type: 'block',
        tick_limit: 20,
        max_item_before_pool: 20,
        items_per_page: 40,
        use_db_after: 100,
    };
    
    $.ajatus.renderer = function(settings, r) {
        this.settings = $.extend({}, $.ajatus.renderer_defaults, (settings || {}));

        if (this.settings.type == 'block') {
            this.handler = new $.ajatus.renderer._block(this.settings, r);
        } else if (this.settings.type == 'scroll') {
            this.handler = new $.ajatus.renderer._scroll(this.settings, r);
        }
	};
	$.extend($.ajatus.renderer.prototype, {
	    add_item: function(callback, args) {
            this.handler.add_item(callback, args);
	    },
	    
	    items_added: function(doc_count) {
	        if (typeof this.handler.items_added == 'function') {
	            this.handler.items_added(doc_count);
            }
	    },
	    
	    start: function() {
	        this.handler.start();
	    }
	});
	
	$.ajatus.renderer._block = function(settings, renderer) {
        this.settings = $.extend({
            tick_limit: 20,
            max_item_before_pool: 20
        }, settings);
        this.render_lock = false;	    
	    this.items = [];
	    this.done_items = [];
	    
	    this.renderer = renderer;
    };
	$.extend($.ajatus.renderer._block.prototype, {
	    add_item: function(callback, args) {
            var item = {
                callback: callback,
                args: args || []
            };
	        this.items.push(item);
	        $.ajatus.events.named_lock_pool.increase('block_renderer');
	        
	        if (! this.render_lock) {
	            this.start();
	        }
	    },
	    
	    start: function() {
            this._create_lock();
	    },
	    
	    _on_tick: function() {
	        var _self = this;
	        if (this.items.length == 0)
	        {
	            $.ajatus.events.named_lock_pool.clear('block_renderer')
	            return true;
	        }
	        this.items = $.grep(this.items, function(n, i){
	            if (i < _self.settings.tick_limit)
	            {	                
	                var functionToCall = eval(n.callback);
	                
                    if (functionToCall.apply(functionToCall, n.args)) {
    	                _self.done_items.push(i);
    	                $.ajatus.events.named_lock_pool.decrease('block_renderer');
                    }
	            } else {
	                return true;
	            }	            
	        });
	        this.renderer.enable_sorting();
	    },
	    
	    _create_lock: function() {
	        var _self = this;
            _self.render_lock = new $.ajatus.events.lock({
                disable_application: false,
    	        watch: {    	            
    	            validate: function() {
    	                _self._on_tick();
    	                return $.ajatus.events.named_lock_pool.count('block_renderer') == 0;
    	            },
    	            interval: 400,
                    safety_runs: 2
    	        },
    	        on_release: function() {
    	            _self._lock_finished();
    	        }
    	    });
	    },
	    
	    _lock_finished: function() {
	    }
    });

    $.ajatus.renderer._scroll = function(settings, renderer) {
        this.settings = $.extend({
            items_per_page: 30,
            preload_distance: 900,
            use_db: false,
            db: {},
            render_item_after_load: true
        }, settings);
        
        this.render_lock = false;
        
        this.renderer = renderer;
        
	    this.items = [];
	    this.done_items = [];
	    this.started = false;
	    
	    this.status_holder = null;
	    this.spacer = null;
	    
	    this.from_back_btn = false;
	    
	    this.scroller_id = $.ajatus.utils.generate_id();
	    this.mouse_state = 'up';
	    this.page_count = 1;
	    this.total_docs = 0;
	    
	    this.done_items = 0;
	    
	    this.indicator = new $.ajatus.elements.indicator();
    };    
    $.extend($.ajatus.renderer._scroll.prototype, {
	    add_item: function() {
	        var item = {};
	        if (typeof arguments[0] == 'object')
	        {
	            item = {
	                doc: arguments[0]
	            };
	        } else {
	            if (   arguments.length >= 1
	                && typeof arguments[0] == 'string')
	            {
	                item = {
                        callback: arguments[0],
                        args: arguments[1] || []
                    };
	            }
	        }
	        
	        this.items.push(item);
	        $.ajatus.events.named_lock_pool.increase('scroll_renderer');
	    },
	    
	    items_added: function(total_docs) {
	        if (! this.started) {
	            this.start();
	        }
            
            if (   this.settings.use_db
                && typeof total_docs != 'undefined')
            {                
                this.total_docs = total_docs;
                this.page_count = (total_docs / this.settings.items_per_page);
            } else {
                this.page_count = (this.items.length / this.settings.items_per_page);
            }
            
	        $('div.renderer_scroll_status_holder .total_pages').val(this.page_count);
	    },
	    
	    load_items: function(d) {
            // console.log("load_items: ");
            // console.log(d);
	        
	        this.indicator.show();
	        
	        if (   typeof d == 'undefined'
	            || d == null)
	        {
	            var opts = {
                    descending: true,
                    count: 1
                };
                if (typeof this.settings.db.static != 'undefined') {
                    var d = $.jqCouch.connection('view').get($.ajatus.preferences.client.content_database, this.settings.db.static, opts).rows[0];
                } else {
                    var d = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, this.settings.db.temp, opts).rows[0];
                }
	        }

            var _self = this;
            var on_success = function(data) {
                $.each(data.rows, function(i,doc){
                    var doc = new $.ajatus.document(doc);
                    if (_self.settings.render_item_after_load) {
                        _self.renderer.render_item(doc, i);
                    } else {
                        _self.add_item(doc);
                    }
                });
                
                _self.indicator.hide();
                
                return data;
            };

            var opts = {
                descending: true,
                startkey: '"'+d.key+'"',
                startkey_docid: d._id || d.id,
                count: this.settings.items_per_page,
                skip: 1
            };
            if (typeof this.settings.db.static != 'undefined') {
                $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, this.settings.db.static, opts);
            } else {
                $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, this.settings.db.temp, opts);
            }
	    },
	    
	    start: function() {
	        if (this.started) {
	            return;
            }
            
            $.ajatus.events.named_lock_pool.clear('scroll_renderer');
            
	        if (this.status_holder == null) {
	            this.status_holder = $('<div class="renderer_scroll_status_holder" />').appendTo($.ajatus.application_content_area).hide();
	            $('<input type="hidden" name="scroll_id" class="scroll_id" />').val(this.scroller_id).appendTo(this.status_holder);
	            $('<input type="hidden" name="scroll_total_pages" class="total_pages" />').val('1').appendTo(this.status_holder);
	            $('<input type="hidden" name="scroll_history_index" class="history_index" />').val('').appendTo(this.status_holder);
	            $('<input type="hidden" name="scroll_mouse_state" class="mouse_state" />').val('up').appendTo(this.status_holder);
	        }
        	
        	if (this.spacer == null) {
        	    this.spacer = $('<div class="renderer_scroll_spacer" />').appendTo($.ajatus.application_content_area).css({
        	        position: 'absolute',
        	        top: '0px',
        	        left: '0px',
        	        visibility: 'hidden'
        	    });
        	}
        	
        	var _self = this;
        	$(document).mousedown(function(e){
                $('.mouse_state', _self.status_holder).val('down');
            	_self.mouse_state = 'down';
        	});
        	$(document).mouseup(function(e){
        	    $('.mouse_state', _self.status_holder).val('up');
        	    _self.mouse_state = 'up';
        	});
        	
        	$('.history_index', this.status_holder).val('1');
	        
	        this._create_lock();
	        
	        $.ajatus.views.on_change_actions.add('$.ajatus.events.named_lock_pool.clear("scroll_renderer");');
	        
	        this.started = true;
	    },
	    
	    _on_tick: function() {
	        var _self = this;
	        
	        if (   !this.settings.use_db
	            && this.items.length == 0)
	        {
	            $.ajatus.events.named_lock_pool.clear('scroll_renderer');
	            return true;
	        }
	        
            // console.log("this.total_docs: "+this.total_docs);
            // console.log("this.done_items: "+this.done_items);
            // console.log("_on_tick this.page_count: "+this.page_count);
            // console.log("_on_tick history_index: "+$('.history_index', this.status_holder).val());
            // console.log("_on_tick this.items.length: "+this.items.length);
	        
	        if (this.mouse_state == 'up' && $(document).height() - $(document).scrollTop() < this.settings.preload_distance) {	            
	            if (this.settings.use_db) {
        	        this.indicator.show();
        	        
	                var last_doc = null;
        	        $.each(this.items, function(i,n){
        	            if (typeof n['doc'] != 'undefined') {
        	                last_doc = n.doc;

        	                if (_self.renderer.render_item(n.doc, i)) {
            	                $.ajatus.events.named_lock_pool.decrease('scroll_renderer');
            	                _self.done_items += 1;        	                    
        	                }

        	                return true;
    	                }
    	                
        	            last_doc = n.args[1];
        	            
    	                var functionToCall = eval(n.callback);

                        if (functionToCall.apply(functionToCall, n.args)) {
        	                $.ajatus.events.named_lock_pool.decrease('scroll_renderer');
        	                _self.done_items += 1;
                        }
        	        });
                    
                    this.items = [];
        	        if (this.done_items < this.total_docs) {
        	            this.load_items(last_doc);
        	        }
    	        } else {
    	            this.indicator.show();
    	            
        	        this.items = $.grep(this.items, function(n, i){
        	            if (i < _self.settings.items_per_page)
        	            {
            	            if (typeof n['doc'] != 'undefined') {
            	                if (_self.renderer.render_item(n.doc, i)) {
                	                $.ajatus.events.named_lock_pool.decrease('scroll_renderer');            	                    
            	                }

            	                return false;
        	                }
        	                
                            var functionToCall = eval(n.callback);

                            if (functionToCall.apply(functionToCall, n.args)) {
                                $.ajatus.events.named_lock_pool.decrease('scroll_renderer');
                            }
        	            } else {
        	                return true;
        	            }
        	        });
        	        
        	        this.indicator.hide();
    	        }
	            
    	        var hi = $('.history_index', this.status_holder).val();
    	        $('.history_index', this.status_holder).val(parseInt(hi)+1);
    	        this.renderer.enable_sorting();
	        }
	        
	        if (parseInt($('.history_index', this.status_holder).val()) > this.page_count && this.page_count != 1 && this.items.length <= 0) {
	            $.ajatus.events.named_lock_pool.clear('scroll_renderer');	            
	            return true;
            }
	    },
	    
	    _create_lock: function() {
	        var _self = this;
	        $.ajatus.events.named_lock_pool.increase('scroll_renderer');
	        
            _self.render_lock = new $.ajatus.events.lock({
                disable_application: false,
    	        watch: {    	            
    	            validate: function() {
    	                _self._on_tick();
    	                return $.ajatus.events.named_lock_pool.count('scroll_renderer') == 0;
    	            },
    	            interval: 200,
                    safety_runs: 2,
                    max_runs: false
    	        },
    	        on_release: function() {
    	            _self._lock_finished();
    	        }
    	    });
	    },
	    
	    _lock_finished: function() {
	        this.indicator.close();
	    }
    });

})(jQuery);