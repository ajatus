/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.elements = $.ajatus.elements || {};
    
    /**
     * Ajatus Dialog
     */
    $.ajatus.elements.dialog = function(title, msg, opts)
    {                
        $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/jquery/plugins/jqModal.css');
        $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/jquery/plugins/dialog/dialog.css');
        
        this.holder = $('#dynamic-elements');
        this.options = {
            modal: true,
            overlay: 30,
            closable: true,
            dialog: {
                width: 300,
                height: 200,
                nested: false
            }
        };
        
        this.options = $.ajatus.utils.merge_configs(this.options, opts);        
        
        this.id = this.create(title, msg, opts);
        return this;
    };
    $.extend($.ajatus.elements.dialog.prototype, {
        create: function(title, msg, options)
        {
            var closable = true;
            if (   typeof this.options.closable != 'undefined'
                && this.options.closable == false)
            {
                this.options.modal = false;
                closable = false;
            }
            
            var id = this._generate_id();
            var dialog = this._create_dialog_structure(id, closable);
            
            if (typeof title != 'undefined') {
                $('.jqmdTC',dialog).html(title);
            }
            if (typeof msg != 'undefined') {
                $('.jqmdMSG',dialog).html(msg);
            }
            
            this.holder.append(dialog);
            
            var content_target = $('.jqmdMSG',dialog);
            
            var options = $.extend({
                target: content_target,
                onHide: function(h) { 
                    content_target.html('');
                    h.o.remove();
                    h.w.fadeOut(300);
                }
            }, this.options, options);
            
            $('#' + id).jqm(options);
            
            return id;
        },
        
        get_dialog: function() {
            return $('#' + this.id);
        },

        get_content_holder: function() {
            return $('#' + this.id + ' .jqmdMSG');
        },
        
        set_content: function(content) {
            $('#' + this.id + ' .jqmdMSG').html(content);
        },
        
        append_content: function(content)
        {
            $('#' + this.id + ' .jqmdMSG').append(content);
        },
        
        prepend_content: function(content)
        {
            $('#' + this.id + ' .jqmdMSG').prepend(content);
        },
        
        open: function()
        {
            $('#' + this.id).jqmShow();
        },
        
        close: function()
        {
            $('#' + this.id).jqmHide();
        },

        destroy: function()
        {
            $('#' + this.id).jqmHide();
            $('#' + this.id).remove();
        },
                
        _create_dialog_structure: function(id, closable)
        {            
            var body = $('<div />')
            .attr('id', id)
            .addClass('jqmDialog')
            .hide()
            .css({
                width: this.options.dialog.width+"px",
                height: this.options.dialog.height+"px"
            });
            
            if (this.options.dialog.nested) {
                body.addClass('jqmdAbove');
            }
            
            var frame = $('<div class="jqmdTL"><div class="jqmdTR"><div class="jqmdTC"></div></div></div><div class="jqmdBL"><div class="jqmdBR"><div class="jqmdBC"><div class="jqmdMSG"></div></div></div></div>');
            
            $('.jqmdBC', frame).css({
                height: this.options.dialog.height - 70 + "px"
            });
            
            if (   typeof closable == 'undefined'
                && closable)
            {
                frame.append('<input type="image" src="' + $.ajatus.preferences.client.theme_url + 'css/jquery/plugins/dialog/close.gif" class="jqmdX jqmClose" />');
            }
            
            body.html(frame);
            
            return body;
        },
        
        _generate_id: function()
        {
            return "ajatus_dialog_" + $.ajatus.utils.generate_id();
        }
    });
    /**
     * Ajatus Dialog ends
     */
     
    $.ajatus.elements.messages = {
        msgs: [],
        watchers: [],
        holder: null
    };
    $.extend($.ajatus.elements.messages, {
        create: function(title, message, options)
        {
            if (typeof options == 'undefined') {
                options = {
                    closable: true,
                    visible: 5
                };
            }
            if (typeof options.type == 'undefined') {
                options.type = 'notification';
            }
            if (typeof options.closable == 'undefined') {
                options.closable = true;
            }
            options.holder = $.ajatus.elements.messages.holder;            
            var msg = new $.ajatus.elements.message(title, message, options);

            $.ajatus.elements.messages.msgs.push(msg);
            
            if (   options.visible
                && options.visible > 0)
            {
                var watcher_opts = {
                    auto_start: true,
                    safety_runs: 0,
                    interval: 200,
                    timed: (options.visible * 1000)
                };
                var watcher = new $.ajatus.events.watcher(watcher_opts);
                watcher.element.bind('on_stop', function(e, watcher_id){
                    msg.fade('out', true);
                });
            }
             
            return msg;
        },
        static: function(title, message, options)
        {
            var msg = new $.ajatus.elements.message(title, message, options);
            return msg;
        },
        remove: function(msg_id)
        {
            $.ajatus.elements.messages.msgs = $.grep($.ajatus.elements.messages.msgs, function(n,i){
                if (n.id == msg_id) {
                    n.remove();
                    return false;
                } else {
                    return true;
                }
            });
        },
        clear: function()
        {
            $.ajatus.elements.messages.msgs = [];
            $.ajatus.elements.messages.holder.html('');
        },
        gen_id: function()
        {
            var date = new Date();
            var id = 'msg_' + date.getTime();
            return id;
        },
        set_holder: function(holder)
        {
            if (typeof holder == 'undefined') {
                var holder = $('#system_messages', $.ajatus.application_element);
            }
            $.ajatus.elements.messages.holder = holder;
        }
    });
     
    $.ajatus.elements.message = function(title, msg, options)
    {
        this.options = $.extend({
            auto_show: true,
            disappear: false,
            holder: false,
            closable: false,
            type: 'static'
        }, options || {});
        this.holder = this.options.holder;
        this.body_div = null;
         
        if (! this.holder) {
            this.holder = $('<div />');
            this.holder.appendTo($.ajatus.application_content_area);
        }
         
        this.id = this.create(title, msg);
        return this;
    };
    $.extend($.ajatus.elements.message.prototype, {
        create: function(title, msg) {
            var id = $.ajatus.elements.messages.gen_id();
            this.body_div = jQuery('<div class="ajatus_elements_message" />').attr({
                id: id
            }).addClass(this.options.type).hide();
            this.body_div.appendTo(this.holder);
            
            if (this.options.closable) {
                var self = this;
                var close_handle = jQuery('<div class="close_handle" />')
                close_handle.appendTo(this.body_div)
                close_handle.bind('click', function(){
                    self.fade('out', true);
                });
            }

            var title_item = jQuery('<h2 />').attr('class', 'title').html(title);
            title_item.appendTo(this.body_div);
            
            var msg_item = jQuery('<div />').attr('class', 'message').html(msg);
            msg_item.appendTo(this.body_div);
            
            if (this.options.auto_show) {
                this.show();
            }
            
            return id;
        },
        show: function() {
            this.fade('in');
            //this.body_div.show();
        },
        hide: function() {
            this.fade('out');
            //this.body_div.hide();
        },
        fade: function(direction, destroy) {
            var self = this;
            if (direction == 'out') {
                this.body_div.fadeOut('slow', function(){
                    if (destroy) {
                        self.destroy();
                    }
                });
            } else {
                this.body_div.fadeIn('normal', function(){
                });
            }
        },
        remove: function() {
            this.body_div.remove();            
        },
        destroy: function() {
            $.ajatus.elements.messages.remove(this.id);
        },
        bind_in_content: function(action, selector, func) {
            $(selector, this.body_div).bind(action, func);
        }
    });
    
    $.ajatus.elements.indicator = function(label, options) {
        this.options = $.extend({
            auto_show: false
        }, options || {});
        this.body_div = null;
        
        this.label = label || $.ajatus.i10n.get('Loading');
         
        this.id = this.create();
        return this;
    };
    $.extend($.ajatus.elements.indicator.prototype, {
        create: function() {
            var id = this._gen_id();
            
            this.body_div = jQuery('<div class="ajatus_elements_indicator" />').attr({
                id: id
            }).hide();
            this.body_div.appendTo($.ajatus.application_dynamic_elements);
            
            if (this.label != '') {
                var label_item = jQuery('<div />').attr('class', 'label').html(this.label);
                label_item.appendTo(this.body_div);                
            }
            
            // var indicator = jQuery('<div />').attr('class', 'loader');
            // indicator.appendTo(this.body_div);
            
            if (this.options.auto_show) {
                this.show();
            }
            
            return id;
        },
        show: function() {
            this.body_div.show();
        },
        hide: function() {
            this.body_div.hide();
        },
        close: function() {
            this.hide();
            this.body_div.remove();            
        },
        _gen_id: function()
        {
            var date = new Date();
            var id = 'indicator_' + date.getTime();
            return id;
        }
    });
    
})(jQuery);