/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.extension = $.ajatus.extension || {};
    
    $.ajatus.extensions = {
        loaded: [],
        lock: false
    };
    $.extend($.ajatus.extensions, {
        init: function(options){
            options = $.extend({
                on_ready: function(){return;}
            }, options || {});
            
            $.ajatus.events.named_lock_pool.increase('init_extensions');
            $.ajatus.extensions.lock = new $.ajatus.events.lock({
    	        watch: {
    	            validate: function(){return $.ajatus.events.named_lock_pool.count('init_extensions') == 0;},
    	            interval: 200,
                    safety_runs: 0
    	        },
    	        on_release: options.on_ready
    	    });
    	    
            if ($.ajatus.preferences.client.extensions.length > 0) {
                $.each($.ajatus.preferences.client.extensions, function(i,ext){
                    $.ajatus.extensions.load(ext);
                });
            }
            
            $.ajatus.events.named_lock_pool.decrease('init_extensions');
        },
        load: function(extension) {
            $.ajatus.events.named_lock_pool.increase('init_extensions');
            
            var ext_url = $.ajatus.preferences.client.application_url + 'js/extensions/' + extension + '.js';            
            $.ajatus.utils.load_script(ext_url, "$.ajatus.extensions.extension_loaded", [extension]);
        },
        extension_loaded: function(extension) {
            $.ajatus.extensions.loaded[extension] = $.ajatus.extension[extension];
            $.ajatus.extensions.init_extension(extension);            
        },
        init_extension: function(extension){
            $.ajatus.events.named_lock_pool.decrease('init_extensions');
            
            return true;            
        }
    });
    
})(jQuery);