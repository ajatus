/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){

    $.ajatus.document = function(doc, check_metadata, check_schema) {
        if (typeof check_metadata == 'undefined') {
            var check_metadata = true;
        }
        if (typeof check_schema == 'undefined') {
            var check_schema = false;
        }
        
        // console.log("Check schema: "+check_schema);
        // console.log("Check metadata: "+check_metadata);
                
        var self = this;
        this.ready_doc = {};
        
        $.each(doc, function(i,n){
            if (   (i == '_id' && n != '')
                || i == 'id' && n != '')
            {
                self.ready_doc['_id'] = n;
            } else if (i == '_rev' && n != '') {
                self.ready_doc[i] = n;
            } else if (i == 'key' && n != '') {
                self.ready_doc[i] = n;
            } else if (i == 'value' && n != '') {
                self.ready_doc[i] = n;
            }
        });
        
        if (typeof this.ready_doc['value'] == 'undefined') {
            $.ajatus.debug("Error: Document didn't have value field defined!", "ajatus.document");            
            return this.ready_doc;
        }
        
        if (check_metadata) {
            if (! this.ready_doc.value.metadata) {
                this.add_metadata();
            } else {
                this.check_metadata();
            }
        }

        if (! $.ajatus.preferences.client.content_types[this.ready_doc.value._type]) {
            this.ready_doc.value._type = 'note';
        }

        if (check_schema) {
            this.check_schema();
        }
        
        this.prepare_doc_title();
        
        return this.ready_doc;
    }
    $.extend($.ajatus.document.prototype, {
        check_schema: function() {
            var self = this;
            var type = $.ajatus.preferences.client.content_types[this.ready_doc.value._type];
            var changed = false;
            var new_schema_parts = {};

            $.each(type.original_schema, function(k,si){
                // console.log("self.ready_doc.value.metadata["+k+"]:"+self.ready_doc.value.metadata[k])
                if (typeof(self.ready_doc.value[k]) == 'undefined') {
                    def_val = '';
                    if (typeof si['def_val'] != 'undefined') {
                        def_val = si['def_val'];
                    }
                    new_schema_parts[k] = {
                        val: def_val,
                        widget: si.widget
                    };
                    changed = true;
                } else {
                    def_val = self.ready_doc.value[k].val || '';
                    if (typeof si['def_val'] != 'undefined') {
                        def_val = si['def_val'];
                    }                    
                    self.ready_doc.value[k] = $.extend({
                        val: def_val,
                        widget: si.widget                        
                    }, self.ready_doc.value[k]);
                }
            });
            
            if (changed) {
                // console.log("Found schema parts that need to be updated:");
                // console.log(new_schema_parts);
                self._fix_doc_schema(new_schema_parts);
            }            
        },
        _fix_doc_schema: function(new_parts) {
            // console.log("FIX SCHEMA:");
            // console.log(new_parts);
            var _self = this;

            doc = _self.ready_doc;//new $.ajatus.document.loader(_self.ready_doc._id, _self.ready_doc._rev, false, false);
            doc.value = $.extend(doc.value, new_parts);
            
            var db_path = $.ajatus.preferences.client.content_database;
            
    	    $.jqCouch.connection('doc').save(db_path, doc);
            _self.ready_doc = doc;//new $.ajatus.document(doc);
        },
        add_metadata: function() {
            var self = this;
            this.ready_doc.value.metadata = {};
            $.each($.ajatus.document.metadata, function(k,m){
                self.ready_doc.value.metadata[k] = {
                    val: '',
                    widget: m.widget
                };
            });
        },
        check_metadata: function() {
            // console.log("check metadata");
            var self = this;
            var changed = false;
            var new_md_parts = {};
            $.each($.ajatus.document.metadata, function(k,m){
                // console.log("self.ready_doc.value.metadata["+k+"]:"+self.ready_doc.value.metadata[k])
                if (typeof(self.ready_doc.value.metadata[k]) == 'undefined') {
                    def_val = '';
                    if (typeof m['def_val'] != 'undefined') {
                        def_val = m['def_val'];
                    }
                    new_md_parts[k] = {
                        val: def_val,
                        widget: m.widget
                    };
                    changed = true;
                } else {
                    def_val = self.ready_doc.value.metadata[k].val || '';
                    if (typeof m['def_val'] != 'undefined') {
                        def_val = m['def_val'];
                    }                    
                    self.ready_doc.value.metadata[k] = $.extend({
                        val: def_val,
                        widget: m.widget                        
                    }, self.ready_doc.value.metadata[k]);
                }
            });
            
            if (changed) {
                self._fix_metadata(new_md_parts);
            }
        },
        _fix_metadata: function(new_metadata) {
            var _self = this;
            
            // console.log("_fix_metadata for: "+_self.ready_doc._id);
            // console.log(new_metadata);
            
            doc = new $.ajatus.document.loader(_self.ready_doc._id, _self.ready_doc._rev, false);

            // console.log("Before:");
            // console.log(doc.value.metadata);
            
            doc.value.metadata = $.extend({}, doc.value.metadata, new_metadata);
            
            // console.log("After:");
            // console.log(doc.value.metadata);
            
            var db_path = $.ajatus.preferences.client.content_database;
            
    	    $.jqCouch.connection('doc').save(db_path, doc);
    	    _self.ready_doc = new $.ajatus.document(doc);
        },
        prepare_doc_title: function() {
            var self = this;

            if ($.ajatus.preferences.client.content_types[this.ready_doc.value._type]['title_item']) {
                $.ajatus.preferences.client.content_types[this.ready_doc.value._type].schema['title'] = {
                    label: 'Title',
                    widget: {
                        name: 'text',
                        config: {}
                    },
                    hidden: true
                };

                var title_val = '';
                $.each($.ajatus.preferences.client.content_types[this.ready_doc.value._type]['title_item'], function(i,part){
                    if (self.ready_doc.value[part]) {
                        var widget = new $.ajatus.widget(self.ready_doc.value[part].widget.name, self.ready_doc.value[part].widget.config);
                        var val = widget.value_on_view(self.ready_doc.value[part].val, 'plain');
                        if (typeof val != 'string') {
                            val = self.ready_doc.value[part].val;
                        }
                        title_val += val;
                    } else {
                        title_val += $.ajatus.i10n.get(part);
                    }
                });

                this.ready_doc.value['title'] = {
                    val: title_val,
                    widget: $.ajatus.preferences.client.content_types[this.ready_doc.value._type].schema['title'].widget
                };
            }
        }
    });
    $.ajatus.document.loader = function(id, rev, check_metadata) {
        if (typeof check_metadata == 'undefined') {
            var check_metadata = true;            
        }
        
        var args = {};
        if (   typeof rev != 'undefined'
            && rev != '')
        {
            args['rev'] = rev;
        }
        
        return new $.ajatus.document(
            $.jqCouch.connection('doc').get($.ajatus.preferences.client.content_database + '/' + id, args),
            check_metadata,
            true
        );
    };
    $.ajatus.document.revisions = {
        get: function(doc) {
            // console.log("get_revisions for "+doc._id);
            if (typeof doc._revs_info == 'undefined') {
                doc = $.jqCouch.connection('doc').get($.ajatus.preferences.client.content_database + '/' + doc._id, {
                    _rev: doc._rev,
                    revs_info: true
                });
            }

            var available_revs = [];
            var active_rev_status = false;
            $.each(doc._revs_info, function(i,n){
                if (n.rev == doc._rev) {
                    active_rev_status = n.status;
                } else {
                    if (n.status == 'disk') {
                        available_revs.push(n);
                    }
                }
            });

            var revs_data = {};
            revs_data['active'] = {
                rev: doc._rev,
                status: active_rev_status
            };
            revs_data['available'] = available_revs;
            revs_data['all'] = doc._revs_info;

            // console.log("Revs data:");
            // console.log(revs_data);

            return revs_data;            
        },
        navigate_to: function(pos, current, rev_data) {
            // console.log("Navigate_to ("+pos+"), current rev: "+current);
            
            var behind = [];
            var ahead = [];
            var revisions = rev_data.all;
            
            var put_behind = true;
            var rev_idxs = {};
            $.each(revisions, function(i,n){
                rev_idxs[n.rev] = i;
                if (n.rev == current) {
                    put_behind = false;
                    return;
                }
                if (put_behind) {
                    behind.push(n.rev);
                } else {
                    ahead.push(n.rev);
                }
            });
            
            var curr_key = rev_idxs[current];
            
            // console.log("Behind:");
            // console.log(behind);
            // console.log("Ahead:");
            // console.log(ahead);
            
            var return_rev = null;
            
            switch(pos) {
                case 'first':
                    if (typeof ahead[(ahead.length-1)] != 'undefined') {
                        return_rev = ahead[(ahead.length-1)];
                        curr_key = rev_idxs[return_rev];
                    }
                break;
                case 'last':
                    if (typeof behind[0] != 'undefined') {
                        return_rev = behind[0];
                        curr_key = rev_idxs[return_rev];
                    }
                    if (rev_data.active.rev == current) {
                        curr_key = rev_idxs[current];
                        return_rev = null;
                    }
                break;
                case 'next':
                    if (typeof behind[(behind.length-1)] != 'undefined') {
                        return_rev = behind[(behind.length-1)];
                        curr_key = rev_idxs[return_rev];                        
                    } else if (typeof ahead[0] != 'undefined') {
                        return_rev = ahead[0];
                        curr_key = rev_idxs[return_rev];
                    }
                    
                    if (rev_data.active.rev == current) {
                        curr_key = rev_idxs[current];
                        return_rev = null;
                    }
                break;
                case 'prev':
                    if (typeof ahead[0] != 'undefined') {
                        return_rev = ahead[0];
                        curr_key = rev_idxs[return_rev];
                    }
                break
            }

            // console.log("Total revs: "+revisions.length);
            // console.log("curr_key: "+curr_key);
            // console.log("Current: "+(revisions.length - (curr_key)));
            // 
            // console.log("Return rev: "+return_rev);
            
            return {
                total: revisions.length,
                current: (revisions.length - curr_key),
                rev: return_rev
            };
        }
    };
    $.ajatus.document.metadata = {
        creator: {
            label: 'Creator',
            widget: {
                name: 'text',
                config: {}
            }
        },
        created: {
            label: 'Created',
            widget: {
                name: 'date',
                config: {
                    use_time: true
                }
            }
        },
        revisor: {
            label: 'Revisor',
            widget: {
                name: 'text',
                config: {}
            }
        },
        revised: {
            label: 'Revised',
            widget: {
                name: 'date',
                config: {
                    use_time: true
                }
            }
        },
        archived: {
            label: 'Archived',
            widget: {
                name: 'boolean',
                config: {}
            },
            def_val: false
        },
        deleted: {
            label: 'Deleted',
            widget: {
                name: 'boolean',
                config: {}
            },
            def_val: false
        }
    };
    $.ajatus.document.modify_metadata = function(doc, metadata) {
        $.each(metadata, function(k,v){
            if ($.ajatus.document.metadata[k])
            {
                var wdgt = new $.ajatus.widget($.ajatus.document.metadata[k].widget.name, $.ajatus.document.metadata[k].widget.config);
                doc.value.metadata[k] = {
                    val: wdgt.value_on_save(wdgt.value_on_save(v)),
                    widget: $.ajatus.document.metadata[k].widget
                };
            }
        });
        
        return doc;
    };
    $.ajatus.document.actions = {        
        execute: function(action, tmpdoc, force_load_rev) {
            if (typeof force_load_rev == 'undefined') {
                force_load_rev = false;
            }
            
            if (   typeof tmpdoc._rev != 'undefined'
                && tmpdoc._rev != null
                && !force_load_rev)
            {
                var doc = new $.ajatus.document.loader(tmpdoc._id, tmpdoc._rev);
            } else {
                var doc = new $.ajatus.document.loader(tmpdoc._id);
            }
            
            var act_info = $.ajatus.document.actions._get_action_info(action);
            if (! act_info) {
                var msg = $.ajatus.elements.messages.create(
                    $.ajatus.i10n.get('Unknown document action'),
                    $.ajatus.i10n.get('Unknown action %s requested for object %s!', [action, doc.value.title.val])
                );
                return false;
            }
            
            if (act_info.pool_action)
            {
                var pa = act_info.pool_action;
                var pai = $.ajatus.document.actions._get_action_info(fa);
                if (pai) {
                    var pool_action = {
                        action: fa,
                        doc: doc
                    };
                    var pool_id = $.ajatus.document.history.pool.add(pool_action);
                }
            }
            
            if (act_info.undoable) {                
                $.ajatus.document.actions._execute_temp(act_info.action, doc);
            } else {
                $.ajatus.document.actions._execute_final(act_info.action, doc);
            }
        },
        _execute_temp: function(action, doc) {
            // console.log("_execute_temp: "+action);
            switch(action) {
                case 'delete':
                    var on_success = function(data) {
                        var tmpdoc = {
                            _id: doc._id,
                            _rev: doc._rev
                        };
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('Object deleted'),
                            $.ajatus.i10n.get('Object %s moved to trash.', [doc.value.title.val]) + ' <a href="#undelete.'+doc.value._type+'.'+doc._id+'" class="undo">' + $.ajatus.i10n.get('Undo') + '</a>'
                        );
                        msg.bind_in_content('click', 'a.undo', function(e){
                            $.ajatus.document.actions.execute("undelete", tmpdoc, true);
                            msg.fade('out', true);
                        });
                        
                        return data;
                    };

                    doc = $.ajatus.document.modify_metadata(doc, {
                        deleted: true,
                        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                        revisor: $.ajatus.preferences.local.user.email
                    });
                    
                    $.jqCouch.connection('doc', on_success).save($.ajatus.preferences.client.content_database, doc);
                    
                    $('#object_'+doc._id, $.ajatus.application_content_area).hide().addClass('deleted');
                break;
                case 'archive':
                    var on_success = function(data) {
                        var tmpdoc = {
                            _id: doc._id,
                            _rev: doc._rev
                        };
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('Object archived'),
                            $.ajatus.i10n.get('Object %s moved to archive.', [doc.value.title.val]) + ' <a href="#unarchive.'+doc.value._type+'.'+doc._id+'" class="undo">' + $.ajatus.i10n.get('Undo') + '</a>'
                        );
                        msg.bind_in_content('click', 'a.undo', function(e){
                            $.ajatus.document.actions.execute("unarchive", tmpdoc, true);
                            msg.fade('out', true);
                        });
                        
                        return data;
                    };

                    doc = $.ajatus.document.modify_metadata(doc, {
                        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                        revisor: $.ajatus.preferences.local.user.email,
                        archived: true
                    });
                    
                    $.jqCouch.connection('doc', on_success).save($.ajatus.preferences.client.content_database, doc);
                    
                    $('#object_'+doc._id, $.ajatus.application_content_area).hide().addClass('archived');
                break;
            };
        },
        _execute_final: function(action, doc) {
            switch(action) {
                case 'delete':
                    var on_success = function(data) {
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('Object deleted'),
                            $.ajatus.i10n.get('Object %s removed from Ajatus.', [doc.value.title.val])
                        );
                        
                        return data;
                    };
                    $.jqCouch.connection('doc', on_success).del($.ajatus.preferences.client.content_database, doc);
                    $('#object_'+doc._id, $.ajatus.application_content_area).remove();
                break;
                case 'undelete':
                    var on_success = function(data) {
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('Object restored'),
                            $.ajatus.i10n.get('Object %s restored succesfully.', [doc.value.title.val])
                        );
                        
                        return data;
                    };
                    doc = $.ajatus.document.modify_metadata(doc, {
                        deleted: false,
                        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                        revisor: $.ajatus.preferences.local.user.email
                    });
                    $.jqCouch.connection('doc', on_success).save($.ajatus.preferences.client.content_database, doc);
                    
                    $('#object_'+doc._id, $.ajatus.application_content_area).not('.deleted').remove();
                    $('#object_'+doc._id, $.ajatus.application_content_area).filter('.deleted').show().removeClass('deleted');
                break;
                case 'unarchive':
                    var on_success = function(data) {
                        var msg = $.ajatus.elements.messages.create(
                            $.ajatus.i10n.get('Object restored'),
                            $.ajatus.i10n.get('Object %s restored succesfully.', [doc.value.title.val])
                        );
                        
                        return data;
                    };
                    doc = $.ajatus.document.modify_metadata(doc, {
                        revised: $.ajatus.formatter.date.js_to_iso8601(new Date()),
                        revisor: $.ajatus.preferences.local.user.email,
                        archived: false
                    });
                    $.jqCouch.connection('doc', on_success).save($.ajatus.preferences.client.content_database, doc);
                    
                    $('#object_'+doc._id, $.ajatus.application_content_area).filter('.archived').show().removeClass('archived');
                break;
            };
        },
        empty_pool: function () {
            var items = $.ajatus.document.history.pool.get_all();
            if (items.length > 0) {
                var msg = $.ajatus.elements.messages.create(
                    $.ajatus.i10n.get('Emptying action pool'),
                    $.ajatus.i10n.get('Running %d pooled actions.', [items.length])
                );                
            }
            $.each(items, function(i,item){
                $.ajatus.document.history.pool.remove(i);
                $.ajatus.document.actions.execute(item.action, item.doc);
            });
        },
        _get_action_info: function(action) {
            switch(action) {
                case 'delete':
                    return {
                        action: 'delete',
                        label: $.ajatus.i10n.get('Delete'),
                        undoable: true,
                        undo_label: $.ajatus.i10n.get('Undelete'),
                        pool_action: false
                    }
                break;
                case 'delete_final':
                    return {
                        action: 'delete',
                        label: $.ajatus.i10n.get('Delete'),
                        undoable: false,
                        pool_action: false
                    }
                break;
                case 'undelete':
                    return {
                        action: 'undelete',
                        label: $.ajatus.i10n.get('Undelete'),
                        undoable: false,
                        pool_action: false
                    }
                break;
                case 'archive':
                    return {
                        action: 'archive',
                        label: $.ajatus.i10n.get('Archive'),
                        undoable: true,
                        undo_label: $.ajatus.i10n.get('Restore'),
                        pool_action: false
                    }
                break;
                case 'unarchive':
                    return {
                        action: 'unarchive',
                        label: $.ajatus.i10n.get('Restore'),
                        undoable: false,
                        pool_action: false
                    }
                break;
                default:
                    return false;
            }
        }
    };
    
    $.ajatus.document.history = {};
    $.ajatus.document.history.pool = {
        items: [],
        
        add: function(action) {
            var new_length = $.ajatus.document.history.pool.items.push(action);
            var pool_id = new_length-1;
            return pool_id;
        },        
        remove: function(item_id) {
            $.ajatus.document.history.pool.items = $.grep($.ajatus.document.history.pool.items, function(n,i){
                return i != item_id;
            });
        },        
        get_item: function(item_id) {
            if (items[item_id]) {
                return items[item_id];
            } else {
                return false;
            }
        },        
        get_all: function() {
            return $.ajatus.document.history.pool.items;
        }
    };
    
    $.ajatus.document.scheme = {
        
    };
    $.ajatus.document.scheme.items = {
        configure: function(name, widget) {
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Edit field %s', [name]), '', {
                modal: false,
                dialog: {
                    height: 300
                }
            });

            var prev_vals = $.ajatus.document.scheme.items._read_settings(name, $.ajatus.forms.active, widget);
            var on_save = '$.ajatus.document.scheme.items.save_config';
            
            var content_holder = $.ajatus.document.scheme.items._create_structure( dialog );
            
            var jqform = $('.form', content_holder);
            var contents = $('.contents', content_holder);
            
            var wdgt_settings = $('<div id="widget_settings_holder" />');
            wdgt_settings.appendTo(contents);
            wdgt_settings.html($.ajatus.document.scheme.items._generate_widget_settings(widget, prev_vals));
            
            $('.actions #submit_save', content_holder).bind('click', function(e){
                var fn = on_save;
        	    if (typeof on_save == 'string') {
                    fn = eval(on_save);
                }
                var result = fn.apply(fn, [name, jqform.formToArray(false)]);
                
                if (result) {
                    dialog.close();
                }
            });
            
            dialog.open();
        },
        generate_actions_tpl: function(name, widget, data, row_holder) {
            var item_data = data;
            return [
                'img', { className: 'edit_settings_btn', src: $.ajatus.preferences.client.theme_icons_url + 'preferences.png', title: $.ajatus.i10n.get('Settings'), alt: $.ajatus.i10n.get('Settings'), onclick: function(){$.ajatus.document.scheme.items.configure(name, widget);} }, ''
            ];
        },
        save_config: function(name, form_data) {
            var form_values = {};
            
            $.ajatus.document.scheme.items._process_widget_settings(form_data, form_values);
            
            var config = {};
            $.each(form_values, function(i,n){
                config[i] = n.val;
            });
            
            var row = $('#_element_'+name, $.ajatus.forms.active);

            var add_upd_elem = $('input[name="widget['+name+':additional_updated]"]', row);
            if (typeof add_upd_elem[0] == 'undefined') {
	            row.createAppend(
	                'input', { type: 'hidden', name: 'widget['+name+':field_updated]', value: $.ajatus.converter.toJSON(true) }, ''
                );
            } else {
                add_upd_elem.val($.ajatus.converter.toJSON(true));
            }
            
            var widget_config_input = $('#_element_'+name+'_widget input[name="widget['+name+':config]"]', $.ajatus.forms.active)
                .val($.ajatus.converter.toJSON(config));
            
            return true;
        },
        _create_structure: function(dialog) {
            var cholder = dialog.get_content_holder();
            
            var title = $('<h2 class="title"/>');
            var form = $('<form id="additionals_window_form" class="form" />');
            var form_errors = $('<div class="form_errors" />').hide();
            var content_area = $('<div class="contents"/>');
            var actions = $('<div class="actions" />').html('<input type="submit" id="submit_save" name="save" value="' + $.ajatus.i10n.get('Save') + '" /><input type="submit" id="submit_cancel" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" />');
                                    
            title.appendTo(cholder);
            form.appendTo(cholder);
            form_errors.appendTo(form);
            content_area.appendTo(form);
            actions.appendTo(cholder);

            $('#submit_cancel', actions).bind('click', function(e){
                dialog.close();
                return false;
            });
            
            return cholder;
        },
        _read_settings: function(name, form, widget) {
            var prev_vals = {};
            
            if (typeof form == 'undefined') {
                var form = $.ajatus.forms.active;
            }            
            if (typeof widget != 'undefined') {
                prev_vals = widget.config;
            }
            
            var widget_config_input = $('#_element_'+name+'_widget input[name="widget['+name+':config]"]', form);
            if (typeof widget_config_input[0] != 'undefined') {
                prev_vals = $.ajatus.converter.parseJSON(widget_config_input.val());
            }
            
            return prev_vals;
        },
        _generate_widget_settings: function(widget_name, data) {
            var widget = $.ajatus.widget(widget_name);
            var details = $('<p>' + $.ajatus.i10n.get("Widget %s doesn't support dynamic settings.", [$.ajatus.i10n.get(widget_name)]) + '</p>');
            if (typeof(widget['create_widget_settings']) == 'function') {
                details = widget.create_widget_settings(data || {});
            }
            
            return details;
        },
        _process_widget_settings: function(form_data, form_values) {
    	    $.each(form_data, function(i,row){
                if (row.name.toString().match(/__(.*?)/)) {
                    return;
                }
	            if (row.name.substr(0,6) != "widget") {
	                var item = {};
	                var widget = {};
	                var prev_val = false;
	                var name_parts = [];
	                var name_parts_count = 0;
	                if (row.name.toString().match(/;/g)) {
	                    name_parts = row.name.toString().split(";");
	                    name_parts_count = name_parts.length;
	                }
	                
	                $.each(form_data, function(x,r){
	                    if (r.name == 'widget['+row.name+':name]') {
	                        widget['name'] = r.value;
	                    } else if (r.name == 'widget['+row.name+':config]') {
                            widget['config'] = $.ajatus.converter.parseJSON(r.value);
                        } else if (r.name == 'widget['+row.name+':prev_val]') {
                            prev_val = $.ajatus.converter.parseJSON(r.value);
                        }
                    });

                    var wdgt = new $.ajatus.widget(widget['name'], widget['config']);

	                item['val'] = wdgt.value_on_save(row.value, prev_val);
	                item['widget'] = widget;
	                
	                if (name_parts_count > 0) {
	                    var prevs = [];
	                    for (var i=0; i < name_parts_count; i++) {
	                        var key = "['"+name_parts[i]+"']";
                            
                            if (prevs.length > 0) {
                                var key_prefix = '';
                                $.each(prevs, function(pi, pk){
                                    key_prefix = "['" + pk + "']" + key_prefix;
                                });
                                key = key_prefix + key;
                            }
                            
	                        if (typeof eval("form_values"+key) == 'undefined') {
	                            eval("form_values"+key+"={};");
	                        }
	                        
	                        prevs.push(name_parts[i]);
                            if (i == name_parts_count-1) {
                                if ($.browser.mozilla) {
                                    if (typeof item == 'object') {
                                        eval("form_values"+key+"="+item.toSource()+";");
                                    } else {
                                        eval("form_values"+key+"="+item+";");
                                    }
                                } else {
                                    eval("form_values"+key+"="+item+";");
                                }
                            }
	                    }
	                } else {
	                    form_values[row.name] = item;
	                }
	            }
    	    });
        }
    };
    
    $.ajatus.document.additionals = {
        defaults: {
            auto_show: true
        },
        open: [],
        active: null,
        top_start: 17,
        left_start: 50
    };
    $.extend($.ajatus.document.additionals, {
        create: function(content_type, row_holder, opts) {
            $.ajatus.document.additionals._reset();
            
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Add field'), '', {
                modal: false,
                dialog: {
                    height: 280
                }
            });
            
            var content_holder = $.ajatus.document.scheme.items._create_structure( dialog );
            
            var jqform = $('.form', content_holder);
            var contents = $('.contents', content_holder);
            var form_errors = $('.form_errors', content_holder);
            var wdgts = $.ajatus.document.additionals._generate_widget_list();

            $('<label for="widget"/>').html($.ajatus.i10n.get('Widget')).appendTo(contents);
            var wdgt_sel_holder = $('<div id="widget_selection_holder"/>');
            wdgt_sel_holder.html(wdgts).appendTo(contents);
            
            var wdgt_details = $('<div id="widget_details_holder"/>');
            wdgt_details.appendTo(contents);
            
            var wdgts_on_change = function(e){
                var sel_w = e.currentTarget.value;
                if (sel_w != '') {
                    wdgt_sel_holder.html($.ajatus.i10n.get(sel_w));
                    wdgt_sel_holder.addClass('selected');
                    wdgt_details.html($.ajatus.document.additionals._generate_widget_details(sel_w));

                    var field = $('<input type="hidden" />').attr({
                        name: '_widget',
                        value: sel_w
                    });
                    field.appendTo(jqform);
                }
            };
            
            wdgts.bind('change', wdgts_on_change);
            
            wdgt_sel_holder.bind('dblclick', function(e){
                wdgt_sel_holder.removeClass('selected');
                wdgt_sel_holder.html(wdgts);
                wdgts.bind('change', wdgts_on_change);
            });

            $('.actions #submit_save', content_holder).bind('click', function(e){
                var result = $.ajatus.document.additionals.save_widget(jqform.formToArray(false), jqform);
                
                if (result == true) {
                    $.ajatus.document.additionals.close(dialog);
                } else {
                    if (typeof result == 'object') {
                        form_errors.html('');
                	    var error_holder = $('<ul />');
                	    $.each(result, function(field, data){
                	        $('<li class="error" />').html(data.msg).appendTo(error_holder);
                	    });
                	    error_holder.appendTo(form_errors);
                	    form_errors.show();
                    }
                }
                
                return false;
            });

            $.ajatus.document.additionals.show(dialog);
        },
        edit: function(widget, row_holder, name, data, opts) {
            $.ajatus.document.additionals._reset();

            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Edit field %s', [name]), '', {
                modal: false,
                dialog: {
                    height: 280
                }
            });
            
            var content_holder = $.ajatus.document.scheme.items._create_structure( dialog );
            
            var jqform = $('.form', content_holder);
            var contents = $('.contents', content_holder);
            var form_errors = $('.form_errors', content_holder);

            if (typeof data['widget'] != 'undefined') {
                data.widget.config = $.ajatus.document.additionals._read_settings(name, $.ajatus.forms.active, widget);
            }

            var prev_value = null;
            var prev_val_input = $('#_element_'+name+'_widget input[name="widget['+name+':prev_val]"]', $.ajatus.forms.active);
            if (typeof prev_val_input[0] != 'undefined') {
                prev_value = $.ajatus.converter.parseJSON(prev_val_input.val());
            }

            var wdgt_details = $('<div id="widget_details_holder"/>');
            wdgt_details.appendTo(contents);
            
            wdgt_details.html($.ajatus.document.additionals._generate_widget_details(widget.name, name, data));

            $('.actions #submit_save', content_holder).bind('click', function(e){
                var prev_data = {
                    name: name,
                    data: data
                };
                if (prev_value != null) {
                    prev_data.value = prev_value;
                }
                var result = $.ajatus.document.additionals.save_widget(jqform.formToArray(false), jqform, prev_data);
                
                if (result == true) {
                    $.ajatus.document.additionals.close(dialog);
                } else {
                    if (typeof result == 'object') {
                        form_errors.html('');
                        var error_holder = $('<ul />');                     
                        $.each(result, function(field, data){
                            $('<li class="error" />').html(data.msg).appendTo(error_holder);
                        });
                        error_holder.appendTo(form_errors);
                        form_errors.show();
                    }
                }
            });
            
            $.ajatus.document.additionals.show(dialog);
        },
        config: function(widget, prev_vals, on_save, parent, name) {
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Configure widget %s', [$.ajatus.i10n.get(widget.name)]), '', {
                modal: false,
                dialog: {
                    height: 310,
                    nested: true
                }
            });
            
            var content_holder = $.ajatus.document.scheme.items._create_structure( dialog );
            
            if (typeof prev_vals == 'undefined') {
                var prev_vals = {};
            }
            if (typeof on_save == 'undefined') {
                var on_save = '$.ajatus.document.additionals.save_widget_settings';
            }

            var jqform = $('.form', content_holder);
            var contents = $('.contents', content_holder);

            var wdgt_settings = $('<div id="widget_settings_holder" />');
            wdgt_settings.appendTo(contents);
            wdgt_settings.html($.ajatus.document.additionals._generate_widget_settings(widget, prev_vals));
            
            $('.actions #submit_save', content_holder).bind('click', function(e){
                var parent_dialog = parent;
                if (typeof parent == 'undefined') {
                    parent_dialog = $.ajatus.document.additionals._get_parent(dialog.id);
                }

                var fn = on_save;
        	    if (typeof on_save == 'string') {
                    fn = eval(on_save);
                }
                var result = fn.apply(fn, [jqform.formToArray(false), parent_dialog, name, jqform]);
                
                if (result) {                    
                    $.ajatus.document.additionals.close(dialog);
                }
            });
            
            $.ajatus.document.additionals.show(dialog);
        },
        del: function(widget, row_holder, name, data) {            
            var label = name;
            if (   typeof data == 'object'
                && typeof data.label != 'undefined')
            {
                label = data.label;
            }
        
            var confirmation = confirm($.ajatus.i10n.get("Do you really want to delete item %s", [label]));
            
            if (confirmation == true) {
                $('#_element_'+name, row_holder).remove();                
            }
        },
        show: function(dialog) {
            var dlg = dialog.get_dialog();
            dlg.addClass('additionals_window');
            
            dialog.open();
            $.ajatus.document.additionals.open.push(dialog.id);
            $.ajatus.document.additionals._position(dlg);
        },
        close: function(dialog) {            
            dialog.close();
            
            $.ajatus.document.additionals.open = $.grep($.ajatus.document.additionals.open, function(n,i){
                if (n != dialog.id) {
                    return true;
                } else {
                    return false;
                }
            });
        },
        _position: function(dialog) {
            var top = $.ajatus.document.additionals.top_start;
            var left = $.ajatus.document.additionals.left_start;
            
            if ($.ajatus.document.additionals.open.length > 1) {
                var incr = $.ajatus.document.additionals.open.length * 2;
                top += incr;
                left += incr;
            }

            dialog.css({
                top: top+'%',
                left: left+'%'
            });
        },
        _process_widget_settings: function(form_data, form_values) {
            $.ajatus.document.scheme.items._process_widget_settings(form_data, form_values);
        },
        _settings_to_saved: function(values, holder) {
            $.each(values, function(k,item){
                var field = $('<input type="hidden" />').attr({
                    name: 'widget[config;'+k+':name]',
                    value: item.widget.name
                });
                field.appendTo(holder);
                
                var field = $('<input type="hidden" />').attr({
                    name: 'widget[config;'+k+':config]',
                    value: $.ajatus.converter.toJSON(item.widget.config)
                });
                field.appendTo(holder);
                
                var field = $('<input type="hidden" />').attr({
                    name: 'config;'+k,
                    value: item.val != '' ? $.ajatus.converter.toJSON(item.val) : ''
                });
                field.appendTo(holder);
            });            
        },
        save_widget_settings: function(form_data, parent_dialog, name, form) {
            var form_values = {};
            
            $.ajatus.document.additionals._process_widget_settings(form_data, form_values);
            
            var config_holder = $('.saved_settings', parent_dialog);
            config_holder.html('');
            
            $.ajatus.document.additionals._settings_to_saved(form_values, config_holder);
            
            return true;
        },
        _read_settings: function(name, form, widget) {
            if (typeof form == 'undefined') {
                var form = $.ajatus.forms.active;
            }
                    
            var prev_vals = {};
        
            if (typeof widget != 'undefined') {
                prev_vals = widget.config;
            }
        
            var widget_config_input = $('#_element_'+name+'_widget input[name="widget['+name+':config]"]', form);
            if (typeof widget_config_input[0] != 'undefined') {
                prev_vals = $.ajatus.converter.parseJSON(widget_config_input.val());
            }
        
            return prev_vals;
        },
        update_widget_settings: function(form_data, parent_form, name, form) {
            var form_values = {};
            
            $.ajatus.document.additionals._process_widget_settings(form_data, form_values);
            
            var config = {};
            $.each(form_values, function(i,n){
                config[i] = n.val;
            });
            
            var row = $('#_element_'+name, parent_form);

            var add_upd_elem = $('input[name="widget['+name+':additional_updated]"]', row);
            if (typeof add_upd_elem[0] == 'undefined') {
	            row.createAppend(
	                'input', { type: 'hidden', name: 'widget['+name+':additional_updated]', value: $.ajatus.converter.toJSON(true) }, ''
                );
            } else {
                add_upd_elem.val($.ajatus.converter.toJSON(true));
            }
            
            var additional_input = $('#_element_'+name+' input[name="widget['+name+':additional]"]', parent_form);
            var widget_config_input = $('#_element_'+name+'_widget input[name="widget['+name+':config]"]', parent_form);
            
            var add_data = $.ajatus.converter.parseJSON(additional_input.val());
            if (   typeof add_data.widget != 'undefined'
                && typeof add_data.widget.config != 'undefined')
            {
                add_data.widget.config = config;          
            }
            
            additional_input.val($.ajatus.converter.toJSON(add_data));
            widget_config_input.val($.ajatus.converter.toJSON(config));
            
            return true;
        },
        save_widget: function(form_data, form, prev_data) {
            var form_values = {};
            var sel_widget = 'text';
            
            var has_errors = false;
            var error_fields = {};
            
    	    $.each(form_data, function(i,row){    	        
                if (row.name.toString().match(/__(.*?)/)) {
                    return;
                }
    	        if (row.name == '_widget') {
    	            sel_widget = String(row.value);
    	        }
	            else if (row.name.substr(0,6) != "widget") {
	                var item = {};
	                var widget = {};
	                var prev_val = false;
	                var name_parts = [];
	                var name_parts_count = 0;
	                var row_key = row.name;
	                
	                if (row.name.toString().match(/;/g)) {
	                    name_parts = row.name.toString().split(";");
	                    name_parts_count = name_parts.length;
	                    
	                    row_key = name_parts[0];
	                }
	                
	                $.each(form_data, function(x,r){
	                    if (r.name == 'widget['+row.name+':name]') {
	                        widget['name'] = r.value;
	                    } else if (r.name == 'widget['+row.name+':config]') {
                            widget['config'] = $.ajatus.converter.parseJSON(r.value);
                        } else if (r.name == 'widget['+row.name+':prev_val]') {
                            if (r.value == '') {
                                prev_val = r.value;
                            } else {
                                prev_val = $.ajatus.converter.parseJSON(r.value);
                            }
                        } else if (r.name == 'widget['+row.name+':required]') {
                            widget['required'] = $.ajatus.utils.to_boolean(r.value);
                        }
                    });
                    
                    var wdgt = new $.ajatus.widget(widget['name'], widget['config']);
                    
                    if (typeof widget['required'] != 'undefined') {
            	        wdgt.required = widget['required'];
        	        }
                    
                    if (typeof wdgt.validate == 'function') {
                        var status = wdgt.validate(row_key, row.value);
                        if (typeof status == 'object') {
                            has_errors = true;
                            error_fields[row_key] = status;
                        }
                    }

	                item['val'] = wdgt.value_on_save(row.value, prev_val);

	                item['widget'] = widget;
	                
	                if (name_parts_count > 0) {
	                    var prevs = [];
	                    for (var i=0; i < name_parts_count; i++) {        	                        
	                        var arr_keys = false;
	                        var key_prefix = '';
	                        
	                        if (name_parts[i].match(/\[/g)) {
	                            var arr_keys = name_parts[i].split('[');
	                            
	                            name_parts[i] = name_parts[i].replace(/\[/g, '"][');
	                            var key = '["'+name_parts[i];
	                        } else {
	                            var key = "['"+name_parts[i]+"']";
	                        }
                            
                            if (prevs.length > 0) {
                                $.each(prevs, function(pi, pk){
                                    key_prefix = "['" + pk + "']" + key_prefix;
                                });
                                key = key_prefix + key;
                            }
                            
                            if (arr_keys) {
                                var tmp_key = key_prefix + "['" + arr_keys[0] + "']";
                                if (typeof eval("form_values"+tmp_key) == 'undefined') {
    	                            eval("form_values"+tmp_key+"=[];");
    	                        }                    
                            }
                            
                            var multiple = false;
	                        if (typeof eval("form_values"+key) == 'undefined') {
	                            if (key_prefix != '') {
    	                            eval("form_values"+key+"=new Array();");
	                            } else {
    	                            eval("form_values"+key+"={};");        	                                
	                            }
	                        } else {                         
	                            multiple = true;
	                        }
	                        
	                        prevs.push(name_parts[i]);
	                        
                            if (i == name_parts_count-1) {
                                if (typeof item['val'] == 'undefined') {
                                    return;
                                }
                                if (multiple) {
                                    if (typeof item == 'object') {
                                        if (item.widget.name == 'boolean') {
                                            eval("form_values"+key+"="+item.val+";");
                                        } else {
                                            eval("form_values"+key+".push('"+item.val+"');");                                                    
                                        }
                                    } else {
                                        eval("form_values"+key+".push('"+item+"');");
                                    }                                                
                                } else {
                                    if (typeof item == 'object') {
                                        eval("form_values"+key+"='"+item.val+"';");
                                    } else {
                                        eval("form_values"+key+"='"+item.val+"';");
                                    }
                                }
                            }
	                    }
	                } else {
	                    form_values[row.name] = item;
	                }
	            }
    	    });
            
            var config = {};
            if (typeof form_values['config'] != 'undefined') {
                $.each(form_values['config'], function(i,n){
                    if (typeof n == 'object') {
                        config[i] = n.val != '' ? $.ajatus.converter.parseJSON(n.val) : '';
                    } else {
                        config[i] = n != '' ? $.ajatus.converter.parseJSON(n) : '';
                    }
                });
            }
            
            var item_name = form_values.name.val.toString().toLowerCase();            
            
            if (has_errors) {
                return error_fields;
            }
            
            
            var additional_item = {
                label: form_values.label.val,
                widget: {
                    name: sel_widget,
                    config: config
                },
                def_val: form_values.def_val.val,
                required: form_values.required.val
            };
            
            var row_holder = $('.form_structure ul.row_holder:eq(0)', $.ajatus.forms.active);
            
            if (typeof prev_data != 'undefined') {
                if (typeof prev_data.value != 'undefined') {
                    additional_item.value = prev_data.value;                    
                }
                
                $.ajatus.renderer.form_helpers._add_additional_row('edit', row_holder, item_name, additional_item, {}, prev_data.name);
                
                return true;
            }

            $.ajatus.renderer.form_helpers._add_additional_row('create', row_holder, item_name, additional_item);
            
            return true;
        },
        edit_existing_config: function(name, widget) {            
            var prev_vals = $.ajatus.document.additionals._read_settings(name, $.ajatus.forms.active, widget);
            
            var on_save = '$.ajatus.document.additionals.update_widget_settings';
            
            $.ajatus.document.additionals.config(widget, prev_vals, on_save, $.ajatus.forms.active, name);            
        },
        generate_item_actions_tpl: function(name, widget, data, doc, row_holder) {
            var item_data = data;
            return [
                'img', { className: 'additional_edit_btn', src: $.ajatus.preferences.client.theme_icons_url + 'edit.png', title: $.ajatus.i10n.get('Edit'), alt: $.ajatus.i10n.get('Edit'), onclick: function(){$.ajatus.document.additionals.edit(widget, row_holder, name, item_data, {});} }, '',
                'img', { className: 'additional_delete_btn', src: $.ajatus.preferences.client.theme_icons_url + 'trash.png', title: $.ajatus.i10n.get('Delete'), alt: $.ajatus.i10n.get('Delete'), onclick: function() {$.ajatus.document.additionals.del(widget, row_holder, name, item_data);} }, ''
            ];
        },
        _get_parent: function() {            
            var parent_id = $.ajatus.document.additionals.open[$.ajatus.document.additionals.open.length-2];
            return $('#'+parent_id);
        },
        _create_structure: function(id, options) {            
            var af_win = $('<div class="additionals_window" />').attr({
                id: id
            }).hide();
            var title = $('<h2 class="title"/>');
            var form = $('<form id="additionals_window_form" class="form" />');
            var form_errors = $('<div class="form_errors" />').hide();
            var content_area = $('<div class="contents"/>');
            var actions = $('<div class="actions" />').html('<input type="submit" id="submit_save" name="save" value="' + $.ajatus.i10n.get('Save') + '" /><input type="submit" id="submit_cancel" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" />');
                                    
            af_win.appendTo($.ajatus.application_dynamic_elements);
            title.appendTo(af_win);
            form.appendTo(af_win);
            form_errors.appendTo(form);
            content_area.appendTo(form);
            actions.appendTo(af_win);

            $('#submit_cancel', actions).bind('click', function(e){
                $.ajatus.document.additionals.close(id);
                return false;
            });
            
            return af_win;
        },
        _generate_widget_list: function() {
            var select = $('<select id="widget" name="widget" />');
                        
            $.each($.ajatus.widgets.loaded_widgets, function(i,w){
                var opt = $('<option />').attr({
                    value: w
                }).html($.ajatus.i10n.get(w));
                opt.appendTo(select);
            });

            var opt = $('<option />').attr({
                value: '',
                selected: 'selected'
            }).html($.ajatus.i10n.get('Select one') + '&nbsp;');
            opt.prependTo(select);
            
            return select;
        },
        _generate_widget_details: function(widget_name, name, data) {
            var widget = $.ajatus.widget(widget_name);
            var details = $('<p>' + $.ajatus.i10n.get("Widget %s doesn't support dynamic creation.", [$.ajatus.i10n.get(widget_name)]) + '</p>');
            if (typeof(widget['create_widget_details']) == 'function') {
                details = widget.create_widget_details(data || {}, name);
            }
            
            return details;
        },
        _generate_widget_settings: function(widget_name, data) {
            return $.ajatus.document.scheme.items._generate_widget_settings(widget_name, data);
        },
        _reset: function() {
            $.ajatus.document.additionals.open = [];
            $.ajatus.application_dynamic_elements.html('');
        }
    });

})(jQuery);