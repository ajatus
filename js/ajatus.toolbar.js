(function($){
    $.ajatus = $.ajatus || {};

    $.ajatus.toolbar = {
        settings: {
            enable_clone: false
        },
        visible: false,
        objects: {},
        items: []
    };
    $.extend($.ajatus.toolbar, {
        init: function(settings) {
            $.ajatus.toolbar.settings = $.extend($.ajatus.toolbar.settings, settings || {});
            $.ajatus.toolbar.objects = {};
            $.ajatus.toolbar.items = [];
            
            var main_tb = $('#main-page_toolbar', $.ajatus.application_element).hide();
            if (! main_tb[0]) {
                main_tb = $.ajatus.toolbar.create_main();
            } else {
                $.ajatus.toolbar.prepare_main(main_tb);
            }
            
            $.ajatus.toolbar.objects['main'] = main_tb;
            
            if ($.ajatus.toolbar.settings.enable_clone) {
                var clone_tb = $('#clone-page_toolbar', $.ajatus.application_element).hide();
                if (! clone_tb[0]) {
                    clone_tb = $.ajatus.toolbar.create_clone(main_tb);
                } else {
                    $.ajatus.toolbar.prepare_clone();
                }
                $.ajatus.toolbar.objects['clone'] = clone_tb;
            }
            
            $.ajatus.toolbar.clear();
        },
        show: function(index) {
            if (typeof index == 'undefined') {
                $.each($.ajatus.toolbar.objects, function(i,o){
                    o.show();
                });
            } else {
                if (typeof($.ajatus.toolbar.objects[index]) != 'undefined') {
                    $.ajatus.toolbar.objects[index].show();
                }
            }
            $.ajatus.toolbar.visible = true;
        },
        hide: function(index) {
            if (typeof index == 'undefined') {
                $.each($.ajatus.toolbar.objects, function(i,o){
                    o.hide();
                });
            } else {
                if (typeof($.ajatus.toolbar.objects[index]) != 'undefined') {
                    $.ajatus.toolbar.objects[index].hide();
                }
            }
            $.ajatus.toolbar.visible = false;
        },
        add_item: function() { // title, settings/icon/action, action, action args, insert after
            if (arguments.length <= 0) {
                return;
            }
            var item_obj = {
                id: $.ajatus.utils.generate_id(),
                title: arguments[0],
                icon: null,
                content: [],
                action: null,
                extra_args: [],
                insert_after: null,
                access_key: null
            };            
            var item_settings = {};
            var extra_args = [];
            
            var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['main']);
            
            if (arguments.length == 2) {
                if (typeof arguments[1] == 'object') {
                    $.each($.ajatus.toolbar._parse_settings(arguments[1]), function(key,value){
                        item_obj[key] = value;
                    });
                    extra_args = [item_obj];
                    $.each(item_obj['extra_args'], function(i,n){
                        extra_args.push(n);
                    });
                } else {
                    item_obj['action'] = arguments[1];
                    item_obj['content'] = [
                        'div', {}, title
                    ];                    
                }
            }
            
            if (arguments.length >= 3) {
                item_obj['icon'] = $.ajatus.preferences.client.theme_icons_url + arguments[1];
                item_obj['action'] = arguments[2];
                item_obj['content'] = [
                    'div', {}, [
                        'img', { src: item_obj['icon'], alt: item_obj['title'], title: item_obj['title'] }, ''
                    ]
                ];
            } else if (item_obj['icon'] != null) {
                item_obj['content'] = [
                    'div', {}, [
                        'img', { src: item_obj['icon'], alt: item_obj['title'], title: item_obj['title'] }, ''
                    ]
                ];
            }
            
            if (   arguments.length >= 4
                && typeof arguments[3] == 'object'
                && arguments[3].length > 0)
            {
                item_obj['extra_args'] = arguments[3];
                extra_args = [item_obj];
            }
            
            if (extra_args.length > 0) {
                $.each(item_obj['extra_args'], function(i,n){
                    extra_args.push(n);                    
                });
            }

            if (arguments.length == 5) {
                item_obj['insert_after'] = arguments[4];
            }

            if (item_obj['insert_after'] != null) {         
                var item_elem = $('<li class="item" />').attr({
                    id: item_obj.id
                });                
                item_elem.insertAfter($('#'+item_obj['insert_after'], item_holder));                
                $('#'+item_obj.id, item_holder).createAppend(item_obj['content'][0], item_obj['content'][1], item_obj['content'][2]);
            } else {
                item_holder.createAppend(
                    'li', { className: 'item', id: item_obj.id }, item_obj['content']
                );
            }
            
            $('#'+item_obj.id, item_holder).bind('click', function(e){
                item_obj['action'].apply(item_obj['action'], extra_args);
            });
            
            if (item_obj['access_key'] != null) {
                $.hotkeys.add(item_obj['access_key'], function(){ item_obj['action'].apply(item_obj['action'], extra_args); });
            }
            
            if ($.ajatus.toolbar.settings.enable_clone) {
                var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['clone']);
                
                if (item_obj['insert_after']) {
                    var item_elem = $('<li class="item" />').attr({
                        id: item_obj.id+'_clone'
                    });
                    item_elem.insertAfter($('#'+item_obj['insert_after']+'_clone', item_holder));
                    $('#'+item_obj.id+'_clone', item_holder).createAppend(item_obj['content'][0], item_obj['content'][1], item_obj['content'][2]);
                } else {
                    item_holder.createAppend(
                        'li', { className: 'item', id: item_obj.id+'_clone' }, item_obj['content']
                    );                
                }

                $('#'+item_obj.id, item_holder).bind('click', function(e){
                    item_obj['action'].apply(item_obj['action'], extra_args);
                });
            }
            
            $.ajatus.toolbar.items.push(item_obj);
            
            return item_obj.id;
        },
        remove_item: function(id) {
            var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['main']);
            $('#'+id, item_holder).remove();
            if ($.ajatus.toolbar.settings.enable_clone) {
                var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['clone']);
                $('#'+id+'_clone', item_holder).remove();
            }
            $.ajatus.toolbar.items = $.grep($.ajatus.toolbar.items, function(n,i){
                if (n.id == id) {
                    if (n['access_key'] != null) {
                        $.hotkeys.remove(n['access_key']);
                    }
                    return false;
                }
                return true;
            });
        },
        hide_item: function(id) {
            var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['main']);
            $('#'+id, item_holder).hide();

            if ($.ajatus.toolbar.settings.enable_clone) {
                var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['clone']);
                $('#'+id+'_clone', item_holder).hide();
            }
        },
        show_item: function(id) {
            var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['main']);
            $('#'+id, item_holder).show();
            
            if ($.ajatus.toolbar.settings.enable_clone) {
                var item_holder = $('ul.item_holder', $.ajatus.toolbar.objects['clone']);
                $('#'+id+'_clone', item_holder).show();
            }  
        },
        clear: function() {
            $('ul.item_holder', $.ajatus.toolbar.objects['main']).html('');
            if ($.ajatus.toolbar.settings.enable_clone) {
                $('ul.item_holder', $.ajatus.toolbar.objects['clone']).html('');
            }
            $.ajatus.toolbar.items = [];
        },
        create_main: function() {
            
        },
        create_clone: function() {
            
        },
        prepare_main: function(tb) {
            var tb_content = $('.content', tb);
            tb_content.html('');
            var items_holder = $('<ul class="item_holder"/>');
            items_holder.appendTo(tb_content);
        },
        prepare_clone: function(tb) {
            var tb_content = $('.content', tb);
            tb_content.html('');
            var items_holder = $('<ul class="item_holder"/>');
            items_holder.appendTo(tb_content);            
        },
        clone: function() {
            var main_tb_content = $('.content', $.ajatus.toolbar.objects['main']);
            var clone_tb_content = $('.content', $.ajatus.toolbar.objects['clone']);
            var main_items = $('ul.item_holder', main_tb_content).html();
            $('ul.item_holder', clone_tb_content).html(main_items);            
        },
        _parse_settings: function(s) {
            var settings = {};
            $.each(s, function(k, v){
                if ($.inArray(k, ['icon','action','insert_after','access_key','extra_args']) != -1) {
                    if (k == 'icon') {
                        v = $.ajatus.preferences.client.theme_icons_url + v;
                    }
                    settings[k] = v;
                }
                return;
            });

            return settings;
        }
    });
    
})(jQuery);