/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};

    $.ajatus.types = {};

    $.ajatus.types.init = function(on_ready)
    {
        if (typeof on_ready != 'function') {
            var on_ready = function(){return;};
        }
        
        $.ajatus.events.named_lock_pool.increase('init_types');
        $.ajatus.extensions.lock = new $.ajatus.events.lock({
	        watch: {
	            validate: function(){return $.ajatus.events.named_lock_pool.count('init_types') == 0;},
	            interval: 200,
                safety_runs: 0
	        },
	        on_release: on_ready
	    });
	    
        $.each($.ajatus.preferences.client.types.system, function(i,type){
            var type_url = $.ajatus.preferences.client.application_url + 'js/content_types/'+type+'.js';
            
            $.ajatus.events.named_lock_pool.increase('init_types');
            
            $.ajatus.utils.load_script(type_url, "$.ajatus.types.type_loaded", [type]);
        });

        $.each($.ajatus.preferences.client.types.in_use, function(i,type){
            var type_url = $.ajatus.preferences.client.application_url + 'js/content_types/'+type+'.js';
            
            $.ajatus.events.named_lock_pool.increase('init_types');
            
            $.ajatus.utils.load_script(type_url, "$.ajatus.types.type_loaded", [type]);
        });        

        $.ajatus.events.named_lock_pool.decrease('init_types');
    }
    
    $.ajatus.types.type_loaded = function(type)
    {
        // $.ajatus.preferences.client.content_types[type] = new $.ajatus.content_type[type]();
        
        $.ajatus.types.prepare_type(type);
        
        $.ajatus.events.named_lock_pool.decrease('init_types');
    }
    
    $.ajatus.types.prepare_type = function(type) {
        if (typeof $.ajatus.preferences.client.content_types[type] == 'undefined') {
            return;
        }
        
        //Set some default variables
        if (typeof $.ajatus.preferences.client.content_types[type]['on_frontpage'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].on_frontpage = true;
        }
        
        // Set default pool settings
        if (typeof $.ajatus.preferences.client.content_types[type]['pool_settings'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].pool_settings = {
                enabled: true,
                settings: {
                    type: 'scroll'                                        
                }
            };
        }
        
        // Clone defined scheme if not already done
        if (typeof $.ajatus.preferences.client.content_types[type]['original_schema'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type]['original_schema'] = $.ajatus.utils.object.clone($.ajatus.preferences.client.content_types[type]['schema']);
        }
        
        // Set default list headers
        if (typeof $.ajatus.preferences.client.content_types[type]['list_headers'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type]['list_headers'] = [
                'title', 'created', 'creator'
            ];
        }
        
        // Set default title of not found in schema
        if (   typeof $.ajatus.preferences.client.content_types[type]['schema']['title'] == 'undefined'
            && typeof $.ajatus.preferences.client.content_types[type]['title_item'] == 'undefined')
        {
            $.ajatus.preferences.client.content_types[type]['title_item'] = [
                '_type'
            ];
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['view_header'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].view_header = 'if (doc.value.metadata.deleted.val == false && doc.value.metadata.archived.val == false && doc.value._type == "'+type+'") {';
        }
        if (typeof $.ajatus.preferences.client.content_types[type]['view_footer'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].view_footer = '}';
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['list_map'] == 'undefined') {
            var list_map = 'map( doc.value.metadata.created.val, {"_type": doc.value._type,';
            list_map += '"tags": doc.value.tags,';
            list_map += '"creator": doc.value.metadata.creator,';
            list_map += '"created": doc.value.metadata.created';
            list_map += '});';
            
            $.ajatus.preferences.client.content_types[type].list_map = list_map;
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['gen_views'] != 'undefined') {
            $.ajatus.preferences.client.content_types[type]['gen_views']();
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['listen_signals'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].listen_signals = [];
        }
        if ($.inArray('active_tag_changed', $.ajatus.preferences.client.content_types[type].listen_signals) == -1) {
            $.ajatus.preferences.client.content_types[type].listen_signals.push('active_tag_changed');            
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['on_signal'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].on_signal = {};
        }
        if (typeof $.ajatus.preferences.client.content_types[type].on_signal['active_tag_changed'] == 'undefined') {
            $.ajatus.preferences.client.content_types[type].on_signal['active_tag_changed'] = function() {
                var list_at = $.ajatus.preferences.client.content_types[type].view_header;
                list_at += 'var hm = false;';
                list_at += 'for (var i=0;i<doc.value.tags.val.length;i++) {';
                list_at += 'if (doc.value.tags.val[i] == "'+$.ajatus.tags.active.id+'") { hm = true; }';
                list_at += '} if (hm == true) {';
                list_at += $.ajatus.preferences.client.content_types[type].list_map;
                list_at += '}' + $.ajatus.preferences.client.content_types[type].view_footer;

                $.ajatus.preferences.client.content_types[type].views['list_at'] = $.ajatus.views.generate(list_at);
            }
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['statics'] == 'undefined') {
            if (   typeof $.ajatus.preferences.client.content_types[type]['views'] != 'undefined'
                && typeof $.ajatus.preferences.client.content_types[type]['views']['list'] != 'undefined')
            {
                $.ajatus.preferences.client.content_types[type].statics = {
                    list: $.ajatus.preferences.client.content_types[type].views.list
                };
            }
        }
        
        if (typeof $.ajatus.preferences.client.content_types[type]['render'] != 'function') {
            $.ajatus.preferences.client.content_types[type].render = function(view_name) {
                var self = $.ajatus.preferences.client.content_types[type];
                if (typeof self['render_'+view_name] == 'function') {
                    self['render_'+view_name]();
                } else {
                    $.ajatus.debug('Undefined view '+view_name, type);
                }
                
                var app_tab_holder = $('#tabs-application ul');
                app_tab_holder.html('');
                
                if (typeof self.additional_views != 'undefined') {                    
                    $.each(self.additional_views, function(name, data){
                        var view_hash = '#'+data.hash_key+'.'+type;                    
                        var tab = $('<li><a href="'+view_hash+'"><span>'+$.ajatus.i10n.get(data.title)+'</span></a></li>');
                    
                        tab.appendTo(app_tab_holder);
                        $.ajatus.tabs.prepare(tab);
                    });
                }
            };
        }
        
        $.each($.ajatus.preferences.client.content_types[type].listen_signals, function(i,name){
            if (typeof $.ajatus.preferences.client.content_types[type].on_signal[name] != 'undefined') {
                $.ajatus.events.signals.add_listener(name, $.ajatus.preferences.client.content_types[type].on_signal[name]);
            }
        });
        
        // Set default renderer
        if (   typeof $.ajatus.preferences.client.content_types[type]['render_list'] != 'function'
            && $.ajatus.preferences.client.content_types[type]['in_tabs'] == true)
        {
            $.ajatus.preferences.client.content_types[type].render_list = function() {
                var self = $.ajatus.preferences.client.content_types[type];
                var indicator = new $.ajatus.elements.indicator();
                indicator.show();
                
                $.ajatus.active_type = self;
                $.ajatus.layout.body.set_class('list '+self.name);
                $.ajatus.application_content_area.html('');

                $.ajatus.layout.title.update({
                    view: $.ajatus.i10n.plural($.ajatus.i10n.get(self.title))
                });

                $.ajatus.toolbar.add_item($.ajatus.i10n.get('New %s', [self.name]), {
                    icon: self.name+'-new.png',
                    action: function(){
                        $.ajatus.views.system.create.render(self);
                    },
                    access_key: 'Ctrl+n'
                });
                $.ajatus.toolbar.show();
                
                var doc_count = 0;
                if (   $.ajatus.tags.active != ''
                    && typeof self.views['list_at'] != 'undefined')
                {
                    doc_count = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                        count: 0
                    }).total_rows;
                } else {
                    doc_count = $.jqCouch.connection('view').get($.ajatus.preferences.client.content_database, self.name+'/list', {
                        count: 0
                    }).total_rows;                    
                }

                if (doc_count == 0) {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get(self.title));
                    var msg = $.ajatus.elements.messages.static($.ajatus.i10n.get('Empty results'), $.ajatus.i10n.get('No %s found', [key]));
                    return;
                }

                if (doc_count <= $.ajatus.renderer_defaults.use_db_after) {
                    var on_success = function(data) {
                        var renderer = new $.ajatus.renderer.list(
                            $.ajatus.application_content_area,
                            self,
                            {
                                id: self.name + '_list_holder',
                                pool: self.pool_settings
                            }
                        );

                        $.each(data.rows, function(i,doc){
                            var doc = new $.ajatus.document(doc);
                            renderer.render_item(doc, i);
                        });

                        renderer.items_added();
                        renderer.enable_sorting();
                        
                        indicator.close();
                        return data;
                    };

                    if (   $.ajatus.tags.active != ''
                        && typeof self.views['list_at'] != 'undefined')
                    {
                        $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                            descending: true
                        });
                    } else {
                        $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, self.name+'/list', {
                            descending: true
                        });                        
                    }
                } else {
                    this.pool_settings.settings.use_db = true;
                    if (   $.ajatus.tags.active != ''
                        && typeof self.views['list_at'] != 'undefined')
                    {
                        this.pool_settings.settings.db = {
                            "temp": self.views['list_at']
                        };
                    } else {
                        this.pool_settings.settings.db = {
                            "static": self.name+'/list'
                        };                        
                    }

                    var on_success = function(data) {
                        var renderer = new $.ajatus.renderer.list(
                            $.ajatus.application_content_area,
                            self,
                            {
                                id: self.name + '_list_holder',
                                pool: self.pool_settings
                            }
                        );
                        
                        $.each(data.rows, function(i,doc){
                            var doc = new $.ajatus.document(doc);
                            renderer.render_item(doc, i);
                        });
                        
                        renderer.items_added(doc_count);
                        
                        indicator.close();
                        return data;
                    };

                    if (   $.ajatus.tags.active != ''
                        && typeof self.views['list_at'] != 'undefined')
                    {
                        var first_doc = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                            descending: true,
                            count: 1
                        }).rows[0];

                        $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                            descending: true,
                            startkey: first_doc.key,
                            startkey_docid: first_doc.id,
                            count: $.ajatus.renderer_defaults.items_per_page
                        });
                    } else {
                        var first_doc = $.jqCouch.connection('view').get($.ajatus.preferences.client.content_database, self.name+'/list', {
                            descending: true,
                            count: 1
                        }).rows[0];

                        $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, self.name+'/list', {
                            descending: true,
                            startkey: '"'+first_doc.key+'"',
                            startkey_docid: first_doc.id,
                            count: $.ajatus.renderer_defaults.items_per_page
                        });
                    }
                }
            };
        }
        
        // Set default export renderer if needed
        if (   typeof $.ajatus.preferences.client.content_types[type]['additional_views'] != 'undefined'
            && typeof $.ajatus.preferences.client.content_types[type]['additional_views']['export'] != 'undefined'
            && typeof $.ajatus.preferences.client.content_types[type]['render_export'] != 'function')
        {
            $.ajatus.preferences.client.content_types[type].render_export = function() {
                var self = $.ajatus.preferences.client.content_types[type];
                var indicator = new $.ajatus.elements.indicator();
                indicator.show();
                
                $.ajatus.active_type = self;
                $.ajatus.layout.body.set_class('export '+self.name);
                $.ajatus.application_content_area.html('');
                                
                var doc_count = 0;
                if (   $.ajatus.tags.active != ''
                    && typeof self.views['list_at'] != 'undefined')
                {
                    doc_count = $.jqCouch.connection('view').temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                        count: 0
                    }).total_rows;
                } else {
                    doc_count = $.jqCouch.connection('view').get($.ajatus.preferences.client.content_database, self.name+'/list', {
                        count: 0
                    }).total_rows;                    
                }

                if (doc_count == 0) {
                    var key = $.ajatus.i10n.plural($.ajatus.i10n.get(self.title));
                    var msg = $.ajatus.elements.messages.static(
                        $.ajatus.i10n.get('Empty results'),
                        $.ajatus.i10n.get('Nothing to export') + '<br />' + $.ajatus.i10n.get('No %s found', [key])
                    );
                    return;
                }
                
                var get_items = function(on_success) {
                    if (   $.ajatus.tags.active != ''
                        && typeof self.views['list_at'] != 'undefined')
                    {
                        $.jqCouch.connection('view', on_success).temp($.ajatus.preferences.client.content_database, self.views['list_at'], {
                            descending: true
                        });
                    } else {
                        $.jqCouch.connection('view', on_success).get($.ajatus.preferences.client.content_database, self.name+'/list', {
                            descending: true
                        });                        
                    }
                };
                
                $.ajatus.toolbar.add_item($.ajatus.i10n.get('Export %s', [self.name], self.name), {
                    icon: 'export.png',
                    action: function(){
                        $.ajatus.views.export_view();
                    },
                    access_key: 'Ctrl+x'
                });
                $.ajatus.toolbar.add_item($.ajatus.i10n.get('Archive visible'), {
                    icon: 'archive.png',
                    action: function(){
                        var answer = confirm($.ajatus.i10n.get("Archiving all items in view. Are you sure?"));
                        if (answer == true) {
                            var on_success = function(data) {
                                $.each(data.rows, function(i,doc){
                                    doc = new $.ajatus.document(doc);
                                    $.ajatus.document.actions.execute("archive", doc);
                                });
                            };
                            
                            get_items(on_success);
                        }
                    },
                    access_key: 'Ctrl+a'
                });
                $.ajatus.toolbar.show();
                
                var on_success = function(data) {
                    var list_headers = self.list_headers;
                    if (typeof self.export_list_headers != 'undefined') {
                        list_headers = self.export_list_headers;
                    }
                    
                    var renderer = new $.ajatus.renderer.list(
                        $.ajatus.application_content_area,
                        self,
                        {
                            id: self.name + '_export_list_holder',
                            show_actions: false,
                            headers: list_headers,
                            title: 'Export ' + self.title
                        }
                    );

                    $.each(data.rows, function(i,doc){
                        var doc = new $.ajatus.document(doc);
                        renderer.render_item(doc, i);
                    });

                    renderer.items_added();
                    renderer.enable_sorting();
                    
                    indicator.close();
                    
                    return data;
                };

                get_items(on_success);
            }
        }
    }
    
})(jQuery);