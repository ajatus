/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.views = $.ajatus.views || {};
    
    $.ajatus.views.init = function()
    {
        var views_tab_holder = $('#tabs-views ul');
        var app_tab_holder = $('#tabs-application ul');
        var app_views_holder = $('#application-views ul');
        
        $.each($.ajatus.views.system.available, function(i,sv){
            var view = $.ajatus.views.system[sv];
            if (   view.in_tabs
                && view.tab)
            {
                var view_hash = '#view.'+sv;

                var tab = $('<li><a href="'+view_hash+'"><span>'+$.ajatus.i10n.get(view.title)+'</span></a></li>');

                if (   typeof(view.application_view) != 'undefined'
                    && view.application_view)
                {                    
                    if (typeof view.icon != 'undefined') {
                        tab = $('<li><a href="'+view_hash+'" title="'+$.ajatus.i10n.get(view.title)+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + view.icon + '" /></a></li>');
                        tab.appendTo(app_views_holder);
                    }
                } else {                    
                    tab.appendTo(views_tab_holder);
                }
                $.ajatus.tabs.prepare(tab);
                
                if (   $.ajatus.history
                    && view.history_support) {
                    $.ajatus.history.add_map(view_hash, '$.ajatus.views.system.'+sv+'.render("list");');
                } else {
                    tab.bind('click',view.tab.on_click);
                }
                
                // if (   typeof view.has_additional_views != 'undefined'
                //     && view.has_additional_views
                //     && typeof view.generate_additional_views == 'function')
                // {
                //     app_tab_holder.html('');
                //     var addviews = view.generate_additional_views();
                //     $.each(addviews, function(name, data){                        
                //         var tab = $('<li><a href="'+data.view_hash+'"><span>'+$.ajatus.i10n.get(data.title)+'</span></a></li>');
                //         if (typeof data.icon != 'undefined') {
                //             tab = $('<li class="iconified"><a href="'+data.view_hash+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + data.icon + '" alt="'+$.ajatus.i10n.get(data.title)+'"/></a></li>');
                //         }
                //         tab.appendTo(app_tab_holder);
                // 
                //         $.ajatus.history.add_map(data.view_hash, '$.ajatus.history.update("'+data.view_hash+'");$.ajatus.views.system.'+sv+'.render("'+name+'");');
                //     });
                // }
            }
            
            if (typeof view['statics'] != 'undefined') {
                $.ajatus.views.install_statics(sv, view);
            }
            
            if (   $.ajatus.history
                && view.dynamic_history
                && typeof view.history_register == 'function')
            {
                view.history_register();
            }
            // $.ajatus.history.add_map(view_hash, '$.ajatus.views.system.'+sv+'.render();');
        });
        $('li:first', views_tab_holder).addClass('tabs-selected');
        
        // Add preferences view
        var pref_view = $.ajatus.preferences.view;
        var view_hash = '#view.preferences';
        var tab = $('<li><a href="'+view_hash+'" title="'+$.ajatus.i10n.get(pref_view.title)+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + pref_view.icon + '" /></a></li>');
        tab.appendTo(app_views_holder);
        $.ajatus.tabs.prepare(tab);
        
        $.ajatus.history.add_map(view_hash, '$.ajatus.preferences.view.render("edit");');
        
        if ($.ajatus.preferences.client.developer_tools) {
            var dev_view = $.ajatus.development.view;
            var view_hash = '#view.development';
            var tab = $('<li><a href="'+view_hash+'" title="'+$.ajatus.i10n.get(dev_view.title)+'"><img src="'+ $.ajatus.preferences.client.theme_icons_url + dev_view.icon + '" /></a></li>');
            tab.appendTo(app_views_holder);
            $.ajatus.tabs.prepare(tab);

            $.ajatus.history.add_map(view_hash, '$.ajatus.development.view.render("frontpage");');
            
            $.each($.ajatus.development.subview.available, function(i, name){
                var view = $.ajatus.development.subview[name];
                
                var vhash = '#view.development.'+name;
                if (typeof view.view_hash != 'undefined') {
                    vhash = view.view_hash;
                }
                
                $.ajatus.history.add_map(vhash, '$.ajatus.history.update("'+vhash+'");$.ajatus.development.view.render("'+name+'");');
            });
        }
        
        $.each($.ajatus.preferences.client.content_types, function(key,type){
            if (   type.in_tabs
                && type.tab)
            {
                var view_hash = '#view.'+key;
                var tab = $('<li><a href="'+view_hash+'"><span>'+$.ajatus.i10n.get(type.title)+'</span></a></li>');
                
                tab.appendTo(views_tab_holder);
                
                $.ajatus.tabs.prepare(tab);
                if (   $.ajatus.history
                    && type.history_support) {
                    $.ajatus.history.add_map(view_hash, '$.ajatus.history.update("'+view_hash+'");$.ajatus.views.on_change("'+view_hash+'");$.ajatus.preferences.client.content_types["'+key+'"].render("list");');
                } else {
                    tab.bind('click',type.tab.on_click);
                }
            }
            
            if (typeof type.additional_views != 'undefined') {
                $.each(type.additional_views, function(name, data){
                    var view_hash = '#'+data.hash_key+'.'+key;
                    
                    $.ajatus.history.add_map(view_hash, '$.ajatus.history.update("'+view_hash+'");$.ajatus.views.on_change("'+view_hash+'");$.ajatus.preferences.client.content_types["'+key+'"].render("'+name+'");');
                });
            }
            
            if (typeof type['statics'] != 'undefined') {
                $.ajatus.views.install_statics(key, type);
            }
        });
        
        if ($.ajatus.views['custom'])
        {
            $.ajatus.views.get_custom_views();          
        }
    };
    
    $.ajatus.views.get_custom_views = function()
    {
        if ($.ajatus.preferences.client.custom_views.length > 0)
        {
            $.each($.ajatus.preferences.client.custom_views, function(i,cv){
                $.ajatus.events.lock_pool.increase();
                
                $.getScript($.ajatus.preferences.client.application_url + 'js/views/custom/'+cv+'.js', function(){
                    $.ajatus.views.custom.available[cv] = $.ajatus.views.custom[cv];
                    $.ajatus.views.custom.init(cv);
                    
                    $.ajatus.events.lock_pool.decrease();
                });
            });
        }
    };
    
    $.ajatus.views.install_statics = function(view_name, view)
    {
        var view_exists = false;
        var existing_rev = null;
        var vc = $.jqCouch.connection('view');
        
        var static_views = {
            _id: '_design/' + view_name,
            views: {}
        };
        
        $.each(view.statics, function(name,fn){
            if (   name
                && fn)
            {
                if (typeof(fn) == 'string') {
                    fn = $.ajatus.views.generate(fn);
                }

                static_views.views[name] = fn;
            }
        });
        
        vc.exists($.ajatus.preferences.client.content_database, view_name, function(data){
            view_exists = true;
            existing_rev = data._rev;
        });
        
        var update = view.update_statics;
        if ($.ajatus.maintenance.recreate['views']) {
            update = true;
        }
        
        if (   !view_exists
            || update)
        {
            if (   view_exists
                && update)
            {
                static_views['_rev'] = existing_rev;
            }
            vc.save($.ajatus.preferences.client.content_database, static_views);
        }
    };
    
    $.ajatus.views.on_change_calls = {
        called: {}
    };
    $.ajatus.views.on_change_actions = {
        list: [],
        add: function(action) {
            if ($.inArray(action, $.ajatus.views.on_change_actions.list) == -1) {
                $.ajatus.views.on_change_actions.list.push(action);
            }
        },
        run: function() {
            $.each($.ajatus.views.on_change_actions.list, function(i,a){
                eval(a);
            });
            $.ajatus.views.on_change_actions.list = [];
        }
    }
    $.ajatus.views.on_change = function(new_view, keep_app_tabs)
    {
        // console.log('$.ajatus.views.on_change('+new_view+')');
        
        if (typeof keep_app_tabs == 'undefined') {
            keep_app_tabs = false;
        }
        
        if (   typeof($.ajatus.views.on_change_calls.called[new_view]) == 'undefined'
            || $.ajatus.views.on_change_calls.called[new_view] == 2)
        {
            $.ajatus.views.on_change_calls.called[new_view] = 0;
        }
        
        // $.ajatus.debug('$.ajatus.views.on_change_calls.called['+new_view+']: '+$.ajatus.views.on_change_calls.called[new_view]);
        
        if ($.ajatus.views.on_change_calls.called[new_view] < 2) {
            $.ajatus.views.on_change_calls.called[new_view] += 1;

            // $.ajatus.forms.clear_data();
            if (! keep_app_tabs) {
                $('#tabs-application ul').html('');                
            }
            $.ajatus.events.named_lock_pool.clear('unsaved');
            $.ajatus.elements.messages.clear();
            $.ajatus.toolbar.clear();
            $.ajatus.views.on_change_actions.run();

            $.ajatus.document.actions.empty_pool();
            
            $.ajatus.events.signals.trigger('view_on_change', {
                new_view: new_view
            });
        }
    };
    
    $.ajatus.views.generate = function(fn_str)
    {
        var fn = "function(){var doc = arguments[0]; "+fn_str+"}";
        try {
            return eval('(' + fn + ')');
        } catch(e) {
            return function(){};
        }
        
    }
    
    $.ajatus.views.export_view = function(table, output_type) {
        if (typeof output_type == 'undefined') {
            var output_type = 'csv';
        }
        if (typeof table == 'undefined') {
            var table = $('table.listing', $.ajatus.application_content_area)[0];
        }
        
        var popup = window.open('exported.csv', 'Ajatus CRM - Export', 'width=765,height=480,resizable=false,scrollbars=no');
        var csvout = popup.document;
        
        var line_sep = "\n";
        if ($.browser.safari) {
            line_sep = "<br>";
        }
        
        if (output_type == 'csv') {
            var cntrows = 0;
            var tempdata = "";
        
            var numofRows = table.rows.length-1;
            var numofCells = table.rows[0].cells.length-1;
        
            var rowcsv = [numofRows];
            
            for ( var r = 0; r <= numofRows; r++) {
                var c =0;
                tempdata = "";
                for (c == 0; c<=numofCells; c++) {
                    if (c != numofCells) {
                        tempdata += $.trim($(table.rows[r].cells[c]).text()) + ",";
                    } else {
                        tempdata += $.trim($(table.rows[r].cells[c]).text()) + line_sep;
                    }
                }
                rowcsv[r] = tempdata;
            }
            
            csvout.open("text/comma-separated-values");
            
            for (var rowcnt = 0; rowcnt <= rowcsv.length-1; rowcnt++) {
                csvout.write(rowcsv[rowcnt]);
            }
            
            csvout.close();
        }
    }
    
})(jQuery);