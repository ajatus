/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};

    $.ajatus.forms = {
        active: false,
        active_type: null
    };
    $.extend($.ajatus.forms, {
        clear_data: function() {
            $.ajatus.forms.active = false;
            $.ajatus.forms.active_type = null;
            $.ajatus.forms.process.has_errors = false;
            $.ajatus.forms.process.error_fields = {};
        },
        register: {
            normal: function(form, type) {
                $.ajatus.events.named_lock_pool.increase('unsaved');
                $.ajatus.forms.clear_data();
                $.ajatus.forms.active = form;
                if (typeof type != 'undefined') {
                    $.ajatus.forms.active_type = type;                    
                }
                
        	    jqform = $(form);
        	    jqform.bind('submit',function(e){
        	        return false;
        	    });

        	    $('input[@type=submit][name*=save]', jqform).bind('click', function(){        	        
                    var status = $.ajatus.forms.process.normal(jqform.formToArray(false));
                    
                    $.ajatus.forms.active = false;
                    
                    return false;
        	    });
        	    $('input[@type=submit][name*=cancel]', jqform).bind('click', function(){
            	    $.ajatus.history.navigate(-1);
        	        
        	        $.ajatus.forms.active = false;
        	        return false;
        	    });
            },
            ajax: function(form, callback, type) {
                if (typeof callback == 'undefined') {
                    $.ajatus.forms.register.normal(form);
                    return;
                }
                $.ajatus.events.named_lock_pool.increase('unsaved');
                $.ajatus.forms.clear_data();
                $.ajatus.forms.active = form;
                if (typeof type != 'undefined') {
                    $.ajatus.forms.active_type = type;                    
                }
                
        	    jqform = $(form);
        	    jqform.bind('submit',function(e){
        	        return false;
        	    });

        	    $('input[@type=submit][name*=save]', jqform).bind('click', function(){
            	    $.ajatus.forms.process.ajax(jqform.formToArray(false), callback);
            	    
            	    $.ajatus.forms.active = false;
            	    return false;
        	    });
        	    $('input[@type=submit][name*=cancel]', jqform).bind('click', function(){
            	    $.ajatus.history.navigate(-1);
        	        
        	        $.ajatus.forms.active = false;
        	        return false;
        	    });
            },
            custom: function(form, save_func) {
                if (typeof save_func == 'undefined') {
                    return;
                }                
                $.ajatus.events.named_lock_pool.increase('unsaved');
                $.ajatus.forms.clear_data();
                $.ajatus.forms.active = form;
                
        	    jqform = $(form);
        	    jqform.bind('submit',function(e){
        	        return false;
        	    });

        	    $('input[@type=submit][name*=save]', jqform).bind('click', function(){
            	    var fn = save_func;
            	    if (typeof save_func == 'string') {
                        fn = eval(save_func);
                    }
                    fn.apply(fn, [jqform.formToArray(false), form]);
                    
                    $.ajatus.forms.active = false;
            	    return false;
        	    });
        	    $('input[@type=submit][name*=cancel]', jqform).bind('click', function(){
            	    $.ajatus.history.navigate(-1);
        	        
        	        $.ajatus.forms.active = false;
        	        return false;
        	    });                
            }
        },        
        process: {
            doc: null,
            has_errors: false,
            error_fields: {},
            
            common: function(form_data) {
                $.ajatus.forms.process.has_errors = false;
                $.ajatus.forms.process.error_fields = {};
                
        	    var doc = {
        	        _id: '',
        	        _rev: '',
        	        value: {}
        	    };
        	    var form_values = {};
        	    var form_id = '';
        	    var doc_type = 'note';
                var additionals = false;
                
                var parted_names_done = [];
                
                var prev_values_count = 0;
                var changed_values_count = 0;
                
        	    $.each(form_data, function(i,row){
                    // console.log('i: '+i+' row.name: '+row.name+' row.value: '+row.value);
                    if (row.name.toString().match(/__(.*?)/)) {
                        return;
                    }
        	        if (   row.name == '_id'
        	            && (   typeof row.value != 'undefined'
        	                && row.value != ''))
        	        {
        	            doc['_id'] = String(row.value);
        	            form_id = doc['_id'];
        	        }
        	        else if(   row.name == '_rev'
        	                && (   typeof row.value != 'undefined'
        	                    && row.value != ''))
        	        {
        	            doc['_rev'] = String(row.value);
    	            }
        	        else if(   row.name == '_additionals'
        	                && (   typeof row.value != 'undefined'
        	                    && row.value != ''))
        	        {
        	            //additionals = $.ajatus.converter.parseJSON(row.value);
        	        } else {
        	            if (row.name != 'submit') {
        	                if (row.name == '_type') {
            	                doc_type = row.value;
            	                form_values[row.name] = row.value;
            	            }
            	            else if (   row.name.substr(0,6) != "widget"
            	                     && row.name.substr(0,8) != "metadata")
            	            {
            	                var item = {};
            	                var widget = {};
            	                var prev_val = undefined;
            	                var additional = false;
            	                var additional_updated = false;
            	                var field_updated = false;
                                var row_key = row.name;
                                
              	                var name_parts = [];
            	                var name_parts_count = 0;
            	                if (row_key.toString().match(/\|/g)) {
            	                    name_parts = row_key.toString().split("|");
            	                    name_parts_count = name_parts.length;
            	                    
            	                    if ($.inArray(name_parts[0], parted_names_done) != -1) {
            	                        return;
            	                    }
            	                    
            	                    row_key = name_parts[0];
            	                }
            	                
            	                $.each(form_data, function(x,r){
            	                    if (r.name == 'widget['+row_key+':name]') {
            	                        widget['name'] = r.value;
            	                    } else if (r.name == 'widget['+row_key+':config]') {
        	                            widget['config'] = $.ajatus.converter.parseJSON(r.value);
        	                        } else if (r.name == 'widget['+row_key+':prev_val]') {
        	                            if (r.value == '') {
        	                                prev_val = r.value;
        	                            } else {
        	                                prev_val = $.ajatus.converter.parseJSON(r.value);
        	                            }
        	                            prev_values_count += 1;
        	                        } else if (r.name == 'widget['+row_key+':additional]') {
        	                            additional = $.ajatus.converter.parseJSON(r.value);
        	                        } else if (r.name == 'widget['+row_key+':field_updated]') {
                                        field_updated = $.ajatus.converter.parseJSON(r.value);
            	                    } else if (r.name == 'widget['+row_key+':additional_updated]') {
                                        additional_updated = $.ajatus.converter.parseJSON(r.value);
                	                } else if (r.name == 'widget['+row_key+':required]') {
                                        widget['required'] = $.ajatus.utils.to_boolean(r.value);
                                    }
        	                        
        	                    });

                        	    if (   additional_updated
                        	        || field_updated)
                        	    {
                        	        prev_values_count += 1;
                        	        changed_values_count += 1;
                        	    }

        	                    var wdgt = new $.ajatus.widget(widget['name'], widget['config']);

                    	        if (typeof widget['required'] != 'undefined') {
                        	        wdgt.required = widget['required'];
                    	        }
        	                    
        	                    if (typeof wdgt.validate == 'function') {
        	                        var status = wdgt.validate(row_key, row.value);
        	                        if (typeof status == 'object') {
        	                            $.ajatus.forms.process.has_errors = true;
        	                            $.ajatus.forms.process.error_fields[row_key] = status;
        	                        }
        	                    }
        	                    
        	                    if (name_parts_count > 0) {
        	                        var row_val = {};
        	                        
        	                        $.each(form_data, function(x,r){
        	                            var np = [];
        	                            if (r.name.toString().match(/\|/g)) {
                                            // console.log(r.name+" matches with |");
        	                                np = r.name.toString().split("|");
                                            // console.log(np);
        	                                if (np[0] == name_parts[0]) {
        	                                    if (typeof row_val[np[1]] == 'undefined') {
            	                                    row_val[np[1]] = r.value;
        	                                    }
        	                                }
        	                            }
    	                            });

    	                            parted_names_done.push(name_parts[0]);
                                    
    	                            item['val'] = wdgt.value_on_save(row_val, prev_val, true);
        	                    } else {
                                    item['val'] = wdgt.value_on_save(row.value, prev_val, false);
        	                    }
        	                    
        	                    if (typeof prev_val != 'undefined') {
            	                    if (wdgt.has_data_changed(item['val'], prev_val)) {
            	                        changed_values_count += 1;
            	                    }
        	                    }
        	                    
            	                item['widget'] = widget;
            	                
            	                if (additional) {
            	                    item['additional'] = true;
            	                    if (additionals == false) {
            	                        additionals = {};
            	                    }
            	                    additionals[row_key] = additional;
            	                }
            	                
                                form_values[row_key] = item;
            	            }
            	            else if (row.name.substr(0,8) == "metadata")
            	            {
            	                if (typeof form_values['metadata'] == 'undefined') {
            	                    form_values['metadata'] = {};
            	                }

            	                var re = /\bmetadata\[([a-z]+)\b/;
            	                var results = re.exec(row.name);
            	                var key = results[1];

            	                form_values['metadata'][key] = {
            	                    val: row.value
            	                };
            	            }
        	            }
        	        }
        	    });
        	    
        	    if (additionals) {
        	        form_values['_additionals'] = additionals;
        	    }
        	    
        	    doc['value'] = form_values;

        	    doc = new $.ajatus.document(doc);
                
        	    var now = $.ajatus.formatter.date.js_to_iso8601(new Date());

        	    var new_metadata = {
        	        revised: now,
        	        revisor: $.ajatus.preferences.local.user.email
        	    };
        	    if (   typeof doc._id == 'undefined'
        	        || doc._id == '')
        	    {
        	        new_metadata['created'] = now;
        	        new_metadata['creator'] = $.ajatus.preferences.local.user.email;
        	    }

        	    $.ajatus.forms.process.doc = $.ajatus.document.modify_metadata(doc, new_metadata);
        	    
        	    if (   prev_values_count > 0
        	        && changed_values_count == 0)
        	    {
        	        $.ajatus.forms.process.has_errors = true;
        	        $.ajatus.forms.process.error_fields['form'] = {
        	            msg: $.ajatus.i10n.get('Nothing to update')
        	        };
        	    }
        	    
        	    return $.ajatus.forms.process.doc;
            },
            normal: function(form_data) {
                $.ajatus.events.named_lock_pool.decrease('unsaved');
                
                var doc = $.ajatus.forms.process.common(form_data);
    	        
    	        if ($.ajatus.forms.process.has_errors) {
    	            if (   typeof doc._id == 'undefined'
    	                || doc._id == '')
    	            {
        	            $.ajatus.views.system.create.render(doc.value._type, doc);
    	            } else {
        	            $.ajatus.views.system.edit.render(doc.value._type, doc, true);
    	            }

    	            return false;
    	        } else {
            	    $.jqCouch.connection('doc').save($.ajatus.preferences.client.content_database, doc);
                    doc = new $.ajatus.document(doc);
                    
                    $.ajatus.views.system.edit.render(doc._type, doc);

                    var content_type = $.ajatus.preferences.client.content_types[doc.value._type];
                    var msg = $.ajatus.elements.messages.create(
                        $.ajatus.i10n.get('Object saved'),
                        $.ajatus.i10n.get("%s saved successfully", [content_type.title])
                    );
                    
                    $.ajatus.events.signals.trigger('object_saved', {
                        doc: doc,
                        content_type: content_type,
                        form_type: $.ajatus.forms.active_type
                    });
                    
            	    return true;
    	        }
            },
            ajax: function(form_data, callback) {
                var doc = $.ajatus.forms.process.common(form_data);
                $.jqCouch.connection('doc').save($.ajatus.preferences.client.content_database, doc);
                doc = new $.ajatus.document(doc);
                
                $.ajatus.events.named_lock_pool.decrease('unsaved');

                if (typeof callback == 'function') {
                    callback.apply(callback, [doc]);
                }
                
                $.ajatus.events.signals.trigger('object_saved', {
                    doc: doc,
                    content_type: $.ajatus.preferences.client.content_types[doc.value._type],
                    form_type: $.ajatus.forms.active_type
                });
                
        	    return true;
            }
        },
    });
    
})(jQuery);