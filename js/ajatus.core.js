/*
 * This file is part of
 *
 * {@link http://ajatus.info Ajatus - Distributed CRM}
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */
 
/*
    TODO Tags are links
    TODO Additional fields [widget]
    TODO Skype widget -bergie
    TODO Formatter service
    TODO XMMP & Email formatter
    TODO Gravatar widget
    TODO Implement arhive view
    TODO Implement file attachments
    TODO Documentation (API)
*/

if (typeof console == 'undefined') {
    var console = {};
    console.log = function() {
        return;
    };
}

(function($){
    $.ajatus = {};
    $.ajatus.application_content_area = null;
    
    $.ajatus.version = [0, 6, 'x'];
    
    $.ajatus.active_type = null;
    
    /**
     * Holds all converters available in Ajatus.
     */
    $.ajatus.converter = {};

    /**
     * Parses and evaluates JSON string to Javascript
     * @param {String} json_str JSON String
     * @returns Parsed JSON string or false on failure
     */
    $.ajatus.converter.parseJSON = function (json_str) {
    	try {
    	    var re = new RegExp('[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]');
            return !(re.test(json_str.replace(/"(\\.|[^"\\])*"/g, ''))) && eval('(' + json_str + ')');
        } catch (e) {
            return false;
        }
    }

    /**
     * Parses Javascript to JSON
     * @param {Mixed} item Javascript to be parsed
     * @param {String} type of the Javascript item to be parsed (Optional)
     * @returns JSON string
     * @type String
     */
    $.ajatus.converter.toJSON = function (item,item_type) {
        var m = {
                '\b': '\\b',
                '\t': '\\t',
                '\n': '\\n',
                '\f': '\\f',
                '\r': '\\r',
                '"' : '\\"',
                '\\': '\\\\'
            },
            s = {
                arr: function (x) {
                    var a = ['['], b, f, i, l = x.length, v;
                    for (i = 0; i < l; i += 1) {
                        v = x[i];
                        v = conv(v);
                        if (typeof v == 'string') {
                            if (b) {
                                a[a.length] = ',';
                            }
                            a[a.length] = v;
                            b = true;
                        }
                    }
                    a[a.length] = ']';
                    return a.join('');
                },
                bool: function (x) {
                    return String(x);
                },
                nul: function (x) {
                    return "null";
                },
                num: function (x) {
                    return isFinite(x) ? String(x) : 'null';
                },
                obj: function (x) {
                    if (x) {
                        if (x instanceof Array) {
                            return s.arr(x);
                        }
                        var a = ['{'], b, f, i, v;
                        for (i in x) {
                            v = x[i];
                            v = conv(v);
                            if (typeof v == 'string') {
                                if (b) {
                                    a[a.length] = ',';
                                }
                                a.push(s.str(i), ':', v);
                                b = true;
                            }
                        }
                        a[a.length] = '}';
                        return a.join('');
                    }
                    return 'null';
                },
                str: function (x) {
                    if (/["\\\x00-\x1f]/.test(x)) {
                        x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
                            var c = m[b];
                            if (c) {
                                return c;
                            }
                            c = b.charCodeAt();
                            return '\\u00' +
                                Math.floor(c / 16).toString(16) +
                                (c % 16).toString(16);
                        });
                    }
                    return '"' + x + '"';
                }
            };
            conv = function (x) {
                var itemtype = typeof x;
                switch(itemtype) {
                    case "array":
                      return s.arr(x);
                      break;
                    case "object":
                      return s.obj(x);
                      break;
                    case "string":
                      return s.str(x);
                      break;
                    case "number":
                      return s.num(x);
                      break;
                    case "null":
                      return s.nul(x);
                      break;
                    case "boolean":
                      return s.bool(x);
                      break;
                }
            }

        var itemtype = item_type || typeof item;
        switch(itemtype) {
            case "array":
              return s.arr(item);
              break;
            case "object":
              return s.obj(item);
              break;
            case "string":
              return s.str(item);
              break;
            case "number":
              return s.num(item);
              break;
            case "null":
              return s.nul(item);
              break;
            case "boolean":
              return s.bool(item);
              break;                
            default:
              throw("Unknown type for $.ajatus.converter.toJSON");
            }
    }
    
    /**
     * Holds all formatters available in Ajatus.
     * @constructor
     */
    $.ajatus.formatter = {};

    /**
     * Holds all date formatters available in Ajatus.
     * @constructor
     */
    $.ajatus.formatter.date = {};

    /**
     * Formats numbers < 10 to two digit numbers
     * @param {Number} number Number to format
     * @returns Formatted number
     * @type String
     */
    $.ajatus.formatter.date.fix_length = function(number){
        return ((number < 10) ? '0' : '') + number;
    };

    /**
     * Checks if given value is ISO8601 Formatted Date
     * @param {String} value Value to check
     * @returns True, False if value not string or ISO8601 formatted
     * @type Boolean
     */
    $.ajatus.formatter.date.is_iso8601 = function(value) {
        if (   typeof value != 'string'
            || value == '')
        {
            return false;
        }
        
        var regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
        var isod = value.match(new RegExp(regexp));
        
        if (   typeof isod == 'object'
            && typeof isod[10] != 'undefined')
        {
            return true;
        }
        
        return false;
    };

    /**
     * Converts JS Date to ISO8601 Formatted JS Date
     * @param {Date} date Javascript date to convert
     * @returns ISO8601 formatted Date
     * @type Date
     */
    $.ajatus.formatter.date.js_to_iso8601 = function(date) {
        var str = "";
        str += date.getFullYear();
        str += "-" + $.ajatus.formatter.date.fix_length(date.getMonth() + 1);
        str += "-" + $.ajatus.formatter.date.fix_length(date.getDate());
        str += "T" + $.ajatus.formatter.date.fix_length(date.getHours());
        str += ":" + $.ajatus.formatter.date.fix_length(date.getMinutes());
        str += ":" + $.ajatus.formatter.date.fix_length(date.getSeconds());
        //str += 'Z';
        
        return str;
    };

    /**
     * Converts ISO8601 Formatted JS Date to JS Date
     * @param {Date} iso_date ISO8601 formatted date to convert
     * @returns Javascript Date
     * @type Date
     */
    $.ajatus.formatter.date.iso8601_to_js = function(iso_date) {
        var regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
        var isod = iso_date.match(new RegExp(regexp));
        
        if (   !isod
            || isod.length < 11)
        {
            return new Date();
        }
        
        var date = new Date(isod[1], 0, 1);
        date.setMonth(isod[3] - 1);
        date.setDate(isod[5]);
        date.setHours(isod[7]);
        date.setMinutes(isod[8]);
        date.setSeconds(isod[10]);
        
        return date;
    };
    
    /**
     * Converts Date string from calendar widget to ISO8601 Formatted JS Date
     * @param {String} caldate Date value from calendar widget
     * @param {String} format Format used in conversion (Default: "MDY/")
     * @param {String} caltime Time value from calendar widget
     * @param {String} time_format Format used in conversion (Default: "HMS:")
     * @returns ISO8601 formatted Javascript Date
     * @type Date
     */
    $.ajatus.formatter.date.caldate_to_iso8601 = function(caldate, format, caltime, time_format) {
        
        var format = format || ($.ajatus.i10n.datetime.date || "MDY/");
        var time_format = time_format || ($.ajatus.i10n.datetime.time || "HMS:");
        
        var cdparts = caldate.split(format.charAt(3));
        var fparts = format.split("");
        var date = new Date();
        
        $.each(cdparts, function(i,v){
            v = Number(v);
            if (fparts[i] == 'M') {
                date.setMonth(v-1);
            }
            if (fparts[i] == 'D') {
                date.setDate(v);
            }
            if (fparts[i] == 'Y') {
                date.setFullYear(v);
            }
        });
        
        if (caltime) {
            var time_separator = time_format.charAt(time_format.length - 1);
            var tfparts = time_format.split("");
            var ctparts = caltime.split(time_separator);

            $.each(ctparts, function(i,v){
                v = Number(v);
                if (tfparts[i] == 'H') {
                    date.setHours(v);
                }
                if (tfparts[i] == 'M') {
                    date.setMinutes(v);
                }
                if (tfparts[i] == 'S') {
                    date.setSeconds(v);
                }
            });
        } else {
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
        }
                
        return $.ajatus.formatter.date.js_to_iso8601(date);
    };
    
    /**
     * Converts ISO8601 Formatted JS Date to calendar widget date string
     * @param {Date} iso_date ISO8601 Formatted Date
     * @param {String} format Format used in conversion (Default: "MDY/")
     * @returns Calendar widget supported value
     * @type String
     */
    $.ajatus.formatter.date.iso8601_to_caldate = function(iso_date, format) {
        var format = format || ($.ajatus.i10n.datetime.date || "MDY/");
        
        var date = $.ajatus.formatter.date.iso8601_to_js(iso_date);
        var date_str = '';
        
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        month++;
        
        for (var i = 0; i < 3; i++) {
            date_str += format.charAt(3) +
            (format.charAt(i) == 'D' ? $.ajatus.formatter.date.fix_length(day) :
            (format.charAt(i) == 'M' ? $.ajatus.formatter.date.fix_length(month) :
            (format.charAt(i) == 'Y' ? year : '?')));
        }
        date_str = date_str.substring(format.charAt(3) ? 1 : 0);
        
        return date_str;
    };
    
    /**
     * Converts ISO8601 Formatted JS Date to calendar widget time string
     * @param {Date} iso_date ISO8601 Formatted Date
     * @param {String} format Format used in conversion (Default: "HMS:")
     * @returns Calendar widget supported value
     * @type String
     */
    $.ajatus.formatter.date.iso8601_to_caltime = function(iso_date, format) {
        var format = format || ($.ajatus.i10n.datetime.time || "HMS:");
        
        var date = $.ajatus.formatter.date.iso8601_to_js(iso_date);
        var time_str = '';
        
        var idparts = {
            H: $.ajatus.formatter.date.fix_length(date.getHours()),
            M: $.ajatus.formatter.date.fix_length(date.getMinutes()),
            S: $.ajatus.formatter.date.fix_length(date.getSeconds())
        };

        var separator = format.charAt(format.length - 1);
        var fparts = format.split("");
        
        $.each(fparts, function(i,k){
            if (typeof idparts[k] != 'undefined') {
                time_str += idparts[k] + separator;
            }
        });
        time_str = time_str.substring((time_str.length-1), -1);
        
        return time_str;
    };
    
    $.ajatus.tabs = {};
    $.ajatus.tabs.prepare = function(tab) {
        tab.bind('mouseover', function(e){
            tab.addClass('tabs-hover');
        });
        tab.bind('mouseout', function(e){
            tab.removeClass('tabs-hover');
        });
    };
    $.ajatus.tabs.set_active_by_hash = function(hash) {
        // $.ajatus.debug('$.ajatus.tabs.set_active_by_hash('+hash+')');
        
        var views_tab_holder = $('#tabs-views ul');
        var app_tab_holder = $('#tabs-application ul');
        
        var selected_tab = $('li a[@href$="' + hash + '"]', views_tab_holder).parent();
        if (typeof selected_tab[0] != 'undefined') {
            // $.ajatus.debug('views selected_tab:');
            // $.ajatus.debug(selected_tab);
            $('li', views_tab_holder).removeClass('tabs-selected').blur();
            $('li', app_tab_holder).removeClass('tabs-selected').blur();
            selected_tab.addClass('tabs-selected').focus();
        } else {
            selected_tab = $('li a[@href$="' + hash + '"]', app_tab_holder).parent();
            // $.ajatus.debug('app selected_tab:');
            // $.ajatus.debug(selected_tab);
            if (typeof selected_tab[0] != 'undefined') {
                $('li', views_tab_holder).removeClass('tabs-selected').blur();
                $('li', app_tab_holder).removeClass('tabs-selected').blur();
                selected_tab.addClass('tabs-selected').focus();
            } else {
                $('li', views_tab_holder).removeClass('tabs-selected').blur();
                $('li', app_tab_holder).removeClass('tabs-selected').blur();                
            }            
        }
    };
    $.ajatus.tabs.on_click = function(e) {        
        var trgt = target(e);
        $("li", parent(e)).removeClass("tabs-selected").index(trgt);
        $(trgt).addClass("tabs-selected");
        
        var new_hash = $('a', trgt)[0].hash;
        location.hash = new_hash;
        
        $.ajatus.history.update(new_hash);
        
        $.ajatus.views.on_change(new_hash);
        
        function parent(event) {
            var element = event.target;
            if (element)
            {
                if (element.tagName == "UL")
                {
                    return element;
                }

                while(element.tagName != "UL")
                {
                    element = element.parentNode;
                }
            }
            return element;
        };

        function target(event) {
            var element = event.target;
            if (element)
            {
                if (element.tagName == "UL")
                {
                    element = $(element).find('li').eq(0);
                    return element;
                }

                while(element.tagName != "LI")
                {
                    element = element.parentNode;
                }
            }
            return element;
        };
    };
    
    $.ajatus.security_pass = function() {
        try {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead UniversalBrowserWrite UniversalFileRead");
        } catch (e) {
            alert("Permission UniversalBrowserRead denied. with error "+e);
        }
    };
    
    $.ajatus.init = function(element, options) {
        //$.ajatus.security_pass();
        
        var application_element = $(element);
        $.ajatus.application_element = application_element;
        $.ajatus.application_content_area = $('#middle #content', $.ajatus.application_element);
        $.ajatus.application_dynamic_elements = $('#dynamic-elements', $.ajatus.application_element);
        
        $.ajatus.preferences.client.server_url = window.location.protocol + '//' + document.hostname + (window.location.port != '' ? ':'+window.location.port : '') + '/';
        
        $.ajatus.preferences.client = $.extend({}, $.ajatus.preferences.client_defaults, options);
        //Force the system types to load:
        $.ajatus.preferences.client.types.system = $.ajatus.preferences.client_defaults.types.system;
        
        $.jqCouch.set_defaults({
            server_url: $.ajatus.preferences.client.server_url
        });
        
        var a_db_suffix = $.ajatus.preferences.client.application_database_identifier != '' ? '_db' : 'db';
        $.ajatus.preferences.client.application_database = 'ajatus_' + $.ajatus.preferences.client.application_database_identifier + a_db_suffix;
        $.ajatus.preferences.client.content_database = $.ajatus.preferences.client.application_database + '_content';
        
        $.ajaxSetup({
            type: "GET",
            url: $.ajatus.preferences.client.server_url,
            global: false,
            cache: false,
            dataType: "json"
        });
        
        if ($.ajatus.preferences.client.developer_tools) {
            $.ajatus.utils.load_script($.ajatus.preferences.client.application_url + 'js/ajatus.development.js');
        }
        
        $.ajatus.installer.is_installed();        
        if ($.ajatus.installer.installed == false) {

            if ($.ajatus.preferences.client.language == null) {
                var lang = 'en_GB';
                $.ajatus.preferences.client.language = 'en_GB';
            }
            $.ajatus.i10n.init($.ajatus.preferences.client.language, function(){
                var status = $.ajatus.installer.install();
                $.ajatus.debug("Installer finished with status: "+status);
                if (status) {
                    window.location = window.location + "#view.preferences";
                }
            });
        } else {
            $.ajatus.preload();
        }
        // else
        // {
        //     $.ajatus.installer.uninstall();
        // }
    };
    
    $.ajatus.preload = function() {
        var preference_loader = new $.ajatus.preferences.loader;
        preference_loader.load();

        if ($.ajatus.preferences.client.language == null) {
            var lang = 'en_GB';
            if (typeof($.ajatus.preferences.local.localization) != 'undefined') {
                lang = $.ajatus.preferences.local.localization.language;
            }
            $.ajatus.preferences.client.language = lang;
        }
        
        if ($.ajatus.utils.version.differs($.ajatus.installer.installed_version, $.ajatus.version)) {
            $.ajatus.maintenance.version_upgrade();
        }
        
        $.ajatus.i10n.init($.ajatus.preferences.client.language, function(){
            $.ajatus.types.init(function(){
                $.ajatus.start();
            });            
        });       
    };
    
    $.ajatus.start = function() {
        $.ajatus.debug('started', 'Ajatus start');

	    $.ajatus.events.lock_pool.increase();
        
        // $.ajatus.cookies.init();
        
        if ($.ajatus.preferences.local.layout.theme_name != 'default') {
            $.ajatus.preferences.client.theme_url = 'themes/' + $.ajatus.preferences.local.layout.theme_name + '/';
            $.ajatus.preferences.client.theme_icons_url = 'themes/' + $.ajatus.preferences.local.layout.icon_set + '/images/icons/';
                        
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/structure.css');
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/common.css');
            $.ajatus.layout.styles.load($.ajatus.preferences.client.theme_url + 'css/elements.css');
        }
        
	    $.ajatus.locker = new $.ajatus.events.lock({
	        watch: {
	            validate: function(){return $.ajatus.events.lock_pool.count == 0;},
	            interval: 200,
                safety_runs: 0
            },
            on_release: function(){
                $.ajatus.application_element.addClass('ajatus_initialized');
                
                var show_frontpage = !$.ajatus.history.check();
                
                if (show_frontpage) {
                    $.ajatus.views.system.frontpage.render();                    
                }
                
                // var gen_docs = $.ajatus.utils.doc_generator('contacts', 200, 'contact', {
                //     firstname: 'Firstname',
                //     lastname: 'Lastname',
                //     email: 'firstname.lastname@email.com'
                // });
                // $.jqCouch.connection('doc').bulk_save($.ajatus.preferences.client.content_database, gen_docs);
            }
        });
        
        if (! $.jqCouch.connection('doc').get($.ajatus.preferences.client.application_database + '/version')._id) {
            $.jqCouch.connection('doc').put($.ajatus.preferences.client.application_database + '/version', {value: $.ajatus.version});
        }
        
        $('#header .app_version', $.ajatus.application_element).html($.ajatus.version.join('.'));
        $('#header .tagline', $.ajatus.application_element).html($.ajatus.i10n.get('Distributed CRM'));
        $('#new-item-button').text($.ajatus.i10n.get('new'));
        
        $.ajatus.toolbar.init();
        $.ajatus.history.init();        
        $.ajatus.tags.init();
        $.ajatus.widgets.init();
        $.ajatus.views.init();
        
        $.ajatus.extensions.init({
            on_ready: function(){                
                $.ajatus.active_type = $.ajatus.preferences.client.content_types['note'];

                if ($.ajatus.preferences.modified) {
                    $.ajatus.views.on_change_actions.add('$.ajatus.preferences.view.save($.ajatus.preferences.local)');
                }
                                
                // var gen_docs = $.ajatus.utils.doc_generator('notes', 200);
                // $.jqCouch.connection('doc').bulk_save($.ajatus.preferences.client.content_database, gen_docs);
                // var gen_docs = $.ajatus.utils.doc_generator('events', 200, 'event');
                // $.jqCouch.connection('doc').bulk_save($.ajatus.preferences.client.content_database, gen_docs);
                
                // Release the first lock
                $.ajatus.events.lock_pool.decrease();
            }
        });
        
        $.ajatus.elements.messages.set_holder();
        
        window.onbeforeunload = function() {
            if ($.ajatus.events.named_lock_pool.count('unsaved') > 0) {
                return $.ajatus.i10n.get('You have unsaved changes.');
            }
        }
        
        $.ajatus.debug('ended', 'Ajatus start');
    }
    
    $.ajatus.ajax_error = function(request, caller) {
        $.ajatus.debug("Ajax error request.status: "+request.status);
        
        if (typeof caller != 'undefined') {
            $.ajatus.debug("Caller method: "+caller.method);
            $.ajatus.debug("Caller action:");
            $.ajatus.debug(caller.action);
            
            if (caller.args != undefined) {
                $.ajatus.debug("Caller args:");
                $.ajatus.debug(caller.args);
            }
        }
        
        if (request.responseText != '') {
            var response = eval("(" + request.responseText + ")");
            $.ajatus.debug('response.error.reason: '+response.error.reason);            
        }
    };
    $.ajatus.ajax_success = function(data, caller) {
        $.ajatus.debug("Ajax success");
        
        if (typeof caller != 'undefined') {
            $.ajatus.debug("Caller method: "+caller.method);
            $.ajatus.debug("Caller action: "+caller.action);
            
            if (typeof caller.args != 'undefined') {
                $.ajatus.debug("Caller args: "+caller.args);
            }
        }
        $.ajatus.debug("data: "+data);
        
        return false;
    };
    
    $.ajatus.debug = function(msg, title, type) {
        if ($.ajatus.preferences.client.debug == true) {
            if (typeof(console.log) != 'undefined') {
                debug_console(msg, title, type);
            } else {
                debug_static(msg, title, type);
            }
        }

        function debug_console(msg, title, type) {
            if (typeof type == 'undefined') {
                var type = 'log';
            }
            
            if (typeof(console[type]) != 'undefined') {
                console.log(format(msg, title));
            } else {
                console.log(format(msg, title));
            }
        }
        
        function debug_static(msg, title, type) {
            // TODO: Implement Static debug handler
        }
        
        function format(msg, title) {
            var string = '';
            
            if (typeof title != 'undefined') {
                string += title += ': ';
            }
            
            if (typeof msg != 'string') {
                string += $.ajatus.utils.pretty_print(msg);
            } else {
                string += msg;
            }
            
            return string;            
        }
    };
    
    $.fn.initialize_ajatus = function(options) {
        if (! $(this).is('.ajatus_initialized')) {
            return new $.ajatus.init(this, options);
        }
    };
    
})(jQuery);

function initialize_ajatus() {
    if (typeof ajatus_client_config == 'undefined') {
        var ajatus_client_config = {};
    }
    
    jQuery('#application').initialize_ajatus($.extend({
        // debug: true,
        custom_views: [
            //'test'
        ],
        application_url: '/_utils/ajatus_dev/', // '/_utils/ajatus/'
        application_database_identifier: 'dev', // ''
        developer_tools: true
    }, ajatus_client_config));
}

$(document).ready(function() {
    try {
        initialize_ajatus();        
    } catch(e) {
        alert("could not initialize ajatus! Reason: "+e);
    }
});
