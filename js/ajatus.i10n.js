/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.i10n = {
        lang: 'en_GB',
        dict: {},
        currency: {
            name: '',
            symbol: ''
        },
        datetime: {
            date: 'MDY/',
            time: 'HMS:'
        },
        initialized: false,
        available: null,
        _after_init: null
    };
    $.extend($.ajatus.i10n, {
        init: function(lang, after_init) {
            $.ajatus.i10n.lang = lang || 'en_GB';
            $.ajatus.i10n._after_init = after_init;
            $.ajatus.i10n._load_lang($.ajatus.i10n.lang);
        },
        get_available_langs: function(force_refresh) {
            if (typeof force_refresh == 'undefined') {
                var force_refresh = false;
            }            
            var refresh = false;
            
            if ($.ajatus.i10n.available == null) {
                $.ajatus.i10n.available = [];
                refresh = true;
            }
            
            if (   refresh
                || force_refresh)
            {
                // TODO: Somehow get all available translations from js/languages -folder
                $.ajatus.i10n.available = [
                    'de_DE',
                    'en_GB',
                    'fi_FI',
                    'hu_HU',
                    'nl_NL',
                    'pl_PL',
                    'pt_BR',
                    'sv_SE'
                ];
            }

            return $.ajatus.i10n.available;
        },
        get: function() { //string, keys, pluralize_key            
            if (   arguments.length < 1
                || typeof arguments[0] == 'undefined')
            {
                return;
            }
            
            var string = new String();
            var trans_key = arguments[0].toString().toLowerCase();
            trans_key = $.trim(trans_key);
            //$.ajatus.debug("get "+trans_key, "ajatus.i10n");
            
            var replace_args = [];
            if (   arguments.length > 1
                && typeof(arguments[1]) == 'object')
            {
                replace_args = arguments[1];
            }            

            var auto_plural = false;
            if (arguments.length < 3) {
                auto_plural = true;
            }
            
            if (typeof($.ajatus.i10n.dict[trans_key]) != 'undefined') {                
                string = String($.ajatus.i10n.dict[trans_key]);
            } else {
                string = arguments[0].toString();
            }
            
            if (replace_args.length > 0) {
                string = $.ajatus.i10n.format_result(string, replace_args, auto_plural);
            }
            
            if (   arguments.length > 2
                && typeof(arguments[2]) == 'string')
            {
                string = $.ajatus.i10n.plural(string, arguments[2]);
            }

            var curr_s = "%_CURR_S";
            var curr_n = "%_CURR_N";
            var s_re = new RegExp(curr_s, "i");
            var n_re = new RegExp(curr_n, "i");
            if (string.match(s_re)) {
                var re = new RegExp(curr_s, "gi");
                string = string.replace(re, $.ajatus.i10n.get_currency().symbol);
            }
            if (string.match(n_re)) {
                var re = new RegExp(curr_n, "gi");
                string = string.replace(re, $.ajatus.i10n.get_currency().symbol);
            }
            // $.ajatus.debug("return "+string, "ajatus.i10n");
            if ($.browser.safari) {
                return $.ajatus.utils.utf8.decode(string);
            }
            
            return string;
        },
        get_currency: function() {
            if (arguments.length > 0) {
                switch (arguments[0]) {
                    case 'name':
                        return $.ajatus.i10n.currency.name;
                    break;
                    case 'symbol':
                        return $.ajatus.i10n.currency.name;
                    break;
                    case 'both':
                        return $.ajatus.i10n.currency;
                    break;
                }
            }
            return $.ajatus.i10n.currency;
        },
        format_result: function(string, args, auto_plural) {
            if (typeof auto_plural == 'undefined') {
                var auto_plural = true;
            }
            $.each(args, function(i,value){
                var key = $.ajatus.i10n._resolve_replace_type(value);
                if (   key == '%d'
                    && auto_plural)
                {
                    if (typeof value != 'number') {
                        value = 'null';
                    }
                    var re = new RegExp(/\b[%]?d.(\w+)\b/);
                    var m = re.exec(string);
                    if (   m != null
                        && value > 1)
                    {
                        var sing = m[(m.length-1)];
                        var plural = $.ajatus.i10n.inflector.pluralize(sing);
                        string = string.toString().replace(sing, plural);
                    }
                }
                string = string.toString().replace(key, value);
            });
            return string;
        },
        plural: function(string, key) {
            if (typeof key == 'undefined') {
                var key = string;
            }
            
            var plural_key = $.ajatus.i10n.inflector.pluralize(key);
                        
            var result = string.toString().replace(key, plural_key);
            
            return result;
        },
        _resolve_replace_type: function(value) {
            if (typeof value == 'string') {
                return '%s';
            }
            if (typeof value == 'number') {
                return '%d';
            }
            
            return '%s';
        },
        _load_lang: function(lang) {
            if (   typeof lang == 'undefined'
                || lang == '')
            {
                return;
            }

            $.ajaxSetup({
                async: false
            });
            $.ajatus.events.lock_pool.increase();
            
            var lang_url = $.ajatus.preferences.client.application_url + 'js/languages/'+lang+'.js';
            $.ajatus.utils.load_script(lang_url, "$.ajatus.i10n._lang_loaded", [lang]);
        },
        _lang_loaded: function(lang) {
            $.ajaxSetup({
                async: true
            });
            $.ajatus.i10n.initialized = true;
            $.ajatus.events.lock_pool.decrease();
            
            $.ajatus.i10n._after_init.apply($.ajatus.i10n._after_init, []);
        }
    });
    
    $.ajatus.i10n.inflections = {};
    $.ajatus.i10n.inflector = {
        inflection: null
    };
    $.extend($.ajatus.i10n.inflector, {        
        ordinalize: function(number) {
            $.ajatus.i10n.inflector._get_inflector();
            if ($.ajatus.i10n.inflector.inflection == false) {
                return number;
            }
                        
            if (typeof ($.ajatus.i10n.inflector.inflection['ordinalize']) == 'function') {
                return $.ajatus.i10n.inflector.inflection.ordinalize(number);
            }
            return number;
        },
        pluralize: function(word) {
            $.ajatus.i10n.inflector._get_inflector();
            if ($.ajatus.i10n.inflector.inflection == false) {
                return word;
            }
                        
            var result = '';
            $.each($.ajatus.i10n.inflector.inflection.uncountable, function(i,uncountable){
                if (word.toLowerCase() == uncountable) {
                    result = uncountable;
                }
            });
            if (result != '') {
                return result;
            }
            $.each($.ajatus.i10n.inflector.inflection.irregular, function(i,irr){
                var singular = irr[0];
                var plural = irr[1];
                if ((word.toLowerCase() == singular) || (word == plural)) {
                    result = plural;
                }
            });
            if (result != '') {
                return result;
            }
            $.each($.ajatus.i10n.inflector.inflection.plural, function(i,plur){
                var regex = plur[0];
                var replace_string = plur[1];
                if (regex.test(word)) {
                    result = word.replace(regex, replace_string);
                }
            });
            if (result != '') {
                return result;
            }
            
            return word;
        },
        singularize: function(word) {
            $.ajatus.i10n.inflector._get_inflector();
            if ($.ajatus.i10n.inflector.inflection == false) {
                return word;
            }
            
            var result = '';
            
            $.each($.ajatus.i10n.inflector.inflection.uncountable, function(i,uncountable){
                if (word.toLowerCase() == uncountable) {
                    result = uncountable;
                }
            });
            if (result != '') {
                return result;
            }
            $.each($.ajatus.i10n.inflector.inflection.irregular, function(i,irr){
                var singular = irr[0];
                var plural = irr[1];
                if ((word.toLowerCase() == singular) || (word == plural)) {
                    result = plural;
                }
            });
            if (result != '') {
                return result;
            }
            $.each($.ajatus.i10n.inflector.inflection.singular, function(i,sing){
                var regex = sing[0];
                var replace_string = sing[1];
                if (regex.test(word)) {
                    result = word.replace(regex, replace_string);
                }
            });
            if (result != '') {
                return result;
            }
            
            return word;
        },
        _get_inflector: function(lng) {
            var lang = lng || $.ajatus.i10n.lang;
            
            if ($.ajatus.i10n.inflector.inflection == null) {
                if (typeof($.ajatus.i10n.inflections[lang]) == 'undefined') {
                    $.ajatus.i10n.inflector.inflection = false;
                } else {
                    $.ajatus.i10n.inflector.inflection = $.ajatus.i10n.inflections[lang];            
                }                
            }
        }
    });
    
})(jQuery);