/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.locker = $.ajatus.locker || {};
    $.ajatus.events = $.ajatus.events || {};

    $.ajatus.events.locks = {
        list: []
    };
    $.extend($.ajatus.events.locks, {
        add: function(lock)
        {
            var new_count = $.ajatus.events.locks.list.push(lock);            
            return new_count-1;
        },
        remove: function(index)
        {            
            $.ajatus.events.locks.list = $.grep( $.ajatus.events.locks.list, function(n,i){
                return i == index;
            });
        },
        update: function(index, lock)
        {
            $.ajatus.events.locks.list[index] = lock;
        }
    });
    
    $.ajatus.events.lock = function(options)
    {
        this._manual = true;
        this._watcher = null;
        this.options = $.extend({
            msg: '<h1><img src="'+$.ajatus.preferences.client.theme_url+'images/loading-small.gif" /> ' + $.ajatus.i10n.get('Loading Ajatus') + '...</h1>',
            dialog: false,
            watch: false,
            on_release: false,
            disable_application: true
        }, options);

        this._id = $.ajatus.events.locks.add(this);
        
        if (   !this.options.dialog
            && this.options.disable_application)
        {
            $.blockUI(this.options.msg);
        }
        
        if (this.options.watch)
        {
            var watcher_opts = {
                after: true,
                auto_start: false
            };
            $.each(this.options.watch, function(i,n){
                if (i != 'after')
                {
                    watcher_opts[i] = n;                    
                }
            });
            this._watcher = new $.ajatus.events.watcher(watcher_opts);
            
            var self = this;
            this._watcher.element.bind('on_stop', function(watcher_id){
                self.release();
            });
            
            this._watcher.start();
        }

        $.ajatus.events.locks.update(this);        
        return this;
    };
    $.extend($.ajatus.events.lock.prototype, {
        release: function() {
            if (   !this.options.dialog
                && this.options.disable_application)
            {
                $.unblockUI();
            }
            
            if (   this.options.on_release
                && typeof(this.options.on_release) == 'function')
            {
                this.options.on_release(this);
            }
        },
        update: function(lock_cnt) {
            if (lock_cnt == 0) {
                this._release_lock();
            }
        }
    });
    
    $.ajatus.events.watchers = {
        list: []
    };
    $.extend($.ajatus.events.watchers, {
        add: function(watcher) {
            var new_count = $.ajatus.events.watchers.list.push(watcher);
            return new_count-1;
        },
        remove: function(index) {
            if (typeof $.ajatus.events.watchers.list[index] != 'undefined') {
                delete($.ajatus.events.watchers.list[index]);
            }
            // $.ajatus.events.watchers.list = $.grep( $.ajatus.events.watchers.list, function(n,i){
            //     return i == index;
            // });
        }
    });
    
    $.ajatus.events.watcher = function(options) {
        this.options = $.extend({
            validate: null,
            after: false,
            interval: 1000,
            safety_runs: 3,
            auto_start: true,
            max_runs: 100,
            timed: 0
        }, options);

        if (   typeof(this.options.validate) != 'function'
            && this.options.timed <= 0)
        {
            return false;
        }
        
        if (this.options.timed > 0) {
            this.options.max_runs = false;
        }
        
        this.elapsed = 0;
        this._timeout_id = -1;
        this._done_safety_runs = 0;
        this._done_runs = 0;
        
        this._id = $.ajatus.events.watchers.add(this);
        this.element = $('<div id="ajatus_events_watcher_'+this._id+'" />').appendTo($('body'));

        if (this.options.auto_start) {
            this.start();
        }
        
        return this;
    };
    $.extend($.ajatus.events.watcher.prototype, {
        start: function() {
            if (this.options.interval < 200) {
                this.options.interval = 200;  // Safari needs at least 200 ms
            }
            // console.log("Watcher "+this._id+" started!");
            this._timeout_id = setTimeout("if (typeof $.ajatus.events.watchers.list["+this._id+"] != 'undefined') {$.ajatus.events.watchers.list["+this._id+"]._check();}", this.options.interval);
        },
        _check: function() {
            // console.log("Watcher "+this._id+" is checking status");
            if (typeof $.ajatus.events.watchers.list[this._id] == 'undefined') {
                this._cleanup();
            };
            
            this._done_runs++;
            
            if (this.options.timed > 0) {
                if (   this.elapsed >= this.options.timed
                    || (   this.options.max_runs
                        && this._done_runs >= (this.options.max_runs-this.options.safety_runs)))
                {
                    this._stop();
                } else {
                    this._done_safety_runs = 0;
                    this.elapsed += this.options.interval;
                    this._timeout_id = setTimeout("if (typeof $.ajatus.events.watchers.list["+this._id+"] != 'undefined') {$.ajatus.events.watchers.list["+this._id+"]._check();}", this.options.interval);
                }
            } else {
                if (   this.options.validate()
                    || (   this.options.max_runs
                        && this._done_runs >= (this.options.max_runs-this.options.safety_runs)))
                {
                    // console.log("Watcher "+this._id+" validated!");

                    if (this._done_safety_runs < this.options.safety_runs) {
                        this._done_safety_runs++;
                        //console.log("Watcher "+this._id+" doing safety run: "+this._done_safety_runs+" of "+this.options.safety_runs);
                        this._timeout_id = setTimeout("if (typeof $.ajatus.events.watchers.list["+this._id+"] != 'undefined') {$.ajatus.events.watchers.list["+this._id+"]._check();}", this.options.interval);
                    } else {
                        //console.log("Watcher "+this._id+" has done all safety runs!");                    
                        this._stop();
                    }
                } else {
                    this._done_safety_runs = 0;
                    this._timeout_id = setTimeout("if (typeof $.ajatus.events.watchers.list["+this._id+"] != 'undefined') {$.ajatus.events.watchers.list["+this._id+"]._check();}", this.options.interval);
                }                
            }
            var tick_data = {
                done_runs: this._done_runs,
                elapsed: this.elapsed,
            };
            if (this.options.timed > 0) {
                tick_data['elapsed'] = this.elapsed;
                tick_data['timed'] = this.options.timed;
                tick_data['left_ms'] = this.options.timed - this.elapsed;
                tick_data['left_prc'] = (this.elapsed/this.options.timed)*100;
            }
            this.element.trigger('on_tick',[this._id, tick_data]);
        },
        _stop: function() {
            // console.log("Watcher "+this._id+" ended!");
            this._cleanup();
            
            this.element.trigger('on_stop',[this._id]);
        },
        _cleanup: function() {
            // console.log("CleanUp "+this._id);
            clearTimeout(this._timeout_id);
            //$.ajatus.events.watchers.remove(this._id);
        }
    });
    
    $.ajatus.events.lock_pool = {
        count: 0,
        increase: function() {
            $.ajatus.events.lock_pool.count = $.ajatus.events.named_lock_pool.increase('global');
            // console.log('lock_pool increase to '+$.ajatus.events.lock_pool.count);
        },
        decrease: function() {
            $.ajatus.events.lock_pool.count = $.ajatus.events.named_lock_pool.decrease('global');            
            // console.log('lock_pool decrease to '+$.ajatus.events.lock_pool.count);
        },
        clear: function() {
            $.ajatus.events.named_lock_pool.clear('global');
            $.ajatus.events.lock_pool.count = 0;
        }
    };
    
    $.ajatus.events.named_lock_pool = {
        counts: {},
        count: function(name) {
            if ($.ajatus.events.named_lock_pool.counts[name] == undefined) {
                return 0;
            } else {
                return $.ajatus.events.named_lock_pool.counts[name];
            }            
        },
        increase: function(name) {
            if ($.ajatus.events.named_lock_pool.counts[name] == undefined) {
                $.ajatus.events.named_lock_pool.counts[name] = 0;
            }
            $.ajatus.events.named_lock_pool.counts[name]++;

            //console.log('named_lock_pool increase '+name+' to '+$.ajatus.events.named_lock_pool.counts[name]);
            return $.ajatus.events.named_lock_pool.counts[name];
        },
        decrease: function(name) {
            if ($.ajatus.events.named_lock_pool.counts[name] == undefined) {
                $.ajatus.events.named_lock_pool.counts[name] = 0;
            } else {
                $.ajatus.events.named_lock_pool.counts[name] = $.ajatus.events.named_lock_pool.counts[name]-1;
            }
            // console.log('named_lock_pool decrease '+name+' to '+$.ajatus.events.named_lock_pool.counts[name]);
            return $.ajatus.events.named_lock_pool.counts[name];
        },
        clear: function(name) {
            $.ajatus.events.named_lock_pool.counts[name] = 0;
        }
    };
    
    $.ajatus.events.signals = {
        _listeners: null,
        trigger: function(signal, data) {
            if (   $.ajatus.events.signals._listeners === null
                || typeof $.ajatus.events.signals._listeners[signal] == 'undefined')
            {
                return;
            }
            
            $.each($.ajatus.events.signals._listeners[signal], function(i,listener){
                if (typeof listener == 'function') {
                    listener.apply(listener, [data]);
                }
            });
        },
        add_listener: function(signal, listener) {
            if ($.ajatus.events.signals._listeners === null) {
                $.ajatus.events.signals._listeners = {};
            }
            if (typeof $.ajatus.events.signals._listeners[signal] == 'undefined') {
                $.ajatus.events.signals._listeners[signal] = [];
            }
            
            $.ajatus.events.signals._listeners[signal].push(listener);
        }
    };

})(jQuery);