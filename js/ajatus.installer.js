/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    
    $.ajatus.installer = {
        installed: false,
        installed_version: null,
        
        is_installed: function()
        {
            if ($.jqCouch.connection('db').exists($.ajatus.preferences.client.application_database)) {
                $.ajatus.installer.installed = true;

                var version = $.jqCouch.connection('doc').get($.ajatus.preferences.client.application_database + '/version');
                $.ajatus.installer.installed_version = version.value;
                $.ajatus.installer.installed_version_rev = version._rev;
            }            
        },

        install: function()
        {
            var dbc = $.jqCouch.connection('db');
            
            var row = '';
            var row_failed = '<span class="status_failed">' + $.ajatus.i10n.get('failed').toUpperCase() + '</span><br />';
            var row_ok = '<span class="status_ok">' + $.ajatus.i10n.get('ok').toUpperCase() + '</span><br />';
                        
            $.ajatus.debug('Installing');
            
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Installing'), '', {
                closable: false
            });
            
            dialog.open();
            
            row = $.ajatus.i10n.get('Installing application database') + '... ';
            dialog.append_content(row);
            // jqcouch_db.create($.ajatus.preferences.client.application_database);

            row = row_failed;
            if (dbc.create($.ajatus.preferences.client.application_database).ok) {
                row = row_ok;
            }
            dialog.append_content(row);
            
            row = $.ajatus.i10n.get('Preparing application database') + '... ';
            dialog.append_content(row);
            
            row = row_failed;
            if ($.jqCouch.connection('doc').put($.ajatus.preferences.client.application_database + '/preferences', {value: $.ajatus.preferences.local_defaults}).id) {
                row = row_ok;
            }
            if ($.jqCouch.connection('doc').put($.ajatus.preferences.client.application_database + '/version', {value: $.ajatus.version}).id) {
                row = row_ok;
            }
            dialog.append_content(row);
            
            row = $.ajatus.i10n.get('Installing application content database') + '... ';
            dialog.append_content(row);

            row = row_failed;
            if (dbc.create($.ajatus.preferences.client.content_database).ok) {
                row = row_ok;
            }
            dialog.append_content(row);
            
            var close = jQuery('<br /><br /><span class="jqmClose">' + $.ajatus.i10n.get('Continue') + '</span>');
            dialog.append_content(close);
            jQuery('#' + dialog.id + ' .jqmClose').bind('click', function(e){
                dialog.close();
                $.ajatus.preload();
            }).css({cursor: 'pointer'});
                        
            return true;
        },
        
        uninstall: function()
        {            
            var dbc = $.jqCouch.connection('db');
            
            var row = '';
            var row_failed = '<span class="status_failed">' + $.ajatus.i10n.get('failed').toUpperCase() + '</span><br />';
            var row_ok = '<span class="status_ok">' + $.ajatus.i10n.get('ok').toUpperCase() + '</span><br />';
            
            $.ajatus.debug('uninstalling');
            
            var dialog = new $.ajatus.elements.dialog($.ajatus.i10n.get('Uninstalling'), '', {
                closable: false
            });
            
            dialog.open();
            
            row = $.ajatus.i10n.get('Uninstalling application content database') + '... ';
            dialog.append_content(row);

            row = row_failed;
            if (dbc.del($.ajatus.preferences.client.content_database).ok) {
                row = row_ok;
            }
            dialog.append_content(row);

            row = $.ajatus.i10n.get('Uninstalling application database') + '... ';
            dialog.append_content(row);

            row = row_failed;
            if (dbc.del($.ajatus.preferences.client.application_database).ok) {
                row = row_ok;
            }
            dialog.append_content(row);
            
            var close = jQuery('<br /><br /><span class="jqmClose">' + $.ajatus.i10n.get('Continue') + '</span>');
            dialog.append_content(close);
            jQuery('#' + dialog.id + ' .jqmClose').bind('click', function(e){
                dialog.close();
                window.location.reload();
            }).css({cursor: 'pointer'});
                        
            return true;            
        }
    };
    
})(jQuery);