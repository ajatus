/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};    
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.icongrid = function(options, target) {
        var self = this;
        this.options = $.extend({
            className: 'icongrid_holder',
            id: 'icongrid_holder',
            title: null,
            append: false,
            columns: null,
            rows: null,
            width: 100,
            height: 100,
            units: '%',   
            item_id_suffix: '',
            cell_renderer: null,
            attach: true
        }, options);
        
        this.target = target || $.ajatus.application_content_area;
        
        this.holder = $('<div class="'+this.options.className+'" id="'+this.options.id+'" />');
        
        this.holder.css({
            width: this.options.width.toString() + this.options.units,
            height: this.options.height.toString() + this.options.units
        });
        
        title_str = this.options.title || 'Icon menu';
        title_str = $.ajatus.i10n.get(title_str);
        
        title = $('<h2>'+title_str+'</h2>');
        title.appendTo(this.holder);
    
        this.table = $('<table class="icongrid" width="100%" height="100%"/>');
        this.tbody = $('<tbody />');
        this.tbody.appendTo(this.table);
        this.table.appendTo(this.holder);
        
        this.items = [];
        this.grid_data = [];        
        this._max_items = null;
        this._positioned_items = 0;
        
        if (   this.options.columns != null
            && this.options.rows != null)
        {
            this._max_items = this.options.columns * this.options.rows;
        }        
        
        this._can_add = true;
        this._rendered = false;
        
        if (this.options.attach) {
            this.attach(this.options.append);
        }
    };
    $.extend($.ajatus.renderer.icongrid.prototype, {
        add_item: function(item) {
            if (! this._can_add) {
                return false;
            }
            
            this.items.push(item);

            if (this._rendered) {
                this._update_grid();
            }
        },
        render: function() {
            if (! this.options.attach) {
                this.attach(this.options.append)
            }
            var icnt = this.items.length;
            var half = icnt/2;
            var hh = half/2;
            
            if (this.options.columns == null) {
                this.options.columns = half;
            }
            
            var _self = this;
            $.each(this.items, function(i,item){
                if (   _self._max_items != null
                    && _self._max_items < _self._positioned_items)
                {
                    _self._cann_add = false;
                    return;
                } else {
                    _self.position_item(item);
                    _self._positioned_items += 1;
                }
            });
            
            var max_cells = 0;
            var last_id = 0;
            
            $.each(this.grid_data, function(row_i,row_data){
                var row = $('<tr class="row_'+row_i+'"/>');
                var inserted_cells = 0;
                var cell_cnt = row_data.length;

                if (cell_cnt > max_cells) {
                    max_cells = cell_cnt;
                }
                
                for (var cell_i=0; cell_i<cell_cnt; cell_i++) {
                    var cell = _self.create_cell(cell_i, row_data[cell_i]);
                    cell.appendTo(row);
                    inserted_cells += 1;
                    last_id = cell_i+1;
                }
                                
                if (inserted_cells < max_cells) {
                    for (var idx=last_id; idx<max_cells; idx++) {
                        _self._create_filling_cell(idx).appendTo(row);
                    }
                }
                
                row.appendTo(_self.tbody);
            });
        },
        create_cell: function(idx, data) {
            if (typeof this.options.cell_renderer == 'function') {
                return this.options.cell_renderer(idx, data);
            }
            
            var cell = $('<td class="cell_'+idx+'" />');
            var img = $('<img src="' + $.ajatus.preferences.client.theme_icons_url + data.icon + '" alt="' + data.title + '"/>');

            if (typeof data.hash != 'undefined') {
                var link = $('<a href="'+data.hash+'" title="' + data.title + '"/>');
                img.appendTo(link);
                link.appendTo(cell);
            } else {
                img.appendTo(cell);
            }
            
            return cell;
        },
        position_item: function(data, coords) {
            if (typeof coords == 'undefined') {
                var coords = this.next_coordinates();
            }
            
            if (typeof this.grid_data[coords.y] == 'undefined') {
                this.grid_data[coords.y] = [];
            }
            
            this.grid_data[coords.y][coords.x] = data;
        },
        next_coordinates: function() {
            var coords = {
                x: 0,
                y: 0
            };
            
            var next_x = 0;
            var next_y = 0;
            if (this.grid_data.length > 0) {
                var curr_y = this.grid_data.length - 1;
                var curr_x = this.grid_data[curr_y].length - 1;
                next_y = curr_y;
                next_x = curr_x+1;

                if (next_x == this.options.columns) {
                    next_y += 1;
                    next_x = 0;
                }

                coords = {
                    x: next_x,
                    y: next_y
                }
            }
            
            return coords;
        },
        attach: function(append) {
            if (append) {
                this.holder.appendTo(this.target);
            } else {
                this.target.html(this.holder);
            }
        },
        _create_filling_cell: function(idx) {
            return $('<td class="cell_'+idx+'" />');
        }
    });
    
})(jQuery);