/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.form_helpers = {
        save_focus: function(element) {
            var focus_data = {
                field_name: element.name,
                pos: Number(element.selectionStart)
            };

            if ($.ajatus.utils.to_boolean($.ajatus.preferences.local.auto_save.enabled) == true) {
                var d = new Date();
                var expires_at_ms = (60 + Number($.ajatus.preferences.local.auto_save.interval)) * 1000;
                var ts = d.getTime() + expires_at_ms;
                var expires_at = new Date(ts);
                
                $.cookie("ajatus.auto_save.focus", $.ajatus.converter.toJSON(focus_data), {
                    expires: expires_at
                });
            }
        },
        restore_focus: function(form, field_name, pos) {
            if (typeof pos != 'undefined') {
                var element = $("input[name="+field_name+"]", form)[0];
                if (! element) {
                    element = $("textarea[name="+field_name+"]", form)[0];
                }
                
                if (element.createTextRange) { 
                    var range = element.createTextRange(); 
                    range.move("character", pos); 
                    range.select(); 
                } else if (element.selectionStart) { 
                    element.focus();
                    element.setSelectionRange(pos, pos); 
                }
                
                return;
            }
            
            var cookie = $.ajatus.converter.parseJSON($.cookie("ajatus.auto_save.focus"));

            if (cookie) {
                var element = $("input[name="+cookie.field_name+"]", form)[0];
                if (! element) {
                    element = $("textarea[name="+cookie.field_name+"]", form)[0];
                }
                
                setTimeout("$.ajatus.renderer.form_helpers.restore_focus(null, '"+cookie.field_name+"', "+cookie.pos+")", 300);
            }
        },
        _add_row: function(type, holder, name, data, doc) {
	        if (    data.hidden
	            && data.hidden == true)
	        {
	            return;
	        }
            $.ajatus.debug("add row "+name);
            
            var wdgt_conf = data.widget.config;
            if (   type == 'edit'
                && typeof doc.value[name] != 'undefined'
                && typeof doc.value[name]['widget'] != 'undefined')
            {
                wdgt_conf = doc.value[name].widget.config;
            }
            
	        var widget = new $.ajatus.widget(data.widget.name, wdgt_conf);
	        
	        var required = false;
	        if (   typeof data.widget.required != 'undefined'
	            || typeof data.required != 'undefined')
	        {
    	        widget.required = data.widget.required || data.required;	            
    	        if (widget.required) {
    	            required = true;
    	        }
	        }
            
            var def_val = data.def_val;
	        switch (type) {
	            case 'create':
                    if (   typeof doc == 'object'
                        && typeof doc.value[name] != 'undefined')
                    {
                        def_val = doc.value[name].val;
                    }
                
	                var wdgt_tpl = widget.get_create_tpl(name, def_val);
	            break;
	            case 'edit':
                    var doc_value = {};
                    if (typeof doc.value[name] == 'undefined') {
                        doc_value = {
                            val: '',
                            widget: data.widget
                        };
                    } else {
                        doc_value = doc.value[name];
                    }
                    def_val = doc_value.val;
                    
                    var edit_data = $.extend({
                        _id: doc._id
                    }, doc_value);
                    
	                var wdgt_tpl = widget.get_edit_tpl(name, edit_data);
	            break;
	        }
            
            var actions_tpl = $.ajatus.document.scheme.items.generate_actions_tpl(name, widget, (data || doc), holder);
            
	        if ( typeof data['read_only'] != 'undefined'
	            && data.read_only == true)
	        {
	            wdgt_tpl = $.ajatus.widgets.to_readonly(type, name, def_val, widget);
	        }
	        
            holder.createAppend(
                'li', { id: '_element_'+name, className: 'row' }, [
                    'label', { id: '_element_'+name+'_label' }, $.ajatus.i10n.get(data.label) + (required ? ' *' : ''),
                    'div', { id: '_element_'+name+'_widget', className: widget.name, style: 'width: 80%;' }, wdgt_tpl,
                    'div', { id: '_element_'+name+'_actions', className: 'element_actions' }, actions_tpl,
                    'br', { className: 'clear_fix' }, ''
                ]
            );
            widget.init($('#_element_'+name+'_widget', holder), true);
            
            if (required) {
                $('#_element_'+name+'_label', holder).addClass('required');
            }
            
            $('#_element_'+name+' input', holder).bind('click', function(){
                $.ajatus.renderer.form_helpers.save_focus(this);
            });
            $('#_element_'+name+' textarea', holder).bind('click', function(){
                $.ajatus.renderer.form_helpers.save_focus(this);
            });
            
            $('#_element_'+name+'_label', holder).bind('click', function(e){
                if ($('#_element_'+name+'_widget:visible', holder)[0]) {
                    $('#_element_'+name+'_widget', holder).hide();
                } else {
                    $('#_element_'+name+'_widget', holder).show();
                }
            });
        },
        _add_additional_row: function(type, holder, name, data, doc, replace_row) {
	        var widget = new $.ajatus.widget(data.widget.name, data.widget.config);
            
            if (   typeof doc == 'undefined'
                || typeof doc.value == 'undefined')
            {
                var doc = {
                    value: {}
                };
            }
            
	        var required = false;
	        if (   typeof data.widget.required != 'undefined'
	            || typeof data.required != 'undefined')
	        {
    	        widget.required = data.widget.required || data.required;
    	        if (widget.required) {
    	            required = true;
    	        }
	        }

	        switch (type) {
	            case 'create':
	                var value = typeof data.value != 'undefined' ? data.value : data.def_val;
	                var wdgt_tpl = widget.get_create_tpl(name, value);
	            break;
	            case 'edit':
                    var doc_value = {};
                    if (typeof doc.value[name] == 'undefined') {
                        doc_value = {
                            val: typeof data.value != 'undefined' ? data.value : '',
                            widget: data.widget,
                            additional: true
                        };
                    } else {
                        doc_value = doc.value[name];
                    }
            
                    var edit_data = $.extend({
                        _id: doc._id
                    }, doc_value);
                    
	                var wdgt_tpl = widget.get_edit_tpl(name, edit_data);
	            break;
	        }
	        
	        var additional_actions_tpl = $.ajatus.document.additionals.generate_item_actions_tpl(name, widget, data, doc, holder);
	        
	        if (typeof replace_row != 'undefined') {
	            var row = $('#_element_'+replace_row, holder);
	            
	            // Replace label
	            $('#_element_'+replace_row+'_label', row).html($.ajatus.i10n.get(data.label) + (required ? ' *' : ''));
                // Replace additional data
	            $('input[name="widget['+replace_row+':additional]"]', row).val($.ajatus.converter.toJSON(data));
                
                $('#_element_'+replace_row+'_widget', row).remove();
                $('#_element_'+replace_row+'_additional_actions', row).remove();
                $('br', row).remove();
                
                var add_upd_elem = $('input[name="widget['+replace_row+':additional_updated]"]', row);
                if (typeof add_upd_elem[0] == 'undefined') {
    	            row.createAppend(
    	                'input', { type: 'hidden', name: 'widget['+replace_row+':additional_updated]', value: $.ajatus.converter.toJSON(true) }, ''
                    );
                } else {
                    add_upd_elem.val($.ajatus.converter.toJSON(true));
                }
                
	            row.createAppend(
                    'div', { id: '_element_'+name+'_widget', className: widget.name, style: 'width: 80%;' }, wdgt_tpl
                );
                
	            row.createAppend(
                    'div', { id: '_element_'+name+'_additional_actions' }, additional_actions_tpl
                );
                
	            row.createAppend(
                    'br', { className: 'clear_fix' }, ''
                );

	        } else {
                holder.createAppend(
                    'li', { id: '_element_'+name, className: 'row' }, [
                        'label', { id: '_element_'+name+'_label' }, $.ajatus.i10n.get(data.label) + (required ? ' *' : ''),
                        'input', { type: 'hidden', name: 'widget['+name+':additional]', value: $.ajatus.converter.toJSON(data) }, '',
                        'div', { id: '_element_'+name+'_widget', className: widget.name, style: 'width: 80%;' }, wdgt_tpl,
                        'div', { id: '_element_'+name+'_additional_actions', className: 'additional_element_actions' }, additional_actions_tpl,
                        'br', { className: 'clear_fix' }, ''
                    ]
                );	            
	        }
	        
            widget.init($('#_element_'+name+'_widget', holder), true);

            if (required) {
                $('#_element_'+name+'_label', holder).addClass('required');
            }
            
            $('#_element_'+name+' input', holder).bind('click', function(){
                $.ajatus.renderer.form_helpers.save_focus(this);
            });
            $('#_element_'+name+' textarea', holder).bind('click', function(){
                $.ajatus.renderer.form_helpers.save_focus(this);
            });
            
            $('#_element_'+name+'_label', holder).bind('click', function(e){
                if ($('#_element_'+name+'_widget:visible', holder)[0]) {
                    $('#_element_'+name+'_widget', holder).hide();
                } else {
                    $('#_element_'+name+'_widget', holder).show();
                }
            });
        },
        _register_for_autosave: function(form, action) {
            if ($.ajatus.utils.to_boolean($.ajatus.preferences.local.auto_save.enabled) == false) {
                return false;
            }
            
            $.ajatus.renderer.form_helpers.restore_focus(form);
            
            var save_interval = Number($.ajatus.preferences.local.auto_save.interval);
            var watcher = new $.ajatus.events.watcher({
                auto_start: true,
                safety_runs: 0,
                interval: 200,
                timed: (save_interval * 1000)
            });
            $.ajatus.views.on_change_actions.add('$.ajatus.events.watchers.remove('+watcher._id+');');

            var as_lc_holder = $('.form_structure .auto_save_lc span', form);
            as_lc_holder.html('0');
            if (typeof as_lc_holder[0] == 'undefined') {
                as_holder = $('<div class="auto_save_lc" />').css({
                    display: "none"
                }).prependTo($('.form_structure', form));
                as_holder.html($.ajatus.i10n.get("Auto saving in")+" ");
                as_lc_holder = $('<span/>').appendTo(as_holder);
            }
            
            watcher.element.bind('on_tick', function(e, watcher_id, data){
                // console.log("on_tick");
                // console.log(data);
                if (data['left_prc'] > 70) {
                    var time_left = "0";
                    var ls = data['left_ms'] / 1000;
                    if (ls < 60) {
                        time_left = parseInt(ls).toString();
                    } else {
                        var min = parseInt(ls/60);
                        var sec = parseInt(ls - (min*60));
                        
                        if (sec < 10) {
                            sec = "0" + sec.toString();
                        }
                        
                        time_left = parseInt(min).toString() + ":" + sec;
                    }

                    if (as_lc_holder.parent().css('display') == 'none') {
                        as_lc_holder.parent().fadeIn('slow', function(){
                            as_lc_holder.html(time_left);
                        });
                    } else {
                        as_lc_holder.html(time_left);
                    }
                }
            });
            watcher.element.bind('on_stop', function(e, watcher_id){                
                eval(action);
                
                if ($.ajatus.forms.active) {
                    $.ajatus.renderer.form_helpers._register_for_autosave(form, action);
                }
            });
        }
    };
    
    $.ajatus.renderer.form = function(content_type, doc, doc_as_defaults, latest_rev)
    {
        var self = this;
        this.content_type = content_type;
        this.doc = doc || false;
        this.doc_as_defaults = doc_as_defaults || false;
        this.form = null;
        
        $.ajatus.toolbar.show();
        
        if (   this.doc
            && !this.doc_as_defaults)
        {
            this.form = this.edit(this.content_type, this.doc, latest_rev);
        } else {
            this.form = this.create(this.content_type, this.doc);
        }
        
        return this;
    }
    $.extend($.ajatus.renderer.form.prototype, {
        create: function(content_type, doc)
        {
            if (typeof content_type == 'string') {
                content_type = $.ajatus.preferences.client.content_types[content_type];
            }
            
            var schema_fields = content_type.schema;
            var type_name = content_type.name;

            var form = $('<form id="create_'+type_name+'" name="create_'+type_name+'" />');
            var form_actions = $('<div class="form_actions"><input type="submit" name="save" value="' + $.ajatus.i10n.get('Create') + '" /><input type="submit" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" /></div>');
    	    var form_struct = $('<div class="form_structure" />');
    	    var form_errors = $('<div class="form_errors" />').hide();
    	    var row_holder = $('<ul class="row_holder" />');

            var type_title = content_type.title;
    	    $('<h2>' + $.ajatus.i10n.get('Create %s', [type_title]) + '</h2>').appendTo(form_struct);

    	    if (typeof $.ajatus.forms.process.has_errors != 'undefined') {
        	    var error_holder = $('<ul />');
        	    $.each($.ajatus.forms.process.error_fields, function(field, data){
        	        $('<li class="error" />').html(data.msg).appendTo(error_holder);
        	    });
        	    error_holder.appendTo(form_errors);
        	    form_errors.appendTo(form_struct).show();
    	    }

            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Create'), {
                icon: 'save.png',
                action: function(){
                    $('#create_'+type_name+' input[@type=submit][name*=save]').trigger("click", []);
                },
                access_key: 'Ctrl+s'
            });
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Cancel'), 'cancel.png', function(){
                $('#create_'+type_name+' input[@type=submit][name*=cancel]').trigger("click", []);
            });

    	    var hidden_data = {
    	        _type: type_name
    	    };

            var hidden_fields = function() {
                return [
                    'input', { type: 'hidden', id: '_type', name: '_type', value: this._type }, ''
                ];
            };
            row_holder.tplAppend(hidden_data, hidden_fields);
    	    
    	    $.each(schema_fields, function(i,n){
    	        $.ajatus.renderer.form_helpers._add_row('create', row_holder, i, n, doc);
    	    });

            row_holder.appendTo(form_struct);
            form_struct.appendTo(form);
            form_actions.appendTo(form);
            
            $('li:first', row_holder).addClass('first');
            $('li:last', row_holder).addClass('last');
                        
    	    if (   typeof(content_type['enable_additionals']) != 'undefined'
    	        && content_type.enable_additionals)
    	    {
        	    var additionals_add = $('<div class="additionals_add_btn" />').html($.ajatus.i10n.get('Add field'));
                additionals_add.appendTo(form_struct);
                
                additionals_add.bind('click', function(e){
                    $.ajatus.document.additionals.create(content_type, row_holder);
                });
    	    }
            
            // var save_action = "$('#create_"+type_name+" input[@type=submit][name*=save]').trigger('click', []);";
            // $.ajatus.renderer.form_helpers._register_for_autosave(form, save_action);
            
            return form;
        },
        edit: function(content_type, doc, latest_rev)
        {            
            var schema_fields = content_type.schema;
            var type_name = content_type.name;
            var has_additionals = false;
            
            if (typeof doc.value._additionals != 'undefined') {
                has_additionals = true;
                $.each(doc.value._additionals, function(key,data){                    
                    schema_fields[key] = data;
                });
            }

            var form = $('<form id="edit_'+type_name+'" name="edit_'+type_name+'" />');
            var form_actions = $('<div class="form_actions"><input type="submit" name="save" value="' + $.ajatus.i10n.get('Save') + '" /><input type="submit" name="cancel" value="' + $.ajatus.i10n.get('Cancel') + '" /></div>');
    	    var form_struct = $('<div class="form_structure" />');
    	    var form_errors = $('<div class="form_errors" />').hide();
    	    var row_holder = $('<ul class="row_holder" />');

            var type_title = content_type.title;
    	    $('<h2>' + $.ajatus.i10n.get('Edit %s', [type_title]) + '</h2>').appendTo(form_struct);
    	    
    	    if (typeof $.ajatus.forms.process.has_errors != 'undefined') {
        	    var error_holder = $('<ul />');
        	    $.each($.ajatus.forms.process.error_fields, function(field, data){
        	        $('<li class="error" />').html(data.msg).appendTo(error_holder);
        	    });
        	    error_holder.appendTo(form_errors);
        	    form_errors.appendTo(form_struct).show();
    	    }
            
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Save'), {
                icon: 'save.png',
                action: function(){
                    $('#edit_'+type_name+' input[@type=submit][name*=save]').trigger("click", []);
                },
                access_key: 'Ctrl+s'
            });
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('Cancel'), 'cancel.png', function(){
                $('#edit_'+type_name+' input[@type=submit][name*=cancel]').trigger("click", []);
            });
            $.ajatus.toolbar.add_item($.ajatus.i10n.get('View'), {
                icon: 'view.png',
                action: function(){
                    $.ajatus.views.system.item.render(doc);
                },
                access_key: 'Ctrl+v'
            });

    	    var hidden_data = {
    	        _id: doc._id,
    	        _rev: (latest_rev || doc._rev),
    	        _type: type_name
    	    };

            var hidden_fields = function() {
                return [
                    'input', { type: 'hidden', id: this._id+'_id', name: '_id', value: this._id }, '',
                    'input', { type: 'hidden', id: this._id+'_revision', name: '_rev', value: this._rev }, '',
                    'input', { type: 'hidden', id: this._id+'_type', name: '_type', value: this._type }, ''
                ];
            };
            $(row_holder).tplAppend(hidden_data, hidden_fields);
            
            if (has_additionals) {
                var additionals_hidden = $('<input type="hidden" name="_additionals" id="' + doc._id + '_additionals" value=\'' + $.ajatus.converter.toJSON(doc.value._additionals) + '\' />');
                additionals_hidden.appendTo(row_holder);
            }
            
            $.each(doc.value.metadata, function(k,m){    	        
                var data = $.extend({
                    _id: doc._id
                }, m);
                var md_field = function() {
                    return [
                        'input', { type: 'hidden', id: this._id+'_metadata_'+k, name: 'metadata['+k+']', value: this.val }, '',
                    ];
                };            
                $(row_holder).tplAppend(data, md_field);
            });
            
    	    $.each(schema_fields, function(i,n){
    	        if (   has_additionals
    	            && typeof doc.value._additionals[i] != 'undefined')
    	        {
    	            $.ajatus.renderer.form_helpers._add_additional_row('edit', row_holder, i, n, doc);
    	        } else {
    	            $.ajatus.renderer.form_helpers._add_row('edit', row_holder, i, n, doc);
    	        }
    	    });

            row_holder.appendTo(form_struct);
            form_struct.appendTo(form);
            form_actions.appendTo(form);
            
            $('<div class="clear_fix" />').appendTo(form);
            
            $('li:first', row_holder).addClass('first');
            $('li:last', row_holder).addClass('last');
            $('li:odd', row_holder).addClass('odd');

    	    if (   typeof(content_type['enable_additionals']) != 'undefined'
    	        && content_type.enable_additionals)
    	    {
        	    var additionals_add = $('<div class="additionals_add_btn" />').html($.ajatus.i10n.get('Add field'));
                additionals_add.appendTo(form_struct);
                
                additionals_add.bind('click', function(e){
                    $.ajatus.document.additionals.create(content_type, row_holder);
                });  	        
    	    }

            var save_action = "$('#edit_"+type_name+" input[@type=submit][name*=save]').trigger('click', []);";
            $.ajatus.renderer.form_helpers._register_for_autosave(form, save_action);

            return form;
        }
    });
    
})(jQuery);