/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.list = function(target, content_type, options) {
        var self = this;
        this.options = $.extend({
            className: 'list_holder',
            id: 'list_holder',
            title: null,
            plural_title: true,
            title_suffix: null,
            append: false,
            headers: false,
            schema: false,
            actions: false,
            show_actions: true,
            pool: {
                enabled: false,
                settings: {
                    tick_limit: 20                    
                }
            },
            item_id_suffix: '',
            item_title_as_link: true
        }, options);
        
        this.actions = [
            {
                name: 'view',
                title: $.ajatus.i10n.get('View'),
                icon: 'view.png',
                click_action: '$.ajatus.views.system.item.render(doc);'
            },
            {
                name: 'edit',
                title: $.ajatus.i10n.get('Edit'),
                icon: 'edit.png',
                click_action: '$.ajatus.views.system.edit.render(content_type, { _id: doc._id, _rev: doc._rev });'
            },
            {
                name: 'archive',
                title: $.ajatus.i10n.get('Archive'),
                icon: 'archive.png',
                click_action: '$.ajatus.document.actions.execute("archive", doc);'
            },
            {
                name: 'delete',
                title: $.ajatus.i10n.get('Delete'),
                icon: 'trash.png',
                click_action: '$.ajatus.document.actions.execute("delete", doc);'
            }          
        ];
        
        this.pool = false;
        
        this.target = target;
        this.headers = this.options.headers || content_type.list_headers;
        this.schema_fields = this.options.schema || (content_type.list_schema || content_type.schema);
        
        this.holder = $('<div class="'+options.className+'" id="'+options.id+'" />');
        
        title_str = this.options.title || content_type.title;
        if (this.options.plural_title) {
            title_str = $.ajatus.i10n.plural($.ajatus.i10n.get(title_str));
        } else {
            title_str = $.ajatus.i10n.get(title_str);
        }
        if (this.options.title_suffix != null) {
            title_str += ' ' + this.options.title_suffix;
        }
        title = $('<h2>'+title_str+'</h2>');
        title.appendTo(this.holder);
    
        this.table = $('<table class="listing"/>');
        this.thead = $('<thead><tr></tr></thead>');
        this.tbody = $('<tbody />');
        this.thead.appendTo(this.table);
        this.tbody.appendTo(this.table);
        this.table.appendTo(this.holder);
        
        this.attach(options.append);
    };
    $.extend($.ajatus.renderer.list.prototype, {
        set_actions: function(actions) {
            this.actions = actions;
        },
        render_items: function(docs)
        {
            var self = this;
            $.each(docs, function(i,d){
                self.render_item(d);
            });
        },        
        render_item: function(doc, row_num) {
            if (this.options.pool.enabled) {
                this.pool.add_item("$.ajatus.renderer.list_callback", [this, doc, row_num, this.tbody, this.headers, this.actions])
            } else {
                $.ajatus.renderer.list_callback(this, doc, row_num, this.tbody, this.headers, this.actions);
            }
        },
        items_added: function(doc_count) {
            if (this.options.pool.enabled) {
                this.pool.items_added(doc_count);
            }
        },
        render_row_actions: function(row, doc, actions) {
            if (! this.options.show_actions) {
                return;
            }
            
            var content_type = $.ajatus.preferences.client.content_types[doc.value._type];

            var action_items = [];
            var actions_length = actions.length-1;
            
            $.each(actions, function(i,action){                
                action_items.push('a');
                action_items.push({
                    className: action.name + '_link',
                    title: action.title,
                    href: '#' + action.name + '.' + content_type.name + '.' + (doc._id || doc.id),
                    onclick: function(e){eval(action.click_action);}
                });
                if (   typeof action['icon'] != 'undefined'
                    && action.icon != '')
                {
                    action_items.push([
                        'img', { src: $.ajatus.preferences.client.theme_icons_url + action.icon, title: action.title }, ''
                    ]);
                } else {
                    action_items.push(action.title);
                }
                
                if (i < actions_length)
                {
                    action_items.push('span');
                    action_items.push({});
                    action_items.push('|');                    
                }
            });
            
            $(row).createAppend(
                'td', { className: 'actions' }, action_items
            );
        },
        attach: function(append) {
            this._render_headers();
            
            if (this.options.pool.enabled) {
                this.pool = new $.ajatus.renderer(this.options.pool.settings, this);
            }
                    
            if (append) {
                this.holder.appendTo(this.target);
            } else {
                this.target.html(this.holder);
            }
        },
        enable_sorting: function() {
            $(this.table).tablesorter({
            });
        },
        _render_headers: function() {
            var self = this;
            $.each(this.headers, function(i,h){
                var schema = self.schema_fields[h];
                if (!schema)
                {
                    schema = {
                        label: h
                    };
                }
            
                var className = h;
                if (i == 0)
                {
                    className = 'first_column ' + className;
                }
            
                var column = function() {
                    return [
                        'th', { className: className }, $.ajatus.i10n.get(this.label)
                    ];
                };

    	        $('tr', self.thead).tplAppend(schema, column);
            });

            if (this.options.show_actions) {
                $('tr', self.thead).createAppend(
                    'th', { className: 'actions {sorter: false}' }, $.ajatus.i10n.get('Actions')
                );
            }
        }
    });
    
    $.ajatus.renderer.tinylist = function(target, options)
    {
        this.options = $.extend({}, {
            className: 'tinylist_holder',
            id: 'tinylist_holder',
            title: null,
            title_suffix: null,
            append: true,
            show_headers: true,
            show_actions: true,
            headers: ['_type', 'title'],
            schema: {
                '_type': {
                    label: 'Type'
                },
                'title': {
                    label: $.ajatus.i10n.get('Title')
                }
            },
            actions: [
                {
                    name: 'view',
                    title: 'View',
                    icon: 'view.png',
                    click_action: '$.ajatus.views.system.item.render(doc);'
                }
            ]
        }, options || {});
        
        this.target = target;
        this.headers = this.options.headers;
        this.schema_fields = this.options.schema;
        
        this.holder = $('<div class="'+this.options.className+'" id="'+this.options.id+'" />');

        if (this.options.title != null) {
            title_str = this.options.title;
            if (this.options.title_suffix != null) {
                title_str += ' ' + this.options.title_suffix;
            }
            title = $('<h2>'+title_str+'</h2>');
            title.appendTo(this.holder);            
        }
    
        this.table = $('<table class="listing"/>');
        if (this.options.show_headers) {
            this.thead = $('<thead><tr></tr></thead>');
            this.thead.appendTo(this.table);            
        }
        this.tbody = $('<tbody />');
        this.tbody.appendTo(this.table);
        this.table.appendTo(this.holder);
        
        this.attach(this.options.append);
    };
    $.extend($.ajatus.renderer.tinylist.prototype, {
        set_actions: function(actions) {
            this.actions = actions;
        },
        render_items: function(docs)
        {
            var self = this;
            $.each(docs, function(i,d){
                self.render_item(d, i);
            });
        },        
        render_item: function(doc, row_num) {            
            $.ajatus.renderer.list_callback(this, doc, row_num, this.tbody, this.headers, this.options.actions);
        },
        render_row_actions: function(row, doc, actions) {
            if (! this.options.show_actions) {
                return;
            }
            
            var content_type = $.ajatus.preferences.client.content_types[doc.value._type];

            var action_items = [];
            var actions_length = actions.length-1;
            
            $.each(actions, function(i,action){                
                action_items.push('a');
                action_items.push({
                    className: action.name + '_link',
                    title: $.ajatus.i10n.get(action.title),
                    href: '#' + action.name + '.' + content_type.name + '.' + (doc._id || doc.id),
                    onclick: function(e){eval(action.click_action);}
                });
                if (   typeof action['icon'] != 'undefined'
                    && action.icon != '')
                {
                    action_items.push([
                        'img', { src: $.ajatus.preferences.client.theme_icons_url + action.icon, title: $.ajatus.i10n.get(action.title) }, ''
                    ]);
                } else {
                    action_items.push($.ajatus.i10n.get(action.title));
                }
                
                if (i < actions_length)
                {
                    action_items.push('span');
                    action_items.push({});
                    action_items.push('|');                    
                }
            });
            
            $(row).createAppend(
                'td', { className: 'actions' }, action_items
            );
        },
        attach: function(append) {
            if (this.options.show_headers) {
                this._render_headers();
            }
                    
            if (append) {
                this.holder.appendTo(this.target);
            } else {
                this.target.html(this.holder);
            }
        },
        _render_headers: function() {
            var self = this;
            $.each(this.headers, function(i,h){
                var schema = self.schema_fields[h];
                if (!schema)
                {
                    schema = {
                        label: h
                    };
                }
            
                var className = h;
                if (i == 0)
                {
                    className = 'first_column ' + className;
                }
            
                var column = function() {
                    return [
                        'th', { className: className }, $.ajatus.i10n.get(this.label)
                    ];
                };

    	        $('tr', self.thead).tplAppend(schema, column);
            });
            
            if (this.options.show_actions) {
                $('tr', self.thead).createAppend(
                    'th', { className: 'actions {sorter: false}' }, $.ajatus.i10n.get('Actions')
                );
            }
        }
    });
    
    $.ajatus.renderer.list_callback = function(caller, doc, row_num, tbody, headers, actions) {
        var row_class = 'even';
        if (row_num != undefined)
        {
            if (row_num % 2 == 0)
            {
                row_class = 'odd';
            }
        }
    
        var row = $('<tr class="'+row_class+'"/>').attr({
            id: 'object_'+doc._id + caller.options.item_id_suffix
        });
        row.appendTo(tbody);
        
        $.each(headers, function(i,h){
            var data = $.extend({
                val: null,
                widget: {
                    name: 'text',
                    config: {}
                }
            }, doc.value[h]);
            
            if (   h == '_type'
                || h == '_id'
                || h == '_rev')
            {
                data.val = doc[h] || doc.value[h];
            }
            
            if (data.val === null) {
                var full_doc = new $.ajatus.document.loader(doc._id, doc._rev);
                data = $.extend({
                    val: '',
                    widget: {
                        name: 'text',
                        config: {}
                    }
                }, full_doc.value[h]);
            }
            
            var widget = new $.ajatus.widget(data.widget.name, data.widget.config);
            data.val = widget.value_on_view(data.val, 'list');
            
            var className = h;
            if (i == 0) {
                className = 'first_column ' + className;
            }
                        
            if (h == '_type') {
                var type = data.val;
                var normalized_type = type.toString().replace('_', ' ');
                
                data.val = [
                    'img', { src: $.ajatus.preferences.client.theme_icons_url + type + '.png', title: $.ajatus.i10n.get(normalized_type), alt: type }, ''
                ];
            }

            if (   h == 'title'
                && caller.options.item_title_as_link)
            {
                var type = typeof doc.value['_type'] != 'undefined' ? doc.value._type : 'note';
                var title = '' + data.val;

                data.val = [
                    'a', { href: '#view.' + type + '.' + doc._id }, title
                ];
            }
            
            var column = function() {
                return [
                    'td', { className: className }, this.val
                ];
            };

	        $(row).tplAppend(data, column);
	        
	        return true;
        });
        
        caller.render_row_actions(row, doc, actions);
        
        return true;
    };

})(jQuery);