/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.list_tpl = function(target, content_type, options) {
        var default_options = {
            className: 'list_holder',
            id: 'list_holder',
            title: null,
            plural_title: true,
            title_suffix: null,
            show_count_on_title: true,
            append: false,
            headers: false,
            schema: false,
            actions: null,
            show_actions: true,
            item_id_suffix: '',
            main_template: null,
            pool: {
                enabled: false,
                settings: {
                    type: 'scroll',
                    render_item_after_load: false,
                    tick_limit: 20,
                    use_db: false
                }
            },
        };
        
        this.options = $.extend({}, default_options, options);
        if (   typeof options['pool'] != 'undefined'
            && typeof options['pool']['settings'] != 'undefined')
        {
            this.options.pool.settings = $.extend(default_options.pool.settings, options['pool']['settings']);
        }
                
        this.target = target;
        this.headers = this.options.headers || content_type.list_headers;
        this.schema_fields = this.options.schema || (content_type.list_schema || content_type.schema);

        this.options.actions = [
            {
                name: 'view',
                title: $.ajatus.i10n.get('View'),
                icon: 'view.png'
            },
            {
                name: 'edit',
                title: $.ajatus.i10n.get('Edit'),
                icon: 'edit.png'
            },
            {
                name: 'archive',
                title: $.ajatus.i10n.get('Archive'),
                icon: 'archive.png',
                click_action: '$.ajatus.document.actions.execute("archive", doc);'
            },
            {
                name: 'delete',
                title: $.ajatus.i10n.get('Delete'),
                icon: 'trash.png',
                click_action: '$.ajatus.document.actions.execute("delete", doc);'
            }          
        ];

        this.holder = $('<div class="'+options.className+'" id="'+options.id+'" />');

        title_str = this.options.title || content_type.title;
        if (this.options.plural_title) {
            title_str = $.ajatus.i10n.plural($.ajatus.i10n.get(title_str));
        } else {
            title_str = $.ajatus.i10n.get(title_str);
        }
        if (this.options.title_suffix != null) {
            title_str += ' ' + this.options.title_suffix;
        }

        this.tpl_data = {
            title: title_str,
            total_items: null,
            headers: [],
            items: [],
            show_count: true,
            toString: function() {
                var str = this.title;
                if (this.show_count) {
                    var cnt = this.items.length;
                    if (this.total_items != null) {
                        cnt = this.total_items;
                    }
                    str += ' ('+cnt+')';
                }
				return str;
			}
        };
        this.tpl_data.show_count = this.options.show_count_on_title;

        this.items = [];
        this.processed_items = [];
        
        this.actions_to_bind = {};
        
        if (this.options.main_template == null) {
            this.options.main_template = '{#template MAIN}' +
    		    '<h2>{$T}</h2>' +
    			'<table class="listing">' +
    			    '<thead><tr>' +
        				'{#foreach $T.headers as header}' +
        					'{#include HCOLUMN root=$T.header}' +
        				'{#/for}' +
    			    '</tr></thead>' +
    			    '<tbody>' +
        				'{#foreach $T.items as item}' +
        				    '<tr class="{#cycle values=[\'odd\',\'even\']}">' +
        					    '{#include ICOLUMNS root=$T.item}' +
        					'</tr>' +
        				'{#/for}' +
    			    '</tbody>' +
    			'</table>' + 
    		'{#/template MAIN}';
        }
        this.header_column_tpl = $.createTemplate('<th class="{$T.className}">{$T.content}</th>');
        this.item_columns_tpl = '';
        this.item_actions_tpl = $.createTemplate('<a href="{$T.url}" title="{$T.title}" {#if $T.id} id="{$T.id}" {#/if}><img src="{$T.icon}" class="{$T.className}" alt="{$T.title}"/></a>');
        
        this.pool = null;
        if (this.options.pool.enabled) {
            this.pool = new $.ajatus.renderer(this.options.pool.settings, this);
        }
        
        this.attach(this.options.append);
    };
    $.extend($.ajatus.renderer.list_tpl.prototype, {
        attach: function(append) {                    
            if (append) {
                this.holder.appendTo(this.target);
            } else {
                this.target.html(this.holder);
            }
        },
        render: function() {
            this._collect_headers();
            this._collect_items();
            
            this._render();
        },
        render_item: function(doc, idx) {//from_db
            // if (   typeof from_db != 'undefined'
            //     && from_db)
            // {
            //     this.items.push(doc);
            // }
            return this._render_item(doc, idx);
            
            //Render Row
        },
        add_item: function(doc) {
            if (this.options.pool.enabled) {
                this.pool.add_item(doc);
            } else {
                this.items.push(doc);                
            }
        },
        add_total_count: function(count) {
            if (typeof count != 'undefined') {
                this.tpl_data.total_items = count;

                if (this.options.pool.enabled) {
                    this.pool.items_added(count);
                }
            }
        },
        enable_sorting: function() {
            $('table', this.holder).tablesorter();
        },
        _collect_headers: function() {
            var _self = this;
            $.each(this.headers, function(i,h){
                var schema = _self.schema_fields[h];
                if (! schema) {
                    schema = {
                        label: h
                    };
                }

                var className = h;
                if (i == 0) {
                    className = 'first_column ' + className;
                }
                
                _self.tpl_data.headers.push({
                    className: className,
                    content: $.ajatus.i10n.get(schema.label)
                });
            });
            
            if (this.options.show_actions) {
                this.tpl_data.headers.push({
                    className: 'actions',
                    content: $.ajatus.i10n.get('Actions')
                });                
            }
        },
        _collect_items: function() {
            var _self = this;
            
            for (var i=0; i<this.items.length; i++) {
                var doc = this.items[i];
                
                var row_data = {};
                
                $.each(this.headers, function(i, h){
                    var className = h;
                    if (i == 0) {
                        className = 'first_column ' + className;
                    }
                    
                    var data = $.extend({
                        val: null,
                        widget: {
                            name: 'text',
                            config: {}
                        }
                    }, doc.value[h]);

                    if (   h == '_type'
                        || h == '_id'
                        || h == '_rev')
                    {
                        data.val = doc[h] || doc.value[h];
                    }

                    if (data.val === null) {
                        var full_doc = new $.ajatus.document.loader(doc._id, doc._rev);
                        data = $.extend({
                            val: '',
                            widget: {
                                name: 'text',
                                config: {}
                            }
                        }, full_doc.value[h]);
                    }

                    var widget = new $.ajatus.widget(data.widget.name, data.widget.config);
                    data.val = widget.value_on_view(data.val, 'list');

                    if (h == '_type') {
                        var type = data.val;
                        var normalized_type = type.toString().replace('_', ' ');

                        var img_title = $.ajatus.i10n.get(normalized_type);
                        data.val = '<img src="' + $.ajatus.preferences.client.theme_icons_url + type + '.png' + '" title="'+img_title+'" alt="'+img_title+'"/>';
                    }

                    row_data[h] = {
                        column_className: className,
                        content: data.val
                    };
                    
                    if (_self.options.show_actions) {
                        row_data['actions'] = {
                            items: _self._get_actions_for_item(doc)
                        }
                    }
                    
                    _self.processed_items.push(doc._id);
                });
                
                
                this.tpl_data.items.push(row_data);
            }
        },
        _get_actions_for_item: function(doc, actions) {
            if (typeof actions == 'undefined') {
                var actions = this.options.actions;
            }
            var content_type = $.ajatus.preferences.client.content_types[doc.value._type];

            var actions_length = actions.length-1;
            
            var action_items = [];
            
            var _self = this;
            $.each(actions, function(i,action){
                var action_data = {
                    id: '' + action.name + '_' + content_type.name + '_' + (doc._id || doc.id),
                    url: '#' + action.name + '.' + content_type.name + '.' + (doc._id || doc.id),
                    title: action.title,
                    icon: $.ajatus.preferences.client.theme_icons_url + action.icon,
                    className: action.name + '_link',     
                };
                
                if (typeof action['click_action'] != 'undefined') {
                    _self.actions_to_bind[action_data.id] = function(e){eval(action.click_action);};
                }
                
                action_items.push(action_data);
            });
            
            return action_items;
        },
        _create_items_tpl: function() {
            var item_columns_tpl = '';
            $.each(this.headers, function(i,h){
    	        item_columns_tpl += '<td class="{$T.'+h+'.column_className}">{$T.'+h+'.content}</td>';
            });
            
            if (this.options.show_actions) {
                item_columns_tpl += '<td class="actions">' +
                    '{#foreach $T.actions.items as aitem}' +
					    '{#include IACTIONS root=$T.aitem}' +
    				'{#/for}' +
                '</td>';
                
                this.item_columns_tpl = $.createTemplate(item_columns_tpl, {
                    'IACTIONS': this.item_actions_tpl
                });  
            } else {
                this.item_columns_tpl = $.createTemplate(item_columns_tpl);  
            }
        },
        _render: function() {
            this._create_items_tpl();
            
            $(this.holder).setTemplate(this.options.main_template, {
                'HCOLUMN': this.header_column_tpl,
                'ICOLUMNS': this.item_columns_tpl
            });
            $(this.holder).processTemplate(this.tpl_data);
            
            this._bind_actions();
        },
        _render_item: function(doc, idx) {            
            if ($.inArray(doc._id, this.processed_items) != -1) {
                return true;
            }
            
            var tpl_data = this._get_item_tpl_data(doc);            
            var tbody = $('tbody', this.holder);
            
            var row_class = "even";
            if (idx != 'undefined') {
                if (idx % 2 == 0)
                {
                    row_class = 'odd';
                }                
            }
            
            var item_columns = '';
            $.each(this.headers, function(i,h){
    	        item_columns += '<td class="{$T.'+h+'.column_className}">{$T.'+h+'.content}</td>';
            });
            item_columns += '<td class="actions">' +
                '{#foreach $T.actions.items as aitem}' +
				    '{#include IACTIONS root=$T.aitem}' +
				'{#/for}' +
            '</td>';
            
            var item_row = '<tr class="'+row_class+'">' + item_columns + '</tr>';
            
            item_row_tpl = $.createTemplate(item_row, {
                'IACTIONS': this.item_actions_tpl
            });
            
            $(tbody).setTemplate(item_row_tpl);
            $(tbody).processTemplate(tpl_data, {}, true);
            
            this.processed_items.push(doc._id);
            
            this._bind_actions();
            
            return true;
        },
        _get_item_tpl_data: function(doc) {
            var _self = this;                
            var row_data = {};
            
            $.each(this.headers, function(i, h){
                var className = h;
                if (i == 0) {
                    className = 'first_column ' + className;
                }
                
                var data = $.extend({
                    val: null,
                    widget: {
                        name: 'text',
                        config: {}
                    }
                }, doc.value[h]);

                if (   h == '_type'
                    || h == '_id'
                    || h == '_rev')
                {
                    data.val = doc[h] || doc.value[h];
                }

                if (data.val === null) {
                    var full_doc = new $.ajatus.document.loader(doc._id, doc._rev);
                    data = $.extend({
                        val: '',
                        widget: {
                            name: 'text',
                            config: {}
                        }
                    }, full_doc.value[h]);
                }

                var widget = new $.ajatus.widget(data.widget.name, data.widget.config);
                data.val = widget.value_on_view(data.val, 'list');

                if (h == '_type') {
                    var type = data.val;
                    var normalized_type = type.toString().replace('_', ' ');

                    var img_title = $.ajatus.i10n.get(normalized_type);
                    data.val = '<img src="' + $.ajatus.preferences.client.theme_icons_url + type + '.png' + '" title="'+img_title+'" alt="'+img_title+'"/>';
                }

                row_data[h] = {
                    column_className: className,
                    content: data.val
                };
                
                if (_self.options.show_actions) {
                    row_data['actions'] = {
                        items: _self._get_actions_for_item(doc)
                    }
                }
            });
            
            return row_data;
        },
        _bind_actions: function() {
            var _self = this;
            $.each(this.actions_to_bind, function(id,action){
                var elem = $('#'+id);
                if (typeof elem[0] != 'undefined'
                    && action != null)
                {
                    elem.bind('click', action);
                    _self.actions_to_bind[id] = null;
                }
            });
            // this.actions_to_bind = $.grep(this.actions_to_bind, function(action, id){
            //     var elem = $('#'+id);
            //     console.log(id);
            //     if (typeof elem[0] != 'undefined') {
            //         console.log("binded to "+id);
            //         elem.bind('click', action);
            //         return false;                    
            //     }
            //     return true;
            // });
        }
    });

})(jQuery);