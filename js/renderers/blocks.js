/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.blocks = {
        available_subs: []
    };
    $.extend($.ajatus.renderer.blocks, {
        init: function() {
            $.ajatus.renderer.blocks._collect_subs();
        },        
        create_sub: function(sub_id) {
            var id = 'sub_block-' + $.ajatus.renderer.blocks.available_subs.length;
            if (id == 0) {
                id = 1;
            }
                        
            if (typeof sub_id != 'undefined') {
                id = sub_id;
            }
            
            var elem = $('<div />').attr({
                id: id,
                'class': 'sub_block'
            });
            elem.appendTo($('.sub_blocks', $.ajatus.application_element));
            
            return id;
        },
        clear_sub: function(id) {
            var elm = $('#'+id);
            elm.html('');
        },
        _collect_subs: function() {
            $('.sub_block', $.ajatus.application_element).each(function(i,b){
                $.ajatus.renderer.blocks.available_subs.push(b.id);
            });
        }
    });
    
    $(document).ready(function(){
        $.ajatus.renderer.blocks.init();
    });
    
    $.ajatus.renderer.blocks.sub = function(block_id, options) {        
        if (typeof block_id == 'undefined') {
            var block_id = $.ajatus.renderer.blocks.available_subs[0];
            if (   typeof block_id == 'undefined'
                || block_id == '')
            {
                block_id = $.ajatus.renderer.blocks.create_sub();
            }
        }        
        this.id = block_id;
        this.element = $('#'+this.id);
        if (typeof this.element[0] == 'undefined') {
            this.element = $('#' + $.ajatus.renderer.blocks.create_sub(this.id));
        }
        
        this.options = $.extend({        
        }, options || {});
        
        return this;
    };
    $.extend($.ajatus.renderer.blocks.sub.prototype, {
        set_title: function(title) {
            var elm = $(this.element).find('h2').eq(0);
            if (typeof elm[0] == 'undefined') {
                elm = $('<h2 />');
                elm.prependTo(this.element);
            }
            
            elm.html(title);
        },
        set_content: function(content) {
            var elm = $(this.element).find('div.content').eq(0);
            if (typeof elm[0] == 'undefined') {
                elm = $('<div />').addClass('content');
                elm.prependTo(this.element);
            }
            elm.html(content);
        },
        clear: function() {
            this.element.html('');
        }
    });
    
})(jQuery);