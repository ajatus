/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.metadata = function(doc, holder, options)
	{
	    this.options = $.extend({
	        display: [
	            'created', 'creator', 'revised', 'revisor'
	        ],
	        revisions: true
	    }, options || {});
	    
	    this.doc = doc;
	    this.revisions = false;
	    if (this.options.revisions) {
	        this.revisions = $.ajatus.document.revisions.get(doc);
	    }
	    
	    this.rendered = this.render_html(doc);
	    
	    if (typeof holder != 'undefined') {
    	    this.rendered.appendTo(holder);	        
	    }
	    
	    return this;
	};
	$.extend($.ajatus.renderer.metadata.prototype, {
	    render_html: function(doc)
	    {
	        var md_struct = $('<div class="md_structure" />');
    	    var md_revisions = $('<div class="md_revisions"></div>');
    	    var md_actions = $('<div class="md_actions"></div>');
    	    var row_holder = $('<ul />');

            var schema_fields = $.ajatus.utils.object.clone($.ajatus.document.metadata);
            
            var _self = this;

    	    $.each(schema_fields, function(i,n){
    	        if ($.inArray(i,_self.options.display) == -1) {
    	            return;
    	        }
    	        
    	        var doc_value = {};
    	        if (typeof doc.value.metadata[i] == 'undefined') {
    	            doc_value = {
    	                val: '',
    	                widget: n.widget
    	            }
    	        } else {
    	            doc_value = doc.value.metadata[i];
    	        }
    	        
    	        var data = $.extend({
    	            _id: doc._id
    	        }, doc_value);

    	        var widget = new $.ajatus.widget(n.widget.name, n.widget.config);
                
                var row = function() {
                    return [
                        'li', { id: this._id+'_element_'+i, className: 'row' }, [
                            'label', { id: this._id+'_element_'+i+'_label' }, $.ajatus.i10n.get(n.label),
                            'div', { id: this._id+'_element_'+i+'_widget', className: widget.name }, widget.get_view_tpl(i,data)
                        ]
                    ];
                };                
    	        $(row_holder).tplAppend(data, row);
    	        
                widget.init($('#'+doc._id+'_element_'+i+'_widget',row_holder));    	        
    	    });
    	    
    	    $('li:odd', row_holder).addClass('odd');
            
            this.render_revision_nav(md_revisions);
        
    	    row_holder.appendTo(md_struct);
    	    md_revisions.appendTo(md_struct);
    	    md_actions.appendTo(md_struct);

            return md_struct;
	    },
	    render_revision_nav: function(holder) {
	        if (! this.revisions) {
	            return;
	        }
	        var _self = this;
	        
	        var render_doc = function(type, id, rev) {
	            $.ajatus.views.system.item.render({
	                _id: id,
	                _rev: rev,
                    value: {
                        _type: type
                    }
	            });	            
	        };       
	        
	        var first_btn = $('<img />').attr({
	            src: $.ajatus.preferences.client.theme_icons_url + 'start.png',
	            alt: $.ajatus.i10n.get('First')
            }).bind('click', function(){
                var rev_data = $.ajatus.document.revisions.navigate_to('first', _self.doc._rev, _self.revisions);
                if (rev_data.rev != null) {
                    render_doc(_self.doc.value._type, _self.doc._id, rev_data.rev);
                }
	        });
	        var prev_btn = $('<img />').attr({
	            src: $.ajatus.preferences.client.theme_icons_url + 'previous.png',
	            alt: $.ajatus.i10n.get('Previous')
	        }).bind('click', function(){
                var rev_data = $.ajatus.document.revisions.navigate_to('prev', _self.doc._rev, _self.revisions);
                if (rev_data.rev != null) {
                    render_doc(_self.doc.value._type, _self.doc._id, rev_data.rev);
                }
	        });
	        var last_btn = $('<img />').attr({
	            src: $.ajatus.preferences.client.theme_icons_url + 'finish.png',
	            alt: $.ajatus.i10n.get('Last')
	        }).bind('click', function(){
                var rev_data = $.ajatus.document.revisions.navigate_to('last', _self.doc._rev, _self.revisions);
                if (rev_data.rev != null) {
                    render_doc(_self.doc.value._type, _self.doc._id, rev_data.rev);
                }
            });
	        var next_btn = $('<img />').attr({
	            src: $.ajatus.preferences.client.theme_icons_url + 'forward.png',
	            alt: $.ajatus.i10n.get('Next')
	        }).bind('click', function(){
                var rev_data = $.ajatus.document.revisions.navigate_to('next', _self.doc._rev, _self.revisions);
                if (rev_data.rev != null) {
                    render_doc(_self.doc.value._type, _self.doc._id, rev_data.rev);
                }
            });

            var rev_key = 0;
            $.each(this.revisions.all, function(i,n){
                if (n.rev == _self.doc._rev) {
                    rev_key = i;
                }
            });
	        var rev_stats_str = $.ajatus.i10n.get("%d of %d", [this.revisions.all.length - rev_key, this.revisions.all.length]);
	        
	        var rev_stats = $('<div class="rev_stats" />').html(rev_stats_str);
	        var current_rev = $('<span class="current_rev" />').html(this.doc._rev);
	        
	        rev_stats.appendTo(holder);
	        first_btn.appendTo(holder);
	        prev_btn.appendTo(holder);
	        current_rev.appendTo(holder);
	        next_btn.appendTo(holder);
	        last_btn.appendTo(holder);
	    }
    });
    
})(jQuery);