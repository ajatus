/*
 * This file is part of
 *
 * Ajatus - Distributed CRM
 * @requires jQuery v1.2.1
 * 
 * Copyright (c) 2007 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2007 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

(function($){
    $.ajatus = $.ajatus || {};
    $.ajatus.renderer = $.ajatus.renderer || {};
    
    $.ajatus.renderer.item = function(doc, append)
	{
	    this.item = this.render_html($.ajatus.preferences.client.content_types[doc.value._type], doc);
	    if (   typeof append == 'undefined'
	        || append == false)
	    {
    	    $.ajatus.application_content_area.html(this.item);
	    }
	    else
	    {
    	    this.item.appendTo($.ajatus.application_content_area);
	    }
	    
	    return this;
	};
	$.extend($.ajatus.renderer.item.prototype, {
	    render_html: function(content_type, doc)
	    {
	        var item_struct = $('<div class="item_structure" />');
    	    var item_actions = $('<div class="item_actions"><div class="action_edit">' + $.ajatus.i10n.get('Edit') + '</div></div>');
    	    var row_holder = $('<ul />');

    	    var type_title = content_type.title;
    	    $('<h2>View ' + type_title + '</h2>').appendTo(item_struct);

            var schema_fields = $.ajatus.utils.object.clone(content_type.original_schema);
            
            if (typeof doc.value._additionals != 'undefined') {
                $.each(doc.value._additionals, function(key,data){
                    if (typeof data != 'object') {
                        return false;
                    }
                    schema_fields[key] = data;
                });
            }

    	    $.each(schema_fields, function(i,n){
    	        var doc_value = {};
    	        if (! doc.value[i])
    	        {
    	            doc_value = {
    	                val: '',
    	                widget: n.widget
    	            }
    	        }
    	        else
    	        {
    	            doc_value = doc.value[i];
    	        }
    	        
    	        var data = $.extend({
    	            _id: doc._id
    	        }, doc_value);

    	        var widget = new $.ajatus.widget(n.widget.name, n.widget.config);
                
                var row = function() {
                    return [
                        'li', { id: this._id+'_element_'+i, className: 'row' }, [
                            'label', { id: this._id+'_element_'+i+'_label' }, $.ajatus.i10n.get(n.label),
                            'div', { id: this._id+'_element_'+i+'_widget', className: widget.name }, widget.get_view_tpl(i,data)
                        ]
                    ];
                };                
    	        $(row_holder).tplAppend(data, row);
    	        
                widget.init($('#'+doc._id+'_element_'+i+'_widget',row_holder));    	        
    	    });

    	    $('div.action_edit', item_actions).bind('click', function(e){
                $.ajatus.views.system.edit.render(content_type, doc);
    	    });

    	    row_holder.appendTo(item_struct);
    	    item_actions.appendTo(item_struct);

            return item_struct;
	    }
    });
    
})(jQuery);