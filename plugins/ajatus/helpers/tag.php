<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_helpers_tag
{
    private $connection;
    private $configuration;
    private $cache;
    
    public function __construct(&$connection, &$configuration)
    {
        $this->connection =& $connection;
        $this->configuration =& $configuration;
    }
    
    public function id_to_tag($id)
    {
        static $cache = array();
        if (isset($cache[$id]))
        {
            return $cache[$id]; 
        }
        try
        {
            $tag_object = $GLOBALS['ajatus']->types->tag->get_object($id)->value;
        }
        catch (couchdb_document_exception $e)
        {
            if ($e->getCode() == 404)
            {
                // document does not exist
                return false;
            }
            // Otherwise bubble up
            throw $e;
        }

        $ret = $tag_object->title->val;
        if (   isset($tag_object->title->widget->config->context)
            && !empty($tag_object->title->widget->config->context))
        {
            $ret = "{$tag_object->title->widget->config->context}:{$tag_object->title->val}";
        }
        if (   isset($tag_object->title->widget->config->value)
            && !empty($tag_object->title->widget->config->value))
        {
            $ret .= "={$tag_object->title->widget->config->value}";
        }
        if (!empty($ret))
        {
            $cache[$id] = $ret;
        }
        return $ret;
    }
    
    public function tag_to_id($tag)
    {
        if (is_object($tag))
        {
            return $tag->_id;
        }
        list ($tag, $context, $value) = $this->tag_parts($tag);
        $res = $this->get_tag($tag, $context, $value);
        if ($res)
        {
            return $res->id;
        }
        return false;
    }

    public function ids_to_tags(array $ids)
    {
        $ret = array();
        foreach ($ids as $id)
        {
            $ret[$id] = $this->id_to_tag($id);
        }
        return $ret;
    }

    public function tag_parts($tag)
    {
        $context = '';
        $value = '';
        if (is_object($tag))
        {
            // Already a document, take the parts.
            $tag_object = $tag->value;
            $tag = $tag_object->title->val;
            if (   isset($tag_object->title->widget->config->context)
                && !empty($tag_object->title->widget->config->context))
            {
                $context = $tag_object->title->widget->config->context; 
            }
            if (   isset($tag_object->title->widget->config->value)
                && !empty($tag_object->title->widget->config->value))
            {
                $value = $tag_object->title->widget->config->value; 
            }
            return array($tag, $context, $value);
        }

        $cparts = explode(':', $tag);
        if (   is_array($cparts)
            && count($cparts) === 2)
        {
            $context = $cparts[0];
            $tag = $cparts[1];
        }
        $vparts = explode('=', $tag);
        if (is_array($vparts)
            && count($vparts) === 2)
        {
            $value = $vparts[1];
            $tag = $vparts[0];
        }
        
        return array($tag, $context, $value);
    }
    
    public function tags_to_id(array $tags)
    {
        $results = array();
        
        foreach ($tags as $k => $tag)
        {
            $id = $this->tag_to_id($tag);
            if ($id === false)
            {
                continue;
            }
            $results[$k] = $id;
        }

        return $results;
    }
    
    public function get_tag($name, $context='', $value='')
    {
        $result = false;
        
        $view = 'function(doc){';
        $view .= "if (doc.value._type == 'tag' && doc.value.title.val == '{$name}' ";
        
        if (! empty($context))
        {
            $view .= "&& doc.value.title.widget.config.context == '{$context}' ";
        }
        
        if (! empty($value))
        {
            $view .= "&& doc.value.title.widget.config.value == '{$value}' ";
        }
        
        $view .= ') {';
        
        $view .= 'map( doc._id, {';
        $view .= ajatus_types::prepare_map_values($GLOBALS['ajatus']->types->tag->map_values['list']);
        $view .= '});';
        
        $view .= '}}';
        
        // echo "Builded view: \n{$view}\n";
        
        $content_db = $this->configuration['content_db'];
        $results = $this->connection->$content_db->view->temp($view);
        
        if ($results->total_rows > 0)
        {
            $result = $results->rows[0];
        }
        
        return $result;
    }
    
    public function related(array $tags, array $skip_docs=array(), array $map_values=array(), $include_archived=false)
    {
        if (empty($tags))
        {
            return false;
        }
        
        $view = 'function(doc){';
        $view .= 'if (typeof(doc.value.tags) != "undefined" && doc.value.tags.val.length > 0';
        if (!$include_archived)
        {
            $view .= ' && doc.value.metadata.archived.val == false';
        }
        $view .= ' && doc.value.metadata.deleted.val == false){';
        
        if (! empty($skip_docs))
        {
            $view .= 'if (';
            $sd_cnt = count($skip_docs);
            for ($i=0; $i<$sd_cnt; $i++)
            {
                $view .= "doc._id != '{$skip_docs[$i]->id}' ";
                if ($i < $sd_cnt-1)
                {
                    $view .= '&& ';
                }
            }
            $view .= ') {';
        }
        
        $view .= 'var hm = false;';
        $view .= 'for (var i=0; i<doc.value.tags.val.length; i++){';
        $view .= 'if (';
        
        $tags_cnt = count($tags);
        for ($i=0; $i<$tags_cnt; $i++)
        {
            $view .= "doc.value.tags.val[i] == '{$tags[$i]}' ";
            if ($i < $tags_cnt-1)
            {
                $view .= '|| ';
            }
        }
        
        $view .= ') {';
        $view .= 'hm = true;}';
        $view .= '} if (hm) {';
        $view .= 'map( doc.value._type, {"_id": doc._id, "_rev": doc._rev, "_type": doc.value._type,';
        $view .= '"title": doc.value.title,';
        
        if (! empty($map_values))
        {
            foreach ($map_values as $key => $value)
            {
                if (in_array($key, array('title', 'tags')))
                {
                    continue;
                }

                $view .= json_encode($key) . ": {$value},";
            }
        }
        
        // $.each($.ajatus.preferences.client.content_types, function(key,type){
        //     if (type['title_item']) {
        //         $.each(type['title_item'], function(i,part){
        //             if (type.schema[part]) {
        //                 rel_view += '"'+part+'": doc.value.'+part+',';
        //             }
        //         });
        //     }
        // });
        
        $view .= '"tags": doc.value.tags });';
        $view .= '}}';
        
        if (! empty($skip_docs))
        {
            $view .= '}';
        }
        
        $view .= '}';
        
        // echo "Builded view: \n{$view}\n";
        
        $content_db = $this->configuration['content_db'];
        return $this->connection->$content_db->view->temp($view);
    }

}

?>