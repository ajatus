<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_helpers_date
{
    static function unixtime_to_jsdatetime($unixtime=false)
    {        
        if ($unixtime === false)
        {
            $unixtime = time();
        }
        
        $orig_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        
        $date = new DateTime(strftime("%x %X", $unixtime));
        $date_str = $date->format(DATE_ISO8601);
        $parts = explode('+', $date_str);
        $jsdatetime = $parts[0];
        
        date_default_timezone_set($orig_tz);
        
        return $jsdatetime;
    }
    
    static function jsdatetime_to_unixtime($jsdatetime)
    {
        $orig_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        
        $date = new DateTime($jsdatetime);
        $unixtime = $date->format("U");
        
        date_default_timezone_set($orig_tz);
        
        return $unixtime;
    }
    
    static function unixtime_to_jsdate($unixtime=false)
    {
        if (   $unixtime === false
            || !is_numeric($unixtime))
        {
            $unixtime = time();
        }        
        
        $orig_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        
        $date = new DateTime(strftime("%x %X", $unixtime));
        $date_str = $date->format(DATE_ISO8601);
        $parts = explode('T', $date_str);
        $jsdate = $parts[0];
        
        date_default_timezone_set($orig_tz);
        
        return $jsdate;
    }
    
    static function unixtime_to_jstime($unixtime=false)
    {        
        if ($unixtime === false)
        {
            $unixtime = time();
        }
        
        $orig_tz = date_default_timezone_get();
        date_default_timezone_set('UTC');
        
        $date = new DateTime(strftime("%x %X", $unixtime));
        $date_str = $date->format(DATE_ISO8601);
        $parts = explode('T', $date_str);
        $parts = explode('+', $parts[1]);
        $jsdate = $parts[0];
        
        date_default_timezone_set($orig_tz);
        
        return $jsdate;
    }
}

?>