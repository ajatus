<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_helpers_metadata
{
    private $keys = array();
    private $defaults = array();
    private $metadata;
    
    public function __construct(&$metadata)
    {
        $this->keys = array
        (
            'created', 'creator', 'revised', 'revisor', 'archived', 'deleted'
        );

        $this->defaults = array
        (
            'creator' => array
            (
                'widget' => array
                (
                    'name' => 'text',
                    'config' => null
                ),
                'value' => ''
            ),
            'created' => array
            (
                'widget' => array
                (
                    'name' => 'date',
                    'config' => array( 'use_time' => true )
                ),
                'value' => ''
            ),
            'revisor' => array
            (
                'widget' => array
                (
                    'name' => 'text',
                    'config' => null
                ),
                'value' => ''
            ),
            'revised' => array
            (
                'widget' => array
                (
                    'name' => 'date',
                    'config' => array( 'use_time' => true )
                ),
                'value' => ''
            ),
            'archived' => array
            (
                'widget' => array
                (
                    'name' => 'boolean',
                    'config' => null
                ),
                'value' => false
            ),
            'deleted' => array
            (
                'widget' => array
                (
                    'name' => 'boolean',
                    'config' => null
                ),
                'value' => false
            ),
        );
        
        $this->metadata =& $metadata;
        
        $this->validate();
    }
    
    public function __get($key)
    {
        return $this->read($key);
    }
    
    public function __set($key, $value)
    {
        $this->update($key, $value);
    }
    
    public function read($key)
    {
        if (! in_array($key, $this->keys))
        {
            throw new ajatus_metadata_exception("No key '{$key}' found!");
        }
        
        return $this->metadata->$key->val;
    }
    
    public function update($key, $value)
    {
        if (! in_array($key, $this->keys))
        {
            throw new ajatus_metadata_exception("No key '{$key}' found!");
        }
        
        $this->metadata->$key->val = $value;
    }
    
    private function validate()
    {
        foreach ($this->keys as $key)
        {
            $defaults = $this->defaults[$key];
            if (! isset($this->metadata->$key))
            {
                $this->metadata->$key = new StdClass;
                $this->metadata->$key->val = $defaults->value;
                $this->metadata->$key->widget = new StdClass;
                $this->metadata->$key->widget->name = $defaults['widget']['name'];
                $this->metadata->$key->widget->config = new StdClass;
                if (is_array($defaults['widget']['config']))
                {
                    foreach ($defaults['widget']['config'] as $ck => $cv)
                    {
                        $this->metadata->$key->widget->config->$ck = $cv;
                    }
                }
            }
        }
    }
}

?>