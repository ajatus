<?php
require_once(realpath(dirname(__FILE__) . '/') . '/../ajatus.php');

$db = 'ajatus_dev_db';
$content_db = "{$db}_content";

try
{
    $ajatus = new ajatus(array(
        'host' => 'couchdb_server',
        'db' => $db
    ));
}
catch (ajatus_exception $e)
{
    die( "Error initializing Ajatus! Reason:\n{$e}\n" );
}

//$view = $ajatus->types->tag->generate_view('list_with_docs');

//echo "Generated view:\n{$view}\n";

//$results = $ajatus->connection->$content_db->view->temp($view); //, array( 'count' => 20)
// print_r($results);
//echo "Total rows: {$results->total_rows}\n";

$results = $ajatus->types->note->get_by_tags(array('client:nemein'));
$res_cnt = count($results);

echo "Founded {$res_cnt} notes with tag 'client:nemein'\n";
foreach ($results as $result)
{
    echo "  title: {$result->value->title->val} (id: {$result->id})\n";
}

echo "\n\n";

$results = $ajatus->types->hourreport->get_by_tags(array('client:nemein'));
$res_cnt = count($results);

echo "Founded {$res_cnt} hour reports with tag 'client:nemein'\n";
foreach ($results as $result)
{
    echo "  title: {$result->value->title->val}\n";
}

echo "\n\n";

$results = $ajatus->types->expense->get_by_tags(array('client:nemein'));
$res_cnt = count($results);

echo "Founded {$res_cnt} expense reports with tag 'client:nemein'\n";
foreach ($results as $result)
{
    echo "  title: {$result->value->title->val}\n";
}

echo "\n\n";

?>