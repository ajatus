<?php
require_once(realpath(dirname(__FILE__) . '/') . '/../ajatus.php');

$db = 'ajatus_dev_db';
$content_db = "{$db}_content";

try
{
    $ajatus = new ajatus(array(
        'host' => 'couchdb_server',
        'db' => $db
    ));
}
catch (ajatus_exception $e)
{
    die( "Error initializing Ajatus! Reason:\n{$e}\n" );
}

$expense_reports = $ajatus->types->expense->all(array
    (
        'additional_map_values' => array
        (
            'description' => 'doc.value.description'
        )
    )
)->rows;

$report_count = count($expense_reports);

$status_for_everyone = array();
$totals_for_users = array();

foreach ($expense_reports as $expense)
{
    $reporter = $expense->value->creator->val;
    $amount = $expense->value->amount->val;
    
    $description = $report->value->description->val;
    
    $date_ts = ajatus_helpers_date::jsdatetime_to_unixtime($expense->value->date->val);
    
    $identifier = (string) date("Y-m", $date_ts);
    
    if (! isset($status_for_everyone[$identifier]))
    {
        $status_for_everyone[$identifier] = 0;
    }
    
    if (! isset($status_for_users[$identifier]))
    {
        $status_for_users[$identifier] = array();
    }
    
    if (! isset($status_for_users[$identifier][$reporter]))
    {
        $status_for_users[$identifier][$reporter] = 0;
    }
    
    $status_for_everyone[$identifier] += $amount;
    $status_for_users[$identifier][$reporter] += $amount;
}
ksort($status_for_everyone);

foreach ($status_for_everyone as $identifier => $month_total)
{
    echo "{$identifier}\n------------\n";
    
    foreach ($status_for_users[$identifier] as $reporter => $total)
    {
        echo "  {$reporter} => {$total}\n";
    }

    echo "Total: {$month_total}\n\n";
}

?>