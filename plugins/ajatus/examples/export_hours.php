<?php
require_once(realpath(dirname(__FILE__) . '/') . '/../ajatus.php');

$db = 'ajatus_dev_db';
$content_db = "{$db}_content";

try
{
    $ajatus = new ajatus(array(
        'host' => 'couchdb_server',
        'db' => $db
    ));
}
catch (ajatus_exception $e)
{
    die( "Error initializing Ajatus! Reason:\n{$e}\n" );
}

$by_tags_search = false;
$tags_to_search = array();

if (count($argv) >= 2)
{
    $by_tags_search = true;
    for ($i=1; $i<count($argv);$i++)
    {
        $tags_to_search[] = (string) $argv[$i];
    }    
    
    echo "Using tags " . implode(', ', $tags_to_search) . " to search for reports.\n";
}

if ($by_tags_search)
{    
    $hour_reports = $ajatus->types->hourreport->get_by_tags(array('client:nemein'), array(), array
        (
            'description' => 'doc.value.description'
        )
    );
}
else
{
    $hour_reports = $ajatus->types->hourreport->all(array
        (
            'additional_map_values' => array
            (
                'description' => 'doc.value.description'
            )
        )
    )->rows;
}

$report_count = count($hour_reports);

if ($report_count == 0)
{
    die("No hour reports found!\n");
}

echo "Founded {$report_count} hour reports.\n";

$status_for_everyone = array();
$totals_for_users = array();
foreach ($hour_reports as $report)
{
    $reporter = $report->value->creator->val;
    
    if (empty($reporter))
    {
        continue;
    }
    
    $date_ts = ajatus_helpers_date::jsdatetime_to_unixtime($report->value->date->val);
    $hours = (int) $report->value->hours->val;
    
    $description = $report->value->description->val;
    
    $identifier = (string) date("Y-m", $date_ts);
    
    if (! isset($totals_for_everyone[$identifier]))
    {
        $totals_for_everyone[$identifier] = 0;
    }

    if (! isset($totals_for_users[$identifier]))
    {
        $totals_for_users[$identifier] = array();
    }    

    if (! isset($totals_for_users[$identifier][$reporter]))
    {
        $totals_for_users[$identifier][$reporter] = 0;
    }
    
    $totals_for_everyone[$identifier] += $hours;
    $totals_for_users[$identifier][$reporter] += $hours;

    // $date_str = strftime("%x", $date_ts);    
    // echo "User: {$reporter} reported {$hours} hours in {$date_str} with description: {$description}\n";
}
ksort($totals_for_everyone);

foreach ($totals_for_everyone as $identifier => $count)
{
    echo "Month: {$identifier}\n----------------------\n";
    
    foreach ($totals_for_users[$identifier] as $user => $ucount)
    {
        echo "  {$user} => {$ucount}\n";
    }
    
    echo "Total: {$count}\n\n";
}

?>