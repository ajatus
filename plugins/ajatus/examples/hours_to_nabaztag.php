<?php
require_once(realpath(dirname(__FILE__) . '/') . '/../ajatus.php');
require('Services/Nabaztag.php');

if (count($argv) != 4)
{
    die("Usage: php hours_to_nabaztag.php dbname serialno apitoken\n");
}
$nabaztag = new Services_Nabaztag($argv[2], $argv[3]);

try
{
    $ajatus = new ajatus(array(
        'db' => $argv[1]
    ));
}
catch (ajatus_exception $e)
{
    die( "Error initializing Ajatus! Reason:\n{$e}\n" );
}

$jsdate = ajatus_helpers_date::unixtime_to_jsdate(date("U"));

$hour_reports_today_count = $ajatus->types->hourreport->count(array
    (
        'filter' => array
        (
            'and' => array
            (
                array('doc.value.date.val', 'LIKE', $jsdate)
            )
        )
    )
);

$dateparts = explode('-', $jsdate);
$jsmonthdate = "{$dateparts[0]}-{$dateparts[1]}";

$hour_reports_month_count = $ajatus->types->hourreport->count(array
    (
        'filter' => array
        (
            'and' => array
            (
                array('doc.value.date.val', 'LIKE', $jsmonthdate)
            )
        )
    )
);

echo "Hour reports for today: {$hour_reports_today_count} and for this month: {$hour_reports_month_count}\n";

try
{
    $stat = $nabaztag->say("In Ajatus we have " . round($hour_reports_today_count) . " hours reported today, and " . round($hour_reports_month_count) . " reported this month.");
}
catch (Services_Nabaztag_Exception $e)
{
    die($e);
}

?>