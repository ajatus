<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_type
{
    public $available_views = array();
    
    private $connection;
    private $configuration;
    
    public $design_name = '';
    public $active_view = null;
    public $type_name = 'note';
    
    public $view_header;
    public $view_footer;
    public $map_key;
    public $map_values;
    
    public function __construct(&$connection, &$configuration)
    {
        $this->connection =& $connection;
        $this->configuration =& $configuration;
        
        $this->available_views = array
        (
            'list',
        );
        
        $this->view_header = "if (doc.value.metadata.deleted.val == __DELETED__ && doc.value.metadata.archived.val == __ARCHIVED__ && doc.value._type == '{$this->type_name}') {";
        $this->view_footer = "}";
                
        $this->map_key = 'doc.value.metadata.created.val';
        
        $this->map_values = array
        (
            'title' => 'doc.value.title',
            'tags' => 'doc.value.tags',
            'creator' => 'doc.value.metadata.creator',
            'created' => 'doc.value.metadata.created'
        );
    }

    public function __get($id)
    {
        return $this->get_object($id);
    }
    
    public function get_object($id)
    {
        $content_db = $this->configuration['content_db'];        
        return $this->connection->$content_db->document->$id;
    }
    
    public function generate_view($name, $filter=array())
    {
        if (! in_array($name, $this->available_views))
        {
            return false;
        }
        
        $this->active_view = $name;
        return $this->build_view($filter);
    }
    
    public function count($filter=null)
    {        
        $this->active_view = 'list';
        $content_db = $this->configuration['content_db'];
        
        if (! is_null($filter))
        {
            return $this->connection->$content_db->view->temp( $this->build_view($filter), array( 'count' => 0 ))->total_rows;
        }
        
        return $this->connection->$content_db->view->execute("{$this->design_name}/{$this->active_view}", array( 'count' => 0 ))->total_rows;
    }
    
    public function all($filter=null, $args=null)
    {
        $this->active_view = 'list';
        $content_db = $this->configuration['content_db'];
        
        if (! is_null($filter))
        {
            return $this->connection->$content_db->view->temp( $this->build_view($filter), $args );
        }
        
        return $this->connection->$content_db->view->execute("{$this->design_name}/{$this->active_view}", $args);
    }
    
    public function get_by_tags(array $tags, array $skip_docs=array(), array $additional_map_values=array(), $include_archived=false)
    {
        $results = array();

        $tags = $GLOBALS['ajatus']->helpers->tag->tags_to_id($tags);
        
        $map_values = $this->map_values;
        if (! empty($additional_map_values))
        {
            foreach ($additional_map_values as $key => $value)
            {
                $map_values[$key] = $value;
            }
        }

        $all_results = $GLOBALS['ajatus']->helpers->tag->related($tags, $skip_docs, $map_values, $include_archived);
        if (   is_object($all_results)
            && isset($all_results->total_rows)
            && $all_results->total_rows > 0)
        {
            foreach ($all_results->rows as $row)
            {
                if ($row->value->_type == $this->type_name)
                {
                    $results[] = $row;
                }
            }
        }
        
        return $results;
    }
    
    public function build_view($data=array())
    {
        if (! isset($data['map_key']))
        {
            $data['map_key'] = $this->get_active_map_key();
        }
        
        if (! isset($data['map_values']))
        {
            $data['map_values'] = $this->get_active_map_values();
        }
        
        return ajatus_types::build_view($data, $this->view_header, $this->view_footer);
    }
    
    public function get_active_view()
    {
        if (is_null($this->active_view))
        {
            $this->active_view = $this->available_views[0];
        }
        
        return $this->active_view;
    }

    public function get_active_map_key()
    {
        if (is_null($this->active_view))
        {
            $this->get_active_view();
        }
        
        if (count($this->available_views) == 1)
        {
           return $this->map_key;
        }
        
        return $this->map_key[$this->active_view];
    }
    
    public function get_active_map_values()
    {
        if (is_null($this->active_view))
        {
            $this->get_active_view();
        }
        
        if (count($this->available_views) == 1)
        {
           return $this->map_values;
        }
        
        if (is_string($this->map_values[$this->active_view]))
        {
            $key = $this->map_values[$this->active_view];
            return $this->map_values[$key];
        }
        
        $values = $this->map_values[$this->active_view];
        
        if (is_array($values[0]))
        {
            foreach ($values[0] as $i => $value)
            {
                if (is_string($value))
                {
                    $values[0][$i] = $this->map_values[$value];                    
                }
            }
            return $values[0];
        }
        
        return $values;
    }
}

?>