<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_tests_helpers
{
    public static function get_tests($root_file, $root_class, $add_skip = null)
    {
        $tests = array();
        
        if (   !isset($root_file)
            || !isset($root_class))
        {
            return $tests;
        }
        
        $skip = array( '.', '..', 'all.php' );
        if (is_array($add_skip))
        {
            $skip = array_merge( $skip, $add_skip );
        }
        $skip = array_flip($skip);
        
        $path_parts = pathinfo($root_file);
        $tests_dir = dir($path_parts['dirname']);
        $prefix = str_replace('_all', '', $root_class);

        while(($testfile = $tests_dir->read()) !== false)
        {
            if (array_key_exists($testfile, $skip)) 
            {
                continue;
            }
            
            $path_parts = pathinfo($testfile);
            $test_name = str_replace('.php', '', $path_parts['filename']);

            if ($test_name != '')
            {
                require_once(realpath(dirname($root_file)) . "/{$testfile}");
                $tests[] = "{$prefix}_{$test_name}";
            }
        }
        
        return $tests;
    }
}

?>