<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

require_once('testcase.php');

/**
 * Test to see if ajatus helpers work
 */
class ajatus_tests_helpersTest extends ajatus_tests_testcase
{
    public function setUp()
    {        
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __CLASS__ . "\n";
        }
        parent::setUp();
    }
    
    public function testHelpersExists()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __FUNCTION__ . "\n";
        }
        
        try
        {
            $this->assertTrue(isset($this->ajatus->helpers));
        }
        catch (ajatus_exception $e)
        {
            $this->fail('An unexpected exception has been raised. ' . $e);
        }
    }
}
?>