<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

if (! defined('AJATUS_TEST_RUN')) {
    define('AJATUS_TEST_RUN', true);
}

if (! defined('AJATUS_HOST')) {
    define('AJATUS_HOST', 'couchdb_server');
}
if (! defined('AJATUS_PORT')) {
    define('AJATUS_PORT', 5984);
}
if (! defined('AJATUS_DB')) {
    define('AJATUS_DB', 'ajatus_dev_db');
}

if (! defined('AJATUS_TESTS_ENABLE_OUTPUT')) {
    define('AJATUS_TESTS_ENABLE_OUTPUT', true);
}

if (! defined('TESTS_DIR')) {
    define('TESTS_DIR', realpath(dirname(__FILE__) . '/'));
}

require_once('PHPUnit/Framework.php');

require_once(TESTS_DIR . '/../ajatus.php');

class ajatus_tests_testcase extends PHPUnit_Framework_TestCase
{
    protected $ajatus = null;
    
    protected function setUp()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo "\nsetUp\n\n";
        }
        
        if (! class_exists('ajatus'))
        {
            if (AJATUS_TESTS_ENABLE_OUTPUT)
            {
                echo "SKIPPED: Ajatus library is not available!\n";
            }
            
            $this->markTestSkipped('Ajatus library is not available!');
        }
        
        if (is_null($this->ajatus))
        {
            try
            {
                $this->ajatus = new ajatus(array(
                    'host' => AJATUS_HOST,
                    'port' => AJATUS_PORT,
                    'db' => AJATUS_DB
                ));
            }
            catch (ajatus_exception $e)
            {
                if (AJATUS_TESTS_ENABLE_OUTPUT)
                {
                    echo "SKIPPED: Error initializing Ajatus! Reason:\n{$e}\n";
                }
                
                $this->markTestSkipped("Error initializing Ajatus! Reason:\n{$e}");
            }
        }
    }
    
    protected function tearDown()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo "\n\ntearDown\n\n";
        }
    }
}
?>