<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

require_once('testcase.php');

/**
 * Test to see if ajatus connections work
 */
class ajatus_tests_connectionTest extends ajatus_tests_testcase
{
    public function setUp()
    {        
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __CLASS__ . "\n";
        }
        parent::setUp();
    }
    
    public function testAjatusExists()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __FUNCTION__ . "\n";
        }
        
        try
        {
            $version = $GLOBALS['ajatus']->version;

            if (AJATUS_TESTS_ENABLE_OUTPUT)
            {
                echo "Ajatus library version {$version} found.\n";
            }
        }
        catch (ajatus_exception $e)
        {
            $this->fail('An unexpected exception has been raised. ' . $e);
        }
        
        $this->assertTrue(true);
    }
    
    public function testCanConnect()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __FUNCTION__ . "\n";
        }
        
        try
        {
            $this->assertTrue($this->ajatus->connection->can_connect);
        }
        catch (ajatus_exception $e)
        {
            $this->fail('An unexpected exception has been raised. ' . $e);
        }
    }
    
    public function testDbExists()
    {
        if (AJATUS_TESTS_ENABLE_OUTPUT)
        {
            echo __FUNCTION__ . "\n";
        }
        
        try
        {
            $db = AJATUS_DB;
            $this->assertTrue(isset($this->ajatus->connection->$db));
        }
        catch (ajatus_exception $e)
        {
            $this->fail('An unexpected exception has been raised. ' . $e);
        }
    }

}
?>