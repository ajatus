<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

// interface ajatus_types_interface
// {
//     public function view($name='all');
// }

class ajatus_types
{
    private $connection;
    private $configuration;
    private $cache;
    
    public function __construct(&$connection, $configuration)
    {
        $this->connection =& $connection;
        $this->configuration =& $configuration;
    }
    
    public function __get($type)
    {
        return $this->load_type($type);
    }
    
    private function &load_type($name)
    {
        if (isset($this->cache[$name]))
        {
            return $this->cache[$name];
        }
        
        $classname = "ajatus_types_{$name}";
        
        if (! class_exists($classname))
        {
            throw new ajatus_type_exception("No type {$name} found!");
        }
        
        $this->cache[$name] = new $classname(&$this->connection, &$this->configuration);
        
        return $this->cache[$name];
    }
    
    static function prepare_map_values(array $values)
    {        
        if (! isset($values['_type']))
        {
            $values['_type'] = 'doc.value._type';
        }
        
        $map_values = '';
        $value_count = count($values);
        
        $i = 1;
        foreach ($values as $vk => $vv)
        {
            $map_values .= json_encode($vk) . ": {$vv}";
            if ($i < $value_count)
            {
                $map_values .= ', ';
            }
            
            $i++;
        }
        
        return $map_values;
    }
    
    static function prepare_view_data($data=array())
    {
        if (! isset($data['deleted']))
        {
            $data['deleted'] = false;
        }

        if (! isset($data['archived']))
        {
            $data['archived'] = false;
        }
        
        if (! isset($data['map_key']))
        {
            $data['map_key'] = 'null';
        }
        
        if (! isset($data['map_values']))
        {
            $data['map_values'] = 'doc';
        }
        
        if (   isset($data['additional_map_values'])
            && is_array($data['additional_map_values'])
            && is_array($data['map_values']))
        {
            foreach ($data['additional_map_values'] as $key => $value)
            {
                $data['map_values'][$key] = $value;
            }
        }
        
        if (   isset($data['filter'])
            && is_array($data['filter']))
        {
            list($data['sub_header'], $data['sub_footer']) = ajatus_types::generate_sub_header_and_footer($data['filter']);
            unset($data['filter']);
        }
        
        if (! isset($data['sub_header']))
        {
            $data['sub_header'] = '';
        }
        
        if (! isset($data['sub_footer']))
        {
            $data['sub_footer'] = '';            
        }
        
        foreach ($data as $key => $value)
        {
            if (   preg_match('/^map_values/', $key)
                && is_array($value))
            {
                $data[$key] = ajatus_types::prepare_map_values($value);
            }
            else if(   preg_match('/^map_key/', $key)
                    || $key == 'sub_header'
                    || $key == 'sub_footer')
            {
                $data[$key] = $value;
            }
            else
            {
                $data[$key] = json_encode($value);
            }
        }
        
        return $data;
    }
    
    static function generate_sub_header_and_footer($data)
    {
        $header = 'if (';
        
        $data_count = count($data);
        foreach ($data as $key => $keydata)
        {
            if (   $key != 'and'
                || $key != 'or')
            {
                $separator = $key == 'and' ? ' && ' : ' || ';
                
                if (   is_array($keydata)
                    && !empty($keydata))
                {
                    $block = '( ';
                    $cnt = count($keydata);
                    $i = 1;
                    foreach ($keydata as $kd_arr)
                    {
                        if (strtoupper($kd_arr[1]) == 'LIKE')
                        {
                            $block .= "{$kd_arr[0]}.match(/{$kd_arr[2]}(.*?)/i)";
                        }
                        else
                        {
                            $kd_arr[1] = $kd_arr[1] == '=' ? '==' : $kd_arr[1];
                            $kd_arr[2] = json_encode($kd_arr[2]);

                            $block .= "{$kd_arr[0]} {$kd_arr[1]} {$kd_arr[2]}";                            
                        }

                        if ($i < $cnt)
                        {
                            $block .= $separator;
                        }
                        
                        $i++;
                    }

                    $block .= ' )';
                    $header .= $block;
                    
                    if ($data_count > 1)
                    {
                        $header .= ' && ';
                    }
                }                
            }
        }
        
        $header .= ') {';
        $footer = '}';
        
        return array
        (
            $header,
            $footer
        );
    }
    
    static function build_view($data=array(), $header='', $footer='')
    {
        $data = ajatus_types::prepare_view_data($data);
        
        $view = 'function(doc){';
        $view .= $header;
        $view .= '__SUB_HEADER__';
        $view .= 'map( __MAP_KEY__, {';
        $view .= '__MAP_VALUES__';
        $view .= '});';
        $view .= '__SUB_FOOTER__';
        $view .= $footer;
        $view .= '}';
        
        foreach ($data as $key => $value)
        {
            $replace_key = '__' . strtoupper($key) . '__';
            $view = str_replace($replace_key, $value, $view);
        }
        
        // echo "Builded view: \n{$view}\n";
        
        return $view;
    }
    
    static function build_double_view($data=array(), $header='', $footer='', $loop=array())
    {
        $data = ajatus_types::prepare_view_data($data);

        $loop_start = '';
        $loop_end = '';
        $loop_key = 'i';
        
        if (isset($loop['key']))
        {
            $loop_key = $loop['key'];
        }
        if (isset($loop['start']))
        {
            $loop_start = $loop['start'];
            $loop_end = '}';
        }
        if (isset($loop['end']))
        {
            $loop_end = $loop['end'];
        }

        $view = 'function(doc){';
        $view .= $header;
        $view .= 'map( [__MAP_KEY__, 0], {';
        $view .= '__MAP_VALUES__';
        $view .= '});';
        $view .= $footer;
        $view .= '__SUB_HEADER__';
        $view .= $loop_start;
        $view .= 'map( [__MAP_KEY1__, 1], {';
        $view .= '__MAP_VALUES1__';
        $view .= '});';
        $view .= $loop_end;
        $view .= '__SUB_FOOTER__';
        $view .= '}';
        
        foreach ($data as $key => $value)
        {
            $replace_key = '__' . strtoupper($key) . '__';
            $value = sprintf($value, $loop_key);
            $view = str_replace($replace_key, $value, $view);
        }

        // echo "Builded view: \n{$view}\n";
        
        return $view;
    }
}

?>