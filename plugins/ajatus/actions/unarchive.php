<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_actions_unarchive
{   
    private $connection;
    private $configuration;
    
    public function __construct(&$connection, &$configuration)
    {
        $this->connection =& $connection;
        $this->configuration =& $configuration;
    }
    
    public function __get($id)
    {
        return $this->by_id($id);
    }
    
    public function __set($id, $value)
    {
        if ($value === true)
        {
            $this->by_id($id);
        }
    }
    
    public function by_id($id)
    {
        $db_name = $this->configuration['content_db'];
        if (! isset($this->connection->$db_name->document->$id))
        {
            return false;
        }
        
        return $this->one(&$this->connection->$db_name->document->$id);
    }
    
    public function one(&$object)
    {
        if (! is_a($object, 'couchdb_documents_document'))
        {
            //PONDER: should we throw exception ajatus_action_exception('Given object is not of type "couchdb_documents_document"!') here?
            return false;
        }

        $metadata = new ajatus_helpers_metadata(&$object->value->metadata);
        $metadata->update('archived', false);
        $metadata->update('revised', ajatus_helpers_date::unixtime_to_jsdatetime());
        $metadata->update('revisor', $GLOBALS['ajatus']->user['email']);
        
        return $object->save();
    }

    public function many(array $objects)
    {
        foreach ($objects as $object)
        {
            $this->one(&$object);
        }
        
        return true;
    }
}

?>