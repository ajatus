<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_helpers
{
    private $connection;
    private $configuration;
    private $cache;
    
    public function __construct(&$connection, $configuration)
    {
        $this->connection =& $connection;
        $this->configuration =& $configuration;
    }
    
    public function __get($helper)
    {
        return $this->load_helper($helper);
    }
    
    private function &load_helper($name)
    {
        if (isset($this->cache[$name]))
        {
            return $this->cache[$name];
        }
        
        $classname = "ajatus_helpers_{$name}";
        
        if (! class_exists($classname))
        {
            throw new ajatus_helper_exception("No helper {$name} found!");
        }
        
        $this->cache[$name] = new $classname(&$this->connection, &$this->configuration);
        
        return $this->cache[$name];
    }
    
}

?>