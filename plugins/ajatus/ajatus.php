<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

//error_reporting(E_ALL);

//namespace Ajatus;

if (!defined('AJATUS_ROOT')) {
    define('AJATUS_ROOT', realpath(dirname(__FILE__)) . '/');
}

// Add root to include path since some places still use requires instead of the autoloaded
ini_set('include_path', ini_get('include_path') . ':' . AJATUS_ROOT);

if (! defined('AJATUS_TEST_RUN')) {
    define('AJATUS_TEST_RUN', false);
}

require_once("exceptions.php");

class ajatus
{
    public $version = '0.0.3';
    public $types;
    public $connection;

    public $user;
    private $configuration;
    
    public function __construct(array $config=array())
    {
        // Register autoloader so we get all classes loaded automatically
        spl_autoload_register(array($this, 'autoload'));
        
        if (empty($config))
        {
            $config = array(
                'host' => 'localhost',
                'port' => 5984,
                'db' => 'ajatus_db'
            );
        }
        
        $this->configuration = $config;
        if (!isset($this->configuration['content_db']))
        {
            $this->configuration['content_db'] = "{$this->configuration['db']}_content";
        }
        
        try
        {
            $this->connection = new couchdb_connection(
                array
                (
                    'host' => $this->configuration['host'],
                    'port' => $this->configuration['port']
                )
            );            
        }
        catch(couchdb_exception $e)
        {
            throw new ajatus_exception('Unexpected exception throw! ' . $e);
        }
        
        $db_name = $this->configuration['db'];
        
        $this->types = new ajatus_types(&$this->connection, &$this->configuration);
        $this->helpers = new ajatus_helpers(&$this->connection, &$this->configuration);
        $this->actions = new ajatus_actions(&$this->connection, &$this->configuration);

        $name = '';
        $email = '';
        
        if (isset($this->connection->$db_name->document->preferences))
        {
            $name = $this->connection->$db_name->document->preferences->value->user->name;
            $email = $this->connection->$db_name->document->preferences->value->user->email;
        }
        
        $this->user = array
        (
            'name' => $name,
            'email' => $email
        );
        
        // $count_all_notes_from_two_users = $this->types->note->count(array
        //     (
        //         'filter' => array
        //         (
        //             'or' => array
        //             (
        //                 array('doc.value.metadata.creator.val', '=', 'jerry.jalava@gmail.com'),
        //                 array('doc.value.metadata.creator.val', '=', 'netblade@nemein.com')
        //             ),
        //         )
        //     )
        // );
        // 
        // echo "count_all_notes_from_two_users: {$count_all_notes_from_two_users}\n";
        
        //Creates new or updates existing design doc
        // $view_data = array
        // (
        //     'views' => array
        //     (
        //         'all' => 'function(doc){map(null,doc);}',
        //         'second' => 'function(doc){map(null,doc);}'
        //     )
        // );
        // $this->connection->database->$db_name->view->testview = $view_data;
        
        //Updates only part of the design doc
        // $view_name = "testview/all";
        // $this->connection->database->$db_name->view->$view_name = 'function(doc){map(doc._id,doc);}';
        
        // echo 'Notes count: ' . $this->types->note->count() . "\n";
        // echo 'Notes count (with filter): ' . $this->types->note->count_with_filter() . "\n";
        
        // print_r($this->connection->database->$db_name->view->_all_docs);
        
        //print_r($this->connection->database->$db_name->document->notes_gen_doc_0);
        
        // echo "\nTitle: ' {$this->connection->$db_name->document->notes_gen_doc_0->value->title->val} '\n";
        // echo "Creator: ' {$this->connection->$db_name->document->notes_gen_doc_0->value->metadata->creator->val} '\n";
        
        //print_r($this->connection->$db_name->document->notes_gen_doc_1);
        
        // $this->connection->$db_name->document->notes_gen_doc_0->value->title->val = 'hello world!';
        // 
        // if ($this->connection->$db_name->document->notes_gen_doc_0->save()->ok)
        // {
        //     echo "\nSaved successfully!\n";
        // }
        
        // $this->connection->clear_caches();
        // 
        // echo "\nTitle: ' {$this->connection->$db_name->document->notes_gen_doc_0->value->title->val} '\n";
        // echo "Creator: ' {$this->connection->$db_name->document->notes_gen_doc_0->value->metadata->creator->val} '\n";

        // print_r($this->connection->all_databases());
        // 
        // $r = $this->connection->database->new_db_name = true;
        // print_r($this->connection->all_databases());
        // $r = $this->connection->database->new_db_name = false;
        // 
        // print_r($this->connection->all_databases());
        
        // $app_db =& $this->connection->database->$db_name;
        // 
        // $all = $app_db->view->_all_docs;
        // print_r($all);
        
        // print_r($this->connection->database->$db_name->view->_all_docs->rows[0]->get()->value->title->val);

        // $id = '1FEFDEF70DE71F06E598A8E98959621E';
        // if ($ajatus->actions->archive->$id->ok)
        // {
        //     echo "Object with id {$id} archived.\n";
        // }
        // 
        // if ($ajatus->actions->unarchive->$id->ok)
        // {
        //     echo "Object with id {$id} unarchived.\n";
        // }
        
        // $replicator = $this->connection->new_replicator(array
        //     (
        //         'source' => array( 'db' => 'temp_db' ),
        //         'target' => array( 'db' => 'rep_test' ),
        //         'setup' => array( 'one_way' => true )
        //     )
        // );
        // // $replicator->set_view("function(doc){if (doc.value.type == 'hour_report'){ map(null, doc); }}");
        // print_r( $replicator->replicate() );
        
        $GLOBALS['ajatus'] =& $this; 
    }
    
    public function set_user($name, $email)
    {
        $this->user['name'] = $name;
        $this->user['email'] = $email;
    }
    
    /**
     * Automatically load missing class files
     *
     * @param string $class_name Name of a missing PHP class
     */
    public function autoload($class_name)
    {        
        if (class_exists($class_name))
        {
            return;
        }
        
        $path = str_replace('_', '/', $class_name) . '.php';
        $path = AJATUS_ROOT . str_replace('ajatus/', '', $path);

        if (file_exists($path))
        {
            require_once($path);
        }
    }
}
?>