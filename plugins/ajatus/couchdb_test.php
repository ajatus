<?php

require_once("couchdb/connection.php");

try
{
    $conn = new couchdb_connection(
        array
        (
            'host' => 'couchdb_server'
        )
    );        
}
catch(couchdb_exception $e)
{
    die("Unexpected exception throw! {$e}\n");
}

$test_db_name = 'temp_db';

if (isset($conn->$test_db_name))
{
    echo "Database {$test_db_name} exists.\n";
}

echo "Other databases found:\n";
foreach ($conn->all_databases() as $database)
{
    if ($database->name != $test_db_name)
    {
        echo "    {$database->name}\n";        
    }
}
echo "\n";

?>