<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

/**
 * Ajatus base exception
 */
class ajatus_exception extends Exception
{
    public function __construct($message, $code=0) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * Ajatus connection exception
 */
class ajatus_connection_exception extends ajatus_exception
{
}

/**
 * Ajatus database exception
 */
class ajatus_database_exception extends ajatus_exception
{
    /**
     * 404 = not found
     */
    public function __construct($message, $code=404) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * Ajatus view exception
 */
class ajatus_view_exception extends ajatus_exception
{
    /**
     * 404 = not found
     */
    public function __construct($message, $code=404) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * Ajatus type exception
 */
class ajatus_type_exception extends ajatus_exception
{
}

/**
 * Ajatus helper exception
 */
class ajatus_helper_exception extends ajatus_exception
{
}

/**
 * Ajatus metadata exception
 */
class ajatus_metadata_exception extends ajatus_exception
{
}

/**
 * Ajatus action exception
 */
class ajatus_action_exception extends ajatus_exception
{
}

?>