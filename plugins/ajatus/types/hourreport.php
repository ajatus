<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_types_hourreport extends ajatus_type
{   
    public function __construct(&$connection, &$configuration)
    {
        $this->type_name = 'hour_report';
        parent::__construct(&$connection, &$configuration);
        
        $this->design_name = 'hour_report';

        $this->map_values = array
        (
            'title' => 'doc.value.title',
            'hours' => 'doc.value.hours',
            'date' => 'doc.value.date',
            'tags' => 'doc.value.tags',
            'creator' => 'doc.value.metadata.creator',
            'created' => 'doc.value.metadata.created'
        );
    }    
}

?>