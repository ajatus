<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class ajatus_types_note extends ajatus_type
{   
    public function __construct(&$connection, &$configuration)
    {
        $this->type_name = 'note';
        parent::__construct(&$connection, &$configuration);
        
        $this->design_name = 'note';
        
        $this->map_values = array
        (
            'title' => 'doc.value.title',
            'tags' => 'doc.value.tags',
            'creator' => 'doc.value.metadata.creator',
            'created' => 'doc.value.metadata.created'
        );
    }
    
}

?>