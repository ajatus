<?php
/**
 * This file is part of
 * Ajatus - Distributed CRM
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Copyright (c) 2008 Nemein Oy <http://nemein.com>
 * Website: http://ajatus.info
 * Licensed under the GPL license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */
//implements ajatus_types_interface
class ajatus_types_tag extends ajatus_type
{   
    public function __construct(&$connection, &$configuration)
    {
        $this->type_name = 'tag';
        parent::__construct(&$connection, &$configuration);
        
        $this->design_name = 'tag';
        
        $this->available_views = array
        (
            'list',
            'list_by_title',
            'list_with_docs',
        );

        $this->map_key = array
        (
            'list' => 'doc._id',
            'list_by_title' => 'doc.value.title.val',
            'list_with_docs' => array
            (
                0 => 'doc._id',
                1 => 'doc.value.tags.val[%s]',
            )
        );
        
        $this->map_values = array
        (
            'list' => array
            (
                'title' => 'doc.value.title',
                'color' => '{ val: doc.value.title.widget.config.color }',
                'context' => '{ val: doc.value.title.widget.config.context }',
                'value' => '{ val: doc.value.title.widget.config.value }',
                'creator' => 'doc.value.metadata.creator',
                'created' => 'doc.value.metadata.created'
            ),
            'list_by_title' => 'list',
            'list_with_docs' => array
            (
                array
                (
                    'list',
                    array
                    (
                        'id' => 'doc._id',
                    ),
                )
            ),
        );
    }
    
    public function get_by_tags(array $tags, array $skip_docs=array())
    {
        throw new ajatus_type_exception('Method not available on this type');
    }
    
    public function build_view($data=array())
    {
        if ($this->get_active_view() != 'list_with_docs')
        {
            return ajatus_types::build_view($data, $this->view_header, $this->view_footer);
        }
        
        $loop = array
        (
            'start' => 'for (var i=0;i<doc.value.tags.val.length;i++) {'
        );

        if (! isset($data['filter']))
        {
            $data['filter'] = array
            (
                'and' => array
                (
                    array('doc.value.metadata.deleted.val', '=', false),
                    array('doc.value.metadata.archived.val', '=', false),
                    array('doc.value._type', '!=', 'tag'),
                ),
            );
        }
        
        list($map_key, $map_key1) = $this->get_active_map_key();
        
        if (! isset($data['map_key']))
        {
            $data['map_key'] = $map_key;
        }
        
        if (! isset($data['map_key1']))
        {
            $data['map_key1'] = $map_key1;
        }
        
        list($map_values, $map_values1) = $this->get_active_map_values();
        
        if (! isset($data['map_values']))
        {
            $data['map_values'] = $map_values;
        }
        
        if (! isset($data['map_values1']))
        {
            $data['map_values1'] = $map_values1;
        }
        
        return ajatus_types::build_double_view($data, $this->view_header, $this->view_footer, $loop);
    }
}

?>