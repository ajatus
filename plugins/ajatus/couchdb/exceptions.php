<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

/**
 * CouchDB base exception
 */
class couchdb_exception extends Exception
{
    public function __construct($message, $code=0) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * CouchDB connection exception
 */
class couchdb_connection_exception extends couchdb_exception
{
}

/**
 * CouchDB database exception
 */
class couchdb_database_exception extends couchdb_exception
{
    /**
     * 404 = not found
     */
    public function __construct($message, $code=404) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * CouchDB view exception
 */
class couchdb_view_exception extends couchdb_exception
{
    /**
     * 404 = not found
     */
    public function __construct($message, $code=404) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * CouchDB document exception
 */
class couchdb_document_exception extends couchdb_exception
{
    /**
     * 404 = not found
     */
    public function __construct($message, $code=404) 
    {
        parent::__construct($message, $code);
    }
}

/**
 * CouchDB replicator exception
 */
class couchdb_replicator_exception extends couchdb_exception
{
}

/**
 * CouchDB transport exception
 */
class couchdb_transport_exception extends couchdb_exception
{
    var $request = null;
    
    public function __construct($message, $code=0, &$request) 
    {
        $this->request =& $request;
        parent::__construct($message, $code);
    }
}

?>