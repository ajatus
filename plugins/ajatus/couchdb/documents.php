<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class couchdb_documents
{
    private $configuration;
    private $cache = array();
    
    public function __construct(&$configuration)
    {
        $this->configuration =& $configuration;
    }
    
    public function __get($id)
    {
        return $this->get($id);
    }
    
    public function __set($id, $value)
    {
        if ($value === false)
        {
            return $this->remove($id);
        }
        
        return $this->update($id, $value);
    }
    
    public function __isset($id)
    {
        return $this->exists($id);
    }
    
    public function create($id, $value)
    {
        return $this->update($id, $value);
    }

    public function delete($id)
    {
        return false;
    }
    
    public function get($id)
    {        
        if (! isset($this->cache[$id]))
        {
            $this->cache[$id] =& $this->exists($id);
        }
        
        return $this->cache[$id];
    }
    
    public function convert($data)
    {
        $converted = null;

        if (is_array($data))
        {
            $converted = array();
            foreach ($data as $key => $object)
            {
                $id = null;
                if (isset($object->_id))
                {
                    $id = $object->_id;
                }
                else if (isset($object->id))
                {
                    $id = $object->id;
                }
                
                $converted[$key] = new couchdb_documents_document($id, $this->configuration, $object);
            }
        }
        else
        {
            $id = null;
            if (isset($data->_id))
            {
                $id = $data->_id;
            }
            else if (isset($data->id))
            {
                $id = $data->id;
            }
            
            $converted = new couchdb_documents_document($id, $this->configuration, $data);
        }
        
        return $converted;
    }
    
    private function update($id, $value)
    {
        $existing_doc = $this->exists($id);
        
        $doc = new couchdb_documents_document($id, $this->configuration);
        
        foreach ($value as $key => $data)
        {
            $doc->$key = $data;
        }        
        
        if ($existing_doc)
        {
            $doc->rev = $existing_doc->rev;
        }
        
        return $doc->save();
    }
    
    private function exists($id, $graceful=false)
    {
        if (isset($this->cache[$id]))
        {
            return $this->cache[$id];
        }
        
        try
        {
            $doc = new couchdb_documents_document($id, $this->configuration);
            return $doc->exists();
        }
        catch(couchdb_document_exception $e)
        {
            if ($graceful)
            {
                return false;
            }
            
            if ($e->getCode() == 404)
            {
                throw new couchdb_document_exception("Document '{$id}' doesn't exist!");
            }

            throw $e;
        }
        
        return false;
    }
    
    public function clear_caches()
    {        
        $this->cache = array();
    }
}

?>