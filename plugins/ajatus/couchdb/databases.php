<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class couchdb_databases
{
    private $configuration;
    private $cache = array();
    
    public function __construct(&$configuration)
    {
        $this->configuration =& $configuration;
    }
    
    public function __get($name)
    {
        return $this->get($name);
    }
    
    public function __set($name, $value)
    {
        if ($value === false)
        {            
            return $this->delete($name);
        }
        
        //TODO: Add rename option here also.
        /**
         * If $value is string -> create $value -> copy contents of $name to $value -> remove $name 
        **/
        
        return $this->create($name);
    }
    
    public function __isset($name)
    {
        return $this->exists($name);
    }
    
    public function get_all($names_only=false)
    {
        $transport = new couchdb_transport($this->configuration);
        
        try
        {
            $names = $transport->free_execute('_all_dbs');
            
            if ($names_only)
            {
                return $names;
            }
            
            $dbs = array();
            foreach ($names as $dbname)
            {
                $dbs[$dbname] = new couchdb_databases_database($dbname, $this->configuration);
            }
            
            return $dbs;
        }
        catch (couchdb_transport_exception $e)
        {
            throw new couchdb_database_exception("Unknown error occured while listing databases. {$e}");
        }
    }

    public function create($name)
    {
        $db = new couchdb_databases_database($name, $this->configuration);
        return $db->create();
    }
    
    public function delete($name)
    {
        $this->exists($name);
        $db = new couchdb_databases_database($name, $this->configuration);
                
        return $db->delete();
    }
    
    private function &get($name)
    {        
        if (! isset($this->cache[$name]))
        {
            $this->cache[$name] = $this->exists($name);
        }
        
        return $this->cache[$name];
    }
    
    private function exists($name, $graceful=false)
    {        
        try
        {
            $db = new couchdb_databases_database($name, $this->configuration);
            return $db->exists();
        }
        catch(couchdb_database_exception $e)
        {
            if ($graceful)
            {
                return false;
            }
            
            if ($e->getCode() == 404)
            {
                throw new couchdb_database_exception("Couldn't find database '{$name}'!");
            }

            throw $e;
        }
        
        return false;
    }

    public function clear_caches()
    {
        foreach ($this->cache as $name => $db)
        {
            $db->clear_caches();
        }
        
        $this->cache = array();
    }
}

?>