<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

interface couchdb_replicator_interface
{    
    function replicate();
    
    function check_config();
}

class couchdb_replicator
{    
    public $local_configuration;
    public $configuration;
        
    public function __construct($local_configuration, $configuration=array())
    {
        $this->local_configuration = $local_configuration;
        
        if (empty($configuration))
        {
            throw new couchdb_replicator_exception('No configuration given!');
        }
        
        if (! isset($configuration['source']))
        {
            throw new couchdb_replicator_exception('No source found in configuration!');
        }
        
        if (! isset($configuration['target']))
        {
            throw new couchdb_replicator_exception('No target found in configuration!');
        }
        
        $this->configuration = $configuration;
        
        $this->check_config();
    }
    
    public function check_config()
    {
        return true;
    }
}

?>