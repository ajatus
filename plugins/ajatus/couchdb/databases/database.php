<?php
/**
 * 
 * Copyright (c) 2008 Jerry Jalava <jerry.jalava@gmail.com>
 * Licensed under the GPLv3 license
 * http://www.gnu.org/licenses/gpl.html
 * 
 */

class couchdb_databases_database extends couchdb_transport
{    
    public $name;
    public $view;
    public $document;
    
    public function __construct($name, $configuration)
    {        
        if (empty($name))
        {
            throw new couchdb_database_exception('No database name given!');
        }
        
        $configuration['database'] = $name;
        parent::__construct($configuration);
        
        $this->name = $name;

        $this->view = new couchdb_views($configuration);
        $this->document = new couchdb_documents($configuration);
    }
    
    public function &exists()
    {
        $this->execute(false);
        return $this;//($this->lastStatusCode == 200);
    }
    
    public function create()
    {
        return $this->execute(false, 'PUT');
    }
    
    public function delete()
    {
        return $this->execute(false, 'DELETE');
    }
    
    public function clear_caches()
    {        
        $this->view->clear_caches();
        $this->document->clear_caches();
    }
}

?>