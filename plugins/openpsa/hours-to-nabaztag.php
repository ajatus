<?php
require('Services/Nabaztag.php');
require('CouchDb/Couch.php');

// Argument checks
if (count($argv) != 4)
{
    die("Usage: php hours-to-nabaztag.php dbname serialno apitoken\n");
}
$nabaztag = new Services_Nabaztag($argv[2], $argv[3]);

$couch = new Couch(array('host' => 'localhost', 'port' => 5984));
if (!$couch->running())
{
    die("Cannot connect to CouchDB in {$couch->host()}:{$coudh->port()}\n");
}

$couchdb = $couch->database($argv[1], $couch);
if(!$couchdb->exists()) 
{
    die("CouchDB database {$couchdb->name()} does not exist\n");
}

$total_hours = array();

// Construct the view
//$view = $couchdb->newView();
//$view->function = "function(doc) { return doc; }";
//$hour_reports = $view->documents();

$everything = $couchdb->view('_all_docs')->documents();
foreach ($everything as $object)
{
    $document = $couchdb->get($object->id);
    if ($document->value->_type != 'hour_report')
    {
        continue;
    }
    
    $month = substr($document->value->date->val, 0, 7);
    $day = substr($document->value->date->val, 0, 10);
    if (!isset($total_hours[$month]))
    {
        $total_hours[$month] = array();
        $total_hours[$month]['all'] = 0;
    }
    if (!isset($total_hours[$month][$day]))
    {
        $total_hours[$month][$day] = 0;
    }
    
    $total_hours[$month]['all'] += $document->value->hours->val;
    $total_hours[$month][$day] += $document->value->hours->val;
}
ksort($total_hours);

$this_month = date('Y-m');
$this_day = date('Y-m-d');

try
{
    $stat = $nabaztag->say("In Ajatus we have " . round($total_hours[$this_month][$this_day]) . " hours reported today, and " . round($total_hours[$this_month][all]) . " reported this month.");
}
catch (Services_Nabaztag_Exception $e)
{
    //echo $e;
}
?>