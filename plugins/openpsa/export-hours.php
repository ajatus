<?php
require('CouchDb/Couch.php');
$couch = new Couch(array('host' => 'localhost', 'port' => 5984));
if (!$couch->running())
{
    die("Cannot connect to CouchDB in {$couch->host()}:{$coudh->port()}\n");
}

$couchdb = $couch->database('ajatus_db_content', $couch);
if(!$couchdb->exists()) 
{
    die("CouchDB database {$couchdb->name()} does not exist\n");
}

$hours = array();
$total_hours = array();

// Construct the view
$view = $couchdb->newView();
$view->function = "function(doc) { if (doc.value._type == 'hour_report') { map(null, doc); }}";
$results = $view->documents();

//$everything = $couchdb->view('_all_docs')->documents();
foreach ($results as $result)
{
    $document = $result->value;
    $creator = $document->value->metadata->creator->val;
    if (!isset($hours[$creator]))
    {
        $hours[$creator] = array();
    }
    
    $month = substr($document->value->date->val, 0, 7);
    if (!isset($hours[$creator][$month]))
    {
        $hours[$creator][$month] = array();
    }
    if (!isset($total_hours[$month]))
    {
        $total_hours[$month] = array();
        $total_hours[$month]['all'] = 0;
    }
    if (!isset($total_hours[$month][$creator]))
    {
        $total_hours[$month][$creator] = 0;
    }
    
    $hours[$creator][$month][] = array
    (
        'date' => $document->value->date->val,
        'hours' => $document->value->hours->val,
        'description' => $document->value->description->val,
    );
    
    $total_hours[$month]['all'] += $document->value->hours->val;
    $total_hours[$month][$creator] += $document->value->hours->val;
}
ksort($total_hours);

foreach ($total_hours as $month => $type)
{
    echo "{$month}\n-------\n";
    foreach ($type as $reporter => $hours_sum)
    {
        echo "{$reporter}: {$hours_sum}h\n";
    }
    echo "\n";
}
?>