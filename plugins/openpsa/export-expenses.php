<?php
require('CouchDb/Couch.php');
$couch = new Couch(array('host' => 'localhost', 'port' => 5984));
if (!$couch->running())
{
    die("Cannot connect to CouchDB in {$couch->host()}:{$coudh->port()}\n");
}

$couchdb = $couch->database('ajatus_db_content', $couch);
if(!$couchdb->exists()) 
{
    die("CouchDB database {$couchdb->name()} does not exist\n");
}

$expenses = array();
$total_expenses = array();

// Construct the view
$view = $couchdb->newView();
$view->function = "function(doc) { if (doc.value._type == 'expense') { map(null, doc); }}";
$results = $view->documents();

foreach ($results as $result)
{
    $document = $result->value;
    $creator = $document->value->metadata->creator->val;
    if (!isset($expenses[$creator]))
    {
        $expenses[$creator] = array();
    }
    
    /*if ($document->value->date->val < '2008-06-16T00:00:00')
    {
        continue;
    }*/
    
    $month = substr($document->value->date->val, 0, 7);
    if (!isset($expenses[$creator][$month]))
    {
        $expenses[$creator][$month] = array();
    }
    if (!isset($total_expenses[$month]))
    {
        $total_expenses[$month] = array();
        $total_expenses[$month]['all'] = 0;
    }
    if (!isset($total_expenses[$month][$creator]))
    {
        $total_expenses[$month][$creator] = 0;
    }
    
    $expenses[$creator][$month][] = array
    (
        'date' => $document->value->date->val,
        'amount' => $document->value->amount->val,
        'vat' => $document->value->vat->val,
        'description' => $document->value->description->val,
    );
    
    $total_expenses[$month]['all'] += $document->value->amount->val;
    $total_expenses[$month][$creator] += $document->value->amount->val;
}
ksort($total_expenses);

foreach ($total_expenses as $month => $type)
{
    echo "{$month}\n-------\n";
    foreach ($type as $reporter => $expenses_sum)
    {
        echo "{$reporter}: {$expenses_sum} EUR\n";
    }
    echo "\n";
}
?>