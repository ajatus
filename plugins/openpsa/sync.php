<?php
// Open CouchDb connection to Ajatus
require('CouchDb/Couch.php');
$couch = new Couch(array('host' => 'localhost', 'port' => 5984));
if (!$couch->running())
{
    die("Cannot connect to CouchDB in {$couch->host()}:{$coudh->port()}\n");
}

$couchdb = $couch->database('ajatus_db_content', $couch);
if(!$couchdb->exists()) 
{
    die("CouchDB database {$couchdb->name()} does not exist\n");
}

$url = $argv[1];
if (substr($url, 0, 4) != 'http')
{
    die("Usage:\n php sync.php http://username:password@www.example.net/openpsa\n");
}

if (substr($url, -1) != '/')
{
    $url .= '/';
}

// Get buddies
$fp = @fopen("{$url}contacts/buddylist/xml", 'r');
if (!$fp)
{
    die("Failed retrieving Buddy List\n");
}
$xml = '';
while (!feof($fp)) 
{
  $xml .= fread($fp, 8192);
}
fclose($fp);

if (empty($xml))
{
    die("Failed to read buddy data\n");
}

$simplexml = simplexml_load_string($xml);
if (isset($simplexml->buddy))
{
    foreach ($simplexml->buddy as $buddy)
    {
        $guid = '';
        $buddydata = array();
        foreach ($buddy->attributes() as $key => $value)
        {
            if ($key == 'guid')
            {
                $guid = $value;
            }
        }
        $couch_id = $guid;
                
        foreach ($buddy as $field => $value)
        {
            $buddydata[$field] = (string) $value;
        }
        
        // Get either existing object from Couch or initialize empty object
        $document = $couchdb->get($couch_id);
        $document->value = array();
        $document->value['_type'] = 'contact';
        $document->value['_source'] = 'Midgard';
        $document->value['metadata'] = array();
        $document->value['metadata']['archived'] = array('val' => '');
        $document->value['metadata']['deleted'] = array('val' => 0);
        foreach ($buddydata as $field => $value)
        {
            // Map OpenPsa field names to Ajatus field names
            switch ($field)
            {
                case 'jid':
                    $document->value['xmpp'] = array('val' => $value);
                    break;
                case 'person_homepage':
                    $document->value['homepage'] = array('val' => $value);
                    break;
                case 'notes':          
                    $document->value['description'] = array('val' => $value);
                    break;
                default:
                    $document->value[$field] = array('val' => $value);
            }
        }
        if (!$document->save())
        {
            echo "Failed saving contact {$guid}, {$document->lastStatusCode}\n";
        }
        else
        {
            echo "Contact {$guid} saved\n";
        }
    }
}
?>