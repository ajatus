module("Ajatus::i10n");

test("Requirements", function() {        
	ok( $.ajatus.i10n, "$.ajatus.i10n" );
	ok( $.ajatus.i10n.initialized, "$.ajatus.i10n.initialized");
	ok( $.ajatus.i10n.lang == 'en_GB', "Localization is en_GB");
});

test("Translations", function() {
	ok( $.ajatus.i10n.get('Test %s', ['one']) == 'Testing one', "Testing one" );
	ok( $.ajatus.i10n.get('%d comment', [2], 'comment') == '2 comments', "2 comments" );
	ok( $.ajatus.i10n.get('%d comment in %d doc', [1, 3]) == '1 comment in 3 docs', "1 comment in 3 docs" );
	ok( $.ajatus.i10n.get('%d comment in %d doc', [2, 1]) == '2 comments in 1 doc', "2 comments in 1 doc" );
});