module("jqCouch");

test("config", function() {
    expect(2);
	ok( $.jqcouch, "$.jqcouch" );
	ok( $.ajatus.preferences.client.application_database != '', "Application database defined" );
});

test("DB", function() {
    expect(7);
    var jqcouch_db = $.jqcouch.db;

    jqcouch_db.on_success = function(data, caller){
        ok( data.ok == true, "restart couchdb");
    };
    jqcouch_db.restart();
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.exists == true, "exists "+$.ajatus.preferences.client.application_database);
    };
	jqcouch_db.exists($.ajatus.preferences.client.application_database);
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.length, "get_all data.length");
    };
	jqcouch_db.get_all();
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.ok == true, "create jqcouch_test_db");
    };
	jqcouch_db.create('jqcouch_test_db');
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.db_name == 'jqcouch_test_db', "info: name = jqcouch_test_db");
        ok( data.doc_count == 0, "info: doc_count = 0");
    };
    jqcouch_db.info('jqcouch_test_db');
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.ok == true, "remove jqcouch_test_db");
    };
    jqcouch_db.remove('jqcouch_test_db');
});

test("Doc", function() {
    // expect(2);
    
    var jqcouch_db = $.jqcouch.db;
    jqcouch_db.on_success = function(data, caller){
        ok( data.ok == true, "create jqcouch_test_db");
    };
	jqcouch_db.create('jqcouch_test_db');
	
    var jqcouch_doc = $.jqcouch.doc;

    var doc = {_id:"0",a:1,b:1};
    jqcouch_doc.on_success = function(data, caller){
        ok( data.ok, "save doc 0: data ok");
        ok( data.id == "0", "save doc 0: id = 0");
        ok( typeof data.rev != 'undefined', "save doc 0: has revision");
        doc._rev = data.rev;
    };
    jqcouch_doc.save('jqcouch_test_db', doc);
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.doc_count == 1, "info: doc_count = 1");
    };
    jqcouch_db.info('jqcouch_test_db');
    
    jqcouch_doc.on_success = function(data, caller){
        ok( data.ok, "del doc 0: data ok");
    };
    jqcouch_doc.del("jqcouch_test_db/" + doc._id, doc._rev);
    
    var num_docs_to_create = 500;
    var docs = makeDocs(num_docs_to_create);
    jqcouch_doc.on_success = function(data, caller){
        ok( data.ok, "bulk save "+num_docs_to_create+" docs: data ok");
    };
    jqcouch_doc.bulk_save('jqcouch_test_db', docs);
    
    jqcouch_db.on_success = function(data, caller){
        ok( data.ok == true, "remove jqcouch_test_db");
    };
    jqcouch_db.remove('jqcouch_test_db');
});

test("View", function() {
    var jqcouch_view = $.jqcouch.view;
});

function makeDocs(n, templateDoc) {
    var templateDocSrc = templateDoc ? templateDoc.toSource() : "{}"
    var docs = []
    for (var i=0; i<n; i++) {
        var newDoc = eval("(" + templateDocSrc + ")");
        newDoc._id = (i).toString();
        newDoc.integer = i;
        newDoc.string = (i).toString();
        docs.push(newDoc);
    }
    return docs;
}