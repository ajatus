module("jqCouchv2");

test("config", function() {
    expect(2);
	ok( $.jqCouch, "$.jqCouch" );
	ok( $.ajatus.preferences.client.application_database != '', "Application database defined" );
});

test("DB Connection", function() {
    var dbc = $.jqCouch.connection('db');
    // console.log(db_connection);
    
    ok( dbc.restart(), "Restart CouchDB" );
    
    ok( dbc.exists($.ajatus.preferences.client.application_database), "App DB Exists" );

    ok( dbc.info($.ajatus.preferences.client.application_database).db_name == $.ajatus.preferences.client.application_database, "App DB Info" );

    ok( dbc.create('jqcouchv2_test_db').ok, "Create test DB" );

    ok( dbc.all().length > 0, "More than 0 Databases exists" );
    
    ok( dbc.del('jqcouchv2_test_db').ok, "Delete test DB" );
});

test("Doc Connection", function() {
    var dbc = $.jqCouch.connection('db');
    ok( dbc.create('jqcouchv2_test_db').ok, "Create test DB" );
    
    var dc = $.jqCouch.connection('doc');
    
    ok( dc.get($.ajatus.preferences.client.application_database+'/preferences')._id !== false, "Get App preferences doc" );

    ok( dc.get($.ajatus.preferences.client.application_database+'/non_existant_doc')._id === false, "Get non existant doc" );
    ok( dc.last_error.response.error == 'not_found', "Last error response.error: not_found");
    ok( dc.last_error.response.reason == 'missing', "Last error response.reason: missing");
    
    var map_fun = function(data) {
        if (typeof data.value.metadata != 'undefined') {
            return {metadata: data.value.metadata};
        }
        return {metadata: false};
    };
    ok( dc.get($.ajatus.preferences.client.application_database+'/preferences', {}, map_fun).metadata !== false, "Query with mapping" );
    
    var doc = {_id:"0",a:1,b:1};
    ok( dc.save('jqcouchv2_test_db', doc)._id !== false, "Create doc" );
    ok( typeof doc._rev != 'undefined', "New doc _rev is defined" );
    
    ok( dc.del('jqcouchv2_test_db', doc).ok, "Delete doc 0" );
    ok( doc._deleted, "doc._deleted" );
    
    var num_docs_to_create = 700;
    var docs = genDocs(num_docs_to_create);
    ok( dc.bulk_save('jqcouchv2_test_db', docs).ok, "Bulk create "+num_docs_to_create+" docs" );
    
    ok( dc.all('jqcouchv2_test_db').total_rows > 0, "Get all docs" );
    
    ok( dbc.del('jqcouchv2_test_db').ok, "Delete test DB" );
});

test("View Connection", function() {
    var dbc = $.jqCouch.connection('db');
    ok( dbc.create('jqcouchv2_test_db').ok, "Create test DB" );
    
    var vc = $.jqCouch.connection('view');
    ok( vc.exists($.ajatus.preferences.client.content_database, 'event') !== false, "View event exists" );
    ok( vc.exists($.ajatus.preferences.client.content_database, 'event/all') !== false, "View event/all exists" );
    ok( vc.exists($.ajatus.preferences.client.content_database, 'event/doesnt') === false, "View event/doesnt exists" );
    ok( vc.exists($.ajatus.preferences.client.content_database, 'doesnt/exist') === false, "View doesnt/exist exists" );
    
    var tmp_map = function(doc) {
        if (doc.value._type == 'note') {
            map(doc.value.metadata.created.val, doc);
        }
    };
    ok( vc.temp($.ajatus.preferences.client.content_database, tmp_map).total_rows, "Run simple temp view" );

    var tmp_map = function(doc) {
        if (doc.value._type == 'note') {
            map(doc.value.metadata.created.val, doc);
        }
    };
    var map_fun = function(data) {
        var tmp_data = {
            offset: 0,
            rows: [],
            total_rows: 0
        };
        var tmp_rows = [];
        $.each(data.rows, function(i,d){
            if (i%2==0) {
                tmp_rows.push(d);
            }
        });
        
        if (tmp_rows.length > 0) {
            tmp_data.rows = tmp_rows;
            tmp_data.total_rows = tmp_rows.length;
        }
        
        return tmp_data;
    };
    ok( vc.temp($.ajatus.preferences.client.content_database, tmp_map, {}, map_fun).total_rows, "Run simple temp view with mapping function" );
    
    var new_view = {
        _id: 'test',
        views: {
            id_rev: function(doc) {map(doc._id, doc._rev);},
            rev_id: function(doc) {map(doc._rev, doc._id);}            
        }
    };
    ok( vc.save('jqcouchv2_test_db', new_view).ok, "Save view");
    ok( typeof new_view._rev != 'undefined', "New views revision is defined");
    
    var num_docs_to_create = 400;
    var docs = genDocs(num_docs_to_create);
    ok( $.jqCouch.connection('doc').bulk_save('jqcouchv2_test_db', docs).ok, "Bulk create "+num_docs_to_create+" docs" );
    
    ok( vc.get('jqcouchv2_test_db', 'test/id_rev').total_rows, "Get view test/id_rev");
    
    ok( vc.update_config('cache', true), "Enable cache");
    ok( vc.get('jqcouchv2_test_db', 'test/rev_id').total_rows, "Get view test/rev_id");
    ok( vc.get('jqcouchv2_test_db', 'test/rev_id').rows, "Get view test/rev_id from cache");
    ok( vc.purge_cache(), "Purge cache");
    ok( vc.get('jqcouchv2_test_db', 'test/rev_id').rows, "Get view test/rev_id from empty cache");
            
    ok( dbc.del('jqcouchv2_test_db').ok, "Delete test DB" );
});

function genDocs(n, templateDoc) {
    var templateDocSrc = templateDoc ? templateDoc.toSource() : "{}"
    var docs = []
    for (var i=0; i<n; i++) {
        var newDoc = eval("(" + templateDocSrc + ")");
        newDoc._id = (i).toString();
        newDoc.integer = i;
        newDoc.string = (i).toString();
        docs.push(newDoc);
    }
    return docs;
}