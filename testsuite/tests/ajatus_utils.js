module("Ajatus::utils");

test("Requirements", function() {        
	ok( $.ajatus.utils, "$.ajatus.utils" );
});

test("Boolean", function() {
    ok($.ajatus.utils.to_boolean(true) == true, "true => true");
    ok($.ajatus.utils.to_boolean(false) == false, "false => false");
    ok($.ajatus.utils.to_boolean("true") == true, "'true' => true");
    ok($.ajatus.utils.to_boolean("false") == false, "'false' => false");
    ok($.ajatus.utils.to_boolean(1) == true, "1 => true");
    ok($.ajatus.utils.to_boolean(0) == false, "0 => false");
    ok($.ajatus.utils.to_boolean("x") == false, "'x' => false");
});

test("MD5", function() {
	ok( $.ajatus.utils.md5.encode("abc") == '900150983cd24fb0d6963f7d28e17f72', "abc == 900150983cd24fb0d6963f7d28e17f72" );
	ok( $.ajatus.utils.md5.encode("client:nemein=") == '016409af20109b2d38f5d868646a806d', "client:nemein= == 016409af20109b2d38f5d868646a806d" );
    ok( $.ajatus.utils.md5.encode("project:ajatus=") == 'f73de6e5d6830def0b10238edf65d37f', "project:ajatus= == f73de6e5d6830def0b10238edf65d37f" );
    ok( $.ajatus.utils.md5.encode("project:ajatus=200801") == '67353a86ad5ef28c7dfe6570e22607bd', "project:ajatus=200801 == 67353a86ad5ef28c7dfe6570e22607bd" );
});

test("Base64", function() {
    var b64_key = 'abcdefghijklmnopqrstuvwxyzåäö!';
    var eb64_str = $.ajatus.utils.base64.encode(b64_key);
    var db64_str = $.ajatus.utils.base64.decode(eb64_str);
    
	ok( eb64_str == 'YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXrl5PYh', b64_key+" == YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXrl5PYh" );
	ok( db64_str == b64_key, eb64_str+" == "+b64_key );
});

test("UTF-8", function() {
    var texts = [];
    texts[0] = "1. Ascii: hello"
    texts[1] = "2. Russian: На берегу пустынных волн"
    texts[2] = "3. Math: ∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i),"
    texts[3] = "4. Geek: STARGΛ̊TE SG-1"
    texts[4] = "5. Braille: ⡌⠁⠧⠑ ⠼⠁⠒  ⡍⠜⠇⠑⠹⠰⠎ ⡣⠕⠌"
    
    for (var i=0; i<texts.length; i++) {
        var e = $.ajatus.utils.utf8.encode(texts[i]);
        var d = $.ajatus.utils.utf8.decode(e);
        ok(texts[i] == d, "Encode/Decode: "+texts[i]);
    }
});

var ids = [];
var x = 0;
var ti = null;
function gen_ids(count) {
    for (var i=0; i<count;i++) {
        ids[x] = $.ajatus.utils.generate_id();
        x+=1;
    }
    clearTimeout(ti);
}

for (var i=0; i<100;i++) {
    ti = setTimeout('gen_ids('+150+');', 250);
}

test("Generate id", function() {
    ok(ids.length == 14850, "Generate 14 850 ids");
    
    var match_found = false;
    var match_count = 0;
    var cids = ids;
    for (var i=0; i<ids.length;i++) {
        var key = ids[i];
        cids[i] = '';
        var matched = $.inArray(key, cids);
        if (matched != -1) {
            match_found = true;
            match_count += 1;
        }
    }
    
    ok((match_found == false && match_count == 0), "All generated ids are different");
});